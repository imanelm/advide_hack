<?php

namespace App\Controller;

use App\Entity\Hacker;
use App\Entity\State;
use App\Entity\Notifications;
use App\Entity\OfferReport;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class HackerController extends AbstractController
{
    /**
     * @Route("/hackers", name="hackers")
     */
    public function index() : Response
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $sql = '
            SELECT * FROM hacker 
            ORDER BY username ASC
            
        ';
        $hackers = $conn->prepare($sql);
        $hackers->execute([]);
        $hackers = $hackers->fetchAll();

        //dd($hackers);
        return $this->render('hacker/index.html.twig', compact('hackers'));
    }

    // Route dynamique en fonction de l'id 
    /**
        * @Route("/hackers/{id<[0-9]+>}", name="hacker_show")
    */
    public function show(Hacker $hacker, Request $request, EntityManagerInterface $em) : Response
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();
        /*$sql1 = '
            SELECT * FROM info_supp_ha AS info
            WHERE info.id_hacker = :id_h
        ';
        $info1 = $conn->prepare($sql1);
        $id_h = $hacker->getIdHacker();
        $info1->execute([':id_h' => $id_h]);
        $info1 = $info1->fetchAll();
        */
        $sql2 = '
            SELECT * FROM state
        ';
        $info2 = $conn->prepare($sql2);
        $info2->execute([]);

        //dd($id_h);
        $states = $info2->fetchAll();

        $result = compact('hacker', 'states');

        /*$form = $this -> createForm(AdmissionForm::class, $hacker);
        $form -> handleRequest($request);

        //dd($result['hacker']->getState()->getIdState());
        if($form->isSubmitted() && $form -> isValid())
        {
            if ($_POST['states']=="Admis")
            {
                $newState = 1;
            }
            elseif ($_POST['states']=="En cours de traitement")
            {
                $newState = 2;
            }
            elseif ($_POST['states']=="Recalé")
            {
                $newState = 3;
            }

            $sql_update = '
                UPDATE hacker
                SET state = :newState
                WHERE id_hacker = :id_h
            ';
            $update = $conn->prepare($sql_update);
            $id_h = $hacker->getIdHacker();
            $update->execute([':id_h' => $id_h, ':newState' => $newState]);

            $em = $this -> getDoctrine() -> getManager();
            $em -> persist($hacker);
            $em->flush();
           
           return $this -> redirectToRoute('hacker_show', ['id' => $id_h]);
        }*/

        return $this->render('hacker/show.html.twig', ['result' => $result,
        ]);
    }

    /**
     * @Route("/hackers_del/{id<[0-9]+>}", name="hacker_delete")
     */
    public function delete(EntityManagerInterface $em, Hacker $hacker) : Response
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $sql1 = '
            DELETE FROM offer_report
            WHERE offer_report.id_hacker = :id_h
        ';

        $sql2 = '
            DELETE FROM hacker
            WHERE hacker.id_hacker = :id_h 
        ';

        $info1 = $conn->prepare($sql1);
        $info2 = $conn->prepare($sql2);

        $id_h = $hacker->getIdHacker();
        //dd($id_h);
        $info1->execute([':id_h' => $id_h]);
        $info2->execute([':id_h' => $id_h]);

        $em->flush();    
        return $this->redirectToRoute('hackers');
    }

    /**
     * @Route("/hackers_in/{id<[0-9]+>}", name="hacker_in")
     */
    public function hacker_in(EntityManagerInterface $em, Hacker $hacker) : Response
    {
        $id_h = $hacker -> getIdHacker();
        $admis = $this -> getDoctrine() -> getRepository(State::class) 
                                        -> findBy(['idState' => 1]);
        $hacker -> setState($admis[0]);
        $em -> persist($hacker);

        $notificationHacker = new Notifications();
        $notificationHacker -> setIdHacker($hacker);
        //dd($notification);
        $notificationHacker -> setTypeNotif(6);
        $em -> persist($notificationHacker);

        $em->flush();    
        return $this->redirectToRoute('hacker_show', ['id' => $id_h]);
    }

    /**
     * @Route("/hacker_out/{id<[0-9]+>}", name="hacker_out")
     */
    public function hacker_out(EntityManagerInterface $em, Hacker $hacker) : Response
    {
        $id_h = $hacker -> getIdHacker();
        $recale = $this -> getDoctrine() -> getRepository(State::class) 
                                        -> findBy(['idState' => 3]);
        $hacker -> setState($recale[0]);
        $em -> persist($hacker);

        $notificationHacker = new Notifications();
        $notificationHacker -> setIdHacker($hacker);
        //dd($notification);
        $notificationHacker -> setTypeNotif(11);
        $em -> persist($notificationHacker);

        $em->flush();    
        return $this->redirectToRoute('hacker_show', ['id' => $id_h]);
    }

    /**
     * @Route("/hacker_reports/{id<[0-9]+>}", name="hack_reports")
     */
    public function show_reports(EntityManagerInterface $em, Hacker $hacker) : Response
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();

        /*$sql = '
            SELECT id_report, offer_report.id_hacker, offer_report.id_offer, title, description1, name_ent, entreprise.photo
            FROM offer, entreprise, offer_report, hacker
            WHERE entreprise.id_entreprise = offer.id_entreprise AND offer.id_offer = offer_report.id_offer AND offer_report.id_hacker = :id_h
        ';

        $info = $conn->prepare($sql);

        $id_h = $hacker->getIdHacker();
        //dd($id_e);
        $info->execute([':id_h' => $id_h]);
        $hack_reports = $info->fetchAll(); */
        //dd($hack_reports);

        $hack_reports = $this -> getDoctrine() -> getRepository(OfferReport::class) 
                                        -> findBy(['idHacker' => $hacker->getIdHacker()],
                                                  ['dateSub' => 'DESC']);
                                        
        return $this->render('hacker/hack_reports.html.twig', compact('hack_reports'));
    }

}







