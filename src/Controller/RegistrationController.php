<?php

namespace App\Controller;

use App\Entity\Hacker;
use App\Entity\Entreprise;
use App\Entity\State;
use App\Entity\Notifications;
use App\Form\RegistrationFormHack;
use App\Form\RegistrationFormEnt;
use App\Security\EmailVerifier;
use App\Security\LoginFormAuthenticator;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;



class RegistrationController extends AbstractController
{


    /**
     * @Route("/registerHacker", name="registerHacker")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator): Response
    {
        $user = new Hacker();
        $form = $this->createForm(RegistrationFormHack::class, $user);
        $form->handleRequest($request);

        //upload photo 
        $photoFile = $form->get('photo')->getData();

        if ($photoFile)
        {
            $uploads_directory = $this -> getParameter('uploads_directory');

            $Filename = md5(uniqid()).'.'.$photoFile->guessExtension();

            $photoFile->move(
                        $uploads_directory,
                        $Filename
                        );
            $user->setPhoto($Filename);
        }


        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $encours = $this -> getDoctrine() -> getRepository(State::class) 
                                        -> findBy(['idState' => 2]);
            $user->setState($encours[0]);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);

            $notificationAdmin = new Notifications();
            $notificationAdmin -> setIdHacker($user);
            //dd($notification);
            $notificationAdmin -> setTypeNotif(7);
            $entityManager -> persist($notificationAdmin);

            $entityManager->flush();

            // do anything else you need here, like send an email

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
            return $this->redirectToRoute("home_page");
        }

        return $this->render('registration/registerHacker.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/registerEnt", name="registerEnt")
     */
    public function registerEntreprise(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator): Response
    {
        $user = new Entreprise();
        $form = $this->createForm(RegistrationFormEnt::class, $user);
        $form->handleRequest($request);

         //upload photo 
        $photoFile = $form->get('photo')->getData();

        if($photoFile)
        {

            $uploads_directory = $this -> getParameter('uploads_directory');

            $Filename = md5(uniqid()).'.'.$photoFile->guessExtension();

            $photoFile->move(
                        $uploads_directory,
                        $Filename
                        );
            $user->setPhoto($Filename);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $ShowAuth = $form->getData();

            //dd($ShowAuth);
            if ($ShowAuth == true)
            {
                $user->setShowauth(1);
            }
            else 
            {
                $user->setShowauth(0);
            }
            

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);

            $notificationAdmin = new Notifications();
            $notificationAdmin -> setIdEntreprise($user);
            //dd($notification);
            $notificationAdmin -> setTypeNotif(7);
            $entityManager -> persist($notificationAdmin);

            $entityManager->flush();

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
            return $this->redirectToRoute("home_page");
        }

        return $this->render('registration/registerEnt.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    

}
