<?php

namespace App\Controller;

use App\Entity\Hacker;
use App\Entity\Entreprise;
use App\Entity\Offer;
use App\Entity\OfferReport;
use App\Entity\Notifications;
use App\Entity\ReportPermit;
use App\Entity\State;
use App\Form\SubReportForm;
use App\Form\EditReportForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class OfferReportController extends AbstractController
{
	// Route dynamique en fonction de l'id 
    /**
        * @Route("/report/{id<[0-9]+>}", name="report_show")
    */
    public function ShowReport(OfferReport $report, EntityManagerInterface $em) : Response
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $sql = '
            SELECT offer_report.id_offer, hacker.id_hacker, entreprise.id_entreprise, title, description1, name_ent, entreprise.photo, n_type, username, date_sub
            FROM offer, entreprise, type, hacker, offer_report
            WHERE entreprise.id_entreprise = offer.id_entreprise AND offer.id_type = type.id_type AND hacker.id_hacker = offer_report.id_hacker AND offer.id_offer = offer_report.id_offer AND id_report = :id_r
            ORDER BY date_sub DESC 

        ';
        $info_rep = $conn->prepare($sql);
        $id_r = $report->getIdReport();
        $info_rep->execute([':id_r' => $id_r]);
        $info_rep = $info_rep->fetchAll();

        $result = compact('report', 'info_rep');
        
        //dd($result);

        return $this->render('offer/show_report.html.twig', compact('result'));
    }

    // Route dynamique en fonction de l'id 
    /**
        * @Route("/report_ask/{id<[0-9]+>}", name="report_ask")
    */
    public function AskReport(Offer $offer, EntityManagerInterface $em) : Response
    {
        $id_offer = $offer -> getIdOffer();
        $id_entreprise = $offer -> getIdEntreprise();
         $id_hacker = $this -> getUser();
        if ($id_hacker != null)
        {
            $em = $this->getDoctrine()->getManager();
            
            $notification = new Notifications();
            $notification -> setIdEntreprise($id_entreprise);
            $notification -> setIdHacker($id_hacker);
            $notification -> setIdOffer($offer);
            $notification -> setTypeNotif(3);
            $em -> persist($notification);
            $em->flush();

            $report_permit = new ReportPermit();
            $report_permit -> setIdNotif($notification);
            $em -> persist($report_permit);
            $em->flush();

            $notification -> setIdPermit($report_permit);
            $em -> persist($notification);
            $em->flush();
        }
        $this -> addFlash('message','Your request has been sent');
        return $this -> redirectToRoute('offer_show', ['id' => $id_offer]);
    }

	/**
     * @Route("/sub_report/{id<[0-9]+>}", name="sub_report")
     */
    public function subReport(EntityManagerInterface $em, Offer $offer, Request $request) : Response
    {
    	$user = $this -> getUser();
    	$id_offer = $offer -> getIdOffer();

    	if ($user == null)
    	{
    		$id_hacker = 1;
        }
    	elseif ($user != null)
    	{
    		//dd($user);
	    	$id_hacker = $this -> getUser() -> getIdHacker();
	        //dd(['id_hacker' => $id_hacker, 'id_offer' => $id_offer]);

	        $em = $this->getDoctrine()->getManager();
	        $conn = $this->getDoctrine()->getManager()->getConnection();    

	        $report = new OfferReport();
	        $form = $this->createForm(SubReportForm::class, $report);
	        $form->handleRequest($request);

	        if($form->isSubmitted() && $form -> isValid())
	        {
    			$exp = 20*$_POST['base1']*$_POST['base2']*$_POST['base3'];
    			$impact = 10.41*(1-(1-$_POST['impact1'])*(1-$_POST['impact2'])*(1-$_POST['impact3']));
    			$cvss_base = ((0.6*$impact)+(0.4*$exp)-1.5);
    			if ($impact == 0)
    			{
    				$cvss_base = 0;
    			}
    			elseif ($impact != 0)
    			{
    				$impact = $impact*1.176 ;
    				$cvss_base = round($impact, 1);
    			}

    			$cvss_temp = $cvss_base*$exp*$_POST['temp2']*$_POST['temp3'];
    			$cvss_temp = round($cvss_temp, 1);

    			$adjImpact = 10.41*(1-(1-$_POST['impact1'])*(1-$_POST['impact2'])*(1-$_POST['impact3']));
    			$adjImpact = min(10, $adjImpact);

    	        $report->setIdHacker($user);
    	        $report->setIdOffer($offer);
    	        $report->setCvssBase($cvss_base);
    	        $report->setCvssTemp($cvss_temp);

    	        if ($cvss_base <= 3.9 and $cvss_base >=0)
    	        {
    	        	$severity = "Low";
    	        	$payload_fi = 20;
    	        	$payload_fifi = 60;
    	        }
    	        elseif ($cvss_base >= 4 and $cvss_base <= 6.9)
    	        {
    	        	$severity = "Medium";
    	        	$payload_fi = 60;
    	        	$payload_fifi = 120;
    	        }
    	        elseif ($cvss_base >= 7 and $cvss_base <= 8.9)
    	        {
    	        	$severity = "High";
    	        	$payload_fi = 100;
    	        	$payload_fifi = 300;
    	        }
    	        elseif ($cvss_base >= 9)
    	        {
    	        	$severity = "Critical";
    	        	$payload_fi = 500;
    	        	$payload_fifi = 1000;
    	        }
    	        $report->setSeverity($severity);
    	        //dd($payload_fi);
    	        $report->setPayloadFi($payload_fi);
    	        $report->setPayloadFifi($payload_fifi);

                $dateSub = strtotime(('now'));
                $dateSub = date('m/d/Y',$dateSub);
                $report->setDateSub(new \DateTime($dateSub));
                $encours = $this -> getDoctrine() -> getRepository(State::class) 
                                        -> findBy(['idState' => 2]);
                $report->setState($encours[0]);        	        
    	        $em->persist($report);

                $nbReports = $offer->getNbReports();
                $offer->setNbReports($nbReports + 1);
                $em->persist($offer);

    	        $em->flush();

    	        $id_report = $report->getIdReport();

                $this -> addFlash('message','Your report has been submitted');
    	        return $this->redirectToRoute("report_show", ['id' => $id_report]);
    	    }
	    }
        return $this->render('offer/subReport.html.twig', ['SubReportForm' => $form->createView(), 'offer' => $offer]);
    }

	// Route dynamique en fonction de l'id 
    /**
        * @Route("/report_edit/{id<[0-9]+>}", name="report_edit")
    */
    public function EditReport(OfferReport $report, EntityManagerInterface $em, Request $request) 
    {
        $user = $this -> getUser();

    	if ($user != null)
    	{
	        $em = $this->getDoctrine()->getManager();

	        $form = $this->createForm(EditReportForm::class, $report);
	        $form->handleRequest($request);

	        if($form->isSubmitted() && $form -> isValid())
	        {
	        	if (isset($_POST['base1']) and isset($_POST['base2']) and isset($_POST['base3']) and isset($_POST['impact1']) and isset($_POST['impact2']) and isset($_POST['impact3']) and isset($_POST['temp1']) and isset($_POST['temp2']) and isset($_POST['temp3']))
        		{
	    			$exp = 20*$_POST['base1']*$_POST['base2']*$_POST['base3'];
	    			$impact = 10.41*(1-(1-$_POST['impact1'])*(1-$_POST['impact2'])*(1-$_POST['impact3']));
	    			$cvss_base = ((0.6*$impact)+(0.4*$exp)-1.5);
	    			if ($impact == 0)
	    			{
	    				$cvss_base = 0;
	    			}
	    			elseif ($impact != 0)
	    			{
	    				$impact = $impact*1.176 ;
	    				$cvss_base = round($impact, 1);
	    			}

	    			$cvss_temp = $cvss_base*$exp*$_POST['temp2']*$_POST['temp3'];
	    			$cvss_temp = round($cvss_temp, 1);

	    			$adjImpact = 10.41*(1-(1-$_POST['impact1'])*(1-$_POST['impact2'])*(1-$_POST['impact3']));
	    			$adjImpact = min(10, $adjImpact);

	    	        $report->setCvssBase($cvss_base);
	    	        $report->setCvssTemp($cvss_temp);

	    	        if ($cvss_base <= 3.9 and $cvss_base >=0)
	    	        {
	    	        	$severity = "Low";
	    	        	$payload_fi = 20;
	    	        	$payload_fifi = 60;
	    	        }
	    	        elseif ($cvss_base >= 4 and $cvss_base <= 6.9)
	    	        {
	    	        	$severity = "Medium";
	    	        	$payload_fi = 60;
	    	        	$payload_fifi = 120;
	    	        }
	    	        elseif ($cvss_base >= 7 and $cvss_base <= 8.9)
	    	        {
	    	        	$severity = "High";
	    	        	$payload_fi = 100;
	    	        	$payload_fifi = 300;
	    	        }
	    	        elseif ($cvss_base >= 9)
	    	        {
	    	        	$severity = "Critical";
	    	        	$payload_fi = 500;
	    	        	$payload_fifi = 1000;
	    	        }
	    	        $report->setSeverity($severity);
	    	        $report->setPayloadFi($payload_fi);
	    	        $report->setPayloadFifi($payload_fifi);
    	        }
    	        $em->persist($report);

                $notificationEnt = new Notifications();
                $notificationEnt -> setIdEntreprise($report -> getIdOffer() -> getIdEntreprise());
                $notificationEnt -> setIdReport($report);
                $notificationEnt -> setTypeNotif(2);
                $em -> persist($notificationEnt);

    	        $em->flush();

    	        $id_report = $report->getIdReport();

                $this -> addFlash('message','Your report has been edited');
    	        return $this->redirectToRoute("report_show", ['id' => $id_report]);
    	    }
	    }
	    return $this->render('offer/editReport.html.twig', ['EditReportForm' => $form->createView(), 'report' => $report]);
    }

    /**
        * @Route("/report_accept/{id<[0-9]+>}", name="report_accept")
    */
    public function AcceptReport(OfferReport $report, EntityManagerInterface $em) : Response
    {
        $id_r = $report -> getIdReport();
        $admis = $this -> getDoctrine() -> getRepository(State::class) 
                                        -> findBy(['idState' => 1]);

        $report -> setState($admis[0]);
        $em -> persist($report);

        $notificationHa = new Notifications();
        $notificationHa -> setIdHacker($report -> getIdHacker());
        $notificationHa -> setIdOffer($report -> getIdOffer());
        $notificationHa -> setTypeNotif(12);
        $em -> persist($notificationHa);

        $offer = $report -> getIdOffer();
        $nbThanks = $offer->getNbThanks();
        $offer->setNbThanks($nbThanks + 1);
        $em->persist($offer);

        $em->flush();    

        return $this->redirectToRoute('report_show', ['id' => $id_r]);
    }

    /**
        * @Route("/report_reject/{id<[0-9]+>}", name="report_reject")
    */
    public function RejectReport(OfferReport $report, EntityManagerInterface $em) : Response
    {
        $id_r = $report -> getIdReport();
        $recale = $this -> getDoctrine() -> getRepository(State::class) 
                                        -> findBy(['idState' => 3]);

        $report -> setState($recale[0]);
        $em -> persist($report);

        $notificationHa = new Notifications();
        $notificationHa -> setIdHacker($report -> getIdHacker());
        $notificationHa -> setIdOffer($report -> getIdOffer());
        $notificationHa -> setTypeNotif(13);
        $em -> persist($notificationHa);
        $em->flush();    

        return $this->redirectToRoute('report_show', ['id' => $id_r]);
    }


}