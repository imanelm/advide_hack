<?php

namespace App\Controller;

use App\Entity\Hacker;
use App\Form\SignUpHackerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormBuilder;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Session\Session;

class SignUpHackerController extends AbstractController
{
    /**
     * @Route("/sign_up_hacker", name="sign_up_hacker")
     */
   public function index(Request $request, ManagerRegistry $managerRegistry)
    {
        $Hacker = new Hacker();
        $session = $request->getSession();

  $form =$this->createForm(SignUpHackerType::class, $Hacker);
    
  $form->handleRequest($request);
   if($request->getMethod()=='POST')
        {
          //upload photo 
          $photoFile = $form->get('photo')->getData();
         $uploads_directory = $this -> getParameter('uploads_directory');

        $Filename = md5(uniqid()).'.'.$photoFile->guessExtension();

        $photoFile->move(
                    $uploads_directory,
                      $Filename
                    );
        $Hacker->setPhoto($Filename);

       $dat=$session->get('data');
       $Hacker->setUsername($dat['username']);
       $Hacker->setpassword($dat['password']);
       $Hacker->setEmail($dat['email']);

        if($form->isSubmitted() && $form->isValid()) {

          $data = $request->request->get('form');
          $em = $managerRegistry->getManager();
            $em->persist($Hacker);
            $em->flush();
           return $this->redirectToRoute("home_page");
        }
    }
            return $this->render('sign_up_hacker/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
