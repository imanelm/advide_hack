<?php

namespace App\Controller;

use App\Entity\Hacker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\EditHackerProfileType;
use App\Form\EditEntrepriseProfileType;
use App\Form\TestsFormType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;




class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="profile")
     */
    public function index() : Response
    {
      
        return $this->render('hacker/profile.html.twig');
    }
    
	/**
     * @Route("/profile/EditProfil", name="EditProfil")
     */
    public function EditProfil(Request $request) : Response
    {
       $user = $this -> getUser();
	   $form = $this -> createForm(EditHackerProfileType::class, $user);
	   $form -> handleRequest($request);
	   
	   //upload photo 
        $photoFile = $form->get('photo')->getData();

        if ($photoFile)
        {
            $uploads_directory = $this -> getParameter('uploads_directory');

            $Filename = md5(uniqid()).'.'.$photoFile->guessExtension();

            $photoFile->move(
                        $uploads_directory,
                        $Filename
                        );
            $user->setPhoto($Filename);
        }
	   

        //upload additional file 
        $addFile = $form->get('fichierSup')->getData();

        if ($addFile)
        {
            $uploads_directory = $this -> getParameter('uploads_directory');

            $Filename = md5(uniqid()).'.'.$addFile->guessExtension();

            $addFile->move(
                        $uploads_directory,
                        $Filename
                        );
            $user->setFichierSup($Filename);
        }
        
	   if($form->isSubmitted() && $form -> isValid()){
		   
		   $em = $this -> getDoctrine() -> getManager();
		   $em -> persist($user);
		   $em->flush();
		   
		   $this -> addFlash('message','Your profile has been updated');
		   return $this -> redirectToRoute('EditProfil');
		   
		   
	   }
	  
        return $this->render('hacker/EditHackerProfile.html.twig',[
		'EditForm' => $form -> createView(),
		]);
    }
	/**
     * @Route("/profile/EditProfilEntreprise", name="EditProfilEntreprise")
     */
    public function EditProfilEntreprise(Request $request) : Response
    {
       $user = $this -> getUser();
	   $form = $this -> createForm(EditEntrepriseProfileType::class, $user);
	   $form -> handleRequest($request);
	   
	   //upload photo 
        $photoFile = $form->get('photo')->getData();

        if ($photoFile)
        {
            $uploads_directory = $this -> getParameter('uploads_directory');

            $Filename = md5(uniqid()).'.'.$photoFile->guessExtension();

            $photoFile->move(
                        $uploads_directory,
                        $Filename
                        );
            $user->setPhoto($Filename);
        }
	   
	   if($form->isSubmitted() && $form -> isValid()){
		   
		   $em = $this -> getDoctrine() -> getManager();
		   $em -> persist($user);
		   $em->flush();
		   
		   $this -> addFlash('message','Your profile has been updated');
		   return $this -> redirectToRoute('EditProfilEntreprise');
		   
		   
	   }
	  
        return $this->render('hacker/EditHackerProfile.html.twig',[
		'EditForm' => $form -> createView(),
		]);
    }
	/**
     * @Route("/profile/EditPass", name="EditPass")
     */
    public function EditPass(Request $request, UserPasswordEncoderInterface $passwordEncoder ) 
    {
	if($request->isMethod('POST')){
		
		$em = $this -> getDoctrine() -> getManager();
		$user = $this -> getUser();
		//$passwordEncoder = $this->get('security.password_encoder');
		$oldPassword = $request -> request->get('pass0');
		
		//verify current password
		if($passwordEncoder -> isPasswordValid($user,$oldPassword)){
			//verify if the two passwords match
			if($request -> request->get('pass1') == $request -> request->get('pass2')){
				$user->setPassword($passwordEncoder->encodePassword($user,$request -> request->get('pass1')));
				$em->flush();
				$this -> addFlash('message',' Your password has been successfully changed ');
				return $this -> redirectToRoute('profile');
			}else{
				$this -> addFlash('message',' Passwords do not match ');
				
			}		
		}else{		
			$this -> addFlash('message',' Wrong password ');
	
		}	
		
	}
	  
        return $this->render('hacker/editPass.html.twig');
    }
	/**
     * @Route("/profile/test", name="test")
     */
    public function test(Request $request) : Response
    {
	   $user = $this -> getUser();
	   $form = $this -> createForm(TestsFormType::class, $user);
	   $form -> handleRequest($request);
	   
	   //upload test
        $test = $form->get('test')->getData();
		
		if ($test)
        {
            $uploads_directory = $this -> getParameter('test_directory');
			$name=$user->getName();
			$fname=$user->getFName();
			$_user=$name.'-'.$fname;

            $Filename = $_user.'.'.$test->guessExtension();

            $test->move(
                        $uploads_directory,
                        $Filename
                        );
            $user->setTest($Filename);
        }
		
		if($form->isSubmitted() && $form -> isValid()){
		   
		   $em = $this -> getDoctrine() -> getManager();
		   $em -> persist($user);
		   $em->flush();
		   
		  $this -> addFlash('message','your documet has been successfully uploaded');
		   return $this -> redirectToRoute('profile');
		   
		   
	   }
      
        return $this->render('hacker/test.html.twig',[
		'testForm' => $form -> createView(),
		]);
    }
	/**
      * @Route("/profile/download", name="download_test")
    **/
	public function downloadFileAction(){
		$publicDir = $this->getParameter('kernel.project_dir') . '/public/test.zip';
		$response = new BinaryFileResponse($publicDir);
		$response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT,'test.zip');
		return $response;
	}
	/**
      * @Route("/profile/instructions", name="instructions")
    **/
	public function downloadInstructions(){
		$publicDir = $this->getParameter('kernel.project_dir') . '/public/instructions.pdf';
		$response = new BinaryFileResponse($publicDir);
		$response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT,'instructions.pdf');
		return $response;
	}
}