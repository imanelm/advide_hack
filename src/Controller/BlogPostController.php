<?php

namespace App\Controller;

use App\Entity\BlogPost;
use App\Entity\Comment;
use App\Entity\Reply;
use App\Form\CommentForm;
use App\Form\ReplyForm;
use App\Form\BlogPostForm;
use App\Form\EditBlogPostForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class BlogPostController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index(EntityManagerInterface $em) : Response
    {
    	$blog_posts = $this -> getDoctrine() -> getRepository(BlogPost::class) 
                                        -> findBy([],
                                        		['date' => 'DESC']);

        return $this->render('blog_post/index.html.twig', compact('blog_posts'));
    }

    /**
     * @Route("/blog_post/{id<[0-9]+>}", name="blog_post")
     */
    public function show_post(EntityManagerInterface $em, Request $request, BlogPost $post) : Response
    {
    	$idBp = $post -> getIdBp();
    	$comments = $this -> getDoctrine() -> getRepository(Comment::class) 
                                        -> findBy(['idBp' => $post->getIdBp()],
                                        		['date' => 'DESC']);

        $replies = $this -> getDoctrine() -> getRepository(Reply::class) 
                                        -> findBy([],
                                        		['date' => 'DESC']); 

        /* ------------------------------------------------------------------------------------------ */
                                        
        $comment = new Comment();
        $formComment = $this->createForm(CommentForm::class, $comment);
        $formComment->handleRequest($request);
        if($formComment->isSubmitted() && $formComment->isValid())
        {
        	$user = $this -> getUser();
        	$comment = $formComment->getData();
            if($user)
            {
            	if (method_exists($this -> getUser(),'getIdHacker'))
            	{
            		$comment -> setIdHacker($user);
            	}
            	elseif (method_exists($this -> getUser(),'getIdEntreprise'))
            	{
            		$comment -> setIdEntreprise($user);
            	}
          		$date = strtotime(('now'));
                $date = date('m/d/Y',$date);
                $comment->setDate(new \DateTime($date));

                $comment->setIdBp($post);

                $em->persist($comment);
                $em->flush();
            }
            $this -> addFlash('message','Your comment has been submitted.');
            return $this->redirectToRoute('blog_post', ['id' => $idBp]);
    	}

    	/* ------------------------------------------------------------------------------------------ */
			
			/*//Get question array collection
		    $formBuilderComments = $this->createFormBuilder();
		    $i = 0;


		    //Make a loop for each question
		    foreach($comments as $comment)
		    {
		        //Create an answer form
		        $reply = new Reply();
		        $formBuilder = $this->get('form.factory')->createNamedBuilder($i, 'form', $reply);

		        //Add a answer text box with the question as label
		        $formBuilder -> add('content', TextareaType::class)
				        ;

		        $formBuilderComments->add($formBuilder);

		        $i++;
		    }

		    //Create the form
		    $formReply = $formBuilderComments->getForm();
		    dd($formReply); 


	        if($form->isSubmitted() && $form->isValid())
	        {
	        	$user = $this -> getUser();
	        	$reply = $form->getData();

	            if($user)
	            {
	            	if (method_exists($this -> getUser(),'getIdHacker'))
	            	{
	            		$reply -> setIdHacker($user);
	            	}
	            	elseif (method_exists($this -> getUser(),'getIdEntreprise'))
	            	{
	            		$reply -> setIdEntreprise($user);
	            	}
	          		$date = strtotime(('now'));
	                $date = date('m/d/Y',$date);
	                $reply->setDate(new \DateTime($date));

	                $reply->setIdComment($comment);

	                $em->persist($reply);
	                $em->flush();

	            }
	            return $this->redirectToRoute('blog_post', ['id' => $post -> getIdBp()]);
	        }*/
        

        return $this->render('blog_post/post.html.twig', ['post' => $post, 'comments' => $comments, 'CommentForm' => $formComment->createView()]);
    }

    /**
     * @Route("/new_post", name="new_post")
     */
    public function new_post(EntityManagerInterface $em, Request $request) : Response
    {                             
        $post = new BlogPost();
        $form = $this->createForm(BlogPostForm::class, $post);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
        	$user = $this -> getUser();
        	$post = $form->getData();
            if($user)
            {
            	if (method_exists($this -> getUser(),'getIdHacker'))
            	{
            		$post -> setIdHacker($user);
            	}
            	elseif (method_exists($this -> getUser(),'getIdEntreprise'))
            	{
            		$post -> setIdEntreprise($user);
            	}
          		$date = strtotime(('now'));
                $date = date('m/d/Y',$date);
                $post->setDate(new \DateTime($date));

                $photoFile = $form->get('photo')->getData();
		        if($photoFile)
		        {

		            $uploads_directory = $this -> getParameter('uploads_directory');

		            $Filename = md5(uniqid()).'.'.$photoFile->guessExtension();

		            $photoFile->move(
		                        $uploads_directory,
		                        $Filename
		                        );
		            $post->setPhoto($Filename);
		        }

                $em->persist($post);
                $em->flush();
            }

            return $this->redirectToRoute('blog_post', ['id' => $post -> getIdBp()]);
    	}

    	return $this->render('blog_post/new_post.html.twig', ['BlogPostForm' => $form->createView(),]);
    }	

	/**
     * @Route("/edit_post/{id<[0-9]+>}", name="edit_post")
     */
    public function edit_post(EntityManagerInterface $em, Request $request, BlogPost $post) : Response
    {                             
        $form = $this->createForm(EditBlogPostForm::class, $post);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
        	$post = $form->getData();

            $photoFile = $form->get('photo')->getData();
	        if($photoFile)
	        {

	            $uploads_directory = $this -> getParameter('uploads_directory');

	            $Filename = md5(uniqid()).'.'.$photoFile->guessExtension();

	            $photoFile->move(
	                        $uploads_directory,
	                        $Filename
	                        );
	            $post->setPhoto($Filename);
	        }

                $em->persist($post);
                $em->flush();

                $this -> addFlash('message','You article has been modified.');
                return $this->redirectToRoute('blog_post', ['id' => $post -> getIdBp()]);
    	}


        return $this->render('blog_post/edit_post.html.twig', ['post' => $post, 'EditBlogPostForm' => $form->createView(),]);
    }

    /**
     * @Route("/delete_post/{id<[0-9]+>}", name="delete_post")
     */
    public function delete_post(EntityManagerInterface $em, Request $request, BlogPost $post) : Response
    {   
    	$comments = $this -> getDoctrine() -> getRepository(Comment::class) 
    											-> findBy(['idBp' => $post]);

        if ($comments != null)
        {
        	foreach ($comments as $comment)
        	{	
    			$em->remove($comment);
			}
			$em->flush();
        }

		$em -> remove($post);
	   	$em->flush();

        return $this -> redirectToRoute('blog');
    }

    /**
     * @Route("/delete_comment/{id<[0-9]+>}", name="delete_comment")
     */
    public function delete_comment(EntityManagerInterface $em, Request $request, Comment $comment) : Response
    {   
        $em->remove($comment);
		$em->flush();

        return $this->redirectToRoute('blog_post', ['id' => $comment -> getIdBp() -> getIdBp()]);
    }

    /**
     * @Route("/new_reply/{id<[0-9]+>}", name="new_reply")
     */
    public function new_reply(EntityManagerInterface $em, Request $request, Comment $comment) 
    {   

        $reply = new Reply();
        $form = $this->createForm(ReplyForm::class, $reply);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
        	$user = $this -> getUser();
        	$reply = $form->getData();
            if($user)
            {
            	if (method_exists($this -> getUser(),'getIdHacker'))
            	{
            		$reply -> setIdHacker($user);
            	}
            	elseif (method_exists($this -> getUser(),'getIdEntreprise'))
            	{
            		$reply -> setIdEntreprise($user);
            	}
          		$date = strtotime(('now'));
                $date = date('m/d/Y',$date);
                $reply->setDate(new \DateTime($date));

                $reply->setIdComment($comment);

                $em->persist($reply);
                $em->flush();

            }
            return $this->redirectToRoute('blog_post', ['id' => $comment -> getIdBp() -> getIdBp()]);
        }
        return $this->render('/blog_post/post.html.twig', ['id' => $comment -> getIdBp() -> getIdBp(), 'ReplyForm' => $form->createView()]);    
    }
}










