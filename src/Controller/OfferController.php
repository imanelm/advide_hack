<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Entity\OfferReport;
use App\Entity\Type;
use App\Entity\Notifications;
use App\Entity\State;
use App\Entity\ReportPermit;
use App\Form\SubReportForm;
use App\Form\OfferForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class OfferController extends AbstractController
{
    /**
     * @Route("/offer", name="offers")
     */
    public function index(EntityManagerInterface $em) : Response
    {

    	$conn1 = $this->getDoctrine()->getManager()->getConnection();
        $sql1 = '
            SELECT id_offer, offer.id_entreprise, title, description1, description2, offer.id_type, name_ent, photo, type.n_type, state.n_state, offer.state
            FROM offer, entreprise, type, state
            WHERE entreprise.id_entreprise = offer.id_entreprise AND offer.id_type = type.id_type AND state.id_state = offer.state
            ORDER BY date_add DESC
        ';
        $offers = $conn1->prepare($sql1);
        $offers->execute([]);
        $offers = $offers->fetchAll();

        //dd($offers);
        return $this->render('offer/index.html.twig', compact('offers'));
    }


    // Route dynamique en fonction de l'id 
    /**
    * @Route("/offer/{id<[0-9]+>}", name="offer_show")
    */
    public function show(Offer $offer, EntityManagerInterface $em) : Response
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $sql = '
            SELECT id_offer, offer.id_entreprise, name_ent, photo, n_type, state
            FROM offer, entreprise, type
            WHERE entreprise.id_entreprise = offer.id_entreprise AND offer.id_type = type.id_type AND id_offer = :id_o 
        ';
        $info_ent = $conn->prepare($sql);
        $id_o = $offer->getIdOffer();
        $info_ent->execute([':id_o' => $id_o]);
        $info_ent = $info_ent->fetchAll();

        if (method_exists($this -> getUser(),'getIdHacker'))
        {    
            if ($this -> getUser() -> getIdHacker() != null)
            {
                $notification = $this -> getDoctrine() -> getRepository(Notifications::class) 
                                                        -> findOneBy(['idOffer' => $id_o, 'idHacker' => $this -> getUser() -> getIdHacker()]);
                if ($notification != null)
                {
                    $report_permit = $this -> getDoctrine() -> getRepository(ReportPermit::class) 
                                                        -> findOneBy(['idNotif' => $notification -> getIdNotif()]); 
                //dd($report_permit);    
                    $result = compact('offer', 'info_ent', 'report_permit');
                }
                else
                {
                    $result = compact('offer', 'info_ent');
                }
            }
            else
            {
                $result = compact('offer', 'info_ent');
            }
        }
        else
        {
            $result = compact('offer', 'info_ent');
        }
        //dd($result);
        //dd($result['offer'].'description1');

        return $this->render('offer/show.html.twig', compact('result'));
    }

    /**
     * @Route("/offer_del/{id<[0-9]+>}", name="offer_delete")
     */
    public function delete(EntityManagerInterface $em, Offer $offer) : Response
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();

        $id_o = $offer->getIdOffer();

        $sql0 = '
            UPDATE notifications SET id_permit = NULL
            WHERE notifications.id_offer = :id_o
        ';

        $info0 = $conn->prepare($sql0);
        $info0->execute([':id_o' => $id_o]);

        $sql1 = '
            DELETE FROM report_permit
            WHERE report_permit.id_notif IN (SELECT id_notif FROM notifications WHERE notifications.id_offer = :id_o)
        ';

        $sql2 = '
            DELETE FROM notifications
            WHERE notifications.id_offer = :id_o
        ';

        $sql3 = '
            DELETE FROM offer_report
            WHERE offer_report.id_offer = :id_o
        ';

        $sql4 = '
            DELETE FROM offer
            WHERE id_offer = :id_o
        ';

        $info1 = $conn->prepare($sql1);
        $info1->execute([':id_o' => $id_o]);

        $info2 = $conn->prepare($sql2);
        $info2->execute([':id_o' => $id_o]);

        $info3 = $conn->prepare($sql3);
        $info3->execute([':id_o' => $id_o]);

        $info4 = $conn->prepare($sql4);
        $info4->execute([':id_o' => $id_o]);

        $em->flush();    
        return $this->redirectToRoute('profile');
    }


    /** 
     * @Route("/newoffer", name="offer_creation")
     */
    public function new(Request $request): Response
    {
        $offer = new Offer();
        $form = $this->createForm(OfferForm::class, $offer/*,[
            'vulnerabilities' => $vulnerabilities,
        ]*/);
        $form->handleRequest($request);
        if($form->isSubmitted())
        {
            $user = $this -> getUser();
            if($user)
            {
                $offer = $form->getData();
                //var_dump($user);
                //exit();
                $id_type = $request->request->get('type');
                $type = $this->getDoctrine()->getRepository(Type::class)->find($id_type);
                //var_dump($type);
                $offer->setIdEntreprise($user);
                $offer->setIdType($type);
                $encours = $this -> getDoctrine() -> getRepository(State::class) 
                                        -> findBy(['idState' => 2]);
                $offer->setState($encours[0]);

                $dateAdd = strtotime(('now'));
                $dateAdd = date('m/d/Y',$dateAdd);
                $offer->setDateAdd(new \DateTime($dateAdd));

                //dd($dateAdd);
                //var_dump($offer->getIdEntreprise());
                //var_dump($offer->getIdType()); exit();
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($offer);

                $notificationAdmin = new Notifications();
                $notificationAdmin -> setIdOffer($offer);
                $notificationAdmin ->setIdEntreprise($user);
                $notificationAdmin -> setTypeNotif(8);
                $entityManager -> persist($notificationAdmin);

                $entityManager->flush();
                return $this->redirectToRoute('offers');
            }
            else
            {
                exit("qu'est ce que tu fais dans cette page!!!");
            }
        }

        return $this->render('offer/newoffer.html.twig', [
            'offerForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/offer_in/{id<[0-9]+>}", name="offer_in")
     */
    public function offer_in(EntityManagerInterface $em, Offer $offer) : Response
    {
        $id_o = $offer -> getIdOffer();
        $admis = $this -> getDoctrine() -> getRepository(State::class) 
                                        -> findBy(['idState' => 1]);
        $offer -> setState($admis[0]);
        $em -> persist($offer);

        $notificationEnt = new Notifications();
        $notificationEnt -> setIdEntreprise($offer -> getIdEntreprise());
        $notificationEnt -> setIdOffer($offer);
        $notificationEnt -> setTypeNotif(9);
        $em -> persist($notificationEnt);

        $em->flush();    
        return $this->redirectToRoute('offer_show', ['id' => $id_o]);
    }

    /**
     * @Route("/offer_out/{id<[0-9]+>}", name="offer_out")
     */
    public function offer_out(EntityManagerInterface $em, Offer $offer) : Response
    {
        $id_o = $offer -> getIdOffer();
        $recale = $this -> getDoctrine() -> getRepository(State::class) 
                                        -> findBy(['idState' => 3]);
        $offer -> setState($recale[0]);
        $em -> persist($offer);

        $notificationEnt = new Notifications();
        $notificationEnt -> setIdEntreprise($offer -> getIdEntreprise());
        $notificationEnt -> setIdOffer($offer);
        $notificationEnt -> setTypeNotif(10);
        $em -> persist($notificationEnt);

        $em->flush();    
        return $this->redirectToRoute('offer_show', ['id' => $id_o]);
    }
}
