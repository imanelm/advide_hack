<?php

namespace App\Controller;

use App\Entity\Entreprise;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class EntrepriseController extends AbstractController
{
    /**
     * @Route("/entreprises", name="entreprises")
     */
    public function index() : Response
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $sql = '
            SELECT * FROM entreprise 
            ORDER BY name_ent ASC
        ';
        $entreprises = $conn->prepare($sql);
        $entreprises->execute([]);
        $entreprises = $entreprises->fetchAll();

        //dd($entreprises);
        return $this->render('entreprise/index.html.twig', compact('entreprises'));
    }

    // Route dynamique en fonction de l'id 
    /**
        * @Route("/entreprises/{id<[0-9]+>}", name="entreprise_show")
    */
    public function show(Entreprise $entreprise) : Response
    {
        return $this->render('entreprise/show.html.twig', compact('entreprise'));
    }

    /**
     * @Route("/entreprises_del/{id<[0-9]+>}", name="entreprise_delete")
     */
    public function delete(EntityManagerInterface $em, Entreprise $entreprise) : Response
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();

        $id_e = $entreprise->getIdEntreprise();
        
        $sql0 = '
            UPDATE notifications SET id_permit = NULL
            WHERE notifications.id_entreprise = :id_e
        ';

        $info0 = $conn->prepare($sql0);
        $info0->execute([':id_e' => $id_e]);

        $sql1 = '
            DELETE FROM report_permit
            WHERE report_permit.id_notif IN (SELECT id_notif FROM notifications WHERE notifications.id_entreprise = :id_e)
        ';

        $sql2 = '
            DELETE FROM notifications
            WHERE notifications.id_entreprise = :id_e
        ';

        $sql3 = '
            DELETE FROM offer_report
            WHERE offer_report.id_offer IN (SELECT id_offer FROM offer WHERE offer.id_entreprise = :id_e)
        ';

        $sql4 = '
            DELETE FROM offer
            WHERE offer.id_entreprise = :id_e
        ';

        $sql5 = '
            DELETE FROM entreprise
            WHERE entreprise.id_entreprise = :id_e 
        ';

        $info1 = $conn->prepare($sql1);
        $info1->execute([':id_e' => $id_e]);

        $info2 = $conn->prepare($sql2);
        $info2->execute([':id_e' => $id_e]);

        $info3 = $conn->prepare($sql3);
        $info3->execute([':id_e' => $id_e]);

        $info4 = $conn->prepare($sql4);
        $info4->execute([':id_e' => $id_e]);

        $info5 = $conn->prepare($sql5);
        $info5->execute([':id_e' => $id_e]);

        $em->flush();    
        return $this->redirectToRoute('entreprises');
    }

    /**
     * @Route("/entreprises_offres/{id<[0-9]+>}", name="ent_offres")
     */
    public function show_offers(EntityManagerInterface $em, Entreprise $entreprise) : Response
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();

        $sql = '
            SELECT id_offer, offer.id_entreprise, title, description1, description2, offer.id_type, name_ent, photo, type.n_type, state.n_state, offer.state
            FROM offer, entreprise, type, state
            WHERE entreprise.id_entreprise = offer.id_entreprise AND offer.id_entreprise = :id_e AND type.id_type = offer.id_type AND offer.state = state.id_state
            ORDER BY date_add DESC
        ';

        $info = $conn->prepare($sql);

        $id_e = $entreprise->getIdEntreprise();
        //dd($id_e);
        $info->execute([':id_e' => $id_e]);
        $ent_offers = $info->fetchAll();

        return $this->render('offer/ent_offers.html.twig', compact('ent_offers'));
    }

    /**
     * @Route("/entreprises_reports/{id<[0-9]+>}", name="ent_reports")
     */
    public function show_reports(EntityManagerInterface $em, Entreprise $entreprise) : Response
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();

        $sql = '
            SELECT *
            FROM offer_report, offer, state
            WHERE offer.id_entreprise = :id_e AND offer.id_offer = offer_report.id_offer AND offer_report.state = state.id_state
            ORDER BY date_sub DESC
        ';

        $info = $conn->prepare($sql);

        $id_e = $entreprise->getIdEntreprise();
        $info->execute([':id_e' => $id_e]);
        $ent_reports = $info->fetchAll();

        return $this->render('offer/ent_reports.html.twig', compact('ent_reports'));
    }


}