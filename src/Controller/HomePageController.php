<?php

namespace App\Controller;

use App\Entity\	Offer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class HomePageController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function index(EntityManagerInterface $em) : Response
    {
    	$conn = $this->getDoctrine()->getManager()->getConnection();
        $sql1 = '
            SELECT id_offer, title, offer.id_entreprise, description1, id_type, name_ent, photo, showAuth
            FROM offer, entreprise 
            WHERE entreprise.id_entreprise = offer.id_entreprise AND offer.state = 1
            ORDER BY id_offer DESC
            LIMIT 3
        ';
        $offers = $conn->prepare($sql1);
        $offers->execute([]);

        $offers = $offers->fetchAll();

        return $this->render('home_page/index.html.twig', compact('offers'));
    }
}
