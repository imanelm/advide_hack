<?php

namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormBuilder;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\HttpFoundation\Session\Session;

class SignUpController extends AbstractController
{
    /**
     * @Route("/sign_up", name="sign_up")
     */
    public function index(Request $request, ManagerRegistry $managerRegistry)
    {
    
 
    $alias = array('libelle' => null);
    $formAlias = $this->createFormBuilder($alias)
           
            ->add('username', TextType::class)
            ->add('email', TextType::class)
             ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => ['label' => 'Password'],
                'second_options' => ['label' => 'Confirm Password'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
        
            ->add('save', SubmitType::class, ['label' => 'Valider'])
            ->getForm();

     if ($request->getMethod() == 'POST') {
    $formAlias->handleRequest($request);
 
    if ($formAlias->isSubmitted() && $formAlias->isValid()) {
        
     	$data = $formAlias->getData();
		$session =$request->getSession();
        $session->set('data',$data);
	   // dump($data);
		//die;

		
    return $this->redirectToRoute("sign_up_hacker");
    }
}
            return $this->render('sign_up/index.html.twig', [
            'form' => $formAlias->createView(),
        ]);
    }
}
