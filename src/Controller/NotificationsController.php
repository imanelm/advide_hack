<?php

namespace App\Controller;

use App\Entity\Notifications;
use App\Entity\Entreprise;
use App\Entity\Offer;
use App\Entity\Hacker;
use App\Entity\ReportPermit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class NotificationsController extends AbstractController
{	
	/**
     * @Route("/notifications_ent/{id<[0-9]+>}", name="my_notifications_ent")
     */
    public function index_ent(Entreprise $entreprise, EntityManagerInterface $em) : Response
    {
        $ent_notifications = $this -> getDoctrine() -> getRepository(Notifications::class) 
        											-> findBy(['idEntreprise' => $entreprise],
                                                              ['idNotif' => 'DESC']);
        //dd($ent_notifications);

        return $this->render('entreprise/my_notifications.html.twig', compact('ent_notifications'));
    }

    /**
     * @Route("/notifications_ha/{id<[0-9]+>}", name="my_notifications_ha")
     */
    public function index_ha(Hacker $hacker, EntityManagerInterface $em) : Response
    {
        $ha_notifications = $this -> getDoctrine() -> getRepository(Notifications::class) 
        											-> findBy(['idHacker' => $hacker],
                                                              ['idNotif' => 'DESC']);


        return $this->render('hacker/my_notifications.html.twig', compact('ha_notifications'));
    }

    /**
     * @Route("/notifications_admin", name="notifications_admin")
     */
    public function index_admin(EntityManagerInterface $em) : Response
    {
        $notifications_admin = $this -> getDoctrine() -> getRepository(Notifications::class) 
        											-> findBy([], ['idNotif' => 'DESC']);
		//dd($notifications_admin);


        return $this->render('hacker/notifications_admin.html.twig', compact('notifications_admin'));
    }

    /**
     * @Route("/notifications_read_ent/{id<[0-9]+>}", name="mark_as_read_ent")
     */
    public function read_ent(Notifications $notification, EntityManagerInterface $em) : Response
    {
    	$em = $this -> getDoctrine() -> getManager();

    	if ($notification->getNotifRead() == 0)
		{
			$notification -> setNotifRead(1);
		}
		elseif ($notification->getNotifRead() == 1)
		{
			$notification -> setNotifRead(0);
		}
	   	$em -> persist($notification);
	   	$em->flush();

	   	$idEnt = $notification -> getIdEntreprise() -> getIdEntreprise();

        return $this -> redirectToRoute('my_notifications_ent', ['id' => $idEnt]);
    }

    /**
     * @Route("/notifications_read_ha/{id<[0-9]+>}", name="mark_as_read_ha")
     */
    public function read_ha(Notifications $notification, EntityManagerInterface $em) : Response
    {
    	$em = $this -> getDoctrine() -> getManager();

    	if ($notification->getNotifRead() == 0)
		{
			$notification -> setNotifRead(1);
		}
		elseif ($notification->getNotifRead() == 1)
		{
			$notification -> setNotifRead(0);
		}
	   	$em -> persist($notification);
	   	$em->flush();

	   	$idHa = $notification -> getIdHacker() -> getIdHacker();

        return $this -> redirectToRoute('my_notifications_ha', ['id' => $idHa]);
    }

    /**
     * @Route("/notifications_read_admin/{id<[0-9]+>}", name="mark_as_read_admin")
     */
    public function read_admin(Notifications $notification, EntityManagerInterface $em) : Response
    {
    	$em = $this -> getDoctrine() -> getManager();

    	if ($notification->getNotifRead() == 0)
		{
			$notification -> setNotifRead(1);
		}
		elseif ($notification->getNotifRead() == 1)
		{
			$notification -> setNotifRead(0);
		}
	   	$em -> persist($notification);
	   	$em->flush();

        return $this -> redirectToRoute('notifications_admin');
    }

    /**
     * @Route("/notifications_permit/{id<[0-9]+>}", name="report_permit")
     */
    public function permit(Notifications $notification, EntityManagerInterface $em) : Response
    {
    	$em = $this -> getDoctrine() -> getManager();

    	$report_permit = $this -> getDoctrine() -> getRepository(ReportPermit::class) 
        											-> findOneBy(['idNotif' => $notification]);
		$entreprise = $this -> getDoctrine() -> getRepository(Entreprise::class) 
        											-> findOneBy(['idEntreprise' => $notification->getIdEntreprise()->getIdEntreprise()]);
        $hacker = $this -> getDoctrine() -> getRepository(Hacker::class) 
        											-> findOneBy(['idHacker' => $notification->getIdHacker()->getIdHacker()]);						
        $offer = $this -> getDoctrine() -> getRepository(Offer::class) 
        											-> findOneBy(['idOffer' => $notification->getIdOffer()->getIdOffer()]);											
    	if ($report_permit->getState() == 0)
		{
			$report_permit -> setState(1); //accepted
			$notificationHacker = new Notifications();
	        $notificationHacker -> setIdEntreprise($entreprise);
	        $notificationHacker -> setIdHacker($hacker);
	        $notificationHacker -> setIdOffer($offer);
	        //dd($notification);
	        $notificationHacker -> setTypeNotif(4);
	        $em -> persist($notificationHacker);

			if ($notification->getNotifRead() == 0)
			{
				$notification->setNotifRead(1);
			}
			elseif ($notification->getNotifRead() == 1)
			{
				$notification->setNotifRead(0);
			}
		   	$em -> persist($notification);

		}		
	   	$em -> persist($report_permit);

	   	$em->flush();

	   	$idEnt = $report_permit -> getIdNotif() -> getIdEntreprise() -> getIdEntreprise();

        return $this -> redirectToRoute('my_notifications_ent', ['id' => $idEnt, 'report_permit' => $report_permit]);
    }

    /**
     * @Route("/notifications_reject/{id<[0-9]+>}", name="report_reject")
     */
    public function reject(Notifications $notification, EntityManagerInterface $em) : Response
    {
    	$em = $this -> getDoctrine() -> getManager();

    	$report_permit = $this -> getDoctrine() -> getRepository(ReportPermit::class) 
        											-> findOneBy(['idNotif' => $notification]);
		$entreprise = $this -> getDoctrine() -> getRepository(Entreprise::class) 
        											-> findOneBy(['idEntreprise' => $notification->getIdEntreprise()->getIdEntreprise()]);
        $hacker = $this -> getDoctrine() -> getRepository(Hacker::class) 
        											-> findOneBy(['idHacker' => $notification->getIdHacker()->getIdHacker()]);	
        $offer = $this -> getDoctrine() -> getRepository(Offer::class) 
        											-> findOneBy(['idOffer' => $notification->getIdOffer()->getIdOffer()]);											
    	if ($report_permit->getState() == 0)
		{
			$report_permit -> setState(2); //rejected
			$notificationHacker = new Notifications();
	        $notificationHacker -> setIdEntreprise($entreprise);
	        $notificationHacker -> setIdHacker($hacker);
	        $notificationHacker -> setIdOffer($offer);
	        //dd($notification);
	        $notificationHacker -> setTypeNotif(5);
	        $em -> persist($notificationHacker);

			if ($notification->getNotifRead() == 0)
			{
				$notification->setNotifRead(1);
			}
			elseif ($notification->getNotifRead() == 1)
			{
				$notification->setNotifRead(0);
			}
		   	$em -> persist($notification);

		}		
	   	$em -> persist($report_permit);

	   	$em->flush();

	   	$idEnt = $report_permit -> getIdNotif() -> getIdEntreprise() -> getIdEntreprise();

        return $this -> redirectToRoute('my_notifications_ent', ['id' => $idEnt, 'report_permit' => $report_permit]);
    }

    /**
     * @Route("/notifications_del_ent/{id<[0-9]+>}", name="del_notif_ent")
     */
    public function del_notif_ent(Notifications $notification, EntityManagerInterface $em) : Response
    {
    	$em = $this -> getDoctrine() -> getManager();

    	$idEnt = $notification -> getIdEntreprise() -> getIdEntreprise();

    	$report_permit = $this -> getDoctrine() -> getRepository(ReportPermit::class) 
    											-> findOneBy(['idNotif' => $notification]);
    	$notification -> setIdPermit(null);
    	$em -> persist($notification);
    	$em->flush();

        if ($report_permit != null)
        {
        	$report_permit -> setIdNotif(null);
        	$em -> persist($report_permit);
        	$em->flush();
    	$em -> remove($report_permit);
        }

		$em -> remove($notification);
	   	$em->flush();

        return $this -> redirectToRoute('my_notifications_ent', ['id' => $idEnt]);
    }

    /**
     * @Route("/notifications_del_ha/{id<[0-9]+>}", name="del_notif_ha")
     */
    public function del_notif_ha(Notifications $notification, EntityManagerInterface $em) : Response
    {
    	$em = $this -> getDoctrine() -> getManager();

    	$idHa = $notification -> getIdHacker() -> getIdHacker();

		$em -> remove($notification);
	   	$em->flush();

        return $this -> redirectToRoute('my_notifications_ha', ['id' => $idHa]);
    }

    /**
     * @Route("/notifications_del_admin/{id<[0-9]+>}", name="del_notif_admin")
     */
    public function del_notif_admin(Notifications $notification, EntityManagerInterface $em) : Response
    {
    	$em = $this -> getDoctrine() -> getManager();

		$em -> remove($notification);
	   	$em->flush();

        return $this -> redirectToRoute('notifications_admin');
    }



}