<?php

namespace App\Controller;

use App\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class MessageController extends AbstractController
{
    /**
     * @Route("/messages", name="messages")
     */
    public function index()
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $messages = $conn->prepare("SELECT * FROM message ORDER BY pseudo");
        $messages->execute();
        $messages = $messages->fetchAll();
        $new_messages = array(); //un tableau qui regroupes tous les messages envoyer par la même personne
        foreach($messages as $k => $v)
        {
            if(array_key_exists($v['pseudo'], $new_messages)){
                $new_messages[$v['pseudo']] = array_merge($new_messages[$v['pseudo']], array($v['message']));
            }else{
                $new_messages[$v['pseudo']] = array($v['message']);
            }
        }
        //print_r($result);
        //exit();
        $messages = array();
        foreach($new_messages as $k => $v)
        {
            array_push($messages, array("username" => $k, "messages" => $v));   
        }
        //dd($messages);        
        return $this->render('messages/messages.html.twig', compact('messages'));
    }

    /**
     * @Route("/update_message", name="update_message")
     */
    public function updateMessages()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $username=$_GET['pseudo'];
        $message = $_GET['message'];
        
        $message_obj = new Message();
        $message_obj->setPseudo($username);
        $message_obj->setMessage($message);

        $entityManager->persist($message_obj);
        $entityManager->flush();
        return $this->redirectToRoute('home_page');
    }

    /**
     * @Route("/get_message", name="get_message")
     */
    public function getMessages()
    {
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $username=$_GET['username'];
        if($username == "admin")
        {   
            $result = $conn->prepare("SELECT * FROM message");
            //$result->bind_param("s", $username);
            $result->execute();

            $result = $result->fetchAll();
            
            /*foreach($result as $k => $v)
            {
                //print_r($v); echo "<br/>";
                echo $v['pseudo'];
                echo"\\";
                echo $v['message'];
                echo "\n";
            }*/
            foreach($result as $k => $v)
            {
                //print_r($v); echo "<br/>";
                echo $v['pseudo'];
                echo"\\";
                echo $v['message'];
                echo"\\";
                echo $v['response_to'];
                echo "\n";
            }
            //dd($result);
            exit();
            //print("i'm the Admin");
            //exit();
        }else{
            $result = $conn->prepare("SELECT * FROM message WHERE pseudo=\"".$username."\" OR response_to=\"". $username."\"");
            //$result->bind_param("s", $username);
            $result->execute();

            $result = $result->fetchAll();
            //print_r($result);
            //exit();
            //while ($r = $result->fetchRow()) {
            //	echo $r[1];
            //	echo "\\";
            //	echo $r[2];
            //	echo "\n";
            //}
            foreach($result as $k => $v)
            {
                //print_r($v); echo "<br/>";
                echo $v['pseudo'];
                echo"\\";
                echo $v['message'];
                echo "\n";
            }
            //dd($result);
            exit();
        }
        return $this->redirectToRoute('home_page');

    }

    /**
     * @Route("/response", name="response")
     */
    public function response()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $username=$_GET['response_to'];
        $message = $_GET['message'];
        
        $message_obj = new Message();
        $message_obj->setPseudo("admin");
        $message_obj->setMessage($message);
        $message_obj->setResponseTo($username);
        $entityManager->persist($message_obj);
        $entityManager->flush();
        return $this->redirectToRoute('home_page');
    }

}