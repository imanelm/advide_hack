<?php

namespace App\Form;

use App\Entity\Entreprise;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EditEntrepriseProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameEnt')
            ->add('email')
            ->add('name')
            ->add('fName')
            ->add('num')
            ->add('country')
            ->add('photo', FileType::class, [
                'mapped' => false,
                'label'=>'Upload image',
				'required' => false,

            ])
             ->add('adress', TextType::class, [
             'attr' => [
                'placeholder' => 'Adresse',
                'required' => false,
            ]])
              ->add('site', TextType::class, [
             'attr' => [
                'placeholder' => 'Site',
                'required' => false,
            ]])
        
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Entreprise::class,
        ]);
    }
}
