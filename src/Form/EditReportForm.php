<?php

namespace App\Form;

use App\Entity\OfferReport;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class EditReportForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('scope', TextareaType::class)
            ->add('end_point', TextType::class)
            ->add('tech_env', TextType::class)
            ->add('app_fingerprint', TextType::class)
            ->add('ip_used', TextType::class)
            ->add('report', TextareaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OfferReport::class,
        ]);
    }
}
