<?php

namespace App\Form;

use App\Entity\Hacker;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;




class EditHackerProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email')
            ->add('name')
            ->add('fName')
            ->add('country')
            ->add('photo', FileType::class, [
                'mapped' => false,
                'label'=>'Upload image',
				'required' => false,

            ])
            ->add('adress', TextType::class, [
             'attr' => [
                'placeholder' => 'Adresse',
                'required' => false,
            ]])
            ->add('twitter', TextType::class, [
             'attr' => [
                'placeholder' => 'Twitter',
                'required' => false,
            ]])
            ->add('linkedin', TextType::class, [
             'attr' => [
                'placeholder' => 'Linkedin',
                'required' => false,
            ]])
            ->add('github', TextType::class, [
             'attr' => [
                'placeholder' => 'Github',
                'required' => false,
            ]])
            ->add('fichierSup', FileType::class, [
                'mapped' => false,
                'label'=>'Additional file',
                'required' => false,

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Hacker::class,
        ]);
    }
}
