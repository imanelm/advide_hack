<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notifications
 *
 * @ORM\Table(name="report_permit", indexes={@ORM\Index(name="id_permit", columns={"id_permit"}), @ORM\Index(name="id_offer", columns={"id_offer"})})
 * @ORM\Entity
 */
class ReportPermit
{
	/**
     * @var int
     *
     * @ORM\Column(name="id_permit", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
	private $idPermit;

    /**
     * @var \Notifications
     *
     * @ORM\ManyToOne(targetEntity="Notifications")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_notif", referencedColumnName="id_notif", nullable=true)
     * })
     */
    private $idNotif;

    /**
     * @var int
     *
     * @ORM\Column(name="state", type="integer", nullable=false, options={"default"=0})
     */
    private $state=0;


    public function getIdPermit(): ?int
    {
        return $this->idNotif;
    }

    public function getIdNotif(): ?Notifications
    {
        return $this->idNotif;
    }

    public function setIdNotif(?Notifications $idNotif): self
    {
        $this->idNotif = $idNotif;

        return $this;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }
}