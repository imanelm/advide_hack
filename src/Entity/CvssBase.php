<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CvssBase
 *
 * @ORM\Table(name="cvss_base", indexes={@ORM\Index(name="level_vuln_2", columns={"level_vuln"}), @ORM\Index(name="level_vuln", columns={"level_vuln"})})
 * @ORM\Entity
 */
class CvssBase
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_vuln", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idVuln;

    /**
     * @var string
     *
     * @ORM\Column(name="n_vuln", type="string", length=60, nullable=false)
     */
    private $nVuln;

    /**
     * @var int
     *
     * @ORM\Column(name="level_vuln", type="integer", nullable=false)
     */
    private $levelVuln;

    /**
     * @var float
     *
     * @ORM\Column(name="score_vuln", type="float", precision=10, scale=0, nullable=false)
     */
    private $scoreVuln;

    public function getIdVuln(): ?int
    {
        return $this->idVuln;
    }

    public function getNVuln(): ?string
    {
        return $this->nVuln;
    }

    public function setNVuln(string $nVuln): self
    {
        $this->nVuln = $nVuln;

        return $this;
    }

    public function getLevelVuln(): ?int
    {
        return $this->levelVuln;
    }

    public function setLevelVuln(int $levelVuln): self
    {
        $this->levelVuln = $levelVuln;

        return $this;
    }

    public function getScoreVuln(): ?float
    {
        return $this->scoreVuln;
    }

    public function setScoreVuln(float $scoreVuln): self
    {
        $this->scoreVuln = $scoreVuln;

        return $this;
    }


}
