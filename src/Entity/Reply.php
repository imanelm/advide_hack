<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table(name="reply", indexes={@ORM\Index(name="id_hacker", columns={"id_hacker"}), @ORM\Index(name="id_comment", columns={"id_comment"}), @ORM\Index(name="id_entreprise", columns={"id_entreprise"})})
 * @ORM\Entity
 */
class Reply
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_reply", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idReply;

    /**
     * @var \Comment
     *
     * @ORM\ManyToOne(targetEntity="Comment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_comment", referencedColumnName="id_comment")
     * })
     */
    private $idComment;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=false)
     */
    private $content;

    /**
     * @var \Hacker
     *
     * @ORM\ManyToOne(targetEntity="Hacker")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_hacker", referencedColumnName="id_hacker")
     * })
     */
    private $idHacker;

    /**
     * @var \Entreprise
     *
     * @ORM\ManyToOne(targetEntity="Entreprise")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_entreprise", referencedColumnName="id_entreprise")
     * })
     */
    private $idEntreprise;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;
    

    public function getIdReply(): ?int
    {
        return $this->idReply;
    }

    public function getIdComment(): ?Comment
    {
        return $this->idComment;
    }

    public function setIdComment(Comment $idComment): self
    {
        $this->idComment = $idComment;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIdHacker(): ?Hacker
    {
        return $this->idHacker;
    }

    public function setIdHacker(?Hacker $idHacker): self
    {
        $this->idHacker = $idHacker;

        return $this;
    }

    public function getIdEntreprise(): ?Entreprise
    {
        return $this->idEntreprise;
    }

    public function setIdEntreprise(?Entreprise $idEntreprise): self
    {
        $this->idEntreprise = $idEntreprise;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }


}
