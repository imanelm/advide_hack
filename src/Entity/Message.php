<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message", indexes={@ORM\Index(name="id", columns={"id"})})
 * @ORM\Entity
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_message", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id_message;

    /**
     * @var string
     *
     * @ORM\Column(name="pseudo", type="text", length=32, nullable=false)
     */
    private $pseudo;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", length=65535, nullable=false)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="response_to", type="text", length=32, nullable=false)
     */
    private $responseTo;

    public function getIdMessage(): ?int
    {
        return $this->idMessage;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }
    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;
        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getResponseTo(): ?string
    {
        return $this->responseTo;
    }
    public function setResponseTo(string $responseTo): self
    {
        $this->responseTo = $responseTo;
        return $this;
    }
}