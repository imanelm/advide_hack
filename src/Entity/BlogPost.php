<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BlogPost
 *
 * @ORM\Table(name="blog_post", indexes={@ORM\Index(name="id_entreprise", columns={"id_entreprise"}), @ORM\Index(name="id_hacker", columns={"id_hacker"})})
 * @ORM\Entity
 */
class BlogPost
{

    /**
     * @var int
     *
     * @ORM\Column(name="id_bp", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idBp;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=200, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=false)
     */
    private $content;

    /**
     * @var \Hacker
     *
     * @ORM\ManyToOne(targetEntity="Hacker")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_hacker", referencedColumnName="id_hacker")
     * })
     */
    private $idHacker;

    /**
     * @var \Entreprise
     *
     * @ORM\ManyToOne(targetEntity="Entreprise")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_entreprise", referencedColumnName="id_entreprise")
     * })
     */
    private $idEntreprise;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true, options={"default"="logo.png"})
     */
    private $photo = 'logo.png';
    
    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;


    public function getIdBp(): int
    {
        return $this->idBp;
    }

    public function setIdBp(int $idBp): self
    {
        $this->idBp = $idBp;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIdHacker(): ?Hacker
    {
        return $this->idHacker;
    }

    public function setIdHacker(?Hacker $idHacker): self
    {
        $this->idHacker = $idHacker;

        return $this;
    }

    public function getIdEntreprise(): ?Entreprise
    {
        return $this->idEntreprise;
    }

    public function setIdEntreprise(?Entreprise $idEntreprise): self
    {
        $this->idEntreprise = $idEntreprise;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }


}
