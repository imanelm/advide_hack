<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Type
 *
 * @ORM\Table(name="type")
 * @ORM\Entity
 */
class Type
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_type", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idType;

    /**
     * @var string
     *
     * @ORM\Column(name="n_type", type="string", length=40, nullable=false)
     */
    private $nType;

    public function getIdType(): ?int
    {
        return $this->idType;
    }

    public function setIdType(?Type $idType): self
    {
        $this->idType = $idType;

        return $this;
    }

    public function getNtype(): ?string
    {
        return $this->n_type;
    }

    public function setNtype(?Type $nType): self
    {
        $this->n_type = $nType;

        return $this;
    }




}
