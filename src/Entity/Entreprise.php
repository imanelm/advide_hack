<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Entreprise
 *
 * @ORM\Table(name="entreprise")
 * @ORM\Entity
 */
class Entreprise implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_entreprise", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idEntreprise;

    /**
     * @var string
     *
     * @ORM\Column(name="name_ent", type="string", length=30, nullable=false)
     */
    private $nameEnt;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=20, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="f_name", type="string", length=20, nullable=false)
     */
    private $fName;

    /**
     * @var int
     *
     * @ORM\Column(name="num", type="integer", nullable=false)
     */
    private $num;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=20, nullable=false)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=12, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=false, options={"default"="logo.png"})
     */
    private $photo = 'logo.png';

    /**
     * @var bool
     *
     * @ORM\Column(name="showAuth", type="boolean", nullable=false, options={"default"="0"})
     */
    private $showauth = '0';

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

            /**
     * @var string
     *
     * @ORM\Column(name="adress", type="string", length=100, nullable=true)
     */
    private $adress;

        /**
     * @var string
     *
     * @ORM\Column(name="site", type="string", length=50, nullable=true)
     */
    private $site;

    /**
     * @var int
     *
     * @ORM\Column(name="new_notif", type="integer", nullable=false, options={"default"=0})
     */
    private $newNotif=0;


    public function getIdEntreprise(): ?int
    {
        return $this->idEntreprise;
    }

    public function getNameEnt(): ?string
    {
        return $this->nameEnt;
    }

    public function setNameEnt(string $nameEnt): self
    {
        $this->nameEnt = $nameEnt;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFName(): ?string
    {
        return $this->fName;
    }

    public function setFName(string $fName): self
    {
        $this->fName = $fName;

        return $this;
    }

    public function getNum(): ?int
    {
        return $this->num;
    }

    public function setNum(int $num): self
    {
        $this->num = $num;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getpassword(): ?string
    {
        return $this->password;
    }

    public function setpassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getShowauth(): ?bool
    {
        return $this->showauth;
    }

    public function setShowauth(bool $showauth): self
    {
        $this->showauth = $showauth;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
    public function getUsername(): ?string
    {
        return $this->nameEnt;
    }

         public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }
         public function getSite(): ?string
    {
        return $this->site;
    }

    public function setSite(string $site): self
    {
        $this->site = $site;

        return $this;
    }

    public function getNewNotif(): ?int
    {
        return $this->newNotif;
    }

    public function setNewNotif(int $newNotif): self
    {
        $this->newNotif = $newNotif;

        return $this;
    }
}
