<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OfferReport
 *
 * @ORM\Table(name="offer_report", indexes={@ORM\Index(name="id_hacker", columns={"id_hacker"}), @ORM\Index(name="id_offer", columns={"id_offer"})})
 * @ORM\Entity
 */
class OfferReport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_report", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idReport;

    /**
     * @var string
     *
     * @ORM\Column(name="report", type="text", length=65535, nullable=false)
     */
    private $report;

    /**
     * @var float
     *
     * @ORM\Column(name="cvss_base", type="float", precision=10, scale=0, nullable=false)
     */
    private $cvssBase;

        /**
     * @var float
     *
     * @ORM\Column(name="cvss_impact", type="float", precision=10, scale=0, nullable=false)
     */
    private $cvssImpact;

    /**
     * @var float
     *
     * @ORM\Column(name="cvss_temp", type="float", precision=10, scale=0, nullable=false)
     */
    private $cvssTemp;

    /**
     * @var float
     *
     * @ORM\Column(name="cvss_env", type="float", precision=10, scale=0, nullable=false)
     */
    private $cvssEnv;

    /**
     * @var string
     *
     * @ORM\Column(name="scope", type="text", length=250, nullable=false)
     */
    private $scope;

    /**
     * @var string
     *
     * @ORM\Column(name="end_point", type="text", length=250, nullable=false)
     */
    private $endPoint;

    /**
     * @var string
     *
     * @ORM\Column(name="severity", type="text", length=20, nullable=false)
     */
    private $severity;

    /**
     * @var int
     *
     * @ORM\Column(name="payload_fi", type="integer", nullable=false)
     */
    private $payloadFi;

    /**
     * @var int
     *
     * @ORM\Column(name="payload_fifi", type="integer", nullable=false)
     */
    private $payloadFifi;

    /**
     * @var string
     *
     * @ORM\Column(name="tech_env", type="text", length=100, nullable=false)
     */
    private $techEnv;

    /**
     * @var string
     *
     * @ORM\Column(name="app_fingerprint", type="text", length=100, nullable=false)
     */
    private $appFingerprint;

    /**
     * @var float
     *
     * @ORM\Column(name="ip_used", type="float", precision=10, scale=0, nullable=false)
     */
    private $ipUsed;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_sub", type="date", nullable=true)
     */
    private $dateSub;

    /**
     * @var \Offer
     *
     * @ORM\ManyToOne(targetEntity="Offer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_offer", referencedColumnName="id_offer")
     * })
     */
    private $idOffer;

    /**
     * @var \Hacker
     *
     * @ORM\ManyToOne(targetEntity="Hacker")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_hacker", referencedColumnName="id_hacker")
     * })
     */
    private $idHacker;

    /**
     * @var \State
     *
     * @ORM\ManyToOne(targetEntity="State")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="state", referencedColumnName="id_state")
     * })
     */
    private $state;


    public function getIdReport(): ?int
    {
        return $this->idReport;
    }

    public function getReport(): ?string
    {
        return $this->report;
    }

    public function setReport(string $report): self
    {
        $this->report = $report;

        return $this;
    }

    public function getCvssBase(): ?float
    {
        return $this->cvssBase;
    }

    public function setCvssBase(float $cvssBase): self
    {
        $this->cvssBase = $cvssBase;

        return $this;
    }

    public function getCvssImpact(): ?float
    {
        return $this->cvssImpact;
    }

    public function setCvssImpact(float $cvssImpact): self
    {
        $this->cvssImpact = $cvssImpact;

        return $this;
    }

    public function getCvssTemp(): ?float
    {
        return $this->cvssTemp;
    }

    public function setCvssTemp(float $cvssTemp): self
    {
        $this->cvssTemp = $cvssTemp;

        return $this;
    }

    public function getCvssEnv(): ?float
    {
        return $this->cvssEnv;
    }

    public function setCvssEnv(float $cvssEnv): self
    {
        $this->cvssEnv = $cvssEnv;

        return $this;
    }

    public function getScope(): ?string
    {
        return $this->scope;
    }

    public function setScope(string $scope): self
    {
        $this->scope = $scope;

        return $this;
    }

    public function getEndPoint(): ?string
    {
        return $this->endPoint;
    }

    public function setEndPoint(string $endPoint): self
    {
        $this->endPoint = $endPoint;

        return $this;
    }

    public function getSeverity(): ?string
    {
        return $this->severity;
    }

    public function setSeverity(string $severity): self
    {
        $this->severity = $severity;

        return $this;
    }

    public function getPayloadFi(): ?int
    {
        return $this->payloadFi;
    }

    public function setPayloadFi(int $payloadFi): self
    {
        $this->payloadFi = $payloadFi;

        return $this;
    }

    public function getPayloadFifi(): ?int
    {
        return $this->payloadFifi;
    }

    public function setPayloadFifi(int $payloadFifi): self
    {
        $this->payloadFifi = $payloadFifi;

        return $this;
    }

    public function getTechEnv(): ?string
    {
        return $this->techEnv;
    }

    public function setTechEnv(string $techEnv): self
    {
        $this->techEnv = $techEnv;

        return $this;
    }

    public function getAppFingerprint(): ?string
    {
        return $this->appFingerprint;
    }

    public function setAppFingerprint(string $appFingerprint): self
    {
        $this->appFingerprint = $appFingerprint;

        return $this;
    }

    public function getIpUsed(): ?float
    {
        return $this->ipUsed;
    }

    public function setIpUsed(float $ipUsed): self
    {
        $this->ipUsed = $ipUsed;

        return $this;
    }

    public function getDateSub(): ?\DateTimeInterface
    {
        return $this->dateSub;
    }

    public function setDateSub(?\DateTimeInterface $dateSub): self
    {
        $this->dateSub = $dateSub;

        return $this;
    }

    public function getIdOffer(): ?Offer
    {
        return $this->idOffer;
    }

    public function setIdOffer(?Offer $idOffer): self
    {
        $this->idOffer = $idOffer;

        return $this;
    }

    public function getIdHacker(): ?Hacker
    {
        return $this->idHacker;
    }

    public function setIdHacker(?Hacker $idHacker): self
    {
        $this->idHacker = $idHacker;

        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(State $state): self
    {
        $this->state = $state;

        return $this;
    }


}
