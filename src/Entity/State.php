<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * State
 *
 * @ORM\Table(name="state")
 * @ORM\Entity
 */
class State
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_state", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idState;

    /**
     * @var string
     *
     * @ORM\Column(name="n_state", type="string", length=40, nullable=false)
     */
    private $nState;

    public function getIdState(): ?int
    {
        return $this->idState;
    }

    public function setIdState(?int $idState): self
    {
        $this->idState = $idState;

        return $this;
    }

    public function getNstate(): ?string
    {
        return $this->nState;
    }

    public function setNstate(?string $nState): self
    {
        $this->nState = $nState;

        return $this;
    }




}
