<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Offer
 *
 * @ORM\Table(name="offer", indexes={@ORM\Index(name="id_entreprise", columns={"id_entreprise"})})
 * @ORM\Entity
 */
class Offer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_offer", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idOffer;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", length=80, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description1", type="text", length=65535, nullable=false)
     */
    private $description1;

    /**
     * @var string
     *
     * @ORM\Column(name="description2", type="text", length=65535, nullable=false)
     */
    private $description2;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_add", type="date", nullable=true)
     */
    private $dateAdd;

    /**
     * @var \Type
     *
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type", referencedColumnName="id_type")
     * })
     */
    private $id_type;

    /**
     * @var \Entreprise
     *
     * @ORM\ManyToOne(targetEntity="Entreprise")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_entreprise", referencedColumnName="id_entreprise")
     * })
     */
    private $idEntreprise;

    /**
     * @var \State
     *
     * @ORM\ManyToOne(targetEntity="State")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="state", referencedColumnName="id_state")
     * })
     */
    private $state;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_reports", type="integer", nullable=true, options={"default"=0})
     */
    private $nbReports=0;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_thanks", type="integer", nullable=true, options={"default"=0})
     */
    private $nbThanks=0;


    public function getIdOffer(): ?int
    {
        return $this->idOffer;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
        }

    public function getDescription1(): ?string
    {
        return $this->description1;
    }

    public function setDescription1(string $description1): self
    {
        $this->description1 = $description1;

        return $this;
    }

        public function getDescription2(): ?string
    {
        return $this->description2;
    }

    public function setDescription2(string $description2): self
    {
        $this->description2 = $description2;

        return $this;
    }

    public function getIdType(): ?Type
    {
        return $this->id_type;
    }

    public function setIdType(?Type $id_type): self
    {
        $this->id_type = $id_type;

        return $this;
    }

    public function getIdEntreprise(): ?Entreprise
    {
        return $this->idEntreprise;
    }

    public function setIdEntreprise(?Entreprise $idEntreprise): self
    {
        $this->idEntreprise = $idEntreprise;

        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(State $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->dateAdd;
    }

    public function setDateAdd(?\DateTimeInterface $dateAdd): self
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    public function getNbReports(): ?int
    {
        return $this->nbReports;
    }

    public function setNbReports(?int $nbReports): self
    {
        $this->nbReports = $nbReports;

        return $this;
    }

    public function getNbThanks(): ?int
    {
        return $this->nbThanks;
    }

    public function setNbThanks(?int $nbThanks): self
    {
        $this->nbThanks = $nbThanks;

        return $this;
    }


}
