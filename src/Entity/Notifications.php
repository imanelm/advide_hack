<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notifications
 *
 * @ORM\Table(name="notifications", indexes={@ORM\Index(name="id_report", columns={"id_report"}), @ORM\Index(name="id_entreprise", columns={"id_entreprise"})})
 * @ORM\Entity
 */
class Notifications
{
	/**
     * @var int
     *
     * @ORM\Column(name="id_notif", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
	private $idNotif;

	/**
     * @var int
     *
     * @ORM\Column(name="notif_read", type="integer", nullable=false, options={"default"=0})
     */
    private $notifRead = 0;

    /**
     * @var \Entreprise
     *
     * @ORM\ManyToOne(targetEntity="Entreprise")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_entreprise", referencedColumnName="id_entreprise")
     * })
     */
    private $idEntreprise;

    /**
     * @var \OfferReport
     *
     * @ORM\ManyToOne(targetEntity="OfferReport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_report", referencedColumnName="id_report")
     * })
     */
    private $idReport;

    /**
     * @var \Hacker
     *
     * @ORM\ManyToOne(targetEntity="Hacker")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_hacker", referencedColumnName="id_hacker")
     * })
     */
    private $idHacker;

    /**
     * @var \Offer
     *
     * @ORM\ManyToOne(targetEntity="Offer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_offer", referencedColumnName="id_offer")
     * })
     */
    private $idOffer;

    /**
     * @var int
     *
     * @ORM\Column(name="type_notif", type="integer", nullable=false)
     */
    private $typeNotif;

    /**
     * @var \ReportPermit
     *
     * @ORM\OneToOne(targetEntity="ReportPermit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_permit", referencedColumnName="id_permit", nullable=true)
     * })
     */
    private $idPermit;


    public function getIdNotif(): ?int
    {
        return $this->idNotif;
    }

    public function getNotifRead(): ?int
    {
        return $this->notifRead;
    }

    public function setNotifRead(int $notifRead): self
    {
        $this->notifRead = $notifRead;

        return $this;
    }

    public function getIdEntreprise(): ?Entreprise
    {
        return $this->idEntreprise;
    }

    public function setIdEntreprise(?Entreprise $idEntreprise): self
    {
        $this->idEntreprise = $idEntreprise;

        return $this;
    }

    public function getIdReport(): ?OfferReport
    {
        return $this->idReport;
    }

    public function setIdReport(?OfferReport $idReport): self
    {
        $this->idReport = $idReport;

        return $this;
    }

    public function getIdHacker(): ?Hacker
    {
        return $this->idHacker;
    }

    public function setIdHacker(?Hacker $idHacker): self
    {
        $this->idHacker = $idHacker;

        return $this;
    }

    public function getIdOffer(): ?Offer
    {
        return $this->idOffer;
    }

    public function setIdOffer(?Offer $idOffer): self
    {
        $this->idOffer = $idOffer;

        return $this;
    }

    public function getTypeNotif(): ?int
    {
        return $this->typeNotif;
    }

    public function setTypeNotif(int $typeNotif): self
    {
        $this->typeNotif = $typeNotif;

        return $this;
    }

    public function getIdPermit(): ?ReportPermit
    {
        return $this->idPermit;
    }

    public function setIdPermit(?ReportPermit $idPermit): self
    {
        $this->idPermit = $idPermit;

        return $this;
    }
}