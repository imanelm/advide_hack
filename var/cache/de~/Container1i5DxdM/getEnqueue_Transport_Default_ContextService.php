<?php

namespace Container1i5DxdM;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getEnqueue_Transport_Default_ContextService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private 'enqueue.transport.default.context' shared service.
     *
     * @return \Interop\Queue\Context
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['enqueue.transport.default.context'] = ($container->privates['enqueue.transport.default.connection_factory'] ?? $container->getEnqueue_Transport_Default_ConnectionFactoryService())->createContext();
    }
}
