<?php

namespace Proxies\__CG__\App\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class OfferReport extends \App\Entity\OfferReport implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'idReport', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'report', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'cvssBase', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'cvssImpact', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'cvssTemp', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'cvssEnv', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'scope', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'endPoint', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'severity', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'payloadFi', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'payloadFifi', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'techEnv', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'appFingerprint', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'ipUsed', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'dateSub', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'idOffer', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'idHacker', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'state'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'idReport', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'report', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'cvssBase', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'cvssImpact', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'cvssTemp', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'cvssEnv', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'scope', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'endPoint', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'severity', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'payloadFi', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'payloadFifi', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'techEnv', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'appFingerprint', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'ipUsed', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'dateSub', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'idOffer', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'idHacker', '' . "\0" . 'App\\Entity\\OfferReport' . "\0" . 'state'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (OfferReport $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getIdReport(): ?int
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getIdReport();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIdReport', []);

        return parent::getIdReport();
    }

    /**
     * {@inheritDoc}
     */
    public function getReport(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getReport', []);

        return parent::getReport();
    }

    /**
     * {@inheritDoc}
     */
    public function setReport(string $report): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setReport', [$report]);

        return parent::setReport($report);
    }

    /**
     * {@inheritDoc}
     */
    public function getCvssBase(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCvssBase', []);

        return parent::getCvssBase();
    }

    /**
     * {@inheritDoc}
     */
    public function setCvssBase(float $cvssBase): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCvssBase', [$cvssBase]);

        return parent::setCvssBase($cvssBase);
    }

    /**
     * {@inheritDoc}
     */
    public function getCvssImpact(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCvssImpact', []);

        return parent::getCvssImpact();
    }

    /**
     * {@inheritDoc}
     */
    public function setCvssImpact(float $cvssImpact): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCvssImpact', [$cvssImpact]);

        return parent::setCvssImpact($cvssImpact);
    }

    /**
     * {@inheritDoc}
     */
    public function getCvssTemp(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCvssTemp', []);

        return parent::getCvssTemp();
    }

    /**
     * {@inheritDoc}
     */
    public function setCvssTemp(float $cvssTemp): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCvssTemp', [$cvssTemp]);

        return parent::setCvssTemp($cvssTemp);
    }

    /**
     * {@inheritDoc}
     */
    public function getCvssEnv(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCvssEnv', []);

        return parent::getCvssEnv();
    }

    /**
     * {@inheritDoc}
     */
    public function setCvssEnv(float $cvssEnv): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCvssEnv', [$cvssEnv]);

        return parent::setCvssEnv($cvssEnv);
    }

    /**
     * {@inheritDoc}
     */
    public function getScope(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getScope', []);

        return parent::getScope();
    }

    /**
     * {@inheritDoc}
     */
    public function setScope(string $scope): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setScope', [$scope]);

        return parent::setScope($scope);
    }

    /**
     * {@inheritDoc}
     */
    public function getEndPoint(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEndPoint', []);

        return parent::getEndPoint();
    }

    /**
     * {@inheritDoc}
     */
    public function setEndPoint(string $endPoint): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEndPoint', [$endPoint]);

        return parent::setEndPoint($endPoint);
    }

    /**
     * {@inheritDoc}
     */
    public function getSeverity(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSeverity', []);

        return parent::getSeverity();
    }

    /**
     * {@inheritDoc}
     */
    public function setSeverity(string $severity): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSeverity', [$severity]);

        return parent::setSeverity($severity);
    }

    /**
     * {@inheritDoc}
     */
    public function getPayloadFi(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPayloadFi', []);

        return parent::getPayloadFi();
    }

    /**
     * {@inheritDoc}
     */
    public function setPayloadFi(int $payloadFi): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPayloadFi', [$payloadFi]);

        return parent::setPayloadFi($payloadFi);
    }

    /**
     * {@inheritDoc}
     */
    public function getPayloadFifi(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPayloadFifi', []);

        return parent::getPayloadFifi();
    }

    /**
     * {@inheritDoc}
     */
    public function setPayloadFifi(int $payloadFifi): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPayloadFifi', [$payloadFifi]);

        return parent::setPayloadFifi($payloadFifi);
    }

    /**
     * {@inheritDoc}
     */
    public function getTechEnv(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTechEnv', []);

        return parent::getTechEnv();
    }

    /**
     * {@inheritDoc}
     */
    public function setTechEnv(string $techEnv): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTechEnv', [$techEnv]);

        return parent::setTechEnv($techEnv);
    }

    /**
     * {@inheritDoc}
     */
    public function getAppFingerprint(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAppFingerprint', []);

        return parent::getAppFingerprint();
    }

    /**
     * {@inheritDoc}
     */
    public function setAppFingerprint(string $appFingerprint): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAppFingerprint', [$appFingerprint]);

        return parent::setAppFingerprint($appFingerprint);
    }

    /**
     * {@inheritDoc}
     */
    public function getIpUsed(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIpUsed', []);

        return parent::getIpUsed();
    }

    /**
     * {@inheritDoc}
     */
    public function setIpUsed(float $ipUsed): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIpUsed', [$ipUsed]);

        return parent::setIpUsed($ipUsed);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateSub(): ?\DateTimeInterface
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateSub', []);

        return parent::getDateSub();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateSub(?\DateTimeInterface $dateSub): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateSub', [$dateSub]);

        return parent::setDateSub($dateSub);
    }

    /**
     * {@inheritDoc}
     */
    public function getIdOffer(): ?\App\Entity\Offer
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIdOffer', []);

        return parent::getIdOffer();
    }

    /**
     * {@inheritDoc}
     */
    public function setIdOffer(?\App\Entity\Offer $idOffer): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIdOffer', [$idOffer]);

        return parent::setIdOffer($idOffer);
    }

    /**
     * {@inheritDoc}
     */
    public function getIdHacker(): ?\App\Entity\Hacker
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIdHacker', []);

        return parent::getIdHacker();
    }

    /**
     * {@inheritDoc}
     */
    public function setIdHacker(?\App\Entity\Hacker $idHacker): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIdHacker', [$idHacker]);

        return parent::setIdHacker($idHacker);
    }

    /**
     * {@inheritDoc}
     */
    public function getState(): ?\App\Entity\State
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getState', []);

        return parent::getState();
    }

    /**
     * {@inheritDoc}
     */
    public function setState(\App\Entity\State $state): \App\Entity\OfferReport
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setState', [$state]);

        return parent::setState($state);
    }

}
