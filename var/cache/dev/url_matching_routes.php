<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/blog' => [[['_route' => 'blog', '_controller' => 'App\\Controller\\BlogPostController::index'], null, null, null, false, false, null]],
        '/new_post' => [[['_route' => 'new_post', '_controller' => 'App\\Controller\\BlogPostController::new_post'], null, null, null, false, false, null]],
        '/entreprises' => [[['_route' => 'entreprises', '_controller' => 'App\\Controller\\EntrepriseController::index'], null, null, null, false, false, null]],
        '/hackers' => [[['_route' => 'hackers', '_controller' => 'App\\Controller\\HackerController::index'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'home_page', '_controller' => 'App\\Controller\\HomePageController::index'], null, null, null, false, false, null]],
        '/messages' => [[['_route' => 'messages', '_controller' => 'App\\Controller\\MessageController::index'], null, null, null, false, false, null]],
        '/update_message' => [[['_route' => 'update_message', '_controller' => 'App\\Controller\\MessageController::updateMessages'], null, null, null, false, false, null]],
        '/get_message' => [[['_route' => 'get_message', '_controller' => 'App\\Controller\\MessageController::getMessages'], null, null, null, false, false, null]],
        '/response' => [[['_route' => 'response', '_controller' => 'App\\Controller\\MessageController::response'], null, null, null, false, false, null]],
        '/notifications_admin' => [[['_route' => 'notifications_admin', '_controller' => 'App\\Controller\\NotificationsController::index_admin'], null, null, null, false, false, null]],
        '/offer' => [[['_route' => 'offers', '_controller' => 'App\\Controller\\OfferController::index'], null, null, null, false, false, null]],
        '/newoffer' => [[['_route' => 'offer_creation', '_controller' => 'App\\Controller\\OfferController::new'], null, null, null, false, false, null]],
        '/profile' => [[['_route' => 'profile', '_controller' => 'App\\Controller\\ProfileController::index'], null, null, null, false, false, null]],
        '/profile/EditProfil' => [[['_route' => 'EditProfil', '_controller' => 'App\\Controller\\ProfileController::EditProfil'], null, null, null, false, false, null]],
        '/profile/EditProfilEntreprise' => [[['_route' => 'EditProfilEntreprise', '_controller' => 'App\\Controller\\ProfileController::EditProfilEntreprise'], null, null, null, false, false, null]],
        '/profile/EditPass' => [[['_route' => 'EditPass', '_controller' => 'App\\Controller\\ProfileController::EditPass'], null, null, null, false, false, null]],
        '/profile/test' => [[['_route' => 'test', '_controller' => 'App\\Controller\\ProfileController::test'], null, null, null, false, false, null]],
        '/profile/download' => [[['_route' => 'download_test', '_controller' => 'App\\Controller\\ProfileController::downloadFileAction'], null, null, null, false, false, null]],
        '/profile/instructions' => [[['_route' => 'instructions', '_controller' => 'App\\Controller\\ProfileController::downloadInstructions'], null, null, null, false, false, null]],
        '/registerHacker' => [[['_route' => 'registerHacker', '_controller' => 'App\\Controller\\RegistrationController::register'], null, null, null, false, false, null]],
        '/registerEnt' => [[['_route' => 'registerEnt', '_controller' => 'App\\Controller\\RegistrationController::registerEntreprise'], null, null, null, false, false, null]],
        '/SignIn' => [[['_route' => 'sign_in', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/sign_up' => [[['_route' => 'sign_up', '_controller' => 'App\\Controller\\SignUpController::index'], null, null, null, false, false, null]],
        '/sign_up_hacker' => [[['_route' => 'sign_up_hacker', '_controller' => 'App\\Controller\\SignUpHackerController::index'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/blog_post/([0-9]+)(*:188)'
                .'|/e(?'
                    .'|dit_post/([0-9]+)(*:218)'
                    .'|ntreprises(?'
                        .'|/([0-9]+)(*:248)'
                        .'|_(?'
                            .'|del/([0-9]+)(*:272)'
                            .'|offres/([0-9]+)(*:295)'
                            .'|reports/([0-9]+)(*:319)'
                        .')'
                    .')'
                .')'
                .'|/delete_(?'
                    .'|post/([0-9]+)(*:354)'
                    .'|comment/([0-9]+)(*:378)'
                .')'
                .'|/n(?'
                    .'|ew_reply/([0-9]+)(*:409)'
                    .'|otifications_(?'
                        .'|ent/([0-9]+)(*:445)'
                        .'|ha/([0-9]+)(*:464)'
                        .'|read_(?'
                            .'|ent/([0-9]+)(*:492)'
                            .'|ha/([0-9]+)(*:511)'
                            .'|admin/([0-9]+)(*:533)'
                        .')'
                        .'|permit/([0-9]+)(*:557)'
                        .'|del_(?'
                            .'|ent/([0-9]+)(*:584)'
                            .'|ha/([0-9]+)(*:603)'
                            .'|admin/([0-9]+)(*:625)'
                        .')'
                    .')'
                .')'
                .'|/hacker(?'
                    .'|s(?'
                        .'|/([0-9]+)(*:659)'
                        .'|_(?'
                            .'|del/([0-9]+)(*:683)'
                            .'|in/([0-9]+)(*:702)'
                        .')'
                    .')'
                    .'|_(?'
                        .'|out/([0-9]+)(*:728)'
                        .'|reports/([0-9]+)(*:752)'
                    .')'
                .')'
                .'|/offer(?'
                    .'|/([0-9]+)(*:780)'
                    .'|_(?'
                        .'|del/([0-9]+)(*:804)'
                        .'|in/([0-9]+)(*:823)'
                        .'|out/([0-9]+)(*:843)'
                    .')'
                .')'
                .'|/report(?'
                    .'|/([0-9]+)(*:872)'
                    .'|_(?'
                        .'|a(?'
                            .'|sk/([0-9]+)(*:899)'
                            .'|ccept/([0-9]+)(*:921)'
                        .')'
                        .'|edit/([0-9]+)(*:943)'
                        .'|reject/([0-9]+)(*:966)'
                    .')'
                .')'
                .'|/sub_report/([0-9]+)(*:996)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        188 => [[['_route' => 'blog_post', '_controller' => 'App\\Controller\\BlogPostController::show_post'], ['id'], null, null, false, true, null]],
        218 => [[['_route' => 'edit_post', '_controller' => 'App\\Controller\\BlogPostController::edit_post'], ['id'], null, null, false, true, null]],
        248 => [[['_route' => 'entreprise_show', '_controller' => 'App\\Controller\\EntrepriseController::show'], ['id'], null, null, false, true, null]],
        272 => [[['_route' => 'entreprise_delete', '_controller' => 'App\\Controller\\EntrepriseController::delete'], ['id'], null, null, false, true, null]],
        295 => [[['_route' => 'ent_offres', '_controller' => 'App\\Controller\\EntrepriseController::show_offers'], ['id'], null, null, false, true, null]],
        319 => [[['_route' => 'ent_reports', '_controller' => 'App\\Controller\\EntrepriseController::show_reports'], ['id'], null, null, false, true, null]],
        354 => [[['_route' => 'delete_post', '_controller' => 'App\\Controller\\BlogPostController::delete_post'], ['id'], null, null, false, true, null]],
        378 => [[['_route' => 'delete_comment', '_controller' => 'App\\Controller\\BlogPostController::delete_comment'], ['id'], null, null, false, true, null]],
        409 => [[['_route' => 'new_reply', '_controller' => 'App\\Controller\\BlogPostController::new_reply'], ['id'], null, null, false, true, null]],
        445 => [[['_route' => 'my_notifications_ent', '_controller' => 'App\\Controller\\NotificationsController::index_ent'], ['id'], null, null, false, true, null]],
        464 => [[['_route' => 'my_notifications_ha', '_controller' => 'App\\Controller\\NotificationsController::index_ha'], ['id'], null, null, false, true, null]],
        492 => [[['_route' => 'mark_as_read_ent', '_controller' => 'App\\Controller\\NotificationsController::read_ent'], ['id'], null, null, false, true, null]],
        511 => [[['_route' => 'mark_as_read_ha', '_controller' => 'App\\Controller\\NotificationsController::read_ha'], ['id'], null, null, false, true, null]],
        533 => [[['_route' => 'mark_as_read_admin', '_controller' => 'App\\Controller\\NotificationsController::read_admin'], ['id'], null, null, false, true, null]],
        557 => [[['_route' => 'report_permit', '_controller' => 'App\\Controller\\NotificationsController::permit'], ['id'], null, null, false, true, null]],
        584 => [[['_route' => 'del_notif_ent', '_controller' => 'App\\Controller\\NotificationsController::del_notif_ent'], ['id'], null, null, false, true, null]],
        603 => [[['_route' => 'del_notif_ha', '_controller' => 'App\\Controller\\NotificationsController::del_notif_ha'], ['id'], null, null, false, true, null]],
        625 => [[['_route' => 'del_notif_admin', '_controller' => 'App\\Controller\\NotificationsController::del_notif_admin'], ['id'], null, null, false, true, null]],
        659 => [[['_route' => 'hacker_show', '_controller' => 'App\\Controller\\HackerController::show'], ['id'], null, null, false, true, null]],
        683 => [[['_route' => 'hacker_delete', '_controller' => 'App\\Controller\\HackerController::delete'], ['id'], null, null, false, true, null]],
        702 => [[['_route' => 'hacker_in', '_controller' => 'App\\Controller\\HackerController::hacker_in'], ['id'], null, null, false, true, null]],
        728 => [[['_route' => 'hacker_out', '_controller' => 'App\\Controller\\HackerController::hacker_out'], ['id'], null, null, false, true, null]],
        752 => [[['_route' => 'hack_reports', '_controller' => 'App\\Controller\\HackerController::show_reports'], ['id'], null, null, false, true, null]],
        780 => [[['_route' => 'offer_show', '_controller' => 'App\\Controller\\OfferController::show'], ['id'], null, null, false, true, null]],
        804 => [[['_route' => 'offer_delete', '_controller' => 'App\\Controller\\OfferController::delete'], ['id'], null, null, false, true, null]],
        823 => [[['_route' => 'offer_in', '_controller' => 'App\\Controller\\OfferController::offer_in'], ['id'], null, null, false, true, null]],
        843 => [[['_route' => 'offer_out', '_controller' => 'App\\Controller\\OfferController::offer_out'], ['id'], null, null, false, true, null]],
        872 => [[['_route' => 'report_show', '_controller' => 'App\\Controller\\OfferReportController::ShowReport'], ['id'], null, null, false, true, null]],
        899 => [[['_route' => 'report_ask', '_controller' => 'App\\Controller\\OfferReportController::AskReport'], ['id'], null, null, false, true, null]],
        921 => [[['_route' => 'report_accept', '_controller' => 'App\\Controller\\OfferReportController::AcceptReport'], ['id'], null, null, false, true, null]],
        943 => [[['_route' => 'report_edit', '_controller' => 'App\\Controller\\OfferReportController::EditReport'], ['id'], null, null, false, true, null]],
        966 => [[['_route' => 'report_reject', '_controller' => 'App\\Controller\\OfferReportController::RejectReport'], ['id'], null, null, false, true, null]],
        996 => [
            [['_route' => 'sub_report', '_controller' => 'App\\Controller\\OfferReportController::subReport'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
