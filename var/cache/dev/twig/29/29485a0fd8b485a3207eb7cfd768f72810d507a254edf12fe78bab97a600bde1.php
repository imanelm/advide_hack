<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* registration/registerEnt.html.twig */
class __TwigTemplate_91e76cc301bdf0c3ea9dc8cb73eb2f065bdd4740d060056c2b5df2a09d17d3e7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "registration/registerEnt.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "registration/registerEnt.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "registration/registerEnt.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 4, $this->source); })()), "flashes", [0 => "verify_email_error"], "method", false, false, false, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["flashError"]) {
            // line 5
            echo "        <div class=\"alert alert-danger\" role=\"alert\">";
            echo twig_escape_filter($this->env, $context["flashError"], "html", null, true);
            echo "</div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashError'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "
    <h1>Register</h1>

    <div class=\"card bg-transparent border-0 align-item-center\" style = \"margin-top: 100px;\">

        <div class=\"card bg-transparent mx-auto border-0\" style=\"width:50%; margin-bottom:40px\">
            <div class=\"col card-text d-flex justify-content-center\" style=\"margin-bottom:20px;\">
                <div>
                    <a class=\"p-5\" href=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("registerHacker");
        echo "\">
                        <button type=\"button\" style=\"color:#9D151C; border-color:#9D151C; border-width:1px\" class=\"btn btn-lg shadow-sm\">New Hacker</button>      
                    </a>
                    <a class= \"ml-auto p-5\" href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("registerEnt");
        echo "\" style=\"color:#9D151C\">
                        <button type=\"button\" style=\"background-color:#9D151C;\" class=\"btn border-0 btn-lg active text-white shadow-sm\">New Enterprise</button>
                    </a>
                </div>
            </div>
        </div>

        <div class=\"row d-flex justify-content-center\">

            <div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:80%; padding-top:30px; padding-bottom:30px; padding-right:30px; padding-left:30px;\">

                <div class=\"row d-flex justify-content-center\">

                    ";
        // line 31
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 31, $this->source); })()), 'form_start');
        echo "
                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">
                                ";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 34, $this->source); })()), "email", [], "any", false, false, false, 34), 'label', ["label" => "E-mail"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 37, $this->source); })()), "email", [], "any", false, false, false, 37), 'widget');
        echo "
                            </div>
                            
                        </div>

                         <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">    
                                ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 44, $this->source); })()), "nameEnt", [], "any", false, false, false, 44), 'label', ["label" => "Enterprise Name"]);
        echo "
                            </div>
                            <div  class=\"col\" style=\"float:left;\">
                                ";
        // line 47
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 47, $this->source); })()), "nameEnt", [], "any", false, false, false, 47), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">
                                ";
        // line 53
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 53, $this->source); })()), "plainPassword", [], "any", false, false, false, 53), 'label', ["label" => "Password"]);
        echo "
                            </div>
                            <div  class=\"col\" style=\"float:left;\">
                                ";
        // line 56
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 56, $this->source); })()), "plainPassword", [], "any", false, false, false, 56), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">
                                ";
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 62, $this->source); })()), "num", [], "any", false, false, false, 62), 'label', ["label" => "Phone Number"]);
        echo "
                            </div>
                            <div  class=\"col\" style=\"float:left;\">    
                                ";
        // line 65
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 65, $this->source); })()), "num", [], "any", false, false, false, 65), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">                            
                                ";
        // line 71
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 71, $this->source); })()), "name", [], "any", false, false, false, 71), 'label', ["label" => "Last Name"]);
        echo "
                            </div>
                            <div  class=\"col\" style=\"float:left;\"> 
                                ";
        // line 74
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 74, $this->source); })()), "name", [], "any", false, false, false, 74), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">                     
                                ";
        // line 80
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 80, $this->source); })()), "f_name", [], "any", false, false, false, 80), 'label', ["label" => "First Name"]);
        echo "
                            </div>
                            <div  class=\"col\" style=\"float:left;\"> 
                                ";
        // line 83
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 83, $this->source); })()), "f_name", [], "any", false, false, false, 83), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">  
                                ";
        // line 89
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 89, $this->source); })()), "country", [], "any", false, false, false, 89), 'label', ["label" => "Country"]);
        echo "
                            </div>
                            <div  class=\"col\" style=\"float:left;\"> 
                                ";
        // line 92
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 92, $this->source); })()), "country", [], "any", false, false, false, 92), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">  
                                ";
        // line 98
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 98, $this->source); })()), "photo", [], "any", false, false, false, 98), 'label', ["label" => "Profile photo"]);
        echo "
                            </div>
                            <div  class=\"col\" style=\"float:left;\"> 
                                ";
        // line 101
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 101, $this->source); })()), "photo", [], "any", false, false, false, 101), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"row\" style=\"margin-bottom:5px;\">
                            <div class=\"col text-dark\">
                            ";
        // line 107
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 107, $this->source); })()), "agreeTerms", [], "any", false, false, false, 107), 'label', ["label" => "I have read, understood and accepted the terms of services"]);
        echo "
                            ";
        // line 108
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 108, $this->source); })()), "agreeTerms", [], "any", false, false, false, 108), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"row\" style=\"margin-bottom:5px;\">
                            <div class=\"col text-dark\">
                            ";
        // line 114
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 114, $this->source); })()), "showauth", [], "any", false, false, false, 114), 'label', ["label" => "I give the authorization to be displayed"]);
        echo "
                            ";
        // line 115
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 115, $this->source); })()), "showauth", [], "any", false, false, false, 115), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"text-center\">
                            <button type=\"submit\" class=\"btn btn-lg shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C;\">Register</button>
                        </div>
                    ";
        // line 122
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["registrationForm"]) || array_key_exists("registrationForm", $context) ? $context["registrationForm"] : (function () { throw new RuntimeError('Variable "registrationForm" does not exist.', 122, $this->source); })()), 'form_end');
        echo "
                </div>

            </div>

        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "registration/registerEnt.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 122,  258 => 115,  254 => 114,  245 => 108,  241 => 107,  232 => 101,  226 => 98,  217 => 92,  211 => 89,  202 => 83,  196 => 80,  187 => 74,  181 => 71,  172 => 65,  166 => 62,  157 => 56,  151 => 53,  142 => 47,  136 => 44,  126 => 37,  120 => 34,  114 => 31,  98 => 18,  92 => 15,  82 => 7,  73 => 5,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}

{% block body %}
    {% for flashError in app.flashes('verify_email_error') %}
        <div class=\"alert alert-danger\" role=\"alert\">{{ flashError }}</div>
    {% endfor %}

    <h1>Register</h1>

    <div class=\"card bg-transparent border-0 align-item-center\" style = \"margin-top: 100px;\">

        <div class=\"card bg-transparent mx-auto border-0\" style=\"width:50%; margin-bottom:40px\">
            <div class=\"col card-text d-flex justify-content-center\" style=\"margin-bottom:20px;\">
                <div>
                    <a class=\"p-5\" href=\"{{ url('registerHacker') }}\">
                        <button type=\"button\" style=\"color:#9D151C; border-color:#9D151C; border-width:1px\" class=\"btn btn-lg shadow-sm\">New Hacker</button>      
                    </a>
                    <a class= \"ml-auto p-5\" href=\"{{ url('registerEnt') }}\" style=\"color:#9D151C\">
                        <button type=\"button\" style=\"background-color:#9D151C;\" class=\"btn border-0 btn-lg active text-white shadow-sm\">New Enterprise</button>
                    </a>
                </div>
            </div>
        </div>

        <div class=\"row d-flex justify-content-center\">

            <div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:80%; padding-top:30px; padding-bottom:30px; padding-right:30px; padding-left:30px;\">

                <div class=\"row d-flex justify-content-center\">

                    {{ form_start(registrationForm) }}
                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">
                                {{ form_label(registrationForm.email, 'E-mail') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(registrationForm.email) }}
                            </div>
                            
                        </div>

                         <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">    
                                {{ form_label(registrationForm.nameEnt, 'Enterprise Name') }}
                            </div>
                            <div  class=\"col\" style=\"float:left;\">
                                {{ form_widget(registrationForm.nameEnt) }}
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">
                                {{ form_label(registrationForm.plainPassword, 'Password') }}
                            </div>
                            <div  class=\"col\" style=\"float:left;\">
                                {{ form_widget(registrationForm.plainPassword) }}
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">
                                {{ form_label(registrationForm.num, 'Phone Number') }}
                            </div>
                            <div  class=\"col\" style=\"float:left;\">    
                                {{ form_widget(registrationForm.num) }}
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">                            
                                {{ form_label(registrationForm.name, 'Last Name') }}
                            </div>
                            <div  class=\"col\" style=\"float:left;\"> 
                                {{ form_widget(registrationForm.name) }}
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">                     
                                {{ form_label(registrationForm.f_name, 'First Name') }}
                            </div>
                            <div  class=\"col\" style=\"float:left;\"> 
                                {{ form_widget(registrationForm.f_name) }}
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">  
                                {{ form_label(registrationForm.country, 'Country') }}
                            </div>
                            <div  class=\"col\" style=\"float:left;\"> 
                                {{ form_widget(registrationForm.country) }}
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">  
                                {{ form_label(registrationForm.photo, 'Profile photo') }}
                            </div>
                            <div  class=\"col\" style=\"float:left;\"> 
                                {{ form_widget(registrationForm.photo) }}
                            </div>
                        </div>

                        <div class=\"row\" style=\"margin-bottom:5px;\">
                            <div class=\"col text-dark\">
                            {{ form_label(registrationForm.agreeTerms, 'I have read, understood and accepted the terms of services') }}
                            {{ form_widget(registrationForm.agreeTerms) }}
                            </div>
                        </div>

                        <div class=\"row\" style=\"margin-bottom:5px;\">
                            <div class=\"col text-dark\">
                            {{ form_label(registrationForm.showauth, 'I give the authorization to be displayed') }}
                            {{ form_widget(registrationForm.showauth) }}
                            </div>
                        </div>

                        <div class=\"text-center\">
                            <button type=\"submit\" class=\"btn btn-lg shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C;\">Register</button>
                        </div>
                    {{ form_end(registrationForm) }}
                </div>

            </div>

        </div>
    </div>

{% endblock %}
", "registration/registerEnt.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\registration\\registerEnt.html.twig");
    }
}
