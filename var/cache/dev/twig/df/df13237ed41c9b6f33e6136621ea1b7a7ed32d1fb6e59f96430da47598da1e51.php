<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* entreprise/my_notifications.html.twig */
class __TwigTemplate_0f616f7621c65e7db4606605a597cec59d42cc749a21069f646c007d45fb32f1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "entreprise/my_notifications.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "entreprise/my_notifications.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "entreprise/my_notifications.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "\t
\t\t<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t\t    ";
        // line 8
        if ((0 !== twig_compare(twig_length_filter($this->env, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 8, $this->source); })())), 0))) {
            // line 9
            echo "\t\t\t\t    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 9, $this->source); })())) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 10
                echo "\t\t\t\t    \t";
                if ((((-1 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 4)) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 9))) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 10)))) {
                    // line 11
                    echo "\t\t\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t\t\t        \t\t\t<div class=\"card card-overlay bg-transparent border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t\t    \t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t      \t\t";
                    // line 18
                    if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 18, $this->source); })()), $context["i"], [], "array", false, false, false, 18), "typeNotif", [], "any", false, false, false, 18), 1))) {
                        // line 19
                        echo "\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("hacker_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 19, $this->source); })()), $context["i"], [], "array", false, false, false, 19), "idReport", [], "any", false, false, false, 19), "idHacker", [], "any", false, false, false, 19), "idHacker", [], "any", false, false, false, 19)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 20
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 20, $this->source); })()), $context["i"], [], "array", false, false, false, 20), "idReport", [], "any", false, false, false, 20), "idHacker", [], "any", false, false, false, 20), "username", [], "any", false, false, false, 20), "html", null, true);
                        echo " 
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t\t has submitted a report for 
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 23
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 23, $this->source); })()), $context["i"], [], "array", false, false, false, 23), "idReport", [], "any", false, false, false, 23), "idOffer", [], "any", false, false, false, 23), "idOffer", [], "any", false, false, false, 23)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 24
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 24, $this->source); })()), $context["i"], [], "array", false, false, false, 24), "idReport", [], "any", false, false, false, 24), "idOffer", [], "any", false, false, false, 24), "title", [], "any", false, false, false, 24), "html", null, true);
                        echo ".
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 26
(isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 26, $this->source); })()), $context["i"], [], "array", false, false, false, 26), "typeNotif", [], "any", false, false, false, 26), 2))) {
                        // line 27
                        echo "\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("hacker_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 27, $this->source); })()), $context["i"], [], "array", false, false, false, 27), "idReport", [], "any", false, false, false, 27), "idHacker", [], "any", false, false, false, 27), "idHacker", [], "any", false, false, false, 27)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 28
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 28, $this->source); })()), $context["i"], [], "array", false, false, false, 28), "idReport", [], "any", false, false, false, 28), "idHacker", [], "any", false, false, false, 28), "username", [], "any", false, false, false, 28), "html", null, true);
                        echo " 
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t\t has updated the report for 
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 31
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 31, $this->source); })()), $context["i"], [], "array", false, false, false, 31), "idReport", [], "any", false, false, false, 31), "idOffer", [], "any", false, false, false, 31), "idOffer", [], "any", false, false, false, 31)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 32
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 32, $this->source); })()), $context["i"], [], "array", false, false, false, 32), "idReport", [], "any", false, false, false, 32), "idOffer", [], "any", false, false, false, 32), "title", [], "any", false, false, false, 32), "html", null, true);
                        echo ". 
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 34
(isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 34, $this->source); })()), $context["i"], [], "array", false, false, false, 34), "typeNotif", [], "any", false, false, false, 34), 3))) {
                        // line 35
                        echo "\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("hacker_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 35, $this->source); })()), $context["i"], [], "array", false, false, false, 35), "idHacker", [], "any", false, false, false, 35), "idHacker", [], "any", false, false, false, 35)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 36
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 36, $this->source); })()), $context["i"], [], "array", false, false, false, 36), "idHacker", [], "any", false, false, false, 36), "username", [], "any", false, false, false, 36), "html", null, true);
                        echo " 
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t\t wants to submit a report for
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 39
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 39, $this->source); })()), $context["i"], [], "array", false, false, false, 39), "idOffer", [], "any", false, false, false, 39), "idOffer", [], "any", false, false, false, 39)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 40
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 40, $this->source); })()), $context["i"], [], "array", false, false, false, 40), "idOffer", [], "any", false, false, false, 40), "title", [], "any", false, false, false, 40), "html", null, true);
                        echo ".
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 42
(isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 42, $this->source); })()), $context["i"], [], "array", false, false, false, 42), "typeNotif", [], "any", false, false, false, 42), 9))) {
                        // line 43
                        echo "\t\t\t\t\t\t\t\t\t\t      \t\t\tCongratulations ! Your
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 44
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 44, $this->source); })()), $context["i"], [], "array", false, false, false, 44), "idOffer", [], "any", false, false, false, 44), "idOffer", [], "any", false, false, false, 44)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\toffer
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t\thas been accepted ! 
\t\t\t\t\t\t\t\t\t      \t\t\t";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 48
(isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 48, $this->source); })()), $context["i"], [], "array", false, false, false, 48), "typeNotif", [], "any", false, false, false, 48), 10))) {
                        // line 49
                        echo "\t\t\t\t\t\t\t\t\t\t      \t\t\tWe are sorry, we regret to inform you that your 
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 50
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 50, $this->source); })()), $context["i"], [], "array", false, false, false, 50), "idOffer", [], "any", false, false, false, 50), "idOffer", [], "any", false, false, false, 50)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\toffer
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t\twill not be published. 
\t\t\t\t\t\t\t\t\t\t      \t\t";
                    }
                    // line 55
                    echo "\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    ";
                    // line 58
                    if (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 58, $this->source); })()), $context["i"], [], "array", false, false, false, 58), "typeNotif", [], "any", false, false, false, 58), 1)) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 58, $this->source); })()), $context["i"], [], "array", false, false, false, 58), "typeNotif", [], "any", false, false, false, 58), 2)))) {
                        // line 59
                        echo "\t\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 62
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("report_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 62, $this->source); })()), $context["i"], [], "array", false, false, false, 62), "idReport", [], "any", false, false, false, 62), "idReport", [], "any", false, false, false, 62)]), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">View report</div>
\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    ";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 70
(isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 70, $this->source); })()), $context["i"], [], "array", false, false, false, 70), "typeNotif", [], "any", false, false, false, 70), 3))) {
                        // line 71
                        echo "\t\t\t\t\t\t\t\t\t\t    \t";
                        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 71, $this->source); })()), $context["i"], [], "array", false, false, false, 71), "idPermit", [], "any", false, false, false, 71), "state", [], "any", false, false, false, 71), 0))) {
                            // line 72
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                            // line 75
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("report_permit", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 75, $this->source); })()), $context["i"], [], "array", false, false, false, 75), "idNotif", [], "any", false, false, false, 75)]), "html", null, true);
                            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Accept the request</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t\t\t\t    </div>\t
\t\t\t\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                            // line 86
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("report_reject", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 86, $this->source); })()), $context["i"], [], "array", false, false, false, 86), "idNotif", [], "any", false, false, false, 86)]), "html", null, true);
                            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Reject the request</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t\t\t\t    </div>\t
\t\t\t\t\t\t\t\t\t\t\t    ";
                        }
                        // line 95
                        echo "\t\t\t\t\t\t\t\t\t    ";
                    }
                    // line 96
                    echo "\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t      \t\t";
                    // line 99
                    if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 99, $this->source); })()), $context["i"], [], "array", false, false, false, 99), "notifRead", [], "any", false, false, false, 99), 0))) {
                        // line 100
                        echo "\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("mark_as_read_ent", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 100, $this->source); })()), $context["i"], [], "array", false, false, false, 100), "idNotif", [], "any", false, false, false, 100)]), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Mark as read</div>
\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t\t";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 105
(isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 105, $this->source); })()), $context["i"], [], "array", false, false, false, 105), "notifRead", [], "any", false, false, false, 105), 1))) {
                        // line 106
                        echo "\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("mark_as_read_ent", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 106, $this->source); })()), $context["i"], [], "array", false, false, false, 106), "idNotif", [], "any", false, false, false, 106)]), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Mark as unread</div>
\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t\t";
                    }
                    // line 112
                    echo "\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>\t
\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                    // line 118
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("del_notif_ent", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_notifications"]) || array_key_exists("ent_notifications", $context) ? $context["ent_notifications"] : (function () { throw new RuntimeError('Variable "ent_notifications" does not exist.', 118, $this->source); })()), $context["i"], [], "array", false, false, false, 118), "idNotif", [], "any", false, false, false, 118)]), "html", null, true);
                    echo "\">
\t\t\t\t\t\t\t\t\t\t              \t<svg width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-x-circle\" style=\"margin-right:20px;\" fill=\"#9D151C\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>\t

\t\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t   </div>
\t\t\t\t\t   ";
                }
                // line 134
                echo "\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " 
\t\t\t";
        }
        // line 136
        echo "\t\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "entreprise/my_notifications.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  309 => 136,  300 => 134,  281 => 118,  273 => 112,  263 => 106,  261 => 105,  252 => 100,  250 => 99,  245 => 96,  242 => 95,  230 => 86,  216 => 75,  211 => 72,  208 => 71,  206 => 70,  195 => 62,  190 => 59,  188 => 58,  183 => 55,  175 => 50,  172 => 49,  170 => 48,  163 => 44,  160 => 43,  158 => 42,  153 => 40,  149 => 39,  143 => 36,  138 => 35,  136 => 34,  131 => 32,  127 => 31,  121 => 28,  116 => 27,  114 => 26,  109 => 24,  105 => 23,  99 => 20,  94 => 19,  92 => 18,  83 => 11,  80 => 10,  75 => 9,  73 => 8,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}
\t
\t\t<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t\t    {% if ent_notifications|length != 0 %}
\t\t\t\t    {% for i in 0..ent_notifications|length-1 %}
\t\t\t\t    \t{% if ent_notifications[i].typeNotif < 4 or ent_notifications[i].typeNotif == 9 or ent_notifications[i].typeNotif == 10 %}
\t\t\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t\t\t        \t\t\t<div class=\"card card-overlay bg-transparent border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t\t    \t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t      \t\t{% if ent_notifications[i].typeNotif == 1 %}
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('hacker_show', {'id' : ent_notifications[i].idReport.idHacker.idHacker}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ent_notifications[i].idReport.idHacker.username }} 
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t\t has submitted a report for 
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('offer_show', {'id' : ent_notifications[i].idReport.idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ent_notifications[i].idReport.idOffer.title }}.
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t{% elseif ent_notifications[i].typeNotif  == 2 %}
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('hacker_show', {'id' : ent_notifications[i].idReport.idHacker.idHacker}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ent_notifications[i].idReport.idHacker.username }} 
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t\t has updated the report for 
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('offer_show', {'id' : ent_notifications[i].idReport.idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ent_notifications[i].idReport.idOffer.title}}. 
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t{% elseif ent_notifications[i].typeNotif  == 3 %}
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('hacker_show', {'id' : ent_notifications[i].idHacker.idHacker}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ent_notifications[i].idHacker.username }} 
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t\t wants to submit a report for
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('offer_show', {'id' : ent_notifications[i].idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ent_notifications[i].idOffer.title }}.
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t{% elseif ent_notifications[i].typeNotif  == 9 %}
\t\t\t\t\t\t\t\t\t\t      \t\t\tCongratulations ! Your
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('offer_show', {'id' : ent_notifications[i].idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\toffer
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t\thas been accepted ! 
\t\t\t\t\t\t\t\t\t      \t\t\t{% elseif ent_notifications[i].typeNotif  == 10 %}
\t\t\t\t\t\t\t\t\t\t      \t\t\tWe are sorry, we regret to inform you that your 
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('offer_show', {'id' : ent_notifications[i].idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\toffer
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t\twill not be published. 
\t\t\t\t\t\t\t\t\t\t      \t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    {% if ent_notifications[i].typeNotif  == 1 or ent_notifications[i].typeNotif  == 2 %}
\t\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{url('report_show', {'id' : ent_notifications[i].idReport.idReport}) }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">View report</div>
\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    {% elseif ent_notifications[i].typeNotif  == 3 %}
\t\t\t\t\t\t\t\t\t\t    \t{% if ent_notifications[i].idPermit.state == 0 %}
\t\t\t\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{url('report_permit', {'id' : ent_notifications[i].idNotif}) }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Accept the request</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t\t\t\t    </div>\t
\t\t\t\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{url('report_reject', {'id' : ent_notifications[i].idNotif}) }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Reject the request</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t\t\t\t    </div>\t
\t\t\t\t\t\t\t\t\t\t\t    {% endif %}
\t\t\t\t\t\t\t\t\t    {% endif %}
\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t      \t\t{% if ent_notifications[i].notifRead == 0 %}
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{url('mark_as_read_ent', {'id' : ent_notifications[i].idNotif}) }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Mark as read</div>
\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t\t{% elseif ent_notifications[i].notifRead == 1 %}
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{url('mark_as_read_ent', {'id' : ent_notifications[i].idNotif}) }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Mark as unread</div>
\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>\t
\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{url('del_notif_ent', {'id' : ent_notifications[i].idNotif}) }}\">
\t\t\t\t\t\t\t\t\t\t              \t<svg width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-x-circle\" style=\"margin-right:20px;\" fill=\"#9D151C\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>\t

\t\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t   </div>
\t\t\t\t\t   {% endif %}
\t\t\t\t\t{% endfor %} 
\t\t\t{% endif %}
\t\t</div>
{% endblock %}", "entreprise/my_notifications.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\entreprise\\my_notifications.html.twig");
    }
}
