<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* offer/show_report.html.twig */
class __TwigTemplate_bb0f2ae6e1c6d946af7565149008c348bc74089d40051aa38becfa5ec73b3b8c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/show_report.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/show_report.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "offer/show_report.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo " 
<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t<div class=\"card border-0 bg-transparent\" style=\"margin-top:90px;\">
\t    <div class=\"tab-content py-4 text-center\">
\t    \t<div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(217, 217, 217, 0.5); width:100%; margin-bottom: 20px;\">
\t            <div class=\"card-body\">
\t            \t<div class=\"row justify-content-center d-flex align-items-center\">
\t\t\t\t    \t<div class=\"col-2\">
\t\t\t\t\t     \t<h4 class=\"text-title text-white text-center\">";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 13, $this->source); })()), "info_rep", [], "array", false, false, false, 13), 0, [], "array", false, false, false, 13), "title", [], "array", false, false, false, 13), "html", null, true);
        echo "</h4>
\t\t\t\t\t    </div>
\t\t\t\t\t    <div class=\"col-2\">
\t\t\t\t\t      \t<div class=\"text-title text-white text-center\">";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 16, $this->source); })()), "info_rep", [], "array", false, false, false, 16), 0, [], "array", false, false, false, 16), "description1", [], "array", false, false, false, 16), "html", null, true);
        echo "</div>
\t\t\t\t\t    </div>
\t\t\t\t\t    ";
        // line 18
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 18), "idHacker", [], "any", true, true, false, 18)) {
            // line 19
            echo "\t\t\t\t\t\t    ";
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 19, $this->source); })()), "user", [], "any", false, false, false, 19), "idHacker", [], "any", false, false, false, 19), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 19, $this->source); })()), "report", [], "array", false, false, false, 19), "idHacker", [], "any", false, false, false, 19), "idHacker", [], "any", false, false, false, 19)))) {
                // line 20
                echo "\t\t\t\t\t\t    \t<div class=\"col-2\">
\t\t\t\t\t\t\t    \t<a href=\"";
                // line 21
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("report_edit", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 21, $this->source); })()), "report", [], "array", false, false, false, 21), "idReport", [], "any", false, false, false, 21)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Update Report</div>
\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    ";
            }
            // line 28
            echo "\t\t\t\t\t    ";
        }
        // line 29
        echo "\t\t\t\t\t    <div class=\"col-2\">
\t\t\t\t\t    \t<a href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 30, $this->source); })()), "report", [], "array", false, false, false, 30), "idOffer", [], "any", false, false, false, 30), "idOffer", [], "any", false, false, false, 30)]), "html", null, true);
        echo "\">
\t\t\t\t\t\t      \t<svg width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-arrow-left-circle-fill\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t  \t\t<path fill-rule=\"evenodd\" d=\"M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-7.646 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L6.207 7.5H11a.5.5 0 0 1 0 1H6.207l2.147 2.146z\"/>
\t\t\t\t\t\t\t</svg>
\t\t\t\t            </a>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t            </div>
          \t</div>
        </div>
        ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 40, $this->source); })()), "flashes", [0 => "message"], "method", false, false, false, 40));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 41
            echo "\t\t\t<div class=\" form-group text-center\">
\t\t\t\t<div class=\"alert alert-success mx-auto\" style=\"margin-top:5px; margin-bottom:-10px; width:40%;\" role=\"alert\"> ";
            // line 42
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
\t\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "\t    \t<div class=\"card-overlay border-0 bg-white shadow mx-auto\" style=\"width:60%; margin-bottom:20px;\">
\t\t        
\t\t        <div class=\"d-flex flex-column bd-highlight mb-2\" style=\"margin-left:40px; margin-top:20px\">    
\t\t        \t<div class=\"row\">
\t\t            \t
\t\t            \t<div class=\"col-8\">
\t\t\t            <h4 class=\"text-secondary\">Report Details</h4>    
\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Scope : </h5> 
\t                        <div class=\"p-2\">";
        // line 53
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 53, $this->source); })()), "report", [], "array", false, false, false, 53), "scope", [], "any", false, false, false, 53), "html", null, true);
        echo "</div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">End Point : </h5> 
\t                        <div class=\"p-2\">";
        // line 56
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 56, $this->source); })()), "report", [], "array", false, false, false, 56), "endPoint", [], "any", false, false, false, 56), "html", null, true);
        echo "</div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Application Fingerprint : </h5> 
\t                        <div class=\"p-2\">";
        // line 59
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 59, $this->source); })()), "report", [], "array", false, false, false, 59), "appFingerprint", [], "any", false, false, false, 59), "html", null, true);
        echo "</div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">IP Used : </h5> 
\t                        <div class=\"p-2\">";
        // line 62
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 62, $this->source); })()), "report", [], "array", false, false, false, 62), "ipUsed", [], "any", false, false, false, 62), "html", null, true);
        echo "</div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">CVSS Score : </h5> 
\t                        <div class=\"p-2\">";
        // line 65
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 65, $this->source); })()), "report", [], "array", false, false, false, 65), "cvssBase", [], "any", false, false, false, 65), "html", null, true);
        echo "</div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Severity : </h5> 
\t                        <div class=\"p-2\">";
        // line 68
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 68, $this->source); })()), "report", [], "array", false, false, false, 68), "severity", [], "any", false, false, false, 68), "html", null, true);
        echo "</div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Payload : </h5> 
\t                        <div class=\"p-2\">
\t                        \t<div class=\"text-secondary\" style=\"font-weight:bold; float:left; margin-right:5px;\">Find It : </div>
\t                        \t<div style=\"float:left\"> ";
        // line 73
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 73, $this->source); })()), "report", [], "array", false, false, false, 73), "payloadFi", [], "any", false, false, false, 73), "html", null, true);
        echo " €</div>
\t                        \t<div class=\"text-secondary\" style=\"font-weight:bold; float:left; margin-right:5px; margin-left:30px;\">Find It & Fix It : </div>
\t                        \t<div > ";
        // line 75
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 75, $this->source); })()), "report", [], "array", false, false, false, 75), "payloadFifi", [], "any", false, false, false, 75), "html", null, true);
        echo " €</div>
\t                        </div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Report Description : </h5> 
\t                        <div class=\"p-2\">";
        // line 79
        echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 79, $this->source); })()), "report", [], "array", false, false, false, 79), "report", [], "any", false, false, false, 79);
        echo "</div>

\t\t\t                ";
        // line 81
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 81), "idEntreprise", [], "any", true, true, false, 81)) {
            // line 82
            echo "\t\t\t                \t";
            if (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 82, $this->source); })()), "user", [], "any", false, false, false, 82), "idEntreprise", [], "any", false, false, false, 82), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 82, $this->source); })()), "report", [], "array", false, false, false, 82), "idOffer", [], "any", false, false, false, 82), "idEntreprise", [], "any", false, false, false, 82), "idEntreprise", [], "any", false, false, false, 82))) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 82, $this->source); })()), "report", [], "array", false, false, false, 82), "state", [], "any", false, false, false, 82), "idState", [], "any", false, false, false, 82), 2)))) {
                // line 83
                echo "\t\t\t\t\t                <div class=\"row\">
\t\t\t\t\t                \t<div class=\"col-3\">
\t\t\t\t\t\t\t\t\t        <a href=\"";
                // line 85
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("report_accept", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 85, $this->source); })()), "report", [], "array", false, false, false, 85), "idReport", [], "any", false, false, false, 85)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t            <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); margin-top:10px; margin-bottom:10px\">
\t\t\t\t\t\t\t\t\t              \t<h5 style=\"color:#9D151C;\">Accept Report</h5>
\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t        </a>
\t\t\t\t\t\t\t\t        </div>
\t\t\t\t\t\t\t\t        <div class=\"col-3\">
\t\t\t\t\t\t\t\t\t        <a href=\"";
                // line 92
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("report_reject", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 92, $this->source); })()), "report", [], "array", false, false, false, 92), "idReport", [], "any", false, false, false, 92)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t            <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); margin-top:10px; margin-bottom:10px\">
\t\t\t\t\t\t\t\t\t              \t<h5 style=\"color:#9D151C;\">Reject Report</h5>
\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t        </a>
\t\t\t\t\t\t\t\t        </div>
\t\t\t\t\t\t\t        </div>
\t\t\t\t\t        \t";
            }
            // line 100
            echo "\t\t\t\t\t      \t";
        }
        // line 101
        echo "\t\t\t\t\t    </div>
\t\t\t\t\t    <div class=\"col\">
\t\t            \t\t<h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Submission Date : </h5> 
\t                        <div class=\"p-2\">";
        // line 104
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 104, $this->source); })()), "report", [], "array", false, false, false, 104), "dateSub", [], "any", false, false, false, 104), "m/d/Y"), "html", null, true);
        echo "</div>
\t\t            \t</div>  \t
\t\t\t      \t</div>
\t\t        </div>
\t       \t</div>
\t</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "offer/show_report.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 104,  240 => 101,  237 => 100,  226 => 92,  216 => 85,  212 => 83,  209 => 82,  207 => 81,  202 => 79,  195 => 75,  190 => 73,  182 => 68,  176 => 65,  170 => 62,  164 => 59,  158 => 56,  152 => 53,  142 => 45,  133 => 42,  130 => 41,  126 => 40,  113 => 30,  110 => 29,  107 => 28,  97 => 21,  94 => 20,  91 => 19,  89 => 18,  84 => 16,  78 => 13,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}
 
<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t<div class=\"card border-0 bg-transparent\" style=\"margin-top:90px;\">
\t    <div class=\"tab-content py-4 text-center\">
\t    \t<div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(217, 217, 217, 0.5); width:100%; margin-bottom: 20px;\">
\t            <div class=\"card-body\">
\t            \t<div class=\"row justify-content-center d-flex align-items-center\">
\t\t\t\t    \t<div class=\"col-2\">
\t\t\t\t\t     \t<h4 class=\"text-title text-white text-center\">{{result['info_rep'][0]['title']}}</h4>
\t\t\t\t\t    </div>
\t\t\t\t\t    <div class=\"col-2\">
\t\t\t\t\t      \t<div class=\"text-title text-white text-center\">{{result['info_rep'][0]['description1']}}</div>
\t\t\t\t\t    </div>
\t\t\t\t\t    {% if app.user.idHacker is defined %}
\t\t\t\t\t\t    {% if app.user.idHacker == result['report'].idHacker.idHacker %}
\t\t\t\t\t\t    \t<div class=\"col-2\">
\t\t\t\t\t\t\t    \t<a href=\"{{url('report_edit', {'id' : result['report'].idReport}) }}\">
\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Update Report</div>
\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    {% endif %}
\t\t\t\t\t    {% endif %}
\t\t\t\t\t    <div class=\"col-2\">
\t\t\t\t\t    \t<a href=\"{{url('offer_show', {'id' : result['report'].idOffer.idOffer}) }}\">
\t\t\t\t\t\t      \t<svg width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-arrow-left-circle-fill\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t  \t\t<path fill-rule=\"evenodd\" d=\"M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-7.646 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L6.207 7.5H11a.5.5 0 0 1 0 1H6.207l2.147 2.146z\"/>
\t\t\t\t\t\t\t</svg>
\t\t\t\t            </a>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t            </div>
          \t</div>
        </div>
        {% for message in app.flashes('message')%}
\t\t\t<div class=\" form-group text-center\">
\t\t\t\t<div class=\"alert alert-success mx-auto\" style=\"margin-top:5px; margin-bottom:-10px; width:40%;\" role=\"alert\"> {{message}}</div>
\t\t\t</div>
\t\t{%endfor%}
\t    \t<div class=\"card-overlay border-0 bg-white shadow mx-auto\" style=\"width:60%; margin-bottom:20px;\">
\t\t        
\t\t        <div class=\"d-flex flex-column bd-highlight mb-2\" style=\"margin-left:40px; margin-top:20px\">    
\t\t        \t<div class=\"row\">
\t\t            \t
\t\t            \t<div class=\"col-8\">
\t\t\t            <h4 class=\"text-secondary\">Report Details</h4>    
\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Scope : </h5> 
\t                        <div class=\"p-2\">{{result['report'].scope}}</div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">End Point : </h5> 
\t                        <div class=\"p-2\">{{result['report'].endPoint}}</div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Application Fingerprint : </h5> 
\t                        <div class=\"p-2\">{{result['report'].appFingerprint}}</div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">IP Used : </h5> 
\t                        <div class=\"p-2\">{{result['report'].ipUsed}}</div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">CVSS Score : </h5> 
\t                        <div class=\"p-2\">{{result['report'].cvssBase}}</div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Severity : </h5> 
\t                        <div class=\"p-2\">{{result['report'].severity}}</div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Payload : </h5> 
\t                        <div class=\"p-2\">
\t                        \t<div class=\"text-secondary\" style=\"font-weight:bold; float:left; margin-right:5px;\">Find It : </div>
\t                        \t<div style=\"float:left\"> {{result['report'].payloadFi}} €</div>
\t                        \t<div class=\"text-secondary\" style=\"font-weight:bold; float:left; margin-right:5px; margin-left:30px;\">Find It & Fix It : </div>
\t                        \t<div > {{result['report'].payloadFifi}} €</div>
\t                        </div>

\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Report Description : </h5> 
\t                        <div class=\"p-2\">{{result['report'].report|raw}}</div>

\t\t\t                {% if app.user.idEntreprise is defined %}
\t\t\t                \t{% if app.user.idEntreprise == result['report'].idOffer.idEntreprise.idEntreprise and result['report'].state.idState == 2 %}
\t\t\t\t\t                <div class=\"row\">
\t\t\t\t\t                \t<div class=\"col-3\">
\t\t\t\t\t\t\t\t\t        <a href=\"{{url('report_accept', {'id' : result['report'].idReport}) }}\">
\t\t\t\t\t\t\t\t\t            <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); margin-top:10px; margin-bottom:10px\">
\t\t\t\t\t\t\t\t\t              \t<h5 style=\"color:#9D151C;\">Accept Report</h5>
\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t        </a>
\t\t\t\t\t\t\t\t        </div>
\t\t\t\t\t\t\t\t        <div class=\"col-3\">
\t\t\t\t\t\t\t\t\t        <a href=\"{{url('report_reject', {'id' : result['report'].idReport}) }}\">
\t\t\t\t\t\t\t\t\t            <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); margin-top:10px; margin-bottom:10px\">
\t\t\t\t\t\t\t\t\t              \t<h5 style=\"color:#9D151C;\">Reject Report</h5>
\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t        </a>
\t\t\t\t\t\t\t\t        </div>
\t\t\t\t\t\t\t        </div>
\t\t\t\t\t        \t{% endif %}
\t\t\t\t\t      \t{% endif %}
\t\t\t\t\t    </div>
\t\t\t\t\t    <div class=\"col\">
\t\t            \t\t<h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Submission Date : </h5> 
\t                        <div class=\"p-2\">{{result['report'].dateSub|date(\"m/d/Y\")}}</div>
\t\t            \t</div>  \t
\t\t\t      \t</div>
\t\t        </div>
\t       \t</div>
\t</div>
</div>
{% endblock %}

", "offer/show_report.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\offer\\show_report.html.twig");
    }
}
