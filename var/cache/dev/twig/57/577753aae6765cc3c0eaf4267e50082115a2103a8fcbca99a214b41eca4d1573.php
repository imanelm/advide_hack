<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* hacker/test.html.twig */
class __TwigTemplate_a07f94ecb33bf6120f62447cae2a839fcf36123229587f7b6dfc5bcc2777d990 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/test.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/test.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "hacker/test.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
        <div class=\"row d-flex justify-content-center\" style =\"margin-top:120px;\">
            <div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:80%; padding-top:30px; padding-bottom:30px; padding-right:30px; padding-left:30px;\">

                <div class=\"row d-flex justify-content-center\">

                    ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["testForm"]) || array_key_exists("testForm", $context) ? $context["testForm"] : (function () { throw new RuntimeError('Variable "testForm" does not exist.', 12, $this->source); })()), 'form_start');
        echo "
\t\t\t\t\t   <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                <a href=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("download_test");
        echo "\"> Your machine test </a>
\t\t\t\t\t\t\t\t<a href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("instructions");
        echo "\" style=\"margin-left: 50px;\"> instructions </a>
                            </div>
                            
                        </div>


\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["testForm"]) || array_key_exists("testForm", $context) ? $context["testForm"] : (function () { throw new RuntimeError('Variable "testForm" does not exist.', 24, $this->source); })()), "test", [], "any", false, false, false, 24), 'label', ["label" => "test"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["testForm"]) || array_key_exists("testForm", $context) ? $context["testForm"] : (function () { throw new RuntimeError('Variable "testForm" does not exist.', 27, $this->source); })()), "test", [], "any", false, false, false, 27), 'widget');
        echo "
                            </div>
                        </div>
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"text-center\">
                            <button type=\"submit\" class=\"btn btn-lg shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C;\">Save</button>
                        </div>
                        

                    ";
        // line 37
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["testForm"]) || array_key_exists("testForm", $context) ? $context["testForm"] : (function () { throw new RuntimeError('Variable "testForm" does not exist.', 37, $this->source); })()), 'form_end');
        echo "
                </div>

            </div>

        </div>
\t\t 


\t\t
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "hacker/test.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 37,  103 => 27,  97 => 24,  86 => 16,  82 => 15,  76 => 12,  68 => 6,  58 => 5,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}



{% block body %}

        <div class=\"row d-flex justify-content-center\" style =\"margin-top:120px;\">
            <div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:80%; padding-top:30px; padding-bottom:30px; padding-right:30px; padding-left:30px;\">

                <div class=\"row d-flex justify-content-center\">

                    {{ form_start(testForm) }}
\t\t\t\t\t   <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                <a href=\"{{path('download_test')}}\"> Your machine test </a>
\t\t\t\t\t\t\t\t<a href=\"{{path('instructions')}}\" style=\"margin-left: 50px;\"> instructions </a>
                            </div>
                            
                        </div>


\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(testForm.test, 'test') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(testForm.test) }}
                            </div>
                        </div>
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"text-center\">
                            <button type=\"submit\" class=\"btn btn-lg shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C;\">Save</button>
                        </div>
                        

                    {{ form_end(testForm) }}
                </div>

            </div>

        </div>
\t\t 


\t\t
{% endblock %}
", "hacker/test.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\hacker\\test.html.twig");
    }
}
