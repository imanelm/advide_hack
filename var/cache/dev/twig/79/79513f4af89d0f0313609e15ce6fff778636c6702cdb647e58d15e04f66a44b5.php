<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* hacker/editPass.html.twig */
class __TwigTemplate_768f9afba04d9d4bbc086d5df1aee9744770004413a59a74e9cc92c3ed11b91e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/editPass.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/editPass.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "hacker/editPass.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
<div class=\"row\" style=\"margin-top:140px; margin-bottom:185px\">
   <div class=\"col-6 offset-3\">
       <form method=\"post\">
          
           <div class=\"card\">
               <div class=\"card-body bg-light\">
               <h1 class=\"h3 mb-3 font-weight-normal text-center\">Change Password</h1>
\t\t\t   <form method= \"Post\">
\t\t\t       ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 14, $this->source); })()), "flashes", [0 => "message"], "method", false, false, false, 14));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 15
            echo "\t\t\t\t\t\t<div class=\" form-group\">
\t\t\t\t\t\t\t<div class=\"alert alert-danger\" role=\"alert\"> ";
            // line 16
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "                   <div class=\"form-group\">
                       <label for=\"inputEmail\" class=\"sr-only\">Email</label>
                       <input type=\"password\" value=\"\" name=\"pass0\" id=\"inputEmail\" class=\"form-control\" placeholder=\"current password\" required autofocus>
                   </div>
                   <div class=\"form-group\">
                       <label for=\"inputPassword\" class=\"sr-only\">Password</label>
                       <input type=\"password\" name=\"pass1\" id=\"inputPassword\" class=\"form-control\" placeholder=\"new password\" required>
                   </div>
\t\t\t\t   <div class=\"form-group\">
                       <label for=\"inputPassword\" class=\"sr-only\">confirm your password</label>
                       <input type=\"password\" name=\"pass2\" id=\"inputPassword\" class=\"form-control\" placeholder=\"confirm your new password\" required>
                   </div>
                  
                   
                   <button class=\"btn btn-lg btn-block shadow-sm\" style=\"background-color:rgba(217, 217, 217);\" type=\"submit\">
                       Save
                   </button>
\t\t\t\t</form>
               </div>
           </div>
       </form>
   </div>
</div>


\t\t
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "hacker/editPass.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 20,  86 => 16,  83 => 15,  79 => 14,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}

<div class=\"row\" style=\"margin-top:140px; margin-bottom:185px\">
   <div class=\"col-6 offset-3\">
       <form method=\"post\">
          
           <div class=\"card\">
               <div class=\"card-body bg-light\">
               <h1 class=\"h3 mb-3 font-weight-normal text-center\">Change Password</h1>
\t\t\t   <form method= \"Post\">
\t\t\t       {% for message in app.flashes('message')%}
\t\t\t\t\t\t<div class=\" form-group\">
\t\t\t\t\t\t\t<div class=\"alert alert-danger\" role=\"alert\"> {{message}}</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t{%endfor%}
                   <div class=\"form-group\">
                       <label for=\"inputEmail\" class=\"sr-only\">Email</label>
                       <input type=\"password\" value=\"\" name=\"pass0\" id=\"inputEmail\" class=\"form-control\" placeholder=\"current password\" required autofocus>
                   </div>
                   <div class=\"form-group\">
                       <label for=\"inputPassword\" class=\"sr-only\">Password</label>
                       <input type=\"password\" name=\"pass1\" id=\"inputPassword\" class=\"form-control\" placeholder=\"new password\" required>
                   </div>
\t\t\t\t   <div class=\"form-group\">
                       <label for=\"inputPassword\" class=\"sr-only\">confirm your password</label>
                       <input type=\"password\" name=\"pass2\" id=\"inputPassword\" class=\"form-control\" placeholder=\"confirm your new password\" required>
                   </div>
                  
                   
                   <button class=\"btn btn-lg btn-block shadow-sm\" style=\"background-color:rgba(217, 217, 217);\" type=\"submit\">
                       Save
                   </button>
\t\t\t\t</form>
               </div>
           </div>
       </form>
   </div>
</div>


\t\t
{% endblock %}
", "hacker/editPass.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\hacker\\editPass.html.twig");
    }
}
