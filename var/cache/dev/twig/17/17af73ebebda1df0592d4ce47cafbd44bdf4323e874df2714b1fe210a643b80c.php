<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* blog_post/reply.html.twig */
class __TwigTemplate_e814445602d2a3e07774eb45976b3fa2138a892cfe5b3783d6e748497732abbb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog_post/reply.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog_post/reply.html.twig"));

        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["ReplyForm"]) || array_key_exists("ReplyForm", $context) ? $context["ReplyForm"] : (function () { throw new RuntimeError('Variable "ReplyForm" does not exist.', 1, $this->source); })()), 'form_start');
        echo "
\t<div class=\"row justify-content-end\" style=\"margin-left:20px;\">
\t\t<a class=\"text-secondary font-weight-bold\" style = \"font-size:medium;\" data-toggle=\"collapse\" href=\"#replyCollapse\" role=\"button\" aria-expanded=\"false\" aria-controls=\"replyCollapse\">
\t\t    Reply to this answer
\t  \t</a>
\t</div>
\t<div class=\"collapse\" id=\"replyCollapse\">
\t  \t<div class=\"card card-body\" style=\"margin-bottom:10px;\">
                <div class=\"col\" style=\"float:left;\">
                    ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["ReplyForm"]) || array_key_exists("ReplyForm", $context) ? $context["ReplyForm"] : (function () { throw new RuntimeError('Variable "ReplyForm" does not exist.', 10, $this->source); })()), "reply", [], "any", false, false, false, 10), 'widget', ["id" => "reply"]);
        echo "
                </div>
                <div class=\"row\">
                \t<div class=\"text-center\">
                        <button type=\"submit\" class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C; margin-top:30px;\">Submit</button>
                    </div>
            \t</div>
  \t\t</div>
\t</div>
";
        // line 19
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["ReplyForm"]) || array_key_exists("ReplyForm", $context) ? $context["ReplyForm"] : (function () { throw new RuntimeError('Variable "ReplyForm" does not exist.', 19, $this->source); })()), 'form_end');
        echo "







";
        // line 27
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 27, $this->source); })()), "user", [], "any", false, false, false, 27))) {
            // line 28
            echo "                            <div class=\"row justify-content-end\" style=\"margin-left:20px;\">
                                <div class=\"text-secondary font-weight-bold\" style = \"font-size:medium;\">
                                    Reply to this answer
                                </div>
                            </div>
                            ";
            // line 33
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["ReplyForm"]) || array_key_exists("ReplyForm", $context) ? $context["ReplyForm"] : (function () { throw new RuntimeError('Variable "ReplyForm" does not exist.', 33, $this->source); })()), 'form_start');
            echo "
                                <div class=\"row justify-content-end\" style=\"margin-left:20px;\">
                                    
                                </div>
                                <div class=\"row justify-content-end\" style=\"margin-left:20px;\">
                                    <button type=\"submit\" class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C; margin-top:10px; margin-bottom:30px;\">Submit reply</button>
                                </div>
                            ";
            // line 40
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["ReplyForm"]) || array_key_exists("ReplyForm", $context) ? $context["ReplyForm"] : (function () { throw new RuntimeError('Variable "ReplyForm" does not exist.', 40, $this->source); })()), 'form_end');
            echo "
                        ";
        } else {
            // line 42
            echo "                            <div class=\"row justify-content-end\" style=\"margin-left:20px;\">
                                <a href=\"";
            // line 43
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("sign_in");
            echo "\" class=\"text-secondary font-weight-bold\" style = \"font-size:medium;\">
                                    Log in to reply
                                </a>
                            </div>
                        ";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "blog_post/reply.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 43,  102 => 42,  97 => 40,  87 => 33,  80 => 28,  78 => 27,  67 => 19,  55 => 10,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ form_start(ReplyForm) }}
\t<div class=\"row justify-content-end\" style=\"margin-left:20px;\">
\t\t<a class=\"text-secondary font-weight-bold\" style = \"font-size:medium;\" data-toggle=\"collapse\" href=\"#replyCollapse\" role=\"button\" aria-expanded=\"false\" aria-controls=\"replyCollapse\">
\t\t    Reply to this answer
\t  \t</a>
\t</div>
\t<div class=\"collapse\" id=\"replyCollapse\">
\t  \t<div class=\"card card-body\" style=\"margin-bottom:10px;\">
                <div class=\"col\" style=\"float:left;\">
                    {{ form_widget(ReplyForm.reply, {'id': 'reply'}) }}
                </div>
                <div class=\"row\">
                \t<div class=\"text-center\">
                        <button type=\"submit\" class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C; margin-top:30px;\">Submit</button>
                    </div>
            \t</div>
  \t\t</div>
\t</div>
{{ form_end(ReplyForm) }}







{% if app.user is not null %}
                            <div class=\"row justify-content-end\" style=\"margin-left:20px;\">
                                <div class=\"text-secondary font-weight-bold\" style = \"font-size:medium;\">
                                    Reply to this answer
                                </div>
                            </div>
                            {{ form_start(ReplyForm) }}
                                <div class=\"row justify-content-end\" style=\"margin-left:20px;\">
                                    
                                </div>
                                <div class=\"row justify-content-end\" style=\"margin-left:20px;\">
                                    <button type=\"submit\" class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C; margin-top:10px; margin-bottom:30px;\">Submit reply</button>
                                </div>
                            {{ form_end(ReplyForm) }}
                        {% else %}
                            <div class=\"row justify-content-end\" style=\"margin-left:20px;\">
                                <a href=\"{{ url('sign_in') }}\" class=\"text-secondary font-weight-bold\" style = \"font-size:medium;\">
                                    Log in to reply
                                </a>
                            </div>
                        {% endif %}", "blog_post/reply.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\blog_post\\reply.html.twig");
    }
}
