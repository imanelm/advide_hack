<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* offer/newoffer.html.twig */
class __TwigTemplate_5ad5178805995ac052b072723a098ba970fcbdbf4a8d2e631ed67492c1503d8e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'javascript' => [$this, 'block_javascript'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/newoffer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/newoffer.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "offer/newoffer.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        // line 4
        echo "    <script src=\"https://cdn.tiny.cloud/1/1r3xno60hf2olb3nwcalbv7ow89y17xu0n7a8s7fxqpztm09/tinymce/5/tinymce.min.js\" referrerpolicy=\"origin\"/></script>
    <script>
    tinymce.init({
        selector: '#description2',
        setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
        }
    });
  </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 16
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 17
        echo "<body onload=\"wysiwyg();\">
    <div class=\"card bg-transparent border-0 align-item-center\" style = \"margin-top: 100px;\">

        <div class=\"row d-flex justify-content-center\">

            <div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:80%; padding-top:30px; padding-bottom:30px; padding-right:30px; padding-left:30px;\">
                <h4>
                    New Offer 
                </h4>
                ";
        // line 26
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 26), "nameEnt", [], "any", true, true, false, 26)) {
            // line 27
            echo "                                    <tr>
                                        <td>
                                            <strong>Entreprise : </strong> ";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 29, $this->source); })()), "user", [], "any", false, false, false, 29), "idEntreprise", [], "any", false, false, false, 29), "html", null, true);
            echo "
                                        </td>
                                    </tr>
                                     ";
        }
        // line 33
        echo "                    ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["offerForm"]) || array_key_exists("offerForm", $context) ? $context["offerForm"] : (function () { throw new RuntimeError('Variable "offerForm" does not exist.', 33, $this->source); })()), 'form_start');
        echo "
                        <div class=\"row\">
                            <div class=\"col-sm-1 text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px; margin-right: 10px;\">   
                                ";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["offerForm"]) || array_key_exists("offerForm", $context) ? $context["offerForm"] : (function () { throw new RuntimeError('Variable "offerForm" does not exist.', 36, $this->source); })()), "title", [], "any", false, false, false, 36), 'label', ["label" => "Title"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["offerForm"]) || array_key_exists("offerForm", $context) ? $context["offerForm"] : (function () { throw new RuntimeError('Variable "offerForm" does not exist.', 39, $this->source); })()), "title", [], "any", false, false, false, 39), 'widget');
        echo "
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-sm-1 text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px; margin-right: 10px;\">   
                                <label for=\"offer_form_id_type\" class=\"required\">Summary</label>
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 47
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["offerForm"]) || array_key_exists("offerForm", $context) ? $context["offerForm"] : (function () { throw new RuntimeError('Variable "offerForm" does not exist.', 47, $this->source); })()), "description1", [], "any", false, false, false, 47), 'widget', ["id" => "description1"]);
        echo "
                            </div>
                        </div>
                        
                        <div class=\"row\">
                            <div class=\"col-sm-1 text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px; margin-right: 10px;\">   
                                ";
        // line 53
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["offerForm"]) || array_key_exists("offerForm", $context) ? $context["offerForm"] : (function () { throw new RuntimeError('Variable "offerForm" does not exist.', 53, $this->source); })()), "description2", [], "any", false, false, false, 53), 'label', ["label" => "Description"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 56
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["offerForm"]) || array_key_exists("offerForm", $context) ? $context["offerForm"] : (function () { throw new RuntimeError('Variable "offerForm" does not exist.', 56, $this->source); })()), "description2", [], "any", false, false, false, 56), 'widget', ["id" => "description2"]);
        echo "
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col-sm-1 text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px; margin-right: 10px;\">   
                                <label for=\"offer_form_id_type\" class=\"required\">Type</label>
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                <select id=\"offer_form_id_type\" name=\"type\">
                                    <option value=\"1\">Find It</option>
                                    <option value=\"2\">Find It &amp; Fix It</option>
                                </select>
                            </div>
                        </div>
                    ";
        // line 71
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["offerForm"]) || array_key_exists("offerForm", $context) ? $context["offerForm"] : (function () { throw new RuntimeError('Variable "offerForm" does not exist.', 71, $this->source); })()), 'form_end');
        echo "
            </div>
        </div>
    </div>
</body>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "offer/newoffer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 71,  163 => 56,  157 => 53,  148 => 47,  137 => 39,  131 => 36,  124 => 33,  117 => 29,  113 => 27,  111 => 26,  100 => 17,  90 => 16,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}

{% block javascript %}
    <script src=\"https://cdn.tiny.cloud/1/1r3xno60hf2olb3nwcalbv7ow89y17xu0n7a8s7fxqpztm09/tinymce/5/tinymce.min.js\" referrerpolicy=\"origin\"/></script>
    <script>
    tinymce.init({
        selector: '#description2',
        setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
        }
    });
  </script>
{% endblock %}
{% block body %}
<body onload=\"wysiwyg();\">
    <div class=\"card bg-transparent border-0 align-item-center\" style = \"margin-top: 100px;\">

        <div class=\"row d-flex justify-content-center\">

            <div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:80%; padding-top:30px; padding-bottom:30px; padding-right:30px; padding-left:30px;\">
                <h4>
                    New Offer 
                </h4>
                {% if app.user.nameEnt is defined %}
                                    <tr>
                                        <td>
                                            <strong>Entreprise : </strong> {{app.user.idEntreprise}}
                                        </td>
                                    </tr>
                                     {% endif %}
                    {{ form_start(offerForm)}}
                        <div class=\"row\">
                            <div class=\"col-sm-1 text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px; margin-right: 10px;\">   
                                {{ form_label(offerForm.title, 'Title') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(offerForm.title) }}
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col-sm-1 text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px; margin-right: 10px;\">   
                                <label for=\"offer_form_id_type\" class=\"required\">Summary</label>
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(offerForm.description1, {'id': 'description1'}) }}
                            </div>
                        </div>
                        
                        <div class=\"row\">
                            <div class=\"col-sm-1 text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px; margin-right: 10px;\">   
                                {{ form_label(offerForm.description2, 'Description') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(offerForm.description2, {'id': 'description2'}) }}
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col-sm-1 text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px; margin-right: 10px;\">   
                                <label for=\"offer_form_id_type\" class=\"required\">Type</label>
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                <select id=\"offer_form_id_type\" name=\"type\">
                                    <option value=\"1\">Find It</option>
                                    <option value=\"2\">Find It &amp; Fix It</option>
                                </select>
                            </div>
                        </div>
                    {{ form_end(offerForm) }}
            </div>
        </div>
    </div>
</body>
{% endblock %}", "offer/newoffer.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\offer\\newoffer.html.twig");
    }
}
