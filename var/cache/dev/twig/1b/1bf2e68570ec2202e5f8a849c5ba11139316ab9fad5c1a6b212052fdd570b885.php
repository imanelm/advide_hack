<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* layout.html.twig */
class __TwigTemplate_1e7f8baeb91c3214eb183d0720ca8e7b98ef43a3b85513b35303ede08e1cb9eb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascript' => [$this, 'block_javascript'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'footer' => [$this, 'block_footer'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "layout.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "layout.html.twig"));

        // line 1
        echo "<!DOCTYPE HTML>
<html>

<head>
  <meta charset=\"utf-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\" />
  <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
  ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 15
        echo "  ";
        $this->displayBlock('javascript', $context, $blocks);
        // line 34
        echo "\t
</head>

<body>
\t<!- Navation Bar -!>
\t<header>
\t\t";
        // line 40
        $this->displayBlock('header', $context, $blocks);
        // line 162
        echo "\t</header>

\t 

\t   ";
        // line 166
        $this->displayBlock('body', $context, $blocks);
        // line 168
        echo "
\t<button type=\"button\" class=\"open-button\" id=\"contact\" style=\"position: -webkit-sticky; position: sticky; bottom: 1rem; align-self: flex-end; width: 150px; height: 50px; left: 85%; border-radius: 70%\">
       Contact Us!
    </button>
    <div class=\"form-popup\" id=\"myForm\" >
      <form action=\"/action_page.php\" class=\"form-container\">
      \t<div class=\"row justify-content-center\">
        \t<h3 class=\"text-secondary\">Contact Us</h3>
    \t</div>
\t\t<div id=\"loginbox\">
\t\t\t<div class=\"row justify-content-center\" style=\"margin-top:40px;\">
        \t\t<label for=\"pseudo\" ><b class=\"text-secondary\">Enter a username</b></label>
    \t\t</div>
        \t<div class=\"row justify-content-center\">
        \t\t<input type=\"text\" placeholder=\"Enter pseudo\" name=\"pseudo\" id =\"pseudo\" style=\"width:80%;\" required>
    \t\t</div>
        \t<div class=\"row justify-content-center\">
        \t\t<button id=\"loginChoose\" class=\"btn btn-sm\" style=\"background-color:rgba(217, 217, 217); width:50%;\">
        \t\t\t<h4 style=\"color:#9D151C\">Next</h4>
    \t\t\t</button>
    \t\t</div>
\t\t</div>
\t\t
\t\t<div class=\"msg-container\" style=\"height:450px; overflow:auto;\">
\t\t\t<div class=\"msg-area\" id=\"message-area\"></div>
\t\t\t<div class=\"bottom\"><input type=\"text\" name=\"msginput\" class=\"msginput\" id=\"message\" onkeydown=\"if (event.keyCode == 13) sendMessage()\" value=\"\" placeholder=\"Enter your message here ... (Press enter to send message)\"></div>
        </div>
        <div class=\"row justify-content-center\">
\t\t\t<button type=\"button\" class=\"btn cancel\" style=\"background-color:rgba(217, 217, 217); width:50%;\">
\t\t\t\t<b style=\"color:#9D151C\">Close</b>
\t\t\t</button>
\t\t</div>
      </form>
    </div>
\t
\t<footer class=\"footer card bg-white border-0 shadow\" style=\"position:relative; bottom:0px; width:100%;\">
\t\t";
        // line 204
        $this->displayBlock('footer', $context, $blocks);
        // line 225
        echo "\t</footer>

\t<script>
\t
\t\t\$(document).ready(function(){
\t\t\tupdate();
\t\t});

\t\t\$(\"#contact\").click(function(){
\t\t\t\$(\".form-popup\").show();
\t\t\tcheckCookie();
\t\t});

\t\tfunction checkCookie()
\t\t{
\t\tif(document.cookie.indexOf(\"messengerUname\")==-1)
\t\t{
\t\t\tchooseUserName();\t
\t\t}else{
\t\t\t\$(\"#loginbox\").hide();
\t\t}
\t\t}


\t\tfunction chooseUserName()
\t\t{
\t\t\$(\"#loginbox\").show();
\t\t\$(\"#messagebox\").hide();
\t\t\$(\"#loginChoose\").click(function(){
\t\t\tevent.preventDefault();
\t\t\tvar user = \$(\"#pseudo\").val();
\t\t\tdocument.cookie = \"messengerUname=\"+user+\";SameSite=Strict\";
\t\t\tcheckCookie();
\t\t});
\t\t}\t

\t\tfunction getcookie(cname) {
\t\tvar name = cname + \"=\";
\t\tvar ca = document.cookie.split(';');
\t\tfor(var i=0; i<ca.length; i++) {
\t\t\tvar c = ca[i];
\t\t\twhile (c.charAt(0)==' ') c = c.substring(1);
\t\t\tif (c.indexOf(name) == 0) return c.substring(name.length,c.length);
\t\t}
\t\treturn \"\";
\t\t}

\t\tfunction escapehtml(text) {
\t\treturn text
\t\t\t.replace(/&/g, \"&amp;\")
\t\t\t.replace(/</g, \"&lt;\")
\t\t\t.replace(/>/g, \"&gt;\")
\t\t\t.replace(/\"/g, \"&quot;\")
\t\t\t.replace(/'/g, \"&#039;\");
\t\t}

\t\tfunction sendMessage()
\t\t{
\t\tevent.preventDefault();
\t\tvar message = \$(\"#message\").val();
\t\t//alert(message);
\t\tif (message != \"\") {
\t\t\tvar username = getcookie(\"messengerUname\");

\t\t\tvar xmlhttp=new XMLHttpRequest();

\t\t\txmlhttp.onreadystatechange=function() {
\t\t\t\tif (xmlhttp.readyState==4 && xmlhttp.status==200) {
\t\t\t\t\tmessage = escapehtml(message)
\t\t\t\t\tdocument.getElementById(\"messagebox\").innerHTML += \"<div class=\\\"msgc\\\" style=\\\"margin-bottom: 30px;\\\"> <div class=\\\"msg msgfrom\\\">\" + message + \"</div> <div class=\\\"msgarr msgarrfrom\\\"></div> <div class=\\\"msgsentby msgsentbyfrom\\\">Sent by \" + username + \"</div> </div>\";
\t\t\t\t}
\t\t\t}
\t\t\txmlhttp.open(\"GET\",\"/update_message?pseudo=\" + username + \"&message=\" + message,true);
\t\t\txmlhttp.send();
\t\t\t}
\t\t\$(\"#message\").val(\"\");
\t\t}

\t\t\$(\"#sendMessage\").click(function(){
\t\tevent.preventDefault();
\t\tsendMessage();
\t\tupdate();
\t\t});

\t\tfunction update()
\t\t{
\t\t\tvar xmlhttp=new XMLHttpRequest();
\t\tvar username = getcookie(\"messengerUname\");
\t\tvar output = \"\";
\t\txmlhttp.onreadystatechange=function() {
\t\t\tif (xmlhttp.readyState==4 && xmlhttp.status==200) {
\t\t\t\tvar response = xmlhttp.responseText.split(\"\\n\")
\t\t\t\tvar rl = response.length
\t\t\t\tvar item = \"\";
\t\t\t\tfor (var i = 0; i < rl; i++) {
\t\t\t\t\titem = response[i].split(\"\\\\\")
\t\t\t\t\tif (item[1] != undefined) {
\t\t\t\t\t\t//console.log('item 0 : ', item);
\t\t\t\t\t\tif (item[0] == username) {
\t\t\t\t\t\t\toutput += \"<div class=\\\"msgc\\\" style=\\\"margin-bottom: 30px;\\\"> <div class=\\\"msg msgfrom\\\">\" + item[1] + \"</div> <div class=\\\"msgarr msgarrfrom\\\"></div> <div class=\\\"msgsentby msgsentbyfrom\\\">Sent by \" + item[0] + \"</div> </div>\";
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\toutput += \"<div class=\\\"msgc\\\"> <div class=\\\"msg\\\">\" + item[1] + \"</div> <div class=\\\"msgarr\\\"></div> <div class=\\\"msgsentby\\\">Sent by \" + item[0] + \"</div> </div>\";
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}

\t\t\t\tdocument.getElementById(\"message-area\").innerHTML = output;
\t\t\t\tdocument.getElementById(\"message-area\").scrollTop = document.getElementById(\"message-area\").scrollHeight;

\t\t\t}
\t\t}
\t\t\txmlhttp.open(\"GET\",\"/get_message?username=\" + username,true);
\t\t\txmlhttp.send();
\t\t}

\t\t\$(\".cancel\").click(function(){
\t\t\$(\".form-popup\").hide();
\t\t});

\t\tsetInterval(function(){ update(); }, 500);


\t</script>
</body>

</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 7
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Advise'Hack";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "\t<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" integrity=\"sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk\" crossorigin=\"anonymous\">
\t<link rel=\"stylesheet\" href=\"/style.css\"/>
\t<style>
\t\t
\t</style>
  ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        // line 16
        echo "  \t<script src=\"jquery-3.5.1.min.js\"></script>
\t<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script> 
\t<!-- Bootstrap core JS-->
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>
\t\t<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\"></script>
        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js\"></script>
        <!-- Third party plugin JS-->
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js\"></script>
        <!-- Contact form JS-->
        <script src=\"assets/mail/jqBootstrapValidation.js\"></script>
        <script src=\"assets/mail/contact_me.js\"></script>
        <!-- Core theme JS-->
        <script src=\"script.js\"></script>
        <script src=\"js/scripts.js\"></script>
        <script src=\"https://code.jquery.com/jquery-3.5.1.slim.min.js\" integrity=\"sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj\" crossorigin=\"anonymous\"></script>
\t\t<script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js\" integrity=\"sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN\" crossorigin=\"anonymous\"></script>
\t\t<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js\"></script>
  ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 40
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 41
        echo "\t\t\t<nav class=\"navbar navbar-expand-sm navbar-light fixed-top shadow\" style=\"background-color:#9D151C;\" id=\"mainNav\">
\t\t\t\t<div class=\"container\">
\t\t\t\t    <a class=\"navbar-brand\" href=\"";
        // line 43
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("home_page");
        echo "\">
\t\t\t\t      <img src=\"/Images/logo2.png\" height=\"60\" alt=\"\" loading=\"lazy\"/>
\t\t\t\t    </a>
\t\t\t     \t<button class=\"navbar-toggler navbar-toggler-right bg-white rounded\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\" style=\"color:#9D151C\">
\t\t\t     \t\tMenu
\t                    <i class=\"fas fa-bars\"></i>
\t                </button>
\t\t\t\t    <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
\t\t\t\t      <div class=\"navbar-nav\">
\t\t\t\t      \t";
        // line 52
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 52, $this->source); })()), "user", [], "any", false, false, false, 52)) {
            // line 53
            echo "\t\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("hackers");
            echo "\" style=\"margin-right:15px;\">Hackers</a>
\t\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"";
            // line 54
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprises");
            echo "\" style=\"margin-right:15px;\">Entreprises</a>
\t\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"";
            // line 55
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offers");
            echo "\" style=\"margin-right:15px;\">Offers</a>
\t\t\t\t\t    ";
        }
        // line 57
        echo "\t\t\t\t\t\t";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 57), "num", [], "any", true, true, false, 57)) {
            // line 58
            echo "\t\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_creation");
            echo "\" style=\"margin-right:15px;\">New Offer</a>
\t\t\t\t\t    ";
        }
        // line 60
        echo "\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("blog");
        echo "\" style=\"margin-right:15px;\">Blog</a>
\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"#\" style=\"margin-right:15px;\">Contact</a>
\t\t\t\t\t\t";
        // line 62
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 62), "idHacker", [], "any", true, true, false, 62) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 62, $this->source); })()), "user", [], "any", false, false, false, 62), "idHacker", [], "any", false, false, false, 62), 4)))) {
            // line 63
            echo "\t\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("messages");
            echo "\" style=\"margin-right:15px;\">Messages</a>
\t\t\t\t\t\t";
        }
        // line 65
        echo "\t\t\t\t      </div>
\t\t\t\t    </div>
\t\t\t\t ";
        // line 67
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 67, $this->source); })()), "user", [], "any", false, false, false, 67)) {
            // line 68
            echo "\t\t\t\t <div class=\"navbar-collapse collapse w-100 order-3 dual-collapse2\" id=\"navbarResponsive\">
\t\t\t\t        <ul class=\"navbar-nav ml-auto\">
\t\t\t\t        \t";
            // line 70
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 71
                echo "\t\t\t\t        \t\t";
                if ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 71, $this->source); })()), "user", [], "any", false, false, false, 71), "newNotif", [], "any", false, false, false, 71), 0))) {
                    // line 72
                    echo "\t\t\t\t        \t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"";
                    // line 73
                    echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("notifications_admin");
                    echo "\">
\t\t\t\t\t\t                \t<svg style=\"margin-top:4px;\" width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-bell-fill\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t  \t<path d=\"M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z\"/>
\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t            \t</a>
\t\t\t\t\t\t            </li>
\t\t\t\t        \t\t";
                } else {
                    // line 80
                    echo "\t\t\t\t\t            \t<li class=\"nav-item\">
\t\t\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"";
                    // line 81
                    echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("notifications_admin");
                    echo "\">
\t\t\t\t\t\t                \t<svg style=\"margin-top:4px;\" width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-bell\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t  \t<path d=\"M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2z\"/>
\t\t\t\t\t\t\t\t\t\t\t  \t<path fill-rule=\"evenodd\" d=\"M8 1.918l-.797.161A4.002 4.002 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4.002 4.002 0 0 0-3.203-3.92L8 1.917zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5.002 5.002 0 0 1 13 6c0 .88.32 4.2 1.22 6z\"/>
\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t            \t</a>
\t\t\t\t\t\t            </li>
\t\t\t\t\t            ";
                }
                // line 89
                echo "\t\t\t\t            ";
            } else {
                // line 90
                echo "\t\t\t\t\t        \t";
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 90), "nameEnt", [], "any", true, true, false, 90) &&  !$this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN"))) {
                    // line 91
                    echo "\t\t\t\t\t        \t\t";
                    if ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 91, $this->source); })()), "user", [], "any", false, false, false, 91), "newNotif", [], "any", false, false, false, 91), 0))) {
                        // line 92
                        echo "\t\t\t\t\t        \t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"";
                        // line 93
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("my_notifications_ent", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 93, $this->source); })()), "user", [], "any", false, false, false, 93), "idEntreprise", [], "any", false, false, false, 93)]), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t                \t<svg style=\"margin-top:4px;\" width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-bell-fill\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t  \t<path d=\"M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t            \t</a>
\t\t\t\t\t\t\t            </li>
\t\t\t\t\t        \t\t";
                    } else {
                        // line 100
                        echo "\t\t\t\t\t\t            \t<li class=\"nav-item\">
\t\t\t\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"";
                        // line 101
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("my_notifications_ent", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 101, $this->source); })()), "user", [], "any", false, false, false, 101), "idEntreprise", [], "any", false, false, false, 101)]), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t                \t<svg style=\"margin-top:4px;\" width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-bell\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t  \t<path d=\"M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t  \t<path fill-rule=\"evenodd\" d=\"M8 1.918l-.797.161A4.002 4.002 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4.002 4.002 0 0 0-3.203-3.92L8 1.917zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5.002 5.002 0 0 1 13 6c0 .88.32 4.2 1.22 6z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t            \t</a>
\t\t\t\t\t\t\t            </li>
\t\t\t\t\t\t            ";
                    }
                    // line 109
                    echo "\t\t\t\t\t            ";
                } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 109), "mark", [], "any", true, true, false, 109) &&  !$this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN"))) {
                    // line 110
                    echo "\t\t\t\t\t        \t\t";
                    if ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 110, $this->source); })()), "user", [], "any", false, false, false, 110), "newNotif", [], "any", false, false, false, 110), 0))) {
                        // line 111
                        echo "\t\t\t\t\t        \t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"";
                        // line 112
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("my_notifications_ha", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 112, $this->source); })()), "user", [], "any", false, false, false, 112), "idHacker", [], "any", false, false, false, 112)]), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t                \t<svg style=\"margin-top:4px;\" width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-bell-fill\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t  \t<path d=\"M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t            \t</a>
\t\t\t\t\t\t\t            </li>
\t\t\t\t\t        \t\t";
                    } else {
                        // line 119
                        echo "\t\t\t\t\t\t            \t<li class=\"nav-item\">
\t\t\t\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"";
                        // line 120
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("my_notifications_ha", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 120, $this->source); })()), "user", [], "any", false, false, false, 120), "idHacker", [], "any", false, false, false, 120)]), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t                \t<svg style=\"margin-top:4px;\" width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-bell\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t  \t<path d=\"M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t  \t<path fill-rule=\"evenodd\" d=\"M8 1.918l-.797.161A4.002 4.002 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4.002 4.002 0 0 0-3.203-3.92L8 1.917zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5.002 5.002 0 0 1 13 6c0 .88.32 4.2 1.22 6z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t            \t</a>
\t\t\t\t\t\t\t            </li>
\t\t\t\t\t\t            ";
                    }
                    // line 128
                    echo "\t\t\t\t\t            ";
                }
                // line 129
                echo "\t\t\t\t            ";
            }
            // line 130
            echo "\t\t\t\t            <li class=\"nav-item\">
\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"";
            // line 131
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("profile");
            echo "\">
\t\t\t\t                \t<button type=\"button\" class=\"btn btn-outline-light\">";
            // line 132
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 132, $this->source); })()), "user", [], "any", false, false, false, 132), "username", [], "any", false, false, false, 132), "html", null, true);
            echo "</button>
\t\t\t\t            \t</a>
\t\t\t\t            </li>
\t\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"";
            // line 136
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("logout");
            echo "\">
\t\t\t\t                \t<button type=\"button\" class=\"btn btn-outline-light\">log out</button>
\t\t\t\t            \t</a>
\t\t\t\t            </li>
\t\t\t\t        </ul>
\t\t\t\t    </div>
\t\t\t    </div>
\t\t\t</nav>
\t\t\t";
        } else {
            // line 145
            echo "\t\t\t    <div class=\"navbar-collapse collapse w-100 order-3 dual-collapse2\">
\t\t\t        <ul class=\"navbar-nav ml-auto\">
\t\t\t            <li class=\"nav-item\">
\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"";
            // line 148
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("registerHacker");
            echo "\">
\t\t\t                \t<button type=\"button\" class=\"btn btn-outline-light\">Sign Up</button>
\t\t\t            \t</a>
\t\t\t            </li>
\t\t\t            <li class=\"nav-item\">
\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"";
            // line 153
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("sign_in");
            echo "\">
\t\t\t                \t<button type=\"button\" class=\"btn btn-outline-light\">Sign In</button>
\t\t\t            \t</a>
\t\t\t            </li>
\t\t\t        </ul>
\t\t\t    </div>
\t\t\t</nav>
\t\t\t";
        }
        // line 161
        echo "\t\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 166
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 167
        echo "\t   ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 204
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        // line 205
        echo "\t\t\t<p class=\"text-card text-secondary text-center\" style=\"margin-bottom:0.2%; margin-top:0.1%;\">
\t\t\t\t&copy; Copyright ";
        // line 206
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo "
\t\t\t\t&middot; 
\t\t\t\t<a href=\"#\" style=\"color:#9D151C;\">About Us</a>
\t\t\t    &middot; 
\t\t\t    <a href=\"#\" style=\"color:#9D151C;\">Contact Us</a>
\t\t\t</p>
\t\t\t<p class=\"text-card text-secondary text-center\" style=\"margin-bottom:0.1%;\">
\t\t\t\tFind us on : 
\t\t\t\t<a href=\"#\">
\t\t\t\t\t<img src=\"/Images/instagram.png\" alt=\"\" height=\"20\" style=\"margin-left:0.8%; margin-right:0.8%; margin-bottom:0.2%;\"/>
\t\t\t\t</a>
\t\t\t\t<a href=\"#\">
\t\t\t\t\t<img src=\"/Images/twitter.png\" alt=\"\" height=\"20\" style=\"margin-bottom:0.2%; margin-right:0.8%;\"/>
\t\t\t\t</a>
\t\t\t\t<a href=\"#\">
\t\t\t\t\t<img src=\"/Images/linkedin.png\" alt=\"\" height=\"20\" style=\"margin-bottom:0.2%;\"/>
\t\t\t\t</a>
\t\t\t</p>
\t\t";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  615 => 206,  612 => 205,  602 => 204,  592 => 167,  582 => 166,  572 => 161,  561 => 153,  553 => 148,  548 => 145,  536 => 136,  529 => 132,  525 => 131,  522 => 130,  519 => 129,  516 => 128,  505 => 120,  502 => 119,  492 => 112,  489 => 111,  486 => 110,  483 => 109,  472 => 101,  469 => 100,  459 => 93,  456 => 92,  453 => 91,  450 => 90,  447 => 89,  436 => 81,  433 => 80,  423 => 73,  420 => 72,  417 => 71,  415 => 70,  411 => 68,  409 => 67,  405 => 65,  399 => 63,  397 => 62,  391 => 60,  385 => 58,  382 => 57,  377 => 55,  373 => 54,  368 => 53,  366 => 52,  354 => 43,  350 => 41,  340 => 40,  313 => 16,  303 => 15,  288 => 9,  278 => 8,  259 => 7,  124 => 225,  122 => 204,  84 => 168,  82 => 166,  76 => 162,  74 => 40,  66 => 34,  63 => 15,  61 => 8,  57 => 7,  49 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE HTML>
<html>

<head>
  <meta charset=\"utf-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\" />
  <title>{% block title %}Advise'Hack{% endblock %}</title>
  {% block stylesheets %}
\t<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" integrity=\"sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk\" crossorigin=\"anonymous\">
\t<link rel=\"stylesheet\" href=\"/style.css\"/>
\t<style>
\t\t
\t</style>
  {% endblock %}
  {% block javascript %}
  \t<script src=\"jquery-3.5.1.min.js\"></script>
\t<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script> 
\t<!-- Bootstrap core JS-->
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>
\t\t<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\"></script>
        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js\"></script>
        <!-- Third party plugin JS-->
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js\"></script>
        <!-- Contact form JS-->
        <script src=\"assets/mail/jqBootstrapValidation.js\"></script>
        <script src=\"assets/mail/contact_me.js\"></script>
        <!-- Core theme JS-->
        <script src=\"script.js\"></script>
        <script src=\"js/scripts.js\"></script>
        <script src=\"https://code.jquery.com/jquery-3.5.1.slim.min.js\" integrity=\"sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj\" crossorigin=\"anonymous\"></script>
\t\t<script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js\" integrity=\"sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN\" crossorigin=\"anonymous\"></script>
\t\t<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js\"></script>
  {% endblock %}
\t
</head>

<body>
\t<!- Navation Bar -!>
\t<header>
\t\t{% block header %}
\t\t\t<nav class=\"navbar navbar-expand-sm navbar-light fixed-top shadow\" style=\"background-color:#9D151C;\" id=\"mainNav\">
\t\t\t\t<div class=\"container\">
\t\t\t\t    <a class=\"navbar-brand\" href=\"{{ url('home_page')}}\">
\t\t\t\t      <img src=\"/Images/logo2.png\" height=\"60\" alt=\"\" loading=\"lazy\"/>
\t\t\t\t    </a>
\t\t\t     \t<button class=\"navbar-toggler navbar-toggler-right bg-white rounded\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\" style=\"color:#9D151C\">
\t\t\t     \t\tMenu
\t                    <i class=\"fas fa-bars\"></i>
\t                </button>
\t\t\t\t    <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
\t\t\t\t      <div class=\"navbar-nav\">
\t\t\t\t      \t{% if app.user %}
\t\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"{{ url('hackers')}}\" style=\"margin-right:15px;\">Hackers</a>
\t\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"{{ url('entreprises') }}\" style=\"margin-right:15px;\">Entreprises</a>
\t\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"{{ url('offers')}}\" style=\"margin-right:15px;\">Offers</a>
\t\t\t\t\t    {% endif %}
\t\t\t\t\t\t{% if app.user.num is defined %}
\t\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"{{ url('offer_creation')}}\" style=\"margin-right:15px;\">New Offer</a>
\t\t\t\t\t    {% endif %}
\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"{{ url('blog')}}\" style=\"margin-right:15px;\">Blog</a>
\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"#\" style=\"margin-right:15px;\">Contact</a>
\t\t\t\t\t\t{% if app.user.idHacker is defined and app.user.idHacker == 4 %}
\t\t\t\t\t        <a class=\"nav-item nav-link text-white\" href=\"{{ url('messages')}}\" style=\"margin-right:15px;\">Messages</a>
\t\t\t\t\t\t{% endif %}
\t\t\t\t      </div>
\t\t\t\t    </div>
\t\t\t\t {%if app.user %}
\t\t\t\t <div class=\"navbar-collapse collapse w-100 order-3 dual-collapse2\" id=\"navbarResponsive\">
\t\t\t\t        <ul class=\"navbar-nav ml-auto\">
\t\t\t\t        \t{% if is_granted('ROLE_ADMIN') %}
\t\t\t\t        \t\t{% if app.user.newNotif != 0 %}
\t\t\t\t        \t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"{{ url('notifications_admin') }}\">
\t\t\t\t\t\t                \t<svg style=\"margin-top:4px;\" width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-bell-fill\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t  \t<path d=\"M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z\"/>
\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t            \t</a>
\t\t\t\t\t\t            </li>
\t\t\t\t        \t\t{% else %}
\t\t\t\t\t            \t<li class=\"nav-item\">
\t\t\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"{{ url('notifications_admin') }}\">
\t\t\t\t\t\t                \t<svg style=\"margin-top:4px;\" width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-bell\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t  \t<path d=\"M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2z\"/>
\t\t\t\t\t\t\t\t\t\t\t  \t<path fill-rule=\"evenodd\" d=\"M8 1.918l-.797.161A4.002 4.002 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4.002 4.002 0 0 0-3.203-3.92L8 1.917zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5.002 5.002 0 0 1 13 6c0 .88.32 4.2 1.22 6z\"/>
\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t            \t</a>
\t\t\t\t\t\t            </li>
\t\t\t\t\t            {% endif %}
\t\t\t\t            {% else %}
\t\t\t\t\t        \t{% if app.user.nameEnt is defined and not is_granted('ROLE_ADMIN') %}
\t\t\t\t\t        \t\t{% if app.user.newNotif != 0 %}
\t\t\t\t\t        \t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"{{ url('my_notifications_ent', {'id' : app.user.idEntreprise}) }}\">
\t\t\t\t\t\t\t                \t<svg style=\"margin-top:4px;\" width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-bell-fill\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t  \t<path d=\"M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t            \t</a>
\t\t\t\t\t\t\t            </li>
\t\t\t\t\t        \t\t{% else %}
\t\t\t\t\t\t            \t<li class=\"nav-item\">
\t\t\t\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"{{ url('my_notifications_ent', {'id' : app.user.idEntreprise}) }}\">
\t\t\t\t\t\t\t                \t<svg style=\"margin-top:4px;\" width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-bell\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t  \t<path d=\"M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t  \t<path fill-rule=\"evenodd\" d=\"M8 1.918l-.797.161A4.002 4.002 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4.002 4.002 0 0 0-3.203-3.92L8 1.917zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5.002 5.002 0 0 1 13 6c0 .88.32 4.2 1.22 6z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t            \t</a>
\t\t\t\t\t\t\t            </li>
\t\t\t\t\t\t            {% endif %}
\t\t\t\t\t            {% elseif app.user.mark is defined and not is_granted('ROLE_ADMIN')%}
\t\t\t\t\t        \t\t{% if app.user.newNotif != 0 %}
\t\t\t\t\t        \t\t\t<li class=\"nav-item\">
\t\t\t\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"{{ url('my_notifications_ha', {'id' : app.user.idHacker}) }}\">
\t\t\t\t\t\t\t                \t<svg style=\"margin-top:4px;\" width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-bell-fill\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t  \t<path d=\"M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t            \t</a>
\t\t\t\t\t\t\t            </li>
\t\t\t\t\t        \t\t{% else %}
\t\t\t\t\t\t            \t<li class=\"nav-item\">
\t\t\t\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"{{ url('my_notifications_ha', {'id' : app.user.idHacker}) }}\">
\t\t\t\t\t\t\t                \t<svg style=\"margin-top:4px;\" width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-bell\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t  \t<path d=\"M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t  \t<path fill-rule=\"evenodd\" d=\"M8 1.918l-.797.161A4.002 4.002 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4.002 4.002 0 0 0-3.203-3.92L8 1.917zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5.002 5.002 0 0 1 13 6c0 .88.32 4.2 1.22 6z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t            \t</a>
\t\t\t\t\t\t\t            </li>
\t\t\t\t\t\t            {% endif %}
\t\t\t\t\t            {% endif %}
\t\t\t\t            {% endif %}
\t\t\t\t            <li class=\"nav-item\">
\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"{{ url('profile') }}\">
\t\t\t\t                \t<button type=\"button\" class=\"btn btn-outline-light\">{{app.user.username}}</button>
\t\t\t\t            \t</a>
\t\t\t\t            </li>
\t\t\t\t\t\t\t<li class=\"nav-item\">
\t\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"{{ url('logout') }}\">
\t\t\t\t                \t<button type=\"button\" class=\"btn btn-outline-light\">log out</button>
\t\t\t\t            \t</a>
\t\t\t\t            </li>
\t\t\t\t        </ul>
\t\t\t\t    </div>
\t\t\t    </div>
\t\t\t</nav>
\t\t\t{%else%}
\t\t\t    <div class=\"navbar-collapse collapse w-100 order-3 dual-collapse2\">
\t\t\t        <ul class=\"navbar-nav ml-auto\">
\t\t\t            <li class=\"nav-item\">
\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"{{ url('registerHacker') }}\">
\t\t\t                \t<button type=\"button\" class=\"btn btn-outline-light\">Sign Up</button>
\t\t\t            \t</a>
\t\t\t            </li>
\t\t\t            <li class=\"nav-item\">
\t\t\t                <a class=\"nav-item nav-link text-white\" href=\"{{ url('sign_in') }}\">
\t\t\t                \t<button type=\"button\" class=\"btn btn-outline-light\">Sign In</button>
\t\t\t            \t</a>
\t\t\t            </li>
\t\t\t        </ul>
\t\t\t    </div>
\t\t\t</nav>
\t\t\t{% endif %}
\t\t{% endblock %}
\t</header>

\t 

\t   {% block body %}
\t   {% endblock %}

\t<button type=\"button\" class=\"open-button\" id=\"contact\" style=\"position: -webkit-sticky; position: sticky; bottom: 1rem; align-self: flex-end; width: 150px; height: 50px; left: 85%; border-radius: 70%\">
       Contact Us!
    </button>
    <div class=\"form-popup\" id=\"myForm\" >
      <form action=\"/action_page.php\" class=\"form-container\">
      \t<div class=\"row justify-content-center\">
        \t<h3 class=\"text-secondary\">Contact Us</h3>
    \t</div>
\t\t<div id=\"loginbox\">
\t\t\t<div class=\"row justify-content-center\" style=\"margin-top:40px;\">
        \t\t<label for=\"pseudo\" ><b class=\"text-secondary\">Enter a username</b></label>
    \t\t</div>
        \t<div class=\"row justify-content-center\">
        \t\t<input type=\"text\" placeholder=\"Enter pseudo\" name=\"pseudo\" id =\"pseudo\" style=\"width:80%;\" required>
    \t\t</div>
        \t<div class=\"row justify-content-center\">
        \t\t<button id=\"loginChoose\" class=\"btn btn-sm\" style=\"background-color:rgba(217, 217, 217); width:50%;\">
        \t\t\t<h4 style=\"color:#9D151C\">Next</h4>
    \t\t\t</button>
    \t\t</div>
\t\t</div>
\t\t
\t\t<div class=\"msg-container\" style=\"height:450px; overflow:auto;\">
\t\t\t<div class=\"msg-area\" id=\"message-area\"></div>
\t\t\t<div class=\"bottom\"><input type=\"text\" name=\"msginput\" class=\"msginput\" id=\"message\" onkeydown=\"if (event.keyCode == 13) sendMessage()\" value=\"\" placeholder=\"Enter your message here ... (Press enter to send message)\"></div>
        </div>
        <div class=\"row justify-content-center\">
\t\t\t<button type=\"button\" class=\"btn cancel\" style=\"background-color:rgba(217, 217, 217); width:50%;\">
\t\t\t\t<b style=\"color:#9D151C\">Close</b>
\t\t\t</button>
\t\t</div>
      </form>
    </div>
\t
\t<footer class=\"footer card bg-white border-0 shadow\" style=\"position:relative; bottom:0px; width:100%;\">
\t\t{% block footer %}
\t\t\t<p class=\"text-card text-secondary text-center\" style=\"margin-bottom:0.2%; margin-top:0.1%;\">
\t\t\t\t&copy; Copyright {{ 'now'|date('Y') }}
\t\t\t\t&middot; 
\t\t\t\t<a href=\"#\" style=\"color:#9D151C;\">About Us</a>
\t\t\t    &middot; 
\t\t\t    <a href=\"#\" style=\"color:#9D151C;\">Contact Us</a>
\t\t\t</p>
\t\t\t<p class=\"text-card text-secondary text-center\" style=\"margin-bottom:0.1%;\">
\t\t\t\tFind us on : 
\t\t\t\t<a href=\"#\">
\t\t\t\t\t<img src=\"/Images/instagram.png\" alt=\"\" height=\"20\" style=\"margin-left:0.8%; margin-right:0.8%; margin-bottom:0.2%;\"/>
\t\t\t\t</a>
\t\t\t\t<a href=\"#\">
\t\t\t\t\t<img src=\"/Images/twitter.png\" alt=\"\" height=\"20\" style=\"margin-bottom:0.2%; margin-right:0.8%;\"/>
\t\t\t\t</a>
\t\t\t\t<a href=\"#\">
\t\t\t\t\t<img src=\"/Images/linkedin.png\" alt=\"\" height=\"20\" style=\"margin-bottom:0.2%;\"/>
\t\t\t\t</a>
\t\t\t</p>
\t\t{% endblock %}
\t</footer>

\t<script>
\t
\t\t\$(document).ready(function(){
\t\t\tupdate();
\t\t});

\t\t\$(\"#contact\").click(function(){
\t\t\t\$(\".form-popup\").show();
\t\t\tcheckCookie();
\t\t});

\t\tfunction checkCookie()
\t\t{
\t\tif(document.cookie.indexOf(\"messengerUname\")==-1)
\t\t{
\t\t\tchooseUserName();\t
\t\t}else{
\t\t\t\$(\"#loginbox\").hide();
\t\t}
\t\t}


\t\tfunction chooseUserName()
\t\t{
\t\t\$(\"#loginbox\").show();
\t\t\$(\"#messagebox\").hide();
\t\t\$(\"#loginChoose\").click(function(){
\t\t\tevent.preventDefault();
\t\t\tvar user = \$(\"#pseudo\").val();
\t\t\tdocument.cookie = \"messengerUname=\"+user+\";SameSite=Strict\";
\t\t\tcheckCookie();
\t\t});
\t\t}\t

\t\tfunction getcookie(cname) {
\t\tvar name = cname + \"=\";
\t\tvar ca = document.cookie.split(';');
\t\tfor(var i=0; i<ca.length; i++) {
\t\t\tvar c = ca[i];
\t\t\twhile (c.charAt(0)==' ') c = c.substring(1);
\t\t\tif (c.indexOf(name) == 0) return c.substring(name.length,c.length);
\t\t}
\t\treturn \"\";
\t\t}

\t\tfunction escapehtml(text) {
\t\treturn text
\t\t\t.replace(/&/g, \"&amp;\")
\t\t\t.replace(/</g, \"&lt;\")
\t\t\t.replace(/>/g, \"&gt;\")
\t\t\t.replace(/\"/g, \"&quot;\")
\t\t\t.replace(/'/g, \"&#039;\");
\t\t}

\t\tfunction sendMessage()
\t\t{
\t\tevent.preventDefault();
\t\tvar message = \$(\"#message\").val();
\t\t//alert(message);
\t\tif (message != \"\") {
\t\t\tvar username = getcookie(\"messengerUname\");

\t\t\tvar xmlhttp=new XMLHttpRequest();

\t\t\txmlhttp.onreadystatechange=function() {
\t\t\t\tif (xmlhttp.readyState==4 && xmlhttp.status==200) {
\t\t\t\t\tmessage = escapehtml(message)
\t\t\t\t\tdocument.getElementById(\"messagebox\").innerHTML += \"<div class=\\\"msgc\\\" style=\\\"margin-bottom: 30px;\\\"> <div class=\\\"msg msgfrom\\\">\" + message + \"</div> <div class=\\\"msgarr msgarrfrom\\\"></div> <div class=\\\"msgsentby msgsentbyfrom\\\">Sent by \" + username + \"</div> </div>\";
\t\t\t\t}
\t\t\t}
\t\t\txmlhttp.open(\"GET\",\"/update_message?pseudo=\" + username + \"&message=\" + message,true);
\t\t\txmlhttp.send();
\t\t\t}
\t\t\$(\"#message\").val(\"\");
\t\t}

\t\t\$(\"#sendMessage\").click(function(){
\t\tevent.preventDefault();
\t\tsendMessage();
\t\tupdate();
\t\t});

\t\tfunction update()
\t\t{
\t\t\tvar xmlhttp=new XMLHttpRequest();
\t\tvar username = getcookie(\"messengerUname\");
\t\tvar output = \"\";
\t\txmlhttp.onreadystatechange=function() {
\t\t\tif (xmlhttp.readyState==4 && xmlhttp.status==200) {
\t\t\t\tvar response = xmlhttp.responseText.split(\"\\n\")
\t\t\t\tvar rl = response.length
\t\t\t\tvar item = \"\";
\t\t\t\tfor (var i = 0; i < rl; i++) {
\t\t\t\t\titem = response[i].split(\"\\\\\")
\t\t\t\t\tif (item[1] != undefined) {
\t\t\t\t\t\t//console.log('item 0 : ', item);
\t\t\t\t\t\tif (item[0] == username) {
\t\t\t\t\t\t\toutput += \"<div class=\\\"msgc\\\" style=\\\"margin-bottom: 30px;\\\"> <div class=\\\"msg msgfrom\\\">\" + item[1] + \"</div> <div class=\\\"msgarr msgarrfrom\\\"></div> <div class=\\\"msgsentby msgsentbyfrom\\\">Sent by \" + item[0] + \"</div> </div>\";
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\toutput += \"<div class=\\\"msgc\\\"> <div class=\\\"msg\\\">\" + item[1] + \"</div> <div class=\\\"msgarr\\\"></div> <div class=\\\"msgsentby\\\">Sent by \" + item[0] + \"</div> </div>\";
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}

\t\t\t\tdocument.getElementById(\"message-area\").innerHTML = output;
\t\t\t\tdocument.getElementById(\"message-area\").scrollTop = document.getElementById(\"message-area\").scrollHeight;

\t\t\t}
\t\t}
\t\t\txmlhttp.open(\"GET\",\"/get_message?username=\" + username,true);
\t\t\txmlhttp.send();
\t\t}

\t\t\$(\".cancel\").click(function(){
\t\t\$(\".form-popup\").hide();
\t\t});

\t\tsetInterval(function(){ update(); }, 500);


\t</script>
</body>

</html>", "layout.html.twig", "/Users/HoudaBerrada/Sites/advise_hack/templates/layout.html.twig");
    }
}
