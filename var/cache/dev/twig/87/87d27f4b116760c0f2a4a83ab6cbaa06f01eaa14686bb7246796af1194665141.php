<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* blog_post/index.html.twig */
class __TwigTemplate_0f059311d24fd1f382b54b771d297fee46fcb0a7cadb2ec3292855ca11a43874 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog_post/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog_post/index.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "blog_post/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "\t<div class=\"card text-center border-0\" style = \"margin-top: 85px;\">
    \t<h2 class=\"card-body\">Advise'Hack - The Blog</h2>
  \t</div>
\t<div class=\"card bg-transparent border-0 align-item-center\" style = \"margin-bottom:30px\">

\t\t<div class=\"card card-overlay bg-transparent border-0\" style=\"width:100%;\">
\t\t\t";
        // line 11
        if ((0 !== twig_compare(twig_length_filter($this->env, (isset($context["blog_posts"]) || array_key_exists("blog_posts", $context) ? $context["blog_posts"] : (function () { throw new RuntimeError('Variable "blog_posts" does not exist.', 11, $this->source); })())), 0))) {
            // line 12
            echo "\t\t    \t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["blog_posts"]) || array_key_exists("blog_posts", $context) ? $context["blog_posts"] : (function () { throw new RuntimeError('Variable "blog_posts" does not exist.', 12, $this->source); })())) - 1), 3));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 13
                echo "\t\t    \t<div class=\"container\" style=\"margin-bottom:40px;\">
\t            \t<div class=\"row justify-content-center\">
\t\t\t\t    \t";
                // line 15
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(0, 2));
                foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
                    // line 16
                    echo "\t\t\t\t    \t\t";
                    if (twig_get_attribute($this->env, $this->source, ($context["blog_posts"] ?? null), ($context["i"] + $context["j"]), [], "array", true, true, false, 16)) {
                        // line 17
                        echo "\t\t\t\t    \t\t\t<div class=\"col-sm-3 text-center\">
\t\t\t\t    \t\t\t\t<div class=\"row justify-content-center\">
\t\t\t\t    \t\t\t\t\t<div class=\"card bg-transparent text-center align-item-center border-0 rounded-0\" style=\"width:150px;height:120px; padding:5px;\">
\t\t\t\t\t      \t\t\t\t\t<img src=\"/Images/hackersphotos/";
                        // line 20
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["blog_posts"]) || array_key_exists("blog_posts", $context) ? $context["blog_posts"] : (function () { throw new RuntimeError('Variable "blog_posts" does not exist.', 20, $this->source); })()), ($context["i"] + $context["j"]), [], "array", false, false, false, 20), "photo", [], "any", false, false, false, 20), "html", null, true);
                        echo "\" alt=\"\" class=\"card-img\" />
\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t      \t\t\t\t</div>
\t\t\t\t      \t\t\t\t<div class=\"row justify-content-end\">
\t\t\t\t\t      \t\t\t\t<div class=\"text-secondary\" style=\"font-size:small;\">";
                        // line 24
                        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["blog_posts"]) || array_key_exists("blog_posts", $context) ? $context["blog_posts"] : (function () { throw new RuntimeError('Variable "blog_posts" does not exist.', 24, $this->source); })()), ($context["i"] + $context["j"]), [], "array", false, false, false, 24), "date", [], "any", false, false, false, 24), "m/d/Y"), "html", null, true);
                        echo "</div>
\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t      \t\t\t<div class=\"row justify-content-center\">
\t\t\t\t\t      \t\t\t\t<a href=\"";
                        // line 27
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("blog_post", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["blog_posts"]) || array_key_exists("blog_posts", $context) ? $context["blog_posts"] : (function () { throw new RuntimeError('Variable "blog_posts" does not exist.', 27, $this->source); })()), ($context["i"] + $context["j"]), [], "array", false, false, false, 27), "idBp", [], "any", false, false, false, 27)]), "html", null, true);
                        echo "\" class=\"text-secondary\" style=\"font-size:medium; font-weight:bold; margin-top:-5px; margin-bottom:5px;\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["blog_posts"]) || array_key_exists("blog_posts", $context) ? $context["blog_posts"] : (function () { throw new RuntimeError('Variable "blog_posts" does not exist.', 27, $this->source); })()), ($context["i"] + $context["j"]), [], "array", false, false, false, 27), "title", [], "any", false, false, false, 27), "html", null, true);
                        echo "</a>
\t\t\t\t\t      \t\t\t</div>
\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t    ";
                    } else {
                        // line 31
                        echo "\t\t\t\t\t\t    \t<div class=\"col-sm-3\"></div>
\t\t\t\t\t\t    ";
                    }
                    // line 33
                    echo "\t\t\t\t\t    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 34
                echo "\t\t\t\t    </div>
\t\t\t    </div>
\t    \t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 36
            echo " 
\t\t\t";
        }
        // line 38
        echo "\t\t</div>
\t</div>\t\t
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "blog_post/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 38,  139 => 36,  131 => 34,  125 => 33,  121 => 31,  112 => 27,  106 => 24,  99 => 20,  94 => 17,  91 => 16,  87 => 15,  83 => 13,  78 => 12,  76 => 11,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}
\t<div class=\"card text-center border-0\" style = \"margin-top: 85px;\">
    \t<h2 class=\"card-body\">Advise'Hack - The Blog</h2>
  \t</div>
\t<div class=\"card bg-transparent border-0 align-item-center\" style = \"margin-bottom:30px\">

\t\t<div class=\"card card-overlay bg-transparent border-0\" style=\"width:100%;\">
\t\t\t{% if blog_posts|length != 0 %}
\t\t    \t{% for i in range(0, blog_posts|length-1, 3) %}
\t\t    \t<div class=\"container\" style=\"margin-bottom:40px;\">
\t            \t<div class=\"row justify-content-center\">
\t\t\t\t    \t{% for j in 0..2 %}
\t\t\t\t    \t\t{% if blog_posts[i+j] is defined %}
\t\t\t\t    \t\t\t<div class=\"col-sm-3 text-center\">
\t\t\t\t    \t\t\t\t<div class=\"row justify-content-center\">
\t\t\t\t    \t\t\t\t\t<div class=\"card bg-transparent text-center align-item-center border-0 rounded-0\" style=\"width:150px;height:120px; padding:5px;\">
\t\t\t\t\t      \t\t\t\t\t<img src=\"/Images/hackersphotos/{{ blog_posts[i+j].photo}}\" alt=\"\" class=\"card-img\" />
\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t      \t\t\t\t</div>
\t\t\t\t      \t\t\t\t<div class=\"row justify-content-end\">
\t\t\t\t\t      \t\t\t\t<div class=\"text-secondary\" style=\"font-size:small;\">{{blog_posts[i+j].date|date(\"m/d/Y\")}}</div>
\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t      \t\t\t<div class=\"row justify-content-center\">
\t\t\t\t\t      \t\t\t\t<a href=\"{{ url('blog_post', {'id' : blog_posts[i+j].idBp}) }}\" class=\"text-secondary\" style=\"font-size:medium; font-weight:bold; margin-top:-5px; margin-bottom:5px;\">{{blog_posts[i+j].title}}</a>
\t\t\t\t\t      \t\t\t</div>
\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t    {% else %}
\t\t\t\t\t\t    \t<div class=\"col-sm-3\"></div>
\t\t\t\t\t\t    {% endif %}
\t\t\t\t\t    {% endfor %}
\t\t\t\t    </div>
\t\t\t    </div>
\t    \t\t{% endfor %} 
\t\t\t{% endif %}
\t\t</div>
\t</div>\t\t
{% endblock %}", "blog_post/index.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\blog_post\\index.html.twig");
    }
}
