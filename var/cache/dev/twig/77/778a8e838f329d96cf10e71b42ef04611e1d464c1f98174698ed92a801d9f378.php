<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* blog_post/post.html.twig */
class __TwigTemplate_540e067b8b87151395819b508236d400f7f4201e6df30357b137cd585b5c8585 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'javascript' => [$this, 'block_javascript'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog_post/post.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog_post/post.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "blog_post/post.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        // line 4
        echo "    <script src=\"https://cdn.tiny.cloud/1/1r3xno60hf2olb3nwcalbv7ow89y17xu0n7a8s7fxqpztm09/tinymce/5/tinymce.min.js\" referrerpolicy=\"origin\"/></script>
    <script src=\"https://code.jquery.com/jquery-3.5.1.slim.min.js\" integrity=\"sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj\" crossorigin=\"anonymous\"></script>
\t<script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js\" integrity=\"sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN\" crossorigin=\"anonymous\"></script>
\t<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js\" integrity=\"sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV\" crossorigin=\"anonymous\"></script>
\t<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
  \t<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js\"></script>
  \t<script>
    tinymce.init({
        selector: '#comment',
        setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
        }
    });
  </script>
  <script>
    tinymce.init({
        selector: '#reply',
        setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
        }
    });
  </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 32
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 33
        echo "\t<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t\t<div class=\"card text-center border-0 bg-transparent shadow rounded-0\" style = \"margin-top: 85px; width:100%;\">
\t    \t<h2 class=\"card-body text-white\">Advise'Hack - The Blog</h2>
\t  \t</div>
  \t</div>
\t  \t";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 38, $this->source); })()), "flashes", [0 => "message"], "method", false, false, false, 38));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            echo " 
\t\t\t<div class=\" form-group text-center\">
\t\t\t\t<div class=\"alert alert-success mx-auto\" style=\"margin-top:5px; margin-bottom:-10px; width:40%;\" role=\"alert\"> ";
            // line 40
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
\t\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "
  \t<div class=\"card shadow border-0 mx-auto\" style=\"width:80%; margin-top:10px; padding-right:60px; padding-left:20px; margin-bottom:30px;\">
\t\t<div class=\"container text-center\" style=\"margin-top:10px; margin-bottom:40px;\">
\t  \t\t<div class=\"row justify-content-center\" style=\"margin-top:30px; margin-bottom:20px; color:#9D151C;\">
\t  \t\t\t<div class=\"col\">
  \t\t\t\t</div>
  \t\t\t\t<div class=\"col\">
\t  \t\t\t\t<h3>";
        // line 50
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 50, $this->source); })()), "title", [], "any", false, false, false, 50), "html", null, true);
        echo "</h3>\t
  \t\t\t\t</div>
  \t\t\t\t<div class=\"col\">
  \t\t\t\t\t<div class=\"row justify-content-end\">
\t\t                <a href=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("edit_post", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 54, $this->source); })()), "idBp", [], "any", false, false, false, 54)]), "html", null, true);
        echo "\">
\t\t  \t\t\t\t\t<div class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C; margin-top:10px; width:100px;\">Edit Post</div>
\t\t  \t\t\t\t</a>
\t  \t\t\t\t</div>
\t  \t\t\t\t<div class=\"row justify-content-end\">
\t\t  \t\t\t\t<a href=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("delete_post", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 59, $this->source); })()), "idBp", [], "any", false, false, false, 59)]), "html", null, true);
        echo "\">
\t\t  \t\t\t\t\t<div class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C; margin-top:10px; width:100px;\">Delete Post</div>
\t\t  \t\t\t\t</a>
\t  \t\t\t\t</div>
  \t\t\t\t</div>
\t\t\t</div>

\t  \t\t<div class=\"row justify-content-center\">
\t\t\t\t<img src=\"/Images/hackersphotos/";
        // line 67
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 67, $this->source); })()), "photo", [], "any", false, false, false, 67), "html", null, true);
        echo "\" height=\"100\" alt=\"\" class=\"rounded\" style=\"width:50%; height:50%; margin-bottom:20px;\" />
\t\t\t</div>

\t\t\t<div class=\"row text-secondary d-flex justify-content-end\" style=\"font-size:small;\">
\t\t\t\t";
        // line 71
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 71, $this->source); })()), "date", [], "any", false, false, false, 71), "m/d/Y"), "html", null, true);
        echo "
\t\t\t</div>
\t\t\t";
        // line 73
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 73, $this->source); })()), "idHacker", [], "any", false, false, false, 73))) {
            // line 74
            echo "\t\t\t\t<div class=\"row text-secondary d-flex justify-content-end\" style=\"font-size:small;\">
\t\t\t\t\tWritten by &nbsp; <a href=\"";
            // line 75
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("hacker_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 75, $this->source); })()), "idHacker", [], "any", false, false, false, 75), "idHacker", [], "any", false, false, false, 75)]), "html", null, true);
            echo "\" style=\"color:#9D151C;\"> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 75, $this->source); })()), "idHacker", [], "any", false, false, false, 75), "username", [], "any", false, false, false, 75), "html", null, true);
            echo "</a>
\t\t\t\t</div>
\t\t\t";
        } elseif ( !(null === twig_get_attribute($this->env, $this->source,         // line 77
(isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 77, $this->source); })()), "idEntreprise", [], "any", false, false, false, 77))) {
            // line 78
            echo "\t\t\t\t<div class=\"row text-secondary\" style=\"font-size:small;\">
\t\t\t\t\tWritten by &nbsp; <a href=\"";
            // line 79
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprise_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 79, $this->source); })()), "idEntreprise", [], "any", false, false, false, 79), "idEntreprise", [], "any", false, false, false, 79)]), "html", null, true);
            echo "\" style=\"color:#9D151C;\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 79, $this->source); })()), "idEntreprise", [], "any", false, false, false, 79), "nameEnt", [], "any", false, false, false, 79), "html", null, true);
            echo "</a>
\t\t\t\t</div>
\t\t\t";
        }
        // line 82
        echo "\t\t\t<div class=\"row justify-content-start\" style=\"margin-left:30px;\">
\t\t\t\t<div>";
        // line 83
        echo twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 83, $this->source); })()), "content", [], "any", false, false, false, 83);
        echo "</div>
\t\t\t</div>
\t\t</div>
\t\t\t<! -- ------------------------------ Comments ------------------------------ --> 
\t\t<div class=\"container\" style=\"margin-top:10px; margin-bottom:40px;\">
\t\t\t<div class=\"row justify-content-start\" style=\"margin-left:20px;\">
\t\t\t\t<h5 class=\"text-secondary font-weight-bold\">Comments</h5>
\t\t\t</div>

\t\t\t";
        // line 92
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 92, $this->source); })()), "user", [], "any", false, false, false, 92))) {
            echo "\t
\t\t\t\t";
            // line 93
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["CommentForm"]) || array_key_exists("CommentForm", $context) ? $context["CommentForm"] : (function () { throw new RuntimeError('Variable "CommentForm" does not exist.', 93, $this->source); })()), 'form_start');
            echo "
\t\t\t\t\t<div class=\"row\" style=\"margin-left:20px;\">
\t\t\t\t\t\t<a class=\"text-secondary font-weight-bold\" style = \"font-size:medium;\">
\t\t\t\t\t\t    Leave a comment
\t\t\t\t\t  \t</a>
\t\t\t\t\t</div>
\t                <div class=\"row\" style=\"margin-left:20px;\">
\t                    ";
            // line 100
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["CommentForm"]) || array_key_exists("CommentForm", $context) ? $context["CommentForm"] : (function () { throw new RuntimeError('Variable "CommentForm" does not exist.', 100, $this->source); })()), "content", [], "any", false, false, false, 100), 'widget', ["id" => "comment"]);
            echo "
\t                </div>
\t                <div class=\"row\" style=\"margin-left:20px;\">
\t                    <button type=\"submit\" class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C; margin-top:10px; margin-bottom:30px;\">Submit comment</button>
\t            \t</div>
            \t";
            // line 105
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["CommentForm"]) || array_key_exists("CommentForm", $context) ? $context["CommentForm"] : (function () { throw new RuntimeError('Variable "CommentForm" does not exist.', 105, $this->source); })()), "_token", [], "any", false, false, false, 105), 'widget');
            echo "
\t\t\t\t";
            // line 106
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["CommentForm"]) || array_key_exists("CommentForm", $context) ? $context["CommentForm"] : (function () { throw new RuntimeError('Variable "CommentForm" does not exist.', 106, $this->source); })()), 'form_end', ["render_rest" => false]);
            echo "
\t\t\t";
        } else {
            // line 108
            echo "\t\t\t\t<div class=\"row\" style=\"margin-left:20px;\">
\t\t\t\t\t<a href=\"";
            // line 109
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("sign_in");
            echo "\" class=\"text-secondary font-weight-bold\" style = \"font-size:medium;\">
\t\t\t\t\t    Log in to leave a comment
\t\t\t\t  \t</a>
\t\t\t\t</div>
\t\t\t";
        }
        // line 114
        echo "
\t\t\t";
        // line 115
        if ((0 !== twig_compare(twig_length_filter($this->env, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 115, $this->source); })())), 0))) {
            // line 116
            echo "\t\t    \t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 116, $this->source); })())) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 117
                echo "\t\t    \t\t<div class=\"card shadow border-0 rounded-0\" style=\"width:70%; margin-top:10px; padding-top:10px; padding-right:20px; padding-left:20px; margin-bottom:30px;\">

\t\t    \t\t\t<div class=\"row text-secondary\" style=\"margin-left:20px; font-size:medium;\">
\t\t\t\t    \t\t";
                // line 120
                if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 120, $this->source); })()), $context["i"], [], "array", false, false, false, 120), "idHacker", [], "any", false, false, false, 120))) {
                    // line 121
                    echo "\t\t\t    \t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("hacker_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 121, $this->source); })()), $context["i"], [], "array", false, false, false, 121), "idHacker", [], "any", false, false, false, 121), "idHacker", [], "any", false, false, false, 121)]), "html", null, true);
                    echo "\" class=\"font-weight-bold\" style=\"color:#9D151C;\"> ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 121, $this->source); })()), $context["i"], [], "array", false, false, false, 121), "idHacker", [], "any", false, false, false, 121), "username", [], "any", false, false, false, 121), "html", null, true);
                    echo "</a> &nbsp; says :
\t\t\t\t    \t\t";
                } elseif ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 122
(isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 122, $this->source); })()), $context["i"], [], "array", false, false, false, 122), "idEntreprise", [], "any", false, false, false, 122))) {
                    // line 123
                    echo "\t\t\t    \t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprise_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 123, $this->source); })()), $context["i"], [], "array", false, false, false, 123), "idEntreprise", [], "any", false, false, false, 123), "idEntreprise", [], "any", false, false, false, 123)]), "html", null, true);
                    echo "\" class=\"font-weight-bold\" style=\"color:#9D151C;\"> ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 123, $this->source); })()), $context["i"], [], "array", false, false, false, 123), "idEntreprise", [], "any", false, false, false, 123), "nameEnt", [], "any", false, false, false, 123), "html", null, true);
                    echo "</a> &nbsp; says :
\t\t\t\t    \t\t";
                }
                // line 125
                echo "\t\t\t    \t\t\t<div class=\"text-secondary\" style=\"margin-left:10px; font-size:medium;\">
\t\t\t    \t\t\t\t<a href=\"";
                // line 126
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("delete_comment", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 126, $this->source); })()), $context["i"], [], "array", false, false, false, 126), "idComment", [], "any", false, false, false, 126)]), "html", null, true);
                echo "\" class=\"font-weight-bold\" style=\"color:#9D151C;\">
\t\t\t    \t\t\t\t\t<svg width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" class=\"bi bi-x-circle-fill\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.146-3.146a.5.5 0 0 0-.708-.708L8 7.293 4.854 4.146a.5.5 0 1 0-.708.708L7.293 8l-3.147 3.146a.5.5 0 0 0 .708.708L8 8.707l3.146 3.147a.5.5 0 0 0 .708-.708L8.707 8l3.147-3.146z\"/>
\t\t\t\t\t\t\t\t\t</svg>
\t\t\t    \t\t\t\t</a>
\t\t    \t\t\t\t</div>
\t    \t\t\t\t</div>

\t\t\t    \t\t<div class=\"row text-secondary\" style=\"font-size:small; margin-left:10px;\">
\t\t\t\t\t\t\t";
                // line 135
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 135, $this->source); })()), $context["i"], [], "array", false, false, false, 135), "date", [], "any", false, false, false, 135), "l d F Y"), "html", null, true);
                echo "
\t\t\t\t\t\t</div>
\t\t\t    \t\t<div class=\"row\" style=\"margin-left:20px;\">
\t\t\t\t\t\t\t<div>";
                // line 138
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 138, $this->source); })()), $context["i"], [], "array", false, false, false, 138), "content", [], "any", false, false, false, 138);
                echo "</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                // line 190
                echo "
\t\t\t\t\t</div>
\t\t    \t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 192
            echo " 
\t\t\t";
        }
        // line 194
        echo "\t\t</div>
\t</div>\t\t
\t<script>
\t\t\$(document).ready(function(){
\t\t   \$(\"#showit\").click(function(){
\t\t       \$(\"#replyForm\").css(\"display\",\"block\");
\t\t   });
\t\t});
\t</script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "blog_post/post.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  339 => 194,  335 => 192,  327 => 190,  322 => 138,  316 => 135,  304 => 126,  301 => 125,  293 => 123,  291 => 122,  284 => 121,  282 => 120,  277 => 117,  272 => 116,  270 => 115,  267 => 114,  259 => 109,  256 => 108,  251 => 106,  247 => 105,  239 => 100,  229 => 93,  225 => 92,  213 => 83,  210 => 82,  202 => 79,  199 => 78,  197 => 77,  190 => 75,  187 => 74,  185 => 73,  180 => 71,  173 => 67,  162 => 59,  154 => 54,  147 => 50,  138 => 43,  129 => 40,  122 => 38,  115 => 33,  105 => 32,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}

{% block javascript %}
    <script src=\"https://cdn.tiny.cloud/1/1r3xno60hf2olb3nwcalbv7ow89y17xu0n7a8s7fxqpztm09/tinymce/5/tinymce.min.js\" referrerpolicy=\"origin\"/></script>
    <script src=\"https://code.jquery.com/jquery-3.5.1.slim.min.js\" integrity=\"sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj\" crossorigin=\"anonymous\"></script>
\t<script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js\" integrity=\"sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN\" crossorigin=\"anonymous\"></script>
\t<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js\" integrity=\"sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV\" crossorigin=\"anonymous\"></script>
\t<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
  \t<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js\"></script>
  \t<script>
    tinymce.init({
        selector: '#comment',
        setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
        }
    });
  </script>
  <script>
    tinymce.init({
        selector: '#reply',
        setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
        }
    });
  </script>
{% endblock %}

{% block body %}
\t<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t\t<div class=\"card text-center border-0 bg-transparent shadow rounded-0\" style = \"margin-top: 85px; width:100%;\">
\t    \t<h2 class=\"card-body text-white\">Advise'Hack - The Blog</h2>
\t  \t</div>
  \t</div>
\t  \t{% for message in app.flashes('message') %} 
\t\t\t<div class=\" form-group text-center\">
\t\t\t\t<div class=\"alert alert-success mx-auto\" style=\"margin-top:5px; margin-bottom:-10px; width:40%;\" role=\"alert\"> {{message}}</div>
\t\t\t</div>
\t\t{% endfor %}

  \t<div class=\"card shadow border-0 mx-auto\" style=\"width:80%; margin-top:10px; padding-right:60px; padding-left:20px; margin-bottom:30px;\">
\t\t<div class=\"container text-center\" style=\"margin-top:10px; margin-bottom:40px;\">
\t  \t\t<div class=\"row justify-content-center\" style=\"margin-top:30px; margin-bottom:20px; color:#9D151C;\">
\t  \t\t\t<div class=\"col\">
  \t\t\t\t</div>
  \t\t\t\t<div class=\"col\">
\t  \t\t\t\t<h3>{{post.title}}</h3>\t
  \t\t\t\t</div>
  \t\t\t\t<div class=\"col\">
  \t\t\t\t\t<div class=\"row justify-content-end\">
\t\t                <a href=\"{{ url('edit_post', {'id' : post.idBp}) }}\">
\t\t  \t\t\t\t\t<div class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C; margin-top:10px; width:100px;\">Edit Post</div>
\t\t  \t\t\t\t</a>
\t  \t\t\t\t</div>
\t  \t\t\t\t<div class=\"row justify-content-end\">
\t\t  \t\t\t\t<a href=\"{{ url('delete_post', {'id' : post.idBp}) }}\">
\t\t  \t\t\t\t\t<div class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C; margin-top:10px; width:100px;\">Delete Post</div>
\t\t  \t\t\t\t</a>
\t  \t\t\t\t</div>
  \t\t\t\t</div>
\t\t\t</div>

\t  \t\t<div class=\"row justify-content-center\">
\t\t\t\t<img src=\"/Images/hackersphotos/{{post.photo}}\" height=\"100\" alt=\"\" class=\"rounded\" style=\"width:50%; height:50%; margin-bottom:20px;\" />
\t\t\t</div>

\t\t\t<div class=\"row text-secondary d-flex justify-content-end\" style=\"font-size:small;\">
\t\t\t\t{{post.date|date(\"m/d/Y\")}}
\t\t\t</div>
\t\t\t{% if post.idHacker is not null %}
\t\t\t\t<div class=\"row text-secondary d-flex justify-content-end\" style=\"font-size:small;\">
\t\t\t\t\tWritten by &nbsp; <a href=\"{{ url('hacker_show', {'id' : post.idHacker.idHacker}) }}\" style=\"color:#9D151C;\"> {{post.idHacker.username}}</a>
\t\t\t\t</div>
\t\t\t{% elseif post.idEntreprise is not null %}
\t\t\t\t<div class=\"row text-secondary\" style=\"font-size:small;\">
\t\t\t\t\tWritten by &nbsp; <a href=\"{{ url('entreprise_show', {'id' : post.idEntreprise.idEntreprise}) }}\" style=\"color:#9D151C;\">{{post.idEntreprise.nameEnt}}</a>
\t\t\t\t</div>
\t\t\t{% endif %}
\t\t\t<div class=\"row justify-content-start\" style=\"margin-left:30px;\">
\t\t\t\t<div>{{post.content|raw}}</div>
\t\t\t</div>
\t\t</div>
\t\t\t<! -- ------------------------------ Comments ------------------------------ --> 
\t\t<div class=\"container\" style=\"margin-top:10px; margin-bottom:40px;\">
\t\t\t<div class=\"row justify-content-start\" style=\"margin-left:20px;\">
\t\t\t\t<h5 class=\"text-secondary font-weight-bold\">Comments</h5>
\t\t\t</div>

\t\t\t{% if app.user is not null %}\t
\t\t\t\t{{ form_start(CommentForm) }}
\t\t\t\t\t<div class=\"row\" style=\"margin-left:20px;\">
\t\t\t\t\t\t<a class=\"text-secondary font-weight-bold\" style = \"font-size:medium;\">
\t\t\t\t\t\t    Leave a comment
\t\t\t\t\t  \t</a>
\t\t\t\t\t</div>
\t                <div class=\"row\" style=\"margin-left:20px;\">
\t                    {{ form_widget(CommentForm.content, {'id': 'comment'}) }}
\t                </div>
\t                <div class=\"row\" style=\"margin-left:20px;\">
\t                    <button type=\"submit\" class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C; margin-top:10px; margin-bottom:30px;\">Submit comment</button>
\t            \t</div>
            \t{{ form_widget(CommentForm._token) }}
\t\t\t\t{{ form_end(CommentForm, {'render_rest': false}) }}
\t\t\t{% else %}
\t\t\t\t<div class=\"row\" style=\"margin-left:20px;\">
\t\t\t\t\t<a href=\"{{ url('sign_in') }}\" class=\"text-secondary font-weight-bold\" style = \"font-size:medium;\">
\t\t\t\t\t    Log in to leave a comment
\t\t\t\t  \t</a>
\t\t\t\t</div>
\t\t\t{% endif %}

\t\t\t{% if comments|length != 0 %}
\t\t    \t{% for i in 0..comments|length - 1 %}
\t\t    \t\t<div class=\"card shadow border-0 rounded-0\" style=\"width:70%; margin-top:10px; padding-top:10px; padding-right:20px; padding-left:20px; margin-bottom:30px;\">

\t\t    \t\t\t<div class=\"row text-secondary\" style=\"margin-left:20px; font-size:medium;\">
\t\t\t\t    \t\t{% if comments[i].idHacker is not null %}
\t\t\t    \t\t\t\t<a href=\"{{ url('hacker_show', {'id' : comments[i].idHacker.idHacker}) }}\" class=\"font-weight-bold\" style=\"color:#9D151C;\"> {{comments[i].idHacker.username}}</a> &nbsp; says :
\t\t\t\t    \t\t{% elseif comments[i].idEntreprise is not null  %}
\t\t\t    \t\t\t\t<a href=\"{{ url('entreprise_show', {'id' : comments[i].idEntreprise.idEntreprise}) }}\" class=\"font-weight-bold\" style=\"color:#9D151C;\"> {{comments[i].idEntreprise.nameEnt}}</a> &nbsp; says :
\t\t\t\t    \t\t{% endif %}
\t\t\t    \t\t\t<div class=\"text-secondary\" style=\"margin-left:10px; font-size:medium;\">
\t\t\t    \t\t\t\t<a href=\"{{ url('delete_comment', {'id' : comments[i].idComment}) }}\" class=\"font-weight-bold\" style=\"color:#9D151C;\">
\t\t\t    \t\t\t\t\t<svg width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" class=\"bi bi-x-circle-fill\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.146-3.146a.5.5 0 0 0-.708-.708L8 7.293 4.854 4.146a.5.5 0 1 0-.708.708L7.293 8l-3.147 3.146a.5.5 0 0 0 .708.708L8 8.707l3.146 3.147a.5.5 0 0 0 .708-.708L8.707 8l3.147-3.146z\"/>
\t\t\t\t\t\t\t\t\t</svg>
\t\t\t    \t\t\t\t</a>
\t\t    \t\t\t\t</div>
\t    \t\t\t\t</div>

\t\t\t    \t\t<div class=\"row text-secondary\" style=\"font-size:small; margin-left:10px;\">
\t\t\t\t\t\t\t{{comments[i].date|date(\"l d F Y\")}}
\t\t\t\t\t\t</div>
\t\t\t    \t\t<div class=\"row\" style=\"margin-left:20px;\">
\t\t\t\t\t\t\t<div>{{comments[i].content|raw}}</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t{#
\t\t\t\t\t\t{% if app.user is not null %}
\t\t\t\t\t\t\t<div class=\"row justify-content-end\" style=\"margin-left:20px;\">
\t\t\t\t\t\t\t\t<div id=\"showit\" class=\"text-secondary font-weight-bold\" style = \"font-size:medium;\">
\t\t\t\t\t\t\t\t    Reply to this answer
\t\t\t\t\t\t\t  \t</div>
\t\t\t\t\t\t  \t</div>
\t\t\t\t\t\t  \t<div id=\"replyForm\" style=\"display:none\">
\t\t\t\t\t\t\t  \t{{ form_start(ReplyForms[i]) }}
\t\t\t\t\t\t\t\t  \t<div class=\"row justify-content-end\" style=\"margin-left:20px;\">
\t\t\t\t\t\t\t\t  \t\t{{ form_widget(ReplyForms[i].content) }} 
\t\t\t\t\t\t\t\t  \t</div>
\t\t\t\t\t\t\t\t  \t<div class=\"row justify-content-end\" style=\"margin-left:20px;\">
\t\t\t\t\t                    <button type=\"submit\" class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C; margin-top:10px; margin-bottom:30px;\">Submit reply</button>
\t\t\t\t\t            \t</div>
\t\t\t\t            \t{{ form_end(ReplyForms[i]) }}
\t\t\t            \t</div>
\t\t\t\t\t  \t{% else %}
\t\t\t\t\t  \t\t<div class=\"row justify-content-end\" style=\"margin-left:20px;\">
\t\t\t\t\t\t\t\t<a href=\"{{ url('sign_in') }}\" class=\"text-secondary font-weight-bold\" style = \"font-size:medium;\">
\t\t\t\t\t\t\t\t    Log in to reply
\t\t\t\t\t\t\t  \t</a>
\t\t\t\t\t\t  \t</div>
\t\t\t\t\t  \t{% endif %}
\t\t\t\t\t\t
\t\t\t\t\t\t

\t\t\t\t\t\t{% if replies|length != 0 %}
\t\t    \t\t\t\t{% for j in 0..replies|length - 1 %}
\t\t    \t\t\t\t\t{% if replies[j].idComment.idComment == comments[i].idComment %}
\t\t    \t\t\t\t\t\t<div class=\"card card-overlay border-0 rounded-0\" style=\"width:70%; margin-top:10px; padding-right:20px; padding-left:20px; margin-bottom:10px; background-color:#D9D9D9\">
\t\t\t\t\t\t\t    \t\t{% if replies[j].idHacker is not null %}
\t\t\t\t\t\t\t    \t\t\t<div class=\"row text-secondary\" style=\"margin-left:10px; font-size:medium;\">
\t\t\t\t\t\t\t    \t\t\t\t<a href=\"{{ url('hacker_show', {'id' : replies[j].idHacker.idHacker}) }}\" class=\"font-weight-bold\" style=\"color:#9D151C;\"> {{replies[j].idHacker.username}}</a> &nbsp; replied :
\t\t\t\t\t\t    \t\t\t\t</div>
\t\t\t\t\t\t\t    \t\t{% elseif replies[j].idEntreprise is not null %}
\t\t\t\t\t\t\t    \t\t\t<div class=\"row text-secondary\" style=\"margin-left:10px; font-size:medium;\">
\t\t\t\t\t\t\t    \t\t\t\t<a href=\"{{ url('entreprise_show', {'id' : replies[j].idEntreprise.idEntreprise}) }}\" class=\"font-weight-bold\" style=\"color:#9D151C;\"> {{replies[j].idEntreprise.nameEnt}}</a> &nbsp; replied :
\t\t\t\t\t\t    \t\t\t\t</div>
\t\t\t\t\t\t\t    \t\t{% endif %}
\t\t\t\t\t\t\t    \t\t<div class=\"row text-secondary\" style=\"font-size:small; margin-left:10px;\">
\t\t\t\t\t\t\t\t\t\t\t{{replies[j].date|date(\"l d F Y\")}}
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t    \t\t<div class=\"row\" style=\"margin-left:20px;\">
\t\t\t\t\t\t\t\t\t\t\t<div>{{replies[j].content|raw}}</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t    \t\t</div>
\t\t    \t\t\t\t\t{% endif %}
\t\t    \t\t\t\t{% endfor %} 
\t\t\t\t\t\t{% endif %} #}

\t\t\t\t\t</div>
\t\t    \t{% endfor %} 
\t\t\t{% endif %}
\t\t</div>
\t</div>\t\t
\t<script>
\t\t\$(document).ready(function(){
\t\t   \$(\"#showit\").click(function(){
\t\t       \$(\"#replyForm\").css(\"display\",\"block\");
\t\t   });
\t\t});
\t</script>

{% endblock %}", "blog_post/post.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\blog_post\\post.html.twig");
    }
}
