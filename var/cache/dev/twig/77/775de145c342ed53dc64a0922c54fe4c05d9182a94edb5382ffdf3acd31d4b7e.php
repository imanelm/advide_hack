<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* hacker/show.html.twig */
class __TwigTemplate_758b551830409a57a2cdce288dde64c47d3150ef7b9134a308600a27b869225a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/show.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "hacker/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo " 
        \t
\t<div class=\"card border-0 bg-transparent\" style=\"margin-top:90px\">
\t\t<div class=\"tab-content py-4 text-center\">
\t    \t<div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(256, 256, 256, 0.5); width:100%;\">
\t    \t\t<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t\t            <div class=\"card-body\">
\t\t            \t<div class=\"row justify-content-center d-flex align-items-center\">
\t\t\t\t\t    \t<div class=\"col-3\">
\t\t\t\t\t\t     \t<h3 class=\"text-title text-white text-center\">";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 14, $this->source); })()), "hacker", [], "array", false, false, false, 14), "username", [], "any", false, false, false, 14), "html", null, true);
        echo "'s Profile</h3>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t</div>
\t\t            </div>
\t\t        </div>
\t      </div>
\t    </div>
\t    <div class=\"tab-content py-4\">
\t    \t<div class=\"card-overlay border-0 bg-white shadow mx-auto\" style=\"width:60%;\">
\t\t        <div class=\"text-card text-center\" style=\"margin-bottom:30px\">
\t\t        \t<img src=\"/Images/hackersphotos/";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 24, $this->source); })()), "hacker", [], "array", false, false, false, 24), "photo", [], "any", false, false, false, 24), "html", null, true);
        echo "\" alt=\"avatar\" style = \"width:150px; margin-top:30px;\">
\t\t        </div>
\t\t        <div class=\"row\">                  
\t\t            <div class=\" mx-auto\">
\t\t              
\t\t                <table class=\"table table-sm table-borderless\">
\t\t                    <tbody>
\t\t\t\t\t\t\t\t<tr>
\t\t                            <td>
\t\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\">Username : </strong> <div style=\"float:right\">";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 33, $this->source); })()), "hacker", [], "array", false, false, false, 33), "username", [], "any", false, false, false, 33), "html", null, true);
        echo "</div>
\t\t                            </td>
\t\t                        </tr>
\t\t                        <tr>
\t\t                            <td>
\t\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\">First Name : </strong> <div style=\"float:right\">";
        // line 38
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 38, $this->source); })()), "hacker", [], "array", false, false, false, 38), "name", [], "any", false, false, false, 38), "html", null, true);
        echo "</div>
\t\t                            </td>
\t\t                        </tr>
\t\t                        <tr>
\t\t                            <td>
\t\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\">Last Name : </strong> <div style=\"float:right\">";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 43, $this->source); })()), "hacker", [], "array", false, false, false, 43), "fName", [], "any", false, false, false, 43), "html", null, true);
        echo "</div>
\t\t                            </td>
\t\t                        </tr>

\t\t                        <tr>
\t\t                            <td>
\t\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\"g>Country : </strong> <div style=\"float:right\">";
        // line 49
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 49, $this->source); })()), "hacker", [], "array", false, false, false, 49), "country", [], "any", false, false, false, 49), "html", null, true);
        echo "</div>
\t\t                            </td>
\t\t                        </tr>
\t\t\t\t\t\t\t\t<tr>
\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Mark : </strong> <div style=\"float:right\">";
        // line 54
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 54, $this->source); })()), "hacker", [], "array", false, false, false, 54), "mark", [], "any", false, false, false, 54), "html", null, true);
        echo "</div>
\t\t                            </td>
\t\t                        </tr>

\t\t\t\t\t\t\t\t";
        // line 58
        if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 58, $this->source); })()), "hacker", [], "array", false, false, false, 58), "adress", [], "any", false, false, false, 58))) {
            // line 59
            echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Adress : </strong> <div style=\"float:right\">";
            // line 61
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 61, $this->source); })()), "hacker", [], "array", false, false, false, 61), "adress", [], "any", false, false, false, 61), "html", null, true);
            echo "</div>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    ";
        }
        // line 65
        echo "\t\t\t                    ";
        if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 65, $this->source); })()), "hacker", [], "array", false, false, false, 65), "site", [], "any", false, false, false, 65))) {
            // line 66
            echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Website : </strong> 
\t\t\t\t\t\t\t\t\t\t\t<a href=\"https://twitter.com/";
            // line 69
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 69, $this->source); })()), "hacker", [], "array", false, false, false, 69), "twitter", [], "any", false, false, false, 69), "html", null, true);
            echo "\" style=\"float:right; color:#000000;\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 69, $this->source); })()), "hacker", [], "array", false, false, false, 69), "site", [], "any", false, false, false, 69), "html", null, true);
            echo "</a>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    ";
        }
        // line 73
        echo "\t\t\t                    ";
        if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 73, $this->source); })()), "hacker", [], "array", false, false, false, 73), "twitter", [], "any", false, false, false, 73))) {
            // line 74
            echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Twitter : </strong> 
\t\t\t\t\t\t\t\t\t\t\t<a href=\"https://twitter.com/";
            // line 77
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 77, $this->source); })()), "hacker", [], "array", false, false, false, 77), "twitter", [], "any", false, false, false, 77), "html", null, true);
            echo "\" style=\"float:right; color:#000000;\">@";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 77, $this->source); })()), "hacker", [], "array", false, false, false, 77), "twitter", [], "any", false, false, false, 77), "html", null, true);
            echo "</a>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    ";
        }
        // line 81
        echo "\t\t\t                    ";
        if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 81, $this->source); })()), "hacker", [], "array", false, false, false, 81), "linkedin", [], "any", false, false, false, 81))) {
            // line 82
            echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> LinkedIn : </strong> 
\t\t\t\t\t\t\t\t\t\t\t<a href=\"https://twitter.com/";
            // line 85
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 85, $this->source); })()), "hacker", [], "array", false, false, false, 85), "twitter", [], "any", false, false, false, 85), "html", null, true);
            echo "\" style=\"float:right; color:#000000;\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 85, $this->source); })()), "hacker", [], "array", false, false, false, 85), "linkedin", [], "any", false, false, false, 85), "html", null, true);
            echo "</a>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    ";
        }
        // line 89
        echo "\t\t\t                    ";
        if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 89, $this->source); })()), "hacker", [], "array", false, false, false, 89), "github", [], "any", false, false, false, 89))) {
            // line 90
            echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> GitHub : </strong> <div style=\"float:right\">";
            // line 92
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 92, $this->source); })()), "hacker", [], "array", false, false, false, 92), "github", [], "any", false, false, false, 92), "html", null, true);
            echo "</div>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    ";
        }
        // line 96
        echo "\t\t                    </tbody>
\t\t                </table>
\t\t                <div class=\"row justify-content-center \">
\t\t\t                ";
        // line 99
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 100
            echo "\t\t\t\t                ";
            if ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 100, $this->source); })()), "user", [], "any", false, false, false, 100), "idHacker", [], "any", false, false, false, 100), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 100, $this->source); })()), "hacker", [], "array", false, false, false, 100), "idHacker", [], "any", false, false, false, 100)))) {
                // line 101
                echo "\t\t\t\t\t                <div class=\"col-5\">
\t\t\t\t\t\t                    <a href=\"";
                // line 102
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("hacker_delete", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 102, $this->source); })()), "hacker", [], "array", false, false, false, 102), "idHacker", [], "any", false, false, false, 102)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t                        <button type=\"button\" class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); height:35px; margin-bottom:10px\">
\t\t\t\t\t\t                            <div style=\"color:#9D151C; font-weight:bold;\">Delete</div>
\t\t\t\t\t\t                        </button>
\t\t\t\t\t\t                    </a>
\t\t\t\t\t\t            </div>
\t\t\t\t\t            </div>
\t\t\t\t\t\t            ";
                // line 109
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 109, $this->source); })()), "hacker", [], "array", false, false, false, 109), "state", [], "any", false, false, false, 109), "idState", [], "any", false, false, false, 109), 2))) {
                    // line 110
                    echo "\t\t\t\t\t\t            <div class=\"row justify-content-center \">
\t\t\t\t\t\t                <div class=\"col-5\">
\t\t\t\t\t\t                    <a href=\"";
                    // line 112
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("hacker_in", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 112, $this->source); })()), "hacker", [], "array", false, false, false, 112), "idHacker", [], "any", false, false, false, 112)]), "html", null, true);
                    echo "\">
\t\t\t\t\t\t                        <button type=\"button\" class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); height:35px; margin-bottom:10px\">
\t\t\t\t\t\t                            <div style=\"color:#9D151C; font-weight:bold;\">Integrate</div>
\t\t\t\t\t\t                        </button>
\t\t\t\t\t\t                    </a>
\t\t\t\t\t\t            \t</div>
\t\t\t\t\t\t            \t<div class=\"col-5\">
\t\t\t\t\t\t                    <a href=\"";
                    // line 119
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("hacker_out", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 119, $this->source); })()), "hacker", [], "array", false, false, false, 119), "idHacker", [], "any", false, false, false, 119)]), "html", null, true);
                    echo "\">
\t\t\t\t\t\t                        <button type=\"button\" class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); height:35px; margin-bottom:10px\">
\t\t\t\t\t\t                            <div style=\"color:#9D151C; font-weight:bold;\">Reject</div>
\t\t\t\t\t\t                        </button>
\t\t\t\t\t\t                    </a>
\t\t\t\t\t\t            \t</div>\t
\t\t\t\t\t            \t</div>
\t\t\t\t\t                ";
                }
                // line 127
                echo "\t\t\t\t                ";
            }
            echo " 
\t\t\t\t            ";
        }
        // line 129
        echo "\t\t            </div>
\t\t        </div>
\t       \t</div>
\t    </div>
\t</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "hacker/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  278 => 129,  272 => 127,  261 => 119,  251 => 112,  247 => 110,  245 => 109,  235 => 102,  232 => 101,  229 => 100,  227 => 99,  222 => 96,  215 => 92,  211 => 90,  208 => 89,  199 => 85,  194 => 82,  191 => 81,  182 => 77,  177 => 74,  174 => 73,  165 => 69,  160 => 66,  157 => 65,  150 => 61,  146 => 59,  144 => 58,  137 => 54,  129 => 49,  120 => 43,  112 => 38,  104 => 33,  92 => 24,  79 => 14,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}
 
        \t
\t<div class=\"card border-0 bg-transparent\" style=\"margin-top:90px\">
\t\t<div class=\"tab-content py-4 text-center\">
\t    \t<div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(256, 256, 256, 0.5); width:100%;\">
\t    \t\t<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t\t            <div class=\"card-body\">
\t\t            \t<div class=\"row justify-content-center d-flex align-items-center\">
\t\t\t\t\t    \t<div class=\"col-3\">
\t\t\t\t\t\t     \t<h3 class=\"text-title text-white text-center\">{{result['hacker'].username}}'s Profile</h3>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t</div>
\t\t            </div>
\t\t        </div>
\t      </div>
\t    </div>
\t    <div class=\"tab-content py-4\">
\t    \t<div class=\"card-overlay border-0 bg-white shadow mx-auto\" style=\"width:60%;\">
\t\t        <div class=\"text-card text-center\" style=\"margin-bottom:30px\">
\t\t        \t<img src=\"/Images/hackersphotos/{{result['hacker'].photo }}\" alt=\"avatar\" style = \"width:150px; margin-top:30px;\">
\t\t        </div>
\t\t        <div class=\"row\">                  
\t\t            <div class=\" mx-auto\">
\t\t              
\t\t                <table class=\"table table-sm table-borderless\">
\t\t                    <tbody>
\t\t\t\t\t\t\t\t<tr>
\t\t                            <td>
\t\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\">Username : </strong> <div style=\"float:right\">{{result['hacker'].username}}</div>
\t\t                            </td>
\t\t                        </tr>
\t\t                        <tr>
\t\t                            <td>
\t\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\">First Name : </strong> <div style=\"float:right\">{{result['hacker'].name}}</div>
\t\t                            </td>
\t\t                        </tr>
\t\t                        <tr>
\t\t                            <td>
\t\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\">Last Name : </strong> <div style=\"float:right\">{{result['hacker'].fName}}</div>
\t\t                            </td>
\t\t                        </tr>

\t\t                        <tr>
\t\t                            <td>
\t\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\"g>Country : </strong> <div style=\"float:right\">{{result['hacker'].country}}</div>
\t\t                            </td>
\t\t                        </tr>
\t\t\t\t\t\t\t\t<tr>
\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Mark : </strong> <div style=\"float:right\">{{result['hacker'].mark}}</div>
\t\t                            </td>
\t\t                        </tr>

\t\t\t\t\t\t\t\t{% if result['hacker'].adress is not null %}
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Adress : </strong> <div style=\"float:right\">{{result['hacker'].adress}}</div>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    {% endif %}
\t\t\t                    {% if result['hacker'].site is not null %}
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Website : </strong> 
\t\t\t\t\t\t\t\t\t\t\t<a href=\"https://twitter.com/{{result['hacker'].twitter}}\" style=\"float:right; color:#000000;\">{{result['hacker'].site}}</a>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    {% endif %}
\t\t\t                    {% if result['hacker'].twitter is not null %}
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Twitter : </strong> 
\t\t\t\t\t\t\t\t\t\t\t<a href=\"https://twitter.com/{{result['hacker'].twitter}}\" style=\"float:right; color:#000000;\">@{{result['hacker'].twitter}}</a>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    {% endif %}
\t\t\t                    {% if result['hacker'].linkedin is not null %}
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> LinkedIn : </strong> 
\t\t\t\t\t\t\t\t\t\t\t<a href=\"https://twitter.com/{{result['hacker'].twitter}}\" style=\"float:right; color:#000000;\">{{result['hacker'].linkedin}}</a>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    {% endif %}
\t\t\t                    {% if result['hacker'].github is not null %}
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> GitHub : </strong> <div style=\"float:right\">{{result['hacker'].github}}</div>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    {% endif %}
\t\t                    </tbody>
\t\t                </table>
\t\t                <div class=\"row justify-content-center \">
\t\t\t                {% if is_granted('ROLE_ADMIN') %}
\t\t\t\t                {% if app.user.idHacker != result['hacker'].idHacker %}
\t\t\t\t\t                <div class=\"col-5\">
\t\t\t\t\t\t                    <a href=\"{{url('hacker_delete', {'id' : result['hacker'].idHacker }) }}\">
\t\t\t\t\t\t                        <button type=\"button\" class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); height:35px; margin-bottom:10px\">
\t\t\t\t\t\t                            <div style=\"color:#9D151C; font-weight:bold;\">Delete</div>
\t\t\t\t\t\t                        </button>
\t\t\t\t\t\t                    </a>
\t\t\t\t\t\t            </div>
\t\t\t\t\t            </div>
\t\t\t\t\t\t            {% if result['hacker'].state.idState == 2 %}
\t\t\t\t\t\t            <div class=\"row justify-content-center \">
\t\t\t\t\t\t                <div class=\"col-5\">
\t\t\t\t\t\t                    <a href=\"{{url('hacker_in', {'id' : result['hacker'].idHacker }) }}\">
\t\t\t\t\t\t                        <button type=\"button\" class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); height:35px; margin-bottom:10px\">
\t\t\t\t\t\t                            <div style=\"color:#9D151C; font-weight:bold;\">Integrate</div>
\t\t\t\t\t\t                        </button>
\t\t\t\t\t\t                    </a>
\t\t\t\t\t\t            \t</div>
\t\t\t\t\t\t            \t<div class=\"col-5\">
\t\t\t\t\t\t                    <a href=\"{{url('hacker_out', {'id' : result['hacker'].idHacker }) }}\">
\t\t\t\t\t\t                        <button type=\"button\" class=\"btn btn-sm shadow-sm\" style=\"background-color:rgba(217, 217, 217); height:35px; margin-bottom:10px\">
\t\t\t\t\t\t                            <div style=\"color:#9D151C; font-weight:bold;\">Reject</div>
\t\t\t\t\t\t                        </button>
\t\t\t\t\t\t                    </a>
\t\t\t\t\t\t            \t</div>\t
\t\t\t\t\t            \t</div>
\t\t\t\t\t                {% endif %}
\t\t\t\t                {% endif %} 
\t\t\t\t            {% endif %}
\t\t            </div>
\t\t        </div>
\t       \t</div>
\t    </div>
\t</div>

{% endblock %}

", "hacker/show.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\hacker\\show.html.twig");
    }
}
