<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* offer/index.html.twig */
class __TwigTemplate_d11bd88cab6789d9330406bac8953c76a5c4585273f5f679a50f69d7ae9d3451 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/index.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "offer/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "\t
\t\t<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t\t    ";
        // line 8
        if ((0 !== twig_compare(twig_length_filter($this->env, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 8, $this->source); })())), 0))) {
            // line 9
            echo "\t\t\t\t    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 9, $this->source); })())) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 10
                echo "\t\t\t\t    \t";
                if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                    // line 11
                    echo "\t\t\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t\t\t        \t\t\t<div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t\t    \t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t<a href=\"";
                    // line 18
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 18, $this->source); })()), $context["i"], [], "array", false, false, false, 18), "id_offer", [], "array", false, false, false, 18)]), "html", null, true);
                    echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t<h4>";
                    // line 19
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 19, $this->source); })()), $context["i"], [], "array", false, false, false, 19), "title", [], "array", false, false, false, 19), "html", null, true);
                    echo "</h4>
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t<img src=\"/Images/hackersphotos/";
                    // line 21
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 21, $this->source); })()), $context["i"], [], "array", false, false, false, 21), "photo", [], "array", false, false, false, 21), "html", null, true);
                    echo "\" height=\"100\" alt=\"\" class=\"rounded\" style=\"margin-left:20px;\"/>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t     \t\t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t\t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t<p>
\t\t\t\t\t\t\t\t\t\t\t      \t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<h5 style=\"color:#9D151C; margin-right:5px;\">Type</h5>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div style=\"color:black;\">";
                    // line 34
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 34, $this->source); })()), $context["i"], [], "array", false, false, false, 34), "n_type", [], "array", false, false, false, 34), "html", null, true);
                    echo "</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<h5 style=\"color:#9D151C;  margin-right:5px;\">Summary</h5>
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<div style=\"color:black;\">";
                    // line 42
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 42, $this->source); })()), $context["i"], [], "array", false, false, false, 42), "description1", [], "array", false, false, false, 42), "html", null, true);
                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\", style=\"margin-right:-100px;\">\t
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t\t<h5 style=\"color:#9D151C;  margin-right:5px;\">State</h5>
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t\t";
                    // line 50
                    if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 50, $this->source); })()), $context["i"], [], "array", false, false, false, 50), "state", [], "array", false, false, false, 50), 1))) {
                        // line 51
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-success active\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 51, $this->source); })()), $context["i"], [], "array", false, false, false, 51), "n_state", [], "array", false, false, false, 51), "html", null, true);
                        echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 52
(isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 52, $this->source); })()), $context["i"], [], "array", false, false, false, 52), "state", [], "array", false, false, false, 52), 2))) {
                        // line 53
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-warning active text-white\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 53, $this->source); })()), $context["i"], [], "array", false, false, false, 53), "n_state", [], "array", false, false, false, 53), "html", null, true);
                        echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 54
(isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 54, $this->source); })()), $context["i"], [], "array", false, false, false, 54), "state", [], "array", false, false, false, 54), 3))) {
                        // line 55
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-danger active\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 55, $this->source); })()), $context["i"], [], "array", false, false, false, 55), "n_state", [], "array", false, false, false, 55), "html", null, true);
                        echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t";
                    }
                    // line 57
                    echo "\t\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t      \t</p>
\t\t\t\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t\t\t\t\t   \t</div>\t

\t\t\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t\t    
\t\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t   </div>
\t\t\t\t\t  \t";
                } else {
                    // line 69
                    echo " 
\t\t\t\t\t  \t\t";
                    // line 70
                    if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 70, $this->source); })()), $context["i"], [], "array", false, false, false, 70), "state", [], "array", false, false, false, 70), 1))) {
                        // line 71
                        echo "\t\t\t\t\t  \t\t\t<div class=\"row d-flex justify-content-center\">
\t\t\t        \t\t\t<div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t\t    \t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t<a href=\"";
                        // line 78
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 78, $this->source); })()), $context["i"], [], "array", false, false, false, 78), "id_offer", [], "array", false, false, false, 78)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t<h4>";
                        // line 79
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 79, $this->source); })()), $context["i"], [], "array", false, false, false, 79), "title", [], "array", false, false, false, 79), "html", null, true);
                        echo "</h4>
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t<img src=\"/Images/hackersphotos/";
                        // line 81
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 81, $this->source); })()), $context["i"], [], "array", false, false, false, 81), "photo", [], "array", false, false, false, 81), "html", null, true);
                        echo "\" height=\"100\" alt=\"\" class=\"rounded\" style=\"margin-left:20px;\" />
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>

\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t     \t\t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t\t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t<p>
\t\t\t\t\t\t\t\t\t\t\t      \t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<h5 style=\"color:#9D151C; margin-right:5px;\">Type</h5>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div style=\"color:black;\">";
                        // line 95
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 95, $this->source); })()), $context["i"], [], "array", false, false, false, 95), "n_type", [], "array", false, false, false, 95), "html", null, true);
                        echo "</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<h5 style=\"color:#9D151C;  margin-right:5px;\">Summary</h5>
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<div style=\"color:black;\">";
                        // line 103
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 103, $this->source); })()), $context["i"], [], "array", false, false, false, 103), "description1", [], "array", false, false, false, 103), "html", null, true);
                        echo "</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t      \t</p>
\t\t\t\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t\t\t\t\t   \t</div>\t

\t\t\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t\t    
\t\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t   \t</div>
\t\t\t\t\t  \t\t";
                    }
                    // line 117
                    echo "\t\t\t\t\t   \t";
                }
                // line 118
                echo "\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " 
\t\t\t";
        }
        // line 120
        echo "\t\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "offer/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  256 => 120,  247 => 118,  244 => 117,  227 => 103,  216 => 95,  199 => 81,  194 => 79,  190 => 78,  181 => 71,  179 => 70,  176 => 69,  161 => 57,  155 => 55,  153 => 54,  148 => 53,  146 => 52,  141 => 51,  139 => 50,  128 => 42,  117 => 34,  101 => 21,  96 => 19,  92 => 18,  83 => 11,  80 => 10,  75 => 9,  73 => 8,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}
\t
\t\t<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t\t    {% if offers|length != 0 %}
\t\t\t\t    {% for i in 0..offers|length-1 %}
\t\t\t\t    \t{% if is_granted('ROLE_ADMIN') %}
\t\t\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t\t\t        \t\t\t<div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t\t    \t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t<a href=\"{{ url('offer_show', {'id' : offers[i]['id_offer']}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t<h4>{{ offers[i]['title']}}</h4>
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t<img src=\"/Images/hackersphotos/{{ offers[i]['photo']}}\" height=\"100\" alt=\"\" class=\"rounded\" style=\"margin-left:20px;\"/>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t     \t\t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t\t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t<p>
\t\t\t\t\t\t\t\t\t\t\t      \t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<h5 style=\"color:#9D151C; margin-right:5px;\">Type</h5>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div style=\"color:black;\">{{ offers[i]['n_type']}}</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<h5 style=\"color:#9D151C;  margin-right:5px;\">Summary</h5>
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<div style=\"color:black;\">{{ offers[i]['description1']}}</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\", style=\"margin-right:-100px;\">\t
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t\t<h5 style=\"color:#9D151C;  margin-right:5px;\">State</h5>
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t\t{% if offers[i]['state'] == 1 %}
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-success active\">{{ offers[i]['n_state']}}</div>
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t{% elseif offers[i]['state'] == 2 %}
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-warning active text-white\">{{ offers[i]['n_state']}}</div>
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t{% elseif offers[i]['state'] == 3 %}
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-danger active\">{{ offers[i]['n_state']}}</div>
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t      \t</p>
\t\t\t\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t\t\t\t\t   \t</div>\t

\t\t\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t\t    
\t\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t   </div>
\t\t\t\t\t  \t{% else %} 
\t\t\t\t\t  \t\t{% if offers[i]['state'] == 1 %}
\t\t\t\t\t  \t\t\t<div class=\"row d-flex justify-content-center\">
\t\t\t        \t\t\t<div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t\t    \t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t<a href=\"{{ url('offer_show', {'id' : offers[i]['id_offer']}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t<h4>{{ offers[i]['title']}}</h4>
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t<img src=\"/Images/hackersphotos/{{ offers[i]['photo']}}\" height=\"100\" alt=\"\" class=\"rounded\" style=\"margin-left:20px;\" />
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>

\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t     \t\t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t\t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t<p>
\t\t\t\t\t\t\t\t\t\t\t      \t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<h5 style=\"color:#9D151C; margin-right:5px;\">Type</h5>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div style=\"color:black;\">{{ offers[i]['n_type']}}</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<h5 style=\"color:#9D151C;  margin-right:5px;\">Summary</h5>
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<div style=\"color:black;\">{{ offers[i]['description1']}}</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t      \t</p>
\t\t\t\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t\t\t\t\t   \t</div>\t

\t\t\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t\t    
\t\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t   \t</div>
\t\t\t\t\t  \t\t{% endif %}
\t\t\t\t\t   \t{% endif %}
\t\t\t\t\t{% endfor %} 
\t\t\t{% endif %}
\t\t</div>
{% endblock %}", "offer/index.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\offer\\index.html.twig");
    }
}
