<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* blog_post/new_post.html.twig */
class __TwigTemplate_ad86da462eead5bd1634948d37a2e1923274d3a758ddeb1d9dc7c821f8c5022f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'javascript' => [$this, 'block_javascript'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog_post/new_post.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog_post/new_post.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "blog_post/new_post.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        // line 4
        echo "    <script src=\"https://cdn.tiny.cloud/1/1r3xno60hf2olb3nwcalbv7ow89y17xu0n7a8s7fxqpztm09/tinymce/5/tinymce.min.js\" referrerpolicy=\"origin\"/></script>
    <script>
    tinymce.init({
        selector: '#content',
        setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
        }
    });
  </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 17
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 18
        echo "\t<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t\t<div class=\"card text-center border-0 bg-transparent shadow rounded-0\" style = \"margin-top:85px;\">
\t    \t<h2 class=\"card-body text-white\">Advise'Hack - The Blog</h2>
\t  \t</div>
  \t</div>

    <div class=\"card card-overlay bg-transparent text-white border-0 shadow mx-auto\" style=\" margin-bottom:20px; width:80%; padding-top:30px; padding-bottom:30px; padding-right:10px; padding-left:10px;\">

        <div class=\"row justify-content-center\" style=\"margin-bottom:30px; color:#9D151C;\">
            <h3>New Blog Post</h3> 
        </div>
            
        ";
        // line 30
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["BlogPostForm"]) || array_key_exists("BlogPostForm", $context) ? $context["BlogPostForm"] : (function () { throw new RuntimeError('Variable "BlogPostForm" does not exist.', 30, $this->source); })()), 'form_start');
        echo "
            <div class=\"row justify-content-center\">
                <div class=\"col-2\">
                    <div class=\"row text-dark\" style=\"font-weight:bold; margin-bottom:10px;\">   
                        ";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["BlogPostForm"]) || array_key_exists("BlogPostForm", $context) ? $context["BlogPostForm"] : (function () { throw new RuntimeError('Variable "BlogPostForm" does not exist.', 34, $this->source); })()), "title", [], "any", false, false, false, 34), 'label', ["label" => "Title"]);
        echo "
                    </div>
                    <div class=\"row text-dark\" style=\"font-weight:bold; margin-bottom:10px;\">   
                        ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["BlogPostForm"]) || array_key_exists("BlogPostForm", $context) ? $context["BlogPostForm"] : (function () { throw new RuntimeError('Variable "BlogPostForm" does not exist.', 37, $this->source); })()), "photo", [], "any", false, false, false, 37), 'label', ["label" => "Upload Photo"]);
        echo "
                    </div>
                    <div class=\"row text-dark\" style=\"font-weight:bold; margin-bottom:10px;\">   
                        ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["BlogPostForm"]) || array_key_exists("BlogPostForm", $context) ? $context["BlogPostForm"] : (function () { throw new RuntimeError('Variable "BlogPostForm" does not exist.', 40, $this->source); })()), "content", [], "any", false, false, false, 40), 'label', ["label" => "Article"]);
        echo "
                    </div>
                </div>
                <div class=\"col-7\">
                    <div class=\"row\" style=\"margin-bottom:10px;\">
                        ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["BlogPostForm"]) || array_key_exists("BlogPostForm", $context) ? $context["BlogPostForm"] : (function () { throw new RuntimeError('Variable "BlogPostForm" does not exist.', 45, $this->source); })()), "title", [], "any", false, false, false, 45), 'widget');
        echo "
                    </div>
                    <div class=\"row\" style=\"margin-bottom:10px;\">
                        ";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["BlogPostForm"]) || array_key_exists("BlogPostForm", $context) ? $context["BlogPostForm"] : (function () { throw new RuntimeError('Variable "BlogPostForm" does not exist.', 48, $this->source); })()), "photo", [], "any", false, false, false, 48), 'widget');
        echo "
                    </div>
                    <div class=\"row\" style=\"margin-bottom:10px;\">
                        ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["BlogPostForm"]) || array_key_exists("BlogPostForm", $context) ? $context["BlogPostForm"] : (function () { throw new RuntimeError('Variable "BlogPostForm" does not exist.', 51, $this->source); })()), "content", [], "any", false, false, false, 51), 'widget', ["id" => "content"]);
        echo "
                    </div>
                </div>
            </div>
            <div class=\"container\">
                <div class=\"row justify-content-center\">
                    <div class=\"text-center\">
                        <button type=\"submit\" class=\"btn btn-lg shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C; margin-top:30px;\">Submit</button>
                    </div>
                </div>
            </div>
        ";
        // line 62
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["BlogPostForm"]) || array_key_exists("BlogPostForm", $context) ? $context["BlogPostForm"] : (function () { throw new RuntimeError('Variable "BlogPostForm" does not exist.', 62, $this->source); })()), 'form_end');
        echo "
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "blog_post/new_post.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 62,  153 => 51,  147 => 48,  141 => 45,  133 => 40,  127 => 37,  121 => 34,  114 => 30,  100 => 18,  90 => 17,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}

{% block javascript %}
    <script src=\"https://cdn.tiny.cloud/1/1r3xno60hf2olb3nwcalbv7ow89y17xu0n7a8s7fxqpztm09/tinymce/5/tinymce.min.js\" referrerpolicy=\"origin\"/></script>
    <script>
    tinymce.init({
        selector: '#content',
        setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
        }
    });
  </script>
{% endblock %}

{% block body %}
\t<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t\t<div class=\"card text-center border-0 bg-transparent shadow rounded-0\" style = \"margin-top:85px;\">
\t    \t<h2 class=\"card-body text-white\">Advise'Hack - The Blog</h2>
\t  \t</div>
  \t</div>

    <div class=\"card card-overlay bg-transparent text-white border-0 shadow mx-auto\" style=\" margin-bottom:20px; width:80%; padding-top:30px; padding-bottom:30px; padding-right:10px; padding-left:10px;\">

        <div class=\"row justify-content-center\" style=\"margin-bottom:30px; color:#9D151C;\">
            <h3>New Blog Post</h3> 
        </div>
            
        {{ form_start(BlogPostForm) }}
            <div class=\"row justify-content-center\">
                <div class=\"col-2\">
                    <div class=\"row text-dark\" style=\"font-weight:bold; margin-bottom:10px;\">   
                        {{ form_label(BlogPostForm.title, 'Title') }}
                    </div>
                    <div class=\"row text-dark\" style=\"font-weight:bold; margin-bottom:10px;\">   
                        {{ form_label(BlogPostForm.photo, 'Upload Photo') }}
                    </div>
                    <div class=\"row text-dark\" style=\"font-weight:bold; margin-bottom:10px;\">   
                        {{ form_label(BlogPostForm.content, 'Article') }}
                    </div>
                </div>
                <div class=\"col-7\">
                    <div class=\"row\" style=\"margin-bottom:10px;\">
                        {{ form_widget(BlogPostForm.title) }}
                    </div>
                    <div class=\"row\" style=\"margin-bottom:10px;\">
                        {{ form_widget(BlogPostForm.photo) }}
                    </div>
                    <div class=\"row\" style=\"margin-bottom:10px;\">
                        {{ form_widget(BlogPostForm.content, {'id': 'content'}) }}
                    </div>
                </div>
            </div>
            <div class=\"container\">
                <div class=\"row justify-content-center\">
                    <div class=\"text-center\">
                        <button type=\"submit\" class=\"btn btn-lg shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C; margin-top:30px;\">Submit</button>
                    </div>
                </div>
            </div>
        {{ form_end(BlogPostForm) }}
    </div>

{% endblock %}", "blog_post/new_post.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\blog_post\\new_post.html.twig");
    }
}
