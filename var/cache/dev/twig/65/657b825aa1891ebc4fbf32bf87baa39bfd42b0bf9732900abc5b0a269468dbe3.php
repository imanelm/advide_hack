<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* offer/editReport.html.twig */
class __TwigTemplate_d8e1dcc293655269ff7d2c32be092acaeaaefe1fddc5f67e1110a8585423a2c0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'javascript' => [$this, 'block_javascript'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/editReport.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/editReport.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "offer/editReport.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascript"));

        // line 3
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script> 
    <script src=\"https://cdn.tiny.cloud/1/1r3xno60hf2olb3nwcalbv7ow89y17xu0n7a8s7fxqpztm09/tinymce/5/tinymce.min.js\" referrerpolicy=\"origin\"/></script>
    <script>
    tinymce.init({
      selector: '#report'
    });
  </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo "
\t<div class=\"card border-0 bg-transparent\" style=\"margin-top:90px; width:100%;\">
\t    <div class=\"tab-content py-4 text-center\">
\t    \t<div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(256, 256, 256, 0.5); width:100%;\">
\t    \t\t<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t\t            <div class=\"card-body\">
\t\t            \t<div class=\"row justify-content-center d-flex align-items-center\">
\t\t\t\t\t    \t<div class=\"col-3\">
\t\t\t\t\t\t     \t<h3 class=\"text-title text-white text-center\">";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["report"]) || array_key_exists("report", $context) ? $context["report"] : (function () { throw new RuntimeError('Variable "report" does not exist.', 21, $this->source); })()), "idOffer", [], "any", false, false, false, 21), "title", [], "any", false, false, false, 21), "html", null, true);
        echo "</h3>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    <div class=\"col-3\">
\t\t\t\t\t\t      \t<div class=\"text-title text-white text-center\">";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["report"]) || array_key_exists("report", $context) ? $context["report"] : (function () { throw new RuntimeError('Variable "report" does not exist.', 24, $this->source); })()), "idOffer", [], "any", false, false, false, 24), "description1", [], "any", false, false, false, 24), "html", null, true);
        echo "</div>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    ";
        // line 26
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 26), "mark", [], "any", true, true, false, 26)) {
            // line 27
            echo "\t\t\t\t\t\t\t    <div class=\"col-3\">
\t\t\t\t\t\t\t    \t<a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("report_show", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["report"]) || array_key_exists("report", $context) ? $context["report"] : (function () { throw new RuntimeError('Variable "report" does not exist.', 28, $this->source); })()), "idReport", [], "any", false, false, false, 28)]), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t      \t<svg width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-arrow-left-circle-fill\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t  \t\t<path fill-rule=\"evenodd\" d=\"M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-7.646 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L6.207 7.5H11a.5.5 0 0 1 0 1H6.207l2.147 2.146z\"/>
\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    ";
        }
        // line 35
        echo "\t\t\t\t\t\t</div>
\t\t            </div>
\t\t        </div>
          </div>
        </div>

        <div class=\"row d-flex justify-content-center\">

            <div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:80%; padding-top:30px; padding-bottom:30px; padding-right:30px; padding-left:30px;\">

                <div class=\"row d-flex justify-content-center\">

                    ";
        // line 47
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 47, $this->source); })()), 'form_start');
        echo "
                    \t<h3 style=\"color:#9D151C; font-weight:bold;\">Report Details</h3><br>
                        <div class=\"row\">

                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                <div style=\"float:left\">";
        // line 52
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 52, $this->source); })()), "scope", [], "any", false, false, false, 52), 'label', ["label" => "Scope"]);
        echo "</div>
                                <div style=\"float:left; font-weight:bold; color:#9D151C;\">*</div> 
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                <div style=\"float:left\">";
        // line 56
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 56, $this->source); })()), "scope", [], "any", false, false, false, 56), 'widget');
        echo "</div>
                            </div>
                        </div>

                         <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 62, $this->source); })()), "end_point", [], "any", false, false, false, 62), 'label', ["label" => "Endpoint"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 65
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 65, $this->source); })()), "end_point", [], "any", false, false, false, 65), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
        // line 71
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 71, $this->source); })()), "tech_env", [], "any", false, false, false, 71), 'label', ["label" => "Technical environment"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 74
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 74, $this->source); })()), "tech_env", [], "any", false, false, false, 74), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
        // line 80
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 80, $this->source); })()), "app_fingerprint", [], "any", false, false, false, 80), 'label', ["label" => "Application fingerprint"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 83
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 83, $this->source); })()), "app_fingerprint", [], "any", false, false, false, 83), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
        // line 89
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 89, $this->source); })()), "ip_used", [], "any", false, false, false, 89), 'label', ["label" => "IP used"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 92
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 92, $this->source); })()), "ip_used", [], "any", false, false, false, 92), 'widget');
        echo "
                            </div>
                        </div>
                        
                        <h3 style=\"color:#9D151C; font-weight:bold; margin-top:30px;\">CVSS Score</h3><br>

                        <div class=\"container\">

\t                        <h4 class=\"text-secondary\" style=\"font-weight:bold;\">Base Metrics</h4>

\t                        <h5 class=\"text-dark\" style=\"font-weight:bold;\">Access Vector</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"base1\" id=\"1\" value=\"0.395\"> Local
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"base1\" id=\"2\" value=\"0.646\"> Adjacent Network
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"base1\" id=\"3\" value=\"1\"> Network
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Attack Complexity</h5><br>
\t                    \t<div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"base2\" id=\"1\" value=\"0.35\"> High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"base2\" id=\"2\" value=\"0.61\"> Medium
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"base2\" id=\"3\" value=\"0.71\"> Low
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Authentication</h5><br>
\t                    \t<div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"base3\" id=\"1\" value=\"0.45\"> Multiple
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"base3\" id=\"2\" value=\"0.56\"> Single
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"base3\" id=\"3\" value=\"0.704\"> None
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>
\t                \t</div>

\t                \t<div class=\"container\">
\t                    \t<h4 class=\"text-secondary\" style=\"font-weight:bold; margin-top:25px;\">Impact Metrics</h4>
\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Confidentiality</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"impact1\" id=\"1\" value=\"0\"> None
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"impact1\" id=\"2\" value=\"0.275\"> Partial
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"impact1\" id=\"3\" value=\"0.660\"> Complete
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Integrity</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"impact2\" id=\"1\" value=\"0\"> High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"impact2\" id=\"2\" value=\"0.275\"> Medium
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"impact2\" id=\"3\" value=\"0.660\"> Low
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Availability</h5><br>
\t                    \t<div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"impact3\" id=\"1\" value=\"0\"> None
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"impact3\" id=\"2\" value=\"0.275\"> Partial
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"impact3\" id=\"3\" value=\"0.660\"> Complete
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>
\t                    </div>

\t                    <div class=\"container\">
\t                    \t<h4 class=\"text-secondary\" style=\"font-weight:bold; margin-top:25px;\">Temporal Metrics</h4>
\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Exploitability</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"temp1\" id=\"1\" value=\"0.85\"> Unproven
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"temp1\" id=\"2\" value=\"0.9\"> Proof-of-concept
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp1\" id=\"3\" value=\"0.95\"> Functional
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp1\" id=\"4\" value=\"1\"> High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp1\" id=\"5\" value=\"1\"> Not Defined
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Remediation Level</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"temp2\" id=\"1\" value=\"0.87\"> Official Fix
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"temp2\" id=\"2\" value=\"0.9\"> Temporary Fix
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp2\" id=\"3\" value=\"0.95\"> Workaround
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp2\" id=\"4\" value=\"1\"> Unavailable
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp2\" id=\"5\" value=\"1\"> Not Defined
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Report Confidence</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"temp3\" id=\"1\" value=\"0.9\"> Unconfirmed
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"temp3\" id=\"2\" value=\"0.95\"> Uncorroborated
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp3\" id=\"3\" value=\"1\"> Confirmed
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp3\" id=\"4\" value=\"1\"> Not Defined
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>
\t                    </div>

\t                    <div class=\"container\">
\t                    \t<h4 class=\"text-secondary\" style=\"font-weight:bold; margin-top:25px;\">Environmental Metrics</h4>
\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Collateral Damage Potential</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"env1\" id=\"1\" value=\"0\"> checked None
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"env1\" id=\"2\" value=\"0.1\"> Low
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env1\" id=\"3\" value=\"0.3\"> Low-Medium
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env1\" id=\"4\" value=\"0.4\"> Low-High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env1\" id=\"5\" value=\"0.5\"> High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env1\" id=\"6\" value=\"0\"> Not Defined
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Target Distribution</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"env2\" id=\"1\" value=\"0\"> None
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"env2\" id=\"2\" value=\"0.25\"> Low
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env2\" id=\"3\" value=\"0.75\"> Medium
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env2\" id=\"4\" value=\"1\"> High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env2\" id=\"5\" value=\"1\"> Not Defined
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Impact Subscore Modifier</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"env3\" id=\"1\" value=\"0.5\" > Low
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env3\" id=\"2\" value=\"1\"> Medium
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env3\" id=\"3\" value=\"1.51\"> High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env3\" id=\"4\" value=\"1\"> Not Defined
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h3 style=\"color:#9D151C; font-weight:bold; margin-top:10px;\">Report Description</h3>

                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
        // line 327
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 327, $this->source); })()), "report", [], "any", false, false, false, 327), 'label', ["label" => "Report"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left; margin-bottom:30px;\">
                                ";
        // line 330
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 330, $this->source); })()), "report", [], "any", false, false, false, 330), 'widget', ["id" => "report"]);
        echo "
                            </div>

\t                    </div>\t

\t                    <div class=\"container\">
\t                    \t<div class=\"row\">
\t\t                    \t<div class=\"text-center\">
\t\t                            <button type=\"submit\" class=\"btn btn-lg shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C;\">Submit</button>
\t\t                        </div>
\t                    \t</div>
                    \t</div>
                    ";
        // line 342
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["EditReportForm"]) || array_key_exists("EditReportForm", $context) ? $context["EditReportForm"] : (function () { throw new RuntimeError('Variable "EditReportForm" does not exist.', 342, $this->source); })()), 'form_end');
        echo "
                </div>

            </div>

        </div>

\t</div>
\t<script>
\t\t\$(\"label\").click(function(){
\t\t\t\$choices = \$(this).parent().children();
\t\t\t\$choices.each(function(){
\t\t\t\t\$(this).removeClass(\"active\");
\t\t\t});
\t\t\t\$(this).addClass(\"active\");
\t\t});
\t</script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "offer/editReport.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  480 => 342,  465 => 330,  459 => 327,  221 => 92,  215 => 89,  206 => 83,  200 => 80,  191 => 74,  185 => 71,  176 => 65,  170 => 62,  161 => 56,  154 => 52,  146 => 47,  132 => 35,  122 => 28,  119 => 27,  117 => 26,  112 => 24,  106 => 21,  96 => 13,  86 => 12,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}
{% block javascript %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script> 
    <script src=\"https://cdn.tiny.cloud/1/1r3xno60hf2olb3nwcalbv7ow89y17xu0n7a8s7fxqpztm09/tinymce/5/tinymce.min.js\" referrerpolicy=\"origin\"/></script>
    <script>
    tinymce.init({
      selector: '#report'
    });
  </script>
{% endblock %}

{% block body %}

\t<div class=\"card border-0 bg-transparent\" style=\"margin-top:90px; width:100%;\">
\t    <div class=\"tab-content py-4 text-center\">
\t    \t<div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(256, 256, 256, 0.5); width:100%;\">
\t    \t\t<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t\t            <div class=\"card-body\">
\t\t            \t<div class=\"row justify-content-center d-flex align-items-center\">
\t\t\t\t\t    \t<div class=\"col-3\">
\t\t\t\t\t\t     \t<h3 class=\"text-title text-white text-center\">{{report.idOffer.title}}</h3>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    <div class=\"col-3\">
\t\t\t\t\t\t      \t<div class=\"text-title text-white text-center\">{{report.idOffer.description1}}</div>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    {% if app.user.mark is defined %}
\t\t\t\t\t\t\t    <div class=\"col-3\">
\t\t\t\t\t\t\t    \t<a href=\"{{url('report_show', {'id' : report.idReport}) }}\">
\t\t\t\t\t\t\t\t      \t<svg width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-arrow-left-circle-fill\" fill=\"white\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t  \t\t<path fill-rule=\"evenodd\" d=\"M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-7.646 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L6.207 7.5H11a.5.5 0 0 1 0 1H6.207l2.147 2.146z\"/>
\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    {% endif %}
\t\t\t\t\t\t</div>
\t\t            </div>
\t\t        </div>
          </div>
        </div>

        <div class=\"row d-flex justify-content-center\">

            <div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:80%; padding-top:30px; padding-bottom:30px; padding-right:30px; padding-left:30px;\">

                <div class=\"row d-flex justify-content-center\">

                    {{ form_start(EditReportForm) }}
                    \t<h3 style=\"color:#9D151C; font-weight:bold;\">Report Details</h3><br>
                        <div class=\"row\">

                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                <div style=\"float:left\">{{ form_label(EditReportForm.scope, 'Scope') }}</div>
                                <div style=\"float:left; font-weight:bold; color:#9D151C;\">*</div> 
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                <div style=\"float:left\">{{ form_widget(EditReportForm.scope) }}</div>
                            </div>
                        </div>

                         <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditReportForm.end_point, 'Endpoint') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditReportForm.end_point) }}
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditReportForm.tech_env, 'Technical environment') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditReportForm.tech_env) }}
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditReportForm.app_fingerprint, 'Application fingerprint') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditReportForm.app_fingerprint) }}
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditReportForm.ip_used, 'IP used') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditReportForm.ip_used) }}
                            </div>
                        </div>
                        
                        <h3 style=\"color:#9D151C; font-weight:bold; margin-top:30px;\">CVSS Score</h3><br>

                        <div class=\"container\">

\t                        <h4 class=\"text-secondary\" style=\"font-weight:bold;\">Base Metrics</h4>

\t                        <h5 class=\"text-dark\" style=\"font-weight:bold;\">Access Vector</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"base1\" id=\"1\" value=\"0.395\"> Local
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"base1\" id=\"2\" value=\"0.646\"> Adjacent Network
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"base1\" id=\"3\" value=\"1\"> Network
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Attack Complexity</h5><br>
\t                    \t<div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"base2\" id=\"1\" value=\"0.35\"> High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"base2\" id=\"2\" value=\"0.61\"> Medium
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"base2\" id=\"3\" value=\"0.71\"> Low
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Authentication</h5><br>
\t                    \t<div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"base3\" id=\"1\" value=\"0.45\"> Multiple
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"base3\" id=\"2\" value=\"0.56\"> Single
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"base3\" id=\"3\" value=\"0.704\"> None
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>
\t                \t</div>

\t                \t<div class=\"container\">
\t                    \t<h4 class=\"text-secondary\" style=\"font-weight:bold; margin-top:25px;\">Impact Metrics</h4>
\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Confidentiality</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"impact1\" id=\"1\" value=\"0\"> None
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"impact1\" id=\"2\" value=\"0.275\"> Partial
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"impact1\" id=\"3\" value=\"0.660\"> Complete
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Integrity</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"impact2\" id=\"1\" value=\"0\"> High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"impact2\" id=\"2\" value=\"0.275\"> Medium
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"impact2\" id=\"3\" value=\"0.660\"> Low
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Availability</h5><br>
\t                    \t<div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"impact3\" id=\"1\" value=\"0\"> None
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"impact3\" id=\"2\" value=\"0.275\"> Partial
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"impact3\" id=\"3\" value=\"0.660\"> Complete
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>
\t                    </div>

\t                    <div class=\"container\">
\t                    \t<h4 class=\"text-secondary\" style=\"font-weight:bold; margin-top:25px;\">Temporal Metrics</h4>
\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Exploitability</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"temp1\" id=\"1\" value=\"0.85\"> Unproven
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"temp1\" id=\"2\" value=\"0.9\"> Proof-of-concept
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp1\" id=\"3\" value=\"0.95\"> Functional
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp1\" id=\"4\" value=\"1\"> High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp1\" id=\"5\" value=\"1\"> Not Defined
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Remediation Level</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"temp2\" id=\"1\" value=\"0.87\"> Official Fix
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"temp2\" id=\"2\" value=\"0.9\"> Temporary Fix
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp2\" id=\"3\" value=\"0.95\"> Workaround
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp2\" id=\"4\" value=\"1\"> Unavailable
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp2\" id=\"5\" value=\"1\"> Not Defined
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Report Confidence</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"temp3\" id=\"1\" value=\"0.9\"> Unconfirmed
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"temp3\" id=\"2\" value=\"0.95\"> Uncorroborated
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp3\" id=\"3\" value=\"1\"> Confirmed
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"temp3\" id=\"4\" value=\"1\"> Not Defined
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>
\t                    </div>

\t                    <div class=\"container\">
\t                    \t<h4 class=\"text-secondary\" style=\"font-weight:bold; margin-top:25px;\">Environmental Metrics</h4>
\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Collateral Damage Potential</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"env1\" id=\"1\" value=\"0\"> checked None
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"env1\" id=\"2\" value=\"0.1\"> Low
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env1\" id=\"3\" value=\"0.3\"> Low-Medium
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env1\" id=\"4\" value=\"0.4\"> Low-High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env1\" id=\"5\" value=\"0.5\"> High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env1\" id=\"6\" value=\"0\"> Not Defined
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Target Distribution</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t\t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"env2\" id=\"1\" value=\"0\"> None
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"env2\" id=\"2\" value=\"0.25\"> Low
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env2\" id=\"3\" value=\"0.75\"> Medium
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env2\" id=\"4\" value=\"1\"> High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env2\" id=\"5\" value=\"1\"> Not Defined
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h5 class=\"text-dark\" style=\"font-weight:bold;\">Impact Subscore Modifier</h5><br>
\t                        <div class=\"row\">
\t                        \t<div class=\"btn-group btn-group-toggle\" data-toggle=\"buttons\" style=\"margin-top:-25px; margin-bottom:20px;\">
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary active\">
\t\t\t\t\t\t\t\t    \t<input type=\"radio\" name=\"env3\" id=\"1\" value=\"0.5\" > Low
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env3\" id=\"2\" value=\"1\"> Medium
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env3\" id=\"3\" value=\"1.51\"> High
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t  \t<label class=\"btn btn-secondary\">
\t\t\t\t\t\t\t\t\t    <input type=\"radio\" name=\"env3\" id=\"4\" value=\"1\"> Not Defined
\t\t\t\t\t\t\t\t  \t</label>
\t\t\t\t\t\t\t\t</div>
\t                    \t</div>

\t                    \t<h3 style=\"color:#9D151C; font-weight:bold; margin-top:10px;\">Report Description</h3>

                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditReportForm.report, 'Report') }}
                            </div>
                            <div class=\"col\" style=\"float:left; margin-bottom:30px;\">
                                {{ form_widget(EditReportForm.report, {'id': 'report'}) }}
                            </div>

\t                    </div>\t

\t                    <div class=\"container\">
\t                    \t<div class=\"row\">
\t\t                    \t<div class=\"text-center\">
\t\t                            <button type=\"submit\" class=\"btn btn-lg shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C;\">Submit</button>
\t\t                        </div>
\t                    \t</div>
                    \t</div>
                    {{ form_end(EditReportForm) }}
                </div>

            </div>

        </div>

\t</div>
\t<script>
\t\t\$(\"label\").click(function(){
\t\t\t\$choices = \$(this).parent().children();
\t\t\t\$choices.each(function(){
\t\t\t\t\$(this).removeClass(\"active\");
\t\t\t});
\t\t\t\$(this).addClass(\"active\");
\t\t});
\t</script>
{% endblock %}", "offer/editReport.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\offer\\editReport.html.twig");
    }
}
