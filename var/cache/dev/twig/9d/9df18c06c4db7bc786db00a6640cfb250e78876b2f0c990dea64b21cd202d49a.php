<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* hacker/EditHackerProfile.html.twig */
class __TwigTemplate_a781a33eb5668f8c479d7ab13d8b13560eb9509708de81ab7f8e75fbe7cb9d0c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/EditHackerProfile.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/EditHackerProfile.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "hacker/EditHackerProfile.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
        <div class=\"row d-flex justify-content-center\" style =\"margin-top:120px;\">
            <div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:80%; padding-top:30px; padding-bottom:30px; padding-right:30px; padding-left:30px;\">

                <div class=\"row d-flex justify-content-center\">

                    ";
        // line 11
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 11, $this->source); })()), 'form_start');
        echo "
\t\t\t\t\t\t";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 12, $this->source); })()), "flashes", [0 => "message"], "method", false, false, false, 12));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 13
            echo "\t\t\t\t\t\t<div class=\" text-center\" style=\"margin-bottom:20px;\">
\t\t\t\t\t\t\t<div class=\"alert alert-success\" role=\"alert\"> ";
            // line 14
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "\t\t\t\t\t    <div class=\" text-center\" style=\"margin-bottom:20px;\">
\t\t\t\t\t\t\t<img src=\"/Images/hackersphotos/";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 19, $this->source); })()), "user", [], "any", false, false, false, 19), "photo", [], "any", false, false, false, 19), "html", null, true);
        echo "\" alt=\"avatar\" style = \"width : 150px;\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
                        <div class=\"row\">
\t\t\t\t\t\t   ";
        // line 23
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 23), "nameEnt", [], "any", true, true, false, 23)) {
            // line 24
            echo "\t\t\t\t\t\t\t\t<div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
            // line 25
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 25, $this->source); })()), "nameEnt", [], "any", false, false, false, 25), 'label', ["label" => "Entreprise"]);
            echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
            // line 28
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 28, $this->source); })()), "nameEnt", [], "any", false, false, false, 28), 'widget');
            echo "
                            </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t   ";
        } else {
            // line 32
            echo "\t\t\t\t\t\t    <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
            // line 33
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 33, $this->source); })()), "username", [], "any", false, false, false, 33), 'label', ["label" => "Username"]);
            echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
            // line 36
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 36, $this->source); })()), "username", [], "any", false, false, false, 36), 'widget');
            echo "
                            </div>
                           ";
        }
        // line 39
        echo "                            
                        </div>
\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 43, $this->source); })()), "name", [], "any", false, false, false, 43), 'label', ["label" => "First Name"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 46, $this->source); })()), "name", [], "any", false, false, false, 46), 'widget');
        echo "
                            </div>
                        </div>
\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 51, $this->source); })()), "fName", [], "any", false, false, false, 51), 'label', ["label" => "Last Name"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 54
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 54, $this->source); })()), "fName", [], "any", false, false, false, 54), 'widget');
        echo "
                            </div>
                        </div>
\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
        // line 59
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 59, $this->source); })()), "email", [], "any", false, false, false, 59), 'label', ["label" => "Email"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 62, $this->source); })()), "email", [], "any", false, false, false, 62), 'widget');
        echo "
                            </div>
                        </div>
\t\t\t\t\t\t";
        // line 65
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 65), "nameEnt", [], "any", true, true, false, 65)) {
            // line 66
            echo "\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
            // line 68
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 68, $this->source); })()), "num", [], "any", false, false, false, 68), 'label', ["label" => "téléphone"]);
            echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
            // line 71
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 71, $this->source); })()), "num", [], "any", false, false, false, 71), 'widget');
            echo "
                            </div>
\t\t\t\t\t\t</div>
                        ";
        }
        // line 75
        echo "\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
        // line 78
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 78, $this->source); })()), "country", [], "any", false, false, false, 78), 'label', ["label" => "Country"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 81
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 81, $this->source); })()), "country", [], "any", false, false, false, 81), 'widget');
        echo "
                            </div>
                        </div>
\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
        // line 86
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 86, $this->source); })()), "photo", [], "any", false, false, false, 86), 'label', ["label" => "Photo"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 89
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 89, $this->source); })()), "photo", [], "any", false, false, false, 89), 'widget');
        echo "
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
        // line 94
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 94, $this->source); })()), "adress", [], "any", false, false, false, 94), 'label', ["label" => "Adresse"]);
        echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
        // line 97
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 97, $this->source); })()), "adress", [], "any", false, false, false, 97), 'widget');
        echo "
                            </div>
                        </div>

                            ";
        // line 101
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 101), "idHacker", [], "any", true, true, false, 101)) {
            // line 102
            echo "                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
            // line 104
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 104, $this->source); })()), "twitter", [], "any", false, false, false, 104), 'label', ["label" => "Twitter"]);
            echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
            // line 107
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 107, $this->source); })()), "twitter", [], "any", false, false, false, 107), 'widget');
            echo "
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
            // line 112
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 112, $this->source); })()), "linkedin", [], "any", false, false, false, 112), 'label', ["label" => "Linkedin"]);
            echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
            // line 115
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 115, $this->source); })()), "linkedin", [], "any", false, false, false, 115), 'widget');
            echo "
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
            // line 120
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 120, $this->source); })()), "github", [], "any", false, false, false, 120), 'label', ["label" => "Github"]);
            echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
            // line 123
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 123, $this->source); })()), "github", [], "any", false, false, false, 123), 'widget');
            echo "
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
            // line 128
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 128, $this->source); })()), "fichierSup", [], "any", false, false, false, 128), 'label', ["label" => "Additional file"]);
            echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
            // line 131
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 131, $this->source); })()), "fichierSup", [], "any", false, false, false, 131), 'widget');
            echo "
                            </div>
                        </div>

                        ";
        }
        // line 136
        echo "                        

                        ";
        // line 138
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 138), "nameEnt", [], "any", true, true, false, 138)) {
            // line 139
            echo "                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                ";
            // line 141
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 141, $this->source); })()), "site", [], "any", false, false, false, 141), 'label', ["label" => "Site"]);
            echo "
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                ";
            // line 144
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 144, $this->source); })()), "site", [], "any", false, false, false, 144), 'widget');
            echo "
                            </div>
                        </div>
                        ";
        }
        // line 148
        echo "
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"text-center\">
                            <button type=\"submit\" class=\"btn btn-lg shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C;\">Save</button>
                        </div>
                        

                    ";
        // line 155
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["EditForm"]) || array_key_exists("EditForm", $context) ? $context["EditForm"] : (function () { throw new RuntimeError('Variable "EditForm" does not exist.', 155, $this->source); })()), 'form_end');
        echo "
                </div>

            </div>

        </div>
\t\t 


\t\t
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "hacker/EditHackerProfile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  345 => 155,  336 => 148,  329 => 144,  323 => 141,  319 => 139,  317 => 138,  313 => 136,  305 => 131,  299 => 128,  291 => 123,  285 => 120,  277 => 115,  271 => 112,  263 => 107,  257 => 104,  253 => 102,  251 => 101,  244 => 97,  238 => 94,  230 => 89,  224 => 86,  216 => 81,  210 => 78,  205 => 75,  198 => 71,  192 => 68,  188 => 66,  186 => 65,  180 => 62,  174 => 59,  166 => 54,  160 => 51,  152 => 46,  146 => 43,  140 => 39,  134 => 36,  128 => 33,  125 => 32,  118 => 28,  112 => 25,  109 => 24,  107 => 23,  100 => 19,  97 => 18,  87 => 14,  84 => 13,  80 => 12,  76 => 11,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}

        <div class=\"row d-flex justify-content-center\" style =\"margin-top:120px;\">
            <div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:80%; padding-top:30px; padding-bottom:30px; padding-right:30px; padding-left:30px;\">

                <div class=\"row d-flex justify-content-center\">

                    {{ form_start(EditForm) }}
\t\t\t\t\t\t{% for message in app.flashes('message')%}
\t\t\t\t\t\t<div class=\" text-center\" style=\"margin-bottom:20px;\">
\t\t\t\t\t\t\t<div class=\"alert alert-success\" role=\"alert\"> {{message}}</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t{%endfor%}
\t\t\t\t\t    <div class=\" text-center\" style=\"margin-bottom:20px;\">
\t\t\t\t\t\t\t<img src=\"/Images/hackersphotos/{{app.user.photo }}\" alt=\"avatar\" style = \"width : 150px;\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
                        <div class=\"row\">
\t\t\t\t\t\t   {% if app.user.nameEnt is defined %}
\t\t\t\t\t\t\t\t<div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.nameEnt, 'Entreprise') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.nameEnt) }}
                            </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t   {%else%}
\t\t\t\t\t\t    <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.username, 'Username') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.username) }}
                            </div>
                           {% endif %}
                            
                        </div>
\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.name, 'First Name') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.name) }}
                            </div>
                        </div>
\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.fName, 'Last Name') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.fName) }}
                            </div>
                        </div>
\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.email, 'Email') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.email) }}
                            </div>
                        </div>
\t\t\t\t\t\t{% if app.user.nameEnt is defined %}
\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.num, 'téléphone') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.num) }}
                            </div>
\t\t\t\t\t\t</div>
                        {% endif %}
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.country, 'Country') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.country) }}
                            </div>
                        </div>
\t\t\t\t\t\t<div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.photo, 'Photo') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.photo) }}
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.adress, 'Adresse') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.adress) }}
                            </div>
                        </div>

                            {% if app.user.idHacker is defined %}
                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.twitter, 'Twitter') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.twitter) }}
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.linkedin, 'Linkedin') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.linkedin) }}
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.github, 'Github') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.github) }}
                            </div>
                        </div>
                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.fichierSup, 'Additional file') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.fichierSup) }}
                            </div>
                        </div>

                        {% endif %}
                        

                        {% if app.user.nameEnt is defined %}
                        <div class=\"row\">
                            <div class=\"col text-dark\" style=\"float:left; font-weight:bold; margin-bottom:5px;\">   
                                {{ form_label(EditForm.site, 'Site') }}
                            </div>
                            <div class=\"col\" style=\"float:left;\">
                                {{ form_widget(EditForm.site) }}
                            </div>
                        </div>
                        {% endif %}

\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"text-center\">
                            <button type=\"submit\" class=\"btn btn-lg shadow-sm\" style=\"background-color:rgba(217, 217, 217); font-weight:bold; color:#9D151C;\">Save</button>
                        </div>
                        

                    {{ form_end(EditForm) }}
                </div>

            </div>

        </div>
\t\t 


\t\t
{% endblock %}
", "hacker/EditHackerProfile.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\hacker\\EditHackerProfile.html.twig");
    }
}
