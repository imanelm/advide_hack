<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* offer/ent_reports.html.twig */
class __TwigTemplate_ba9007f1792c1ab94583457d4b799ac9365f12fe080415d17ecd1e10e1956ccd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/ent_reports.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/ent_reports.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "offer/ent_reports.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "\t
\t\t<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t\t    ";
        // line 8
        if ((0 !== twig_compare(twig_length_filter($this->env, (isset($context["ent_reports"]) || array_key_exists("ent_reports", $context) ? $context["ent_reports"] : (function () { throw new RuntimeError('Variable "ent_reports" does not exist.', 8, $this->source); })())), 0))) {
            // line 9
            echo "\t\t\t\t    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["ent_reports"]) || array_key_exists("ent_reports", $context) ? $context["ent_reports"] : (function () { throw new RuntimeError('Variable "ent_reports" does not exist.', 9, $this->source); })())) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 10
                echo "
\t\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t\t        \t\t\t<div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t    \t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t     \t\t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t      \t\t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t<p>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px; margin-left:30px;\">\t
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t<div style=\"color:#9D151C; font-weight:bold;\">Offer</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t<a href=\"";
                // line 24
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_reports"]) || array_key_exists("ent_reports", $context) ? $context["ent_reports"] : (function () { throw new RuntimeError('Variable "ent_reports" does not exist.', 24, $this->source); })()), $context["i"], [], "array", false, false, false, 24), "id_offer", [], "array", false, false, false, 24)]), "html", null, true);
                echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div>";
                // line 25
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_reports"]) || array_key_exists("ent_reports", $context) ? $context["ent_reports"] : (function () { throw new RuntimeError('Variable "ent_reports" does not exist.', 25, $this->source); })()), $context["i"], [], "array", false, false, false, 25), "title", [], "array", false, false, false, 25), "html", null, true);
                echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t</p>
\t\t\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t\t\t\t   \t</div>\t
\t\t\t\t\t\t\t\t\t</div>\t

\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t     \t\t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t      \t\t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t<p>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">\t
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t<div style=\"color:#9D151C; font-weight:bold;\">State</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t";
                // line 43
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_reports"]) || array_key_exists("ent_reports", $context) ? $context["ent_reports"] : (function () { throw new RuntimeError('Variable "ent_reports" does not exist.', 43, $this->source); })()), $context["i"], [], "array", false, false, false, 43), "state", [], "array", false, false, false, 43), 1))) {
                    // line 44
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-success active\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_reports"]) || array_key_exists("ent_reports", $context) ? $context["ent_reports"] : (function () { throw new RuntimeError('Variable "ent_reports" does not exist.', 44, $this->source); })()), $context["i"], [], "array", false, false, false, 44), "n_state", [], "array", false, false, false, 44), "html", null, true);
                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t";
                } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 45
(isset($context["ent_reports"]) || array_key_exists("ent_reports", $context) ? $context["ent_reports"] : (function () { throw new RuntimeError('Variable "ent_reports" does not exist.', 45, $this->source); })()), $context["i"], [], "array", false, false, false, 45), "state", [], "array", false, false, false, 45), 2))) {
                    // line 46
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-warning active text-white\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_reports"]) || array_key_exists("ent_reports", $context) ? $context["ent_reports"] : (function () { throw new RuntimeError('Variable "ent_reports" does not exist.', 46, $this->source); })()), $context["i"], [], "array", false, false, false, 46), "n_state", [], "array", false, false, false, 46), "html", null, true);
                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t";
                } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 47
(isset($context["ent_reports"]) || array_key_exists("ent_reports", $context) ? $context["ent_reports"] : (function () { throw new RuntimeError('Variable "ent_reports" does not exist.', 47, $this->source); })()), $context["i"], [], "array", false, false, false, 47), "state", [], "array", false, false, false, 47), 3))) {
                    // line 48
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-danger active\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_reports"]) || array_key_exists("ent_reports", $context) ? $context["ent_reports"] : (function () { throw new RuntimeError('Variable "ent_reports" does not exist.', 48, $this->source); })()), $context["i"], [], "array", false, false, false, 48), "n_state", [], "array", false, false, false, 48), "html", null, true);
                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t";
                }
                // line 50
                echo "\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t</p>
\t\t\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t\t\t\t   \t</div>\t
\t\t\t\t\t\t\t\t\t</div>\t

\t\t\t\t\t\t\t      \t<div class=\"col\">
\t\t\t\t\t\t\t     \t\t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t      \t\t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t<p>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\">\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 63
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("report_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_reports"]) || array_key_exists("ent_reports", $context) ? $context["ent_reports"] : (function () { throw new RuntimeError('Variable "ent_reports" does not exist.', 63, $this->source); })()), $context["i"], [], "array", false, false, false, 63), "id_report", [], "array", false, false, false, 63)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">View report</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t\t            </div>
\t\t\t\t\t\t\t\t\t\t            </div>
\t\t\t\t\t\t\t\t\t\t      \t</p>
\t\t\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t\t\t\t   \t</div>\t
\t\t\t\t\t\t\t\t\t</div>\t

\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t</div>
\t\t\t\t\t   </div>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 79
            echo " 
\t\t\t";
        }
        // line 81
        echo "\t\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "offer/ent_reports.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 81,  181 => 79,  158 => 63,  143 => 50,  137 => 48,  135 => 47,  130 => 46,  128 => 45,  123 => 44,  121 => 43,  100 => 25,  96 => 24,  80 => 10,  75 => 9,  73 => 8,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}
\t
\t\t<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t\t    {% if ent_reports|length != 0 %}
\t\t\t\t    {% for i in 0..ent_reports|length-1 %}

\t\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t\t        \t\t\t<div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t    \t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t     \t\t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t      \t\t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t<p>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px; margin-left:30px;\">\t
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t<div style=\"color:#9D151C; font-weight:bold;\">Offer</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t<a href=\"{{ url('offer_show', {'id' : ent_reports[i]['id_offer']}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div>{{ ent_reports[i]['title']}}</div>
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t</p>
\t\t\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t\t\t\t   \t</div>\t
\t\t\t\t\t\t\t\t\t</div>\t

\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t     \t\t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t      \t\t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t<p>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">\t
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t<div style=\"color:#9D151C; font-weight:bold;\">State</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t{% if ent_reports[i]['state'] == 1 %}
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-success active\">{{ ent_reports[i]['n_state']}}</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t{% elseif ent_reports[i]['state'] == 2 %}
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-warning active text-white\">{{ ent_reports[i]['n_state']}}</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t{% elseif ent_reports[i]['state'] == 3 %}
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-danger active\">{{ ent_reports[i]['n_state']}}</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t</p>
\t\t\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t\t\t\t   \t</div>\t
\t\t\t\t\t\t\t\t\t</div>\t

\t\t\t\t\t\t\t      \t<div class=\"col\">
\t\t\t\t\t\t\t     \t\t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t      \t\t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t<p>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\">\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{url('report_show', {'id' : ent_reports[i]['id_report']}) }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">View report</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t\t            </div>
\t\t\t\t\t\t\t\t\t\t            </div>
\t\t\t\t\t\t\t\t\t\t      \t</p>
\t\t\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t\t\t\t   \t</div>\t
\t\t\t\t\t\t\t\t\t</div>\t

\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t</div>
\t\t\t\t\t   </div>
\t\t\t\t\t{% endfor %} 
\t\t\t{% endif %}
\t\t</div>
{% endblock %}", "offer/ent_reports.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\offer\\ent_reports.html.twig");
    }
}
