<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home_page/index.html.twig */
class __TwigTemplate_9b22ecbd847be4cdf2735eff30170c38a95a048bcf4e551c42b893beedc8d139 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home_page/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home_page/index.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "home_page/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
    <div class=\"text-center\" style = \"padding-top: 78px;\">
        <img src=\"Images/logo.png\" alt=\"...\" height=\"250px\">
      </div>

      <!-- About Us -->
      <div class=\"card bg-transparent text-white rounded-0 border-0\">
        <img src=\"Images/AboutUs_bg.png\" alt=\"...\" width=\"100%\">
        <div class=\"card-img-overlay\">
          <div class=\"card border-0 rounded-0 shadow about-item\" style=\"width:50%; background-color:rgba(256, 256, 256, 0.5); top: 37%; left: 5%;\" data-toggle=\"modal\" data-target=\"#aboutModal1\">
            <div class=\"card-body\">
              <h3 class=\"card-title text-center\">About Us</h3>
              <p class=\"card-text\">
                <b>Advise'Hack</b> is a platform that gives the opportunity to any enterprise to look for any vulnerabilities that my be laying in her systems and, eventually, repairing them. 
              </p>
            </div>
          </div>
        </div>
      </div>

    <!-- About Modal -->
    <div class=\"about-modal modal fade\" id=\"aboutModal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"aboutModal1Label\" aria-hidden=\"true\">
      <div class=\"modal-dialog modal-xl\" role=\"document\">
          <div class=\"modal-content\">
              <button class=\"close\" type=\"button\" data-dismiss=\"modal\" aria-label=\"Close\">
                  <span aria-hidden=\"true\"><i class=\"fas fa-times\"></i></span>
              </button>
              <div class=\"modal-body text-center\">
                      <div class=\"row justify-content-center\">
                          <div class=\"col-lg-8\">
                              <!-- About Modal - Title-->
                              <h2 class=\"text-secondary mb-0\" id=\"aboutModal1Label\">More about us</h2>
                              <!-- About Modal - Text-->
                              <div class=\"card-text\" style=\"margin-top:50px;\">

                                <div class=\"row\">
                                  <h5 style=\"color:#9D151C; margin-bottom:20px;\">Advise'Hack is the one for you</h5>
                                </div>
                                
                                <div class=\"row justify-content-center\">
                                  <div class=\"col-lg-4\">
                                    <div class=\"card border-0 text-center\" style=\"width:200px; height:200px; padding:-20px;\">
                                      <div class=\"row justify-content-center\">
                                        <img src=\"Images/team.png\" alt=\"...\" class=\"card-text text-center\" style=\"width:30%; height:30%; margin-bottom:10px;\">
                                      </div>
                                      <div class=\"row justify-content-center\">
                                        <div class=\"card-text text-center\" style=\"color:#9D151C; font-weight:bold;\">
                                          Trust
                                        </div>
                                        <div class=\"card-text text-center text-dark\">
                                          Our security system garanties the confidentiality of your data.
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class=\"col-lg-4\">
                                    <div class=\"card border-0 text-center\" style=\"width:200px; height:200px; padding:-20px;\">
                                      <div class=\"row justify-content-center\">
                                        <img src=\"Images/flexible.png\" alt=\"...\" class=\"card-text text-center\" style=\"width:30%; height:30%; margin-bottom:10px;\">
                                      </div>
                                      <div class=\"row justify-content-center\">
                                        <div class=\"card-text text-center\" style=\"color:#9D151C; font-weight:bold;\">
                                          Flexibility
                                        </div>
                                        <div class=\"card-text text-center text-dark\">
                                          Our offers not only suit your needs, but also your means. Whatever the size of your enterprise is, Advise'Hack is here for you. 
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class=\"col-lg-4\">
                                    <div class=\"card border-0 text-center\" style=\"width:200px; height:200px; padding:-20px;\">
                                      <div class=\"row justify-content-center\">
                                        <img src=\"Images/personal.png\" alt=\"...\" class=\"card-text text-center\" style=\"width:30%; height:30%; margin-bottom:10px;\">
                                      </div>
                                      <div class=\"row justify-content-center\">
                                        <div class=\"card-text text-center\" style=\"color:#9D151C; font-weight:bold;\">
                                          Skills
                                        </div>
                                        <div class=\"card-text text-center text-dark\">
                                          We make sure that our hackers are highly skilled in order to provide you with the best services.
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class=\"row justify-content-center\" style=\"margin-top:30px;\">
                                  <div class=\"col-lg-4\">
                                    <div class=\"card border-0 text-center\" style=\"width:200px; height:200px; padding:-20px;\">
                                      <div class=\"row justify-content-center\">
                                        <img src=\"Images/good.png\" alt=\"...\" class=\"card-text text-center\" style=\"width:30%; height:30%; margin-bottom:10px;\">
                                      </div>
                                      <div class=\"row justify-content-center\">
                                        <div class=\"card-text text-center\" style=\"color:#9D151C; font-weight:bold;\">
                                          Easy to use
                                        </div>
                                        <div class=\"card-text text-center text-dark\">
                                          Our platform is created in a way that makes its features very accessible and easy to use. 
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <div class=\"col-lg-4\">
                                    <div class=\"card border-0 text-center\" style=\"width:200px; height:200px; padding:-20px;\">
                                      <div class=\"row justify-content-center\">
                                        <img src=\"Images/evolution.png\" alt=\"...\" class=\"card-text text-center\" style=\"width:30%; height:30%; margin-bottom:10px;\">
                                      </div>
                                      <div class=\"row justify-content-center\">
                                        <div class=\"card-text text-center\" style=\"color:#9D151C; font-weight:bold;\">
                                          Continuous evolution
                                        </div>
                                        <div class=\"card-text text-center text-dark\">
                                          Our platform is also a way for the hackers to compete, learn and share knowledge with each other. 
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>


                                <div class=\"row justify-content-start\" style=\"margin-top:60px;\">
                                  <h5 style=\"color:#9D151C; margin-bottom:30px;\">The people behind the screen</h5>
                                </div>

                                <div class=\"row justify-content-start\">
                                  <div class=\"col-sm-2\">
                                    <div class=\"card shadow border-0\" style=\"width:120px; height:120px; padding:-20px;\">
                                      <img src=\"Images/hackersphotos/1a8b2d0f316d3d6f31411b896c6c206e.png\" alt=\"...\" class=\"card-text\" style=\"width:100%;\">
                                    </div>
                                  </div>
                                  <div class=\"col-5\">
                                    <div class=\"card border-0 text-left\" style=\"height:120px; width:400px;\">
                                      <div class=\"card-text\" style=\"font-weight:bold; color:#9D151C; margin-left:20px;\">
                                        Imane El Moul
                                      </div>
                                      <div class=\"card-text text-dark\" style=\"margin-left:20px;\">
                                        Bla bla bla
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class=\"row justify-content-end\" style=\"margin-top:30px;\">
                                  <div class=\"col-sm-2\">
                                    <div class=\"card shadow border-0\" style=\"width:120px; height:120px; padding:-20px;\">
                                      <img src=\"Images/hackersphotos/1a8b2d0f316d3d6f31411b896c6c206e.png\" alt=\"...\" class=\"card-text\" style=\"width:100%;\">
                                    </div>
                                  </div>
                                  <div class=\"col-4\">
                                    <div class=\"card border-0 text-left\" style=\"height:120px; width:400px;\">
                                      <div class=\"card-text\" style=\"font-weight:bold; color:#9D151C; margin-left:20px;\">
                                        Hicham Rahj
                                      </div>
                                      <div class=\"card-text text-dark\" style=\"margin-left:20px;\">
                                        Bla bla bla
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class=\"row justify-content-start\" style=\"margin-top:30px;\">
                                  <div class=\"col-sm-2\">
                                    <div class=\"card shadow border-0\" style=\"width:120px; height:120px; padding:-20px;\">
                                      <img src=\"Images/houda.jpeg\" alt=\"...\" class=\"card-text\" style=\"width:100%;\">
                                    </div>
                                  </div>
                                  <div class=\"col-5\">
                                    <div class=\"card border-0 text-left\" style=\"height:120px; width:400px;\">
                                      <div class=\"card-text\" style=\"font-weight:bold; color:#9D151C; margin-left:20px;\">
                                        Houda Berrada
                                      </div>
                                      <div class=\"card-text text-dark\" style=\"margin-left:20px;\">
                                        Financial ingineering student in her final school year. <br>
                                        <i><b>\"</b>This whole project was a novelty for me, which made it even more fascinating and made</i> me <i>even more eager to learn and document myself about everything.<b>\"</b></i>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class=\"row justify-content-end\" style=\"margin-top:30px;\">
                                  <div class=\"col-sm-2\">
                                    <div class=\"card shadow border-0\" style=\"width:120px; height:120px; padding:-20px;\">
                                      <img src=\"Images/hackersphotos/1a8b2d0f316d3d6f31411b896c6c206e.png\" alt=\"...\" class=\"card-text\" style=\"width:100%;\">
                                    </div>
                                  </div>
                                  <div class=\"col-4\">
                                    <div class=\"card border-0 text-left\" style=\"height:120px; width:400px;\">
                                      <div class=\"card-text\" style=\"font-weight:bold; color:#9D151C; margin-left:20px;\">
                                        Fatiha Ougali
                                      </div>
                                      <div class=\"card-text text-dark\" style=\"margin-left:20px;\">
                                        Bla bla bla
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                              <button class=\"btn-sm shadow-lg border-0 text-white\" style=\"background-color:#9D151C; margin-top:50px;\" data-dismiss=\"modal\">
                                  <i class=\"fas fa-times fa-fw\"></i>
                                  Close Window
                              </button>
                          </div>
                      </div>

              </div>
          </div>
      </div>
    </div>  

    <!- Services -!>
      <div class=\"card bg-transparent text-white rounded-0 border-0\">
        <img src=\"Images/Services_bg.png\" alt=\"...\" width=\"100%\">
        <div class=\"card-img-overlay\" style = \"padding : 2%;\">
          <div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(256, 256, 256, 0.5); width:100%; margin-bottom: 20px;\">
            <div class=\"card-body\">
              <h3 class=\"text-title text-center text-secondary\">Services</h3>
            </div>
          </div>
          <div class=\"container\">
            <div class=\"row\">
              <div class=\"col-sm-6\">
                <div class=\"card border-0 rounded-0 shadow services-item\" style=\"background-color:rgba(256, 256, 256, 0.5);\" data-toggle=\"modal\" data-target=\"#servicesModal1\">
                  <div class=\"card-body services-item-caption\">
                    <h3 class=\"card-title text-center services-item-caption-content\" style=\"color:#9D151C;\">Find It</h3>
                    <p class=\"card-text text-secondary text-center\">Click to see more</p>
                  </div>
                </div>
              </div>
              <div class=\"col-sm-6\">
                <div class=\"card border-0 rounded-0 shadow services-item\" style=\"background-color:rgba(256, 256, 256, 0.5);\" data-toggle=\"modal\" data-target=\"#servicesModal2\">
                  <div class=\"card-body services-item-caption\">
                    <h3 class=\"card-title text-center services-item-caption-content\" style=\"color:#9D151C;\">Find It & Fix It</h3>
                    <p class=\"card-text text-secondary text-center\">Click to see more</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
\t\t  </div>

    <!-- Services Modals !-->
    <!-- Services Modal 1-->
        <div class=\"services-modal modal fade\" id=\"servicesModal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"servicesModal1Label\" aria-hidden=\"true\">
            <div class=\"modal-dialog modal-xl\" role=\"document\">
                <div class=\"modal-content\">
                    <button class=\"close\" type=\"button\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\"><i class=\"fas fa-times\"></i></span>
                    </button>
                    <div class=\"modal-body text-center\">
                        <div class=\"container\">
                            <div class=\"row justify-content-center\">
                                <div class=\"col-lg-8\">
                                    <!-- Services Modal - Title-->
                                    <h2 class=\"text-secondary mb-0\" id=\"servicesModal1Label\">Find It</h2>
                                    <!-- Services Modal - Text-->
                                    <p class=\"card-text text-dark\">By choosing this package, you, as an enterprise, are asking hackers to detect the vulnerabilities that may eventually be in your computer system.</p>
                                    <button class=\"btn-sm shadow-lg border-0 text-white\" style=\"background-color:#9D151C;\" data-dismiss=\"modal\">
                                        <i class=\"fas fa-times fa-fw\"></i>
                                        Close Window
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- Services Modal 2-->
        <div class=\"services-modal modal fade\" id=\"servicesModal2\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"servicesModal2Label\" aria-hidden=\"true\">
            <div class=\"modal-dialog modal-xl\" role=\"document\">
                <div class=\"modal-content\">
                    <button class=\"close\" type=\"button\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\"><i class=\"fas fa-times\"></i></span>
                    </button>
                    <div class=\"modal-body text-center\">
                        <div class=\"container\">
                            <div class=\"row justify-content-center\">
                                <div class=\"col-lg-8\">
                                    <!-- Services Modal - Title-->
                                    <h2 class=\"text-secondary mb-0\" id=\"servicesModal1Label\">Find It & Fix It</h2>
                                    <!-- Services Modal - Text-->
                                    <p class=\"card-text text-dark\">By choosing this package, you, as an enterprise, are not asking hackers to detect the vulnerabilities that may eventually be in your computer system, but also to resolve them. You will therefore be guaranteed to have a perfectly safe system !</p>
                                    <button class=\"btn-sm shadow-lg border-0 text-white\" style=\"background-color:#9D151C;\" data-dismiss=\"modal\">
                                        <i class=\"fas fa-times fa-fw\"></i>
                                        Close Window
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  

    <!- Offers -!>
      <div class=\"card bg-transparent text-white rounded-0 border-0 text-center\">
        <div class=\"card-overlay\" style = \"padding : 2%;\">

          <div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(127, 21, 28);\">
            <div class=\"card-body text-center\">
              <h3 class=\"text-white\">Offers</h3>
            </div>
          </div>

          <div id=\"carouselOffersIndicators\" class=\"carousel slide\" data-ride=\"carousel\" style=\"margin-top:20px;\">
            <ol class=\"carousel-indicators\">
              ";
        // line 315
        if ((twig_get_attribute($this->env, $this->source, ($context["offers"] ?? null), 0, [], "array", true, true, false, 315) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 315, $this->source); })()), 0, [], "array", false, false, false, 315), "showAuth", [], "array", false, false, false, 315), 1)))) {
            // line 316
            echo "                <li data-target=\"#carouselOffersIndicators\" data-slide-to=\"0\" class=\"active\"></li>
                ";
            // line 317
            if ((twig_get_attribute($this->env, $this->source, ($context["offers"] ?? null), 1, [], "array", true, true, false, 317) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 317, $this->source); })()), 1, [], "array", false, false, false, 317), "showAuth", [], "array", false, false, false, 317), 1)))) {
                // line 318
                echo "                  <li data-target=\"#carouselOffersIndicators\" data-slide-to=\"1\"></li>
                  ";
                // line 319
                if ((twig_get_attribute($this->env, $this->source, ($context["offers"] ?? null), 2, [], "array", true, true, false, 319) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 319, $this->source); })()), 2, [], "array", false, false, false, 319), "showAuth", [], "array", false, false, false, 319), 1)))) {
                    // line 320
                    echo "                    <li data-target=\"#carouselOffersIndicators\" data-slide-to=\"2\"></li>
                  ";
                }
                // line 322
                echo "                ";
            }
            // line 323
            echo "              ";
        }
        // line 324
        echo "            </ol>

            <div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(127, 21, 28, 0.5);\">
              <div class=\"card-body\">
                <div class=\"carousel-inner\">

                  ";
        // line 330
        if ((twig_get_attribute($this->env, $this->source, ($context["offers"] ?? null), 0, [], "array", true, true, false, 330) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 330, $this->source); })()), 0, [], "array", false, false, false, 330), "showAuth", [], "array", false, false, false, 330), 1)))) {
            // line 331
            echo "                    
                        <div class=\"carousel-item active\">
                          <h5 class=\"card-title text-center\">
                            <a class=\"text-white\" href=\"";
            // line 334
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 334, $this->source); })()), 0, [], "array", false, false, false, 334), "id_offer", [], "array", false, false, false, 334)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 334, $this->source); })()), 0, [], "array", false, false, false, 334), "title", [], "array", false, false, false, 334), "html", null, true);
            echo "</a>
                          </h5>
                          <p class=\"card-text\">";
            // line 336
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 336, $this->source); })()), 0, [], "array", false, false, false, 336), "description1", [], "array", false, false, false, 336), "html", null, true);
            echo "</p><br>
                        </div>
                      
                    ";
            // line 339
            if ((twig_get_attribute($this->env, $this->source, ($context["offers"] ?? null), 1, [], "array", true, true, false, 339) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 339, $this->source); })()), 1, [], "array", false, false, false, 339), "showAuth", [], "array", false, false, false, 339), 1)))) {
                // line 340
                echo "                      
                          <div class=\"carousel-item\">
                            <h5 class=\"card-title text-center\">
                              <a class=\"text-white\" href=\"";
                // line 343
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 343, $this->source); })()), 0, [], "array", false, false, false, 343), "id_offer", [], "array", false, false, false, 343)]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 343, $this->source); })()), 1, [], "array", false, false, false, 343), "title", [], "array", false, false, false, 343), "html", null, true);
                echo "</a>
                            </h5>
                            <p class=\"card-text\">";
                // line 345
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 345, $this->source); })()), 1, [], "array", false, false, false, 345), "description1", [], "array", false, false, false, 345), "html", null, true);
                echo "</p><br>
                          </div>
                        
                      ";
                // line 348
                if ((twig_get_attribute($this->env, $this->source, ($context["offers"] ?? null), 2, [], "array", true, true, false, 348) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 348, $this->source); })()), 2, [], "array", false, false, false, 348), "showAuth", [], "array", false, false, false, 348), 1)))) {
                    // line 349
                    echo "                        
                          <div class=\"carousel-item\">
                            <h5 class=\"card-title text-center\">
                              <a class=\"text-white\" href=\"";
                    // line 352
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 352, $this->source); })()), 0, [], "array", false, false, false, 352), "id_offer", [], "array", false, false, false, 352)]), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 352, $this->source); })()), 2, [], "array", false, false, false, 352), "title", [], "array", false, false, false, 352), "html", null, true);
                    echo "</a>
                            </h5>
                            <p class=\"card-text\">";
                    // line 354
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["offers"]) || array_key_exists("offers", $context) ? $context["offers"] : (function () { throw new RuntimeError('Variable "offers" does not exist.', 354, $this->source); })()), 2, [], "array", false, false, false, 354), "description1", [], "array", false, false, false, 354), "html", null, true);
                    echo "</p><br>
                          </div>
                        
                      ";
                }
                // line 358
                echo "                    ";
            }
            // line 359
            echo "                  ";
        }
        // line 360
        echo "
                </div>
              </div>
            </div>

            <a class=\"carousel-control-prev\" href=\"#carouselOffersIndicators\" role=\"button\" data-slide=\"prev\">
              <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
              <span class=\"sr-only\">Previous</span>
            </a>
            <a class=\"carousel-control-next\" href=\"#carouselOffersIndicators\" role=\"button\" data-slide=\"next\">
              <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
              <span class=\"sr-only\">Next</span>
            </a>
          </div>

      </div>
    </div>

  <!- Blog -!>
    <div class=\"card text-white rounded-0 border-0 text-center\" style=\"background-color:rgba(127, 21, 28);\">
      <div class=\"card-overlay\" style = \"padding : 2%;\">

        <div class=\"card border-0 rounded-0 shadow-lg\" style=\"background-color:#9D151C;\" >
          <div class=\"card-body text-center\">
            <h3 class=\"text-white\">Latest News</h3>
          </div>
        </div>

        <a href=\"";
        // line 388
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("blog");
        echo "\">
          <button type=\"button\" class=\"button shadow\" style=\"margin-top:30px; margin-bottom:30px;\">
            <span style=\"color:#9D151C; font-weight:bold;\">Visit Our Blog</span>
          </button>
        </a>

          

        
      </div>
    </div>



";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home_page/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  507 => 388,  477 => 360,  474 => 359,  471 => 358,  464 => 354,  457 => 352,  452 => 349,  450 => 348,  444 => 345,  437 => 343,  432 => 340,  430 => 339,  424 => 336,  417 => 334,  412 => 331,  410 => 330,  402 => 324,  399 => 323,  396 => 322,  392 => 320,  390 => 319,  387 => 318,  385 => 317,  382 => 316,  380 => 315,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}

    <div class=\"text-center\" style = \"padding-top: 78px;\">
        <img src=\"Images/logo.png\" alt=\"...\" height=\"250px\">
      </div>

      <!-- About Us -->
      <div class=\"card bg-transparent text-white rounded-0 border-0\">
        <img src=\"Images/AboutUs_bg.png\" alt=\"...\" width=\"100%\">
        <div class=\"card-img-overlay\">
          <div class=\"card border-0 rounded-0 shadow about-item\" style=\"width:50%; background-color:rgba(256, 256, 256, 0.5); top: 37%; left: 5%;\" data-toggle=\"modal\" data-target=\"#aboutModal1\">
            <div class=\"card-body\">
              <h3 class=\"card-title text-center\">About Us</h3>
              <p class=\"card-text\">
                <b>Advise'Hack</b> is a platform that gives the opportunity to any enterprise to look for any vulnerabilities that my be laying in her systems and, eventually, repairing them. 
              </p>
            </div>
          </div>
        </div>
      </div>

    <!-- About Modal -->
    <div class=\"about-modal modal fade\" id=\"aboutModal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"aboutModal1Label\" aria-hidden=\"true\">
      <div class=\"modal-dialog modal-xl\" role=\"document\">
          <div class=\"modal-content\">
              <button class=\"close\" type=\"button\" data-dismiss=\"modal\" aria-label=\"Close\">
                  <span aria-hidden=\"true\"><i class=\"fas fa-times\"></i></span>
              </button>
              <div class=\"modal-body text-center\">
                      <div class=\"row justify-content-center\">
                          <div class=\"col-lg-8\">
                              <!-- About Modal - Title-->
                              <h2 class=\"text-secondary mb-0\" id=\"aboutModal1Label\">More about us</h2>
                              <!-- About Modal - Text-->
                              <div class=\"card-text\" style=\"margin-top:50px;\">

                                <div class=\"row\">
                                  <h5 style=\"color:#9D151C; margin-bottom:20px;\">Advise'Hack is the one for you</h5>
                                </div>
                                
                                <div class=\"row justify-content-center\">
                                  <div class=\"col-lg-4\">
                                    <div class=\"card border-0 text-center\" style=\"width:200px; height:200px; padding:-20px;\">
                                      <div class=\"row justify-content-center\">
                                        <img src=\"Images/team.png\" alt=\"...\" class=\"card-text text-center\" style=\"width:30%; height:30%; margin-bottom:10px;\">
                                      </div>
                                      <div class=\"row justify-content-center\">
                                        <div class=\"card-text text-center\" style=\"color:#9D151C; font-weight:bold;\">
                                          Trust
                                        </div>
                                        <div class=\"card-text text-center text-dark\">
                                          Our security system garanties the confidentiality of your data.
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class=\"col-lg-4\">
                                    <div class=\"card border-0 text-center\" style=\"width:200px; height:200px; padding:-20px;\">
                                      <div class=\"row justify-content-center\">
                                        <img src=\"Images/flexible.png\" alt=\"...\" class=\"card-text text-center\" style=\"width:30%; height:30%; margin-bottom:10px;\">
                                      </div>
                                      <div class=\"row justify-content-center\">
                                        <div class=\"card-text text-center\" style=\"color:#9D151C; font-weight:bold;\">
                                          Flexibility
                                        </div>
                                        <div class=\"card-text text-center text-dark\">
                                          Our offers not only suit your needs, but also your means. Whatever the size of your enterprise is, Advise'Hack is here for you. 
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class=\"col-lg-4\">
                                    <div class=\"card border-0 text-center\" style=\"width:200px; height:200px; padding:-20px;\">
                                      <div class=\"row justify-content-center\">
                                        <img src=\"Images/personal.png\" alt=\"...\" class=\"card-text text-center\" style=\"width:30%; height:30%; margin-bottom:10px;\">
                                      </div>
                                      <div class=\"row justify-content-center\">
                                        <div class=\"card-text text-center\" style=\"color:#9D151C; font-weight:bold;\">
                                          Skills
                                        </div>
                                        <div class=\"card-text text-center text-dark\">
                                          We make sure that our hackers are highly skilled in order to provide you with the best services.
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class=\"row justify-content-center\" style=\"margin-top:30px;\">
                                  <div class=\"col-lg-4\">
                                    <div class=\"card border-0 text-center\" style=\"width:200px; height:200px; padding:-20px;\">
                                      <div class=\"row justify-content-center\">
                                        <img src=\"Images/good.png\" alt=\"...\" class=\"card-text text-center\" style=\"width:30%; height:30%; margin-bottom:10px;\">
                                      </div>
                                      <div class=\"row justify-content-center\">
                                        <div class=\"card-text text-center\" style=\"color:#9D151C; font-weight:bold;\">
                                          Easy to use
                                        </div>
                                        <div class=\"card-text text-center text-dark\">
                                          Our platform is created in a way that makes its features very accessible and easy to use. 
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <div class=\"col-lg-4\">
                                    <div class=\"card border-0 text-center\" style=\"width:200px; height:200px; padding:-20px;\">
                                      <div class=\"row justify-content-center\">
                                        <img src=\"Images/evolution.png\" alt=\"...\" class=\"card-text text-center\" style=\"width:30%; height:30%; margin-bottom:10px;\">
                                      </div>
                                      <div class=\"row justify-content-center\">
                                        <div class=\"card-text text-center\" style=\"color:#9D151C; font-weight:bold;\">
                                          Continuous evolution
                                        </div>
                                        <div class=\"card-text text-center text-dark\">
                                          Our platform is also a way for the hackers to compete, learn and share knowledge with each other. 
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>


                                <div class=\"row justify-content-start\" style=\"margin-top:60px;\">
                                  <h5 style=\"color:#9D151C; margin-bottom:30px;\">The people behind the screen</h5>
                                </div>

                                <div class=\"row justify-content-start\">
                                  <div class=\"col-sm-2\">
                                    <div class=\"card shadow border-0\" style=\"width:120px; height:120px; padding:-20px;\">
                                      <img src=\"Images/hackersphotos/1a8b2d0f316d3d6f31411b896c6c206e.png\" alt=\"...\" class=\"card-text\" style=\"width:100%;\">
                                    </div>
                                  </div>
                                  <div class=\"col-5\">
                                    <div class=\"card border-0 text-left\" style=\"height:120px; width:400px;\">
                                      <div class=\"card-text\" style=\"font-weight:bold; color:#9D151C; margin-left:20px;\">
                                        Imane El Moul
                                      </div>
                                      <div class=\"card-text text-dark\" style=\"margin-left:20px;\">
                                        Bla bla bla
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class=\"row justify-content-end\" style=\"margin-top:30px;\">
                                  <div class=\"col-sm-2\">
                                    <div class=\"card shadow border-0\" style=\"width:120px; height:120px; padding:-20px;\">
                                      <img src=\"Images/hackersphotos/1a8b2d0f316d3d6f31411b896c6c206e.png\" alt=\"...\" class=\"card-text\" style=\"width:100%;\">
                                    </div>
                                  </div>
                                  <div class=\"col-4\">
                                    <div class=\"card border-0 text-left\" style=\"height:120px; width:400px;\">
                                      <div class=\"card-text\" style=\"font-weight:bold; color:#9D151C; margin-left:20px;\">
                                        Hicham Rahj
                                      </div>
                                      <div class=\"card-text text-dark\" style=\"margin-left:20px;\">
                                        Bla bla bla
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class=\"row justify-content-start\" style=\"margin-top:30px;\">
                                  <div class=\"col-sm-2\">
                                    <div class=\"card shadow border-0\" style=\"width:120px; height:120px; padding:-20px;\">
                                      <img src=\"Images/houda.jpeg\" alt=\"...\" class=\"card-text\" style=\"width:100%;\">
                                    </div>
                                  </div>
                                  <div class=\"col-5\">
                                    <div class=\"card border-0 text-left\" style=\"height:120px; width:400px;\">
                                      <div class=\"card-text\" style=\"font-weight:bold; color:#9D151C; margin-left:20px;\">
                                        Houda Berrada
                                      </div>
                                      <div class=\"card-text text-dark\" style=\"margin-left:20px;\">
                                        Financial ingineering student in her final school year. <br>
                                        <i><b>\"</b>This whole project was a novelty for me, which made it even more fascinating and made</i> me <i>even more eager to learn and document myself about everything.<b>\"</b></i>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class=\"row justify-content-end\" style=\"margin-top:30px;\">
                                  <div class=\"col-sm-2\">
                                    <div class=\"card shadow border-0\" style=\"width:120px; height:120px; padding:-20px;\">
                                      <img src=\"Images/hackersphotos/1a8b2d0f316d3d6f31411b896c6c206e.png\" alt=\"...\" class=\"card-text\" style=\"width:100%;\">
                                    </div>
                                  </div>
                                  <div class=\"col-4\">
                                    <div class=\"card border-0 text-left\" style=\"height:120px; width:400px;\">
                                      <div class=\"card-text\" style=\"font-weight:bold; color:#9D151C; margin-left:20px;\">
                                        Fatiha Ougali
                                      </div>
                                      <div class=\"card-text text-dark\" style=\"margin-left:20px;\">
                                        Bla bla bla
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                              <button class=\"btn-sm shadow-lg border-0 text-white\" style=\"background-color:#9D151C; margin-top:50px;\" data-dismiss=\"modal\">
                                  <i class=\"fas fa-times fa-fw\"></i>
                                  Close Window
                              </button>
                          </div>
                      </div>

              </div>
          </div>
      </div>
    </div>  

    <!- Services -!>
      <div class=\"card bg-transparent text-white rounded-0 border-0\">
        <img src=\"Images/Services_bg.png\" alt=\"...\" width=\"100%\">
        <div class=\"card-img-overlay\" style = \"padding : 2%;\">
          <div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(256, 256, 256, 0.5); width:100%; margin-bottom: 20px;\">
            <div class=\"card-body\">
              <h3 class=\"text-title text-center text-secondary\">Services</h3>
            </div>
          </div>
          <div class=\"container\">
            <div class=\"row\">
              <div class=\"col-sm-6\">
                <div class=\"card border-0 rounded-0 shadow services-item\" style=\"background-color:rgba(256, 256, 256, 0.5);\" data-toggle=\"modal\" data-target=\"#servicesModal1\">
                  <div class=\"card-body services-item-caption\">
                    <h3 class=\"card-title text-center services-item-caption-content\" style=\"color:#9D151C;\">Find It</h3>
                    <p class=\"card-text text-secondary text-center\">Click to see more</p>
                  </div>
                </div>
              </div>
              <div class=\"col-sm-6\">
                <div class=\"card border-0 rounded-0 shadow services-item\" style=\"background-color:rgba(256, 256, 256, 0.5);\" data-toggle=\"modal\" data-target=\"#servicesModal2\">
                  <div class=\"card-body services-item-caption\">
                    <h3 class=\"card-title text-center services-item-caption-content\" style=\"color:#9D151C;\">Find It & Fix It</h3>
                    <p class=\"card-text text-secondary text-center\">Click to see more</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
\t\t  </div>

    <!-- Services Modals !-->
    <!-- Services Modal 1-->
        <div class=\"services-modal modal fade\" id=\"servicesModal1\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"servicesModal1Label\" aria-hidden=\"true\">
            <div class=\"modal-dialog modal-xl\" role=\"document\">
                <div class=\"modal-content\">
                    <button class=\"close\" type=\"button\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\"><i class=\"fas fa-times\"></i></span>
                    </button>
                    <div class=\"modal-body text-center\">
                        <div class=\"container\">
                            <div class=\"row justify-content-center\">
                                <div class=\"col-lg-8\">
                                    <!-- Services Modal - Title-->
                                    <h2 class=\"text-secondary mb-0\" id=\"servicesModal1Label\">Find It</h2>
                                    <!-- Services Modal - Text-->
                                    <p class=\"card-text text-dark\">By choosing this package, you, as an enterprise, are asking hackers to detect the vulnerabilities that may eventually be in your computer system.</p>
                                    <button class=\"btn-sm shadow-lg border-0 text-white\" style=\"background-color:#9D151C;\" data-dismiss=\"modal\">
                                        <i class=\"fas fa-times fa-fw\"></i>
                                        Close Window
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- Services Modal 2-->
        <div class=\"services-modal modal fade\" id=\"servicesModal2\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"servicesModal2Label\" aria-hidden=\"true\">
            <div class=\"modal-dialog modal-xl\" role=\"document\">
                <div class=\"modal-content\">
                    <button class=\"close\" type=\"button\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\"><i class=\"fas fa-times\"></i></span>
                    </button>
                    <div class=\"modal-body text-center\">
                        <div class=\"container\">
                            <div class=\"row justify-content-center\">
                                <div class=\"col-lg-8\">
                                    <!-- Services Modal - Title-->
                                    <h2 class=\"text-secondary mb-0\" id=\"servicesModal1Label\">Find It & Fix It</h2>
                                    <!-- Services Modal - Text-->
                                    <p class=\"card-text text-dark\">By choosing this package, you, as an enterprise, are not asking hackers to detect the vulnerabilities that may eventually be in your computer system, but also to resolve them. You will therefore be guaranteed to have a perfectly safe system !</p>
                                    <button class=\"btn-sm shadow-lg border-0 text-white\" style=\"background-color:#9D151C;\" data-dismiss=\"modal\">
                                        <i class=\"fas fa-times fa-fw\"></i>
                                        Close Window
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  

    <!- Offers -!>
      <div class=\"card bg-transparent text-white rounded-0 border-0 text-center\">
        <div class=\"card-overlay\" style = \"padding : 2%;\">

          <div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(127, 21, 28);\">
            <div class=\"card-body text-center\">
              <h3 class=\"text-white\">Offers</h3>
            </div>
          </div>

          <div id=\"carouselOffersIndicators\" class=\"carousel slide\" data-ride=\"carousel\" style=\"margin-top:20px;\">
            <ol class=\"carousel-indicators\">
              {% if offers[0] is defined and offers[0]['showAuth'] == 1%}
                <li data-target=\"#carouselOffersIndicators\" data-slide-to=\"0\" class=\"active\"></li>
                {% if offers[1] is defined and offers[1]['showAuth'] == 1 %}
                  <li data-target=\"#carouselOffersIndicators\" data-slide-to=\"1\"></li>
                  {% if offers[2] is defined and offers[2]['showAuth'] == 1 %}
                    <li data-target=\"#carouselOffersIndicators\" data-slide-to=\"2\"></li>
                  {% endif%}
                {% endif%}
              {% endif%}
            </ol>

            <div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(127, 21, 28, 0.5);\">
              <div class=\"card-body\">
                <div class=\"carousel-inner\">

                  {% if offers[0] is defined and offers[0]['showAuth'] == 1%}
                    
                        <div class=\"carousel-item active\">
                          <h5 class=\"card-title text-center\">
                            <a class=\"text-white\" href=\"{{ url('offer_show', {'id' : offers[0]['id_offer']}) }}\">{{ offers[0]['title']}}</a>
                          </h5>
                          <p class=\"card-text\">{{ offers[0]['description1']}}</p><br>
                        </div>
                      
                    {% if offers[1] is defined and offers[1]['showAuth'] == 1%}
                      
                          <div class=\"carousel-item\">
                            <h5 class=\"card-title text-center\">
                              <a class=\"text-white\" href=\"{{ url('offer_show', {'id' : offers[0]['id_offer']}) }}\">{{ offers[1]['title']}}</a>
                            </h5>
                            <p class=\"card-text\">{{ offers[1]['description1']}}</p><br>
                          </div>
                        
                      {% if offers[2] is defined and offers[2]['showAuth'] == 1%}
                        
                          <div class=\"carousel-item\">
                            <h5 class=\"card-title text-center\">
                              <a class=\"text-white\" href=\"{{ url('offer_show', {'id' : offers[0]['id_offer']}) }}\">{{ offers[2]['title']}}</a>
                            </h5>
                            <p class=\"card-text\">{{ offers[2]['description1']}}</p><br>
                          </div>
                        
                      {% endif%}
                    {% endif%}
                  {% endif%}

                </div>
              </div>
            </div>

            <a class=\"carousel-control-prev\" href=\"#carouselOffersIndicators\" role=\"button\" data-slide=\"prev\">
              <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
              <span class=\"sr-only\">Previous</span>
            </a>
            <a class=\"carousel-control-next\" href=\"#carouselOffersIndicators\" role=\"button\" data-slide=\"next\">
              <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
              <span class=\"sr-only\">Next</span>
            </a>
          </div>

      </div>
    </div>

  <!- Blog -!>
    <div class=\"card text-white rounded-0 border-0 text-center\" style=\"background-color:rgba(127, 21, 28);\">
      <div class=\"card-overlay\" style = \"padding : 2%;\">

        <div class=\"card border-0 rounded-0 shadow-lg\" style=\"background-color:#9D151C;\" >
          <div class=\"card-body text-center\">
            <h3 class=\"text-white\">Latest News</h3>
          </div>
        </div>

        <a href=\"{{ url('blog')}}\">
          <button type=\"button\" class=\"button shadow\" style=\"margin-top:30px; margin-bottom:30px;\">
            <span style=\"color:#9D151C; font-weight:bold;\">Visit Our Blog</span>
          </button>
        </a>

          

        
      </div>
    </div>



{% endblock %}





", "home_page/index.html.twig", "/Users/HoudaBerrada/Sites/advise_hack/templates/home_page/index.html.twig");
    }
}
