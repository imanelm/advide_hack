<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* messages/messages.html.twig */
class __TwigTemplate_0e8c693864181abeb1d77095c3598e13598d3601947c8d40a63148b47ccb9eb5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "messages/messages.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "messages/messages.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "messages/messages.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <div class=\"card bg-transparent border-0 align-item-center  text-center\" style = \"margin-top: 120px;\">
        <div class=\"bs-example\">
\t\t\t<div class=\"accordion\" id=\"accordionExample\">
\t\t\t\t";
        // line 8
        if ((0 !== twig_compare(twig_length_filter($this->env, (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new RuntimeError('Variable "messages" does not exist.', 8, $this->source); })())), 0))) {
            // line 9
            echo "        \t\t    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new RuntimeError('Variable "messages" does not exist.', 9, $this->source); })())) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 10
                echo "\t\t\t\t\t\t<div class=\"card border-0\">
\t\t\t\t\t\t\t<div class=\"row d-flex justify-content-center\">
\t\t\t\t\t\t\t    <div class=\"card card-overlay bg-transparent border-0 rounded-0 shadow\" style=\" margin-bottom:20px; width:60%; padding:10px;\">
\t\t\t\t\t\t\t    \t<div class=\"row align-items-center\">
\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t      \t\t\t\t<div class=\"card-header border-0 bg-transparent\" id=\"#heading";
                // line 16
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h3>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a type=\"button border-0\" data-toggle=\"collapse\" data-target=\"#collapse";
                // line 18
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 19
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new RuntimeError('Variable "messages" does not exist.', 19, $this->source); })()), $context["i"], [], "array", false, false, false, 19), "username", [], "array", false, false, false, 19), "html", null, true);
                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</h3>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"collapse";
                // line 23
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\" class=\"collapse\" aria-labelledby=\"heading";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\" data-parent=\"#accordionExample\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"";
                // line 24
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new RuntimeError('Variable "messages" does not exist.', 24, $this->source); })()), $context["i"], [], "array", false, false, false, 24), "username", [], "array", false, false, false, 24), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 25
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new RuntimeError('Variable "messages" does not exist.', 25, $this->source); })()), $context["i"], [], "array", false, false, false, 25), "messages", [], "array", false, false, false, 25)) - 1)));
                foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
                    // line 26
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"msgc\" style=\"margin-bottom: 30px;\"> 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"msg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 28
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new RuntimeError('Variable "messages" does not exist.', 28, $this->source); })()), $context["i"], [], "array", false, false, false, 28), "messages", [], "array", false, false, false, 28), $context["j"], [], "array", false, false, false, 28), "html", null, true);
                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 31
                echo "\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"msgc\" style=\"margin-bottom: 30px;\"> 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"bottom\"><input type=\"text\" name=\"msginput\" class=\"msginput\" id=\"message_to_";
                // line 34
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new RuntimeError('Variable "messages" does not exist.', 34, $this->source); })()), $context["i"], [], "array", false, false, false, 34), "username", [], "array", false, false, false, 34), "html", null, true);
                echo "\" onkeydown=\"if (event.keyCode == 13) sendResponseTo(";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new RuntimeError('Variable "messages" does not exist.', 34, $this->source); })()), $context["i"], [], "array", false, false, false, 34), "username", [], "array", false, false, false, 34), "html", null, true);
                echo ");\" value=\"\" placeholder=\"Enter your message here ... (Press enter to send message)\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t    </div>
\t\t\t\t    \t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "        \t\t";
        }
        // line 45
        echo "\t\t\t</div>
\t\t</div>
    </div>

    <script>
    \$(document).ready(function(){
\t\t\$(\"#messages\").accordion();
    });



\tfunction sendResponseTo(user)
\t{
\t\tvar responseTo = user.id;
\t\tvar message = \$(\"#message_to_\"+user.id ).val();

\t\tif (message != \"\") {

\t\t\tvar xmlhttp=new XMLHttpRequest();

\t\t\txmlhttp.onreadystatechange=function() {
\t\t\t\tif (xmlhttp.readyState==4 && xmlhttp.status==200) {
\t\t\t\t\tmessage = escapehtml(message)
\t\t\t\t}
\t\t\t}
\t\t    xmlhttp.open(\"GET\",\"/response?response_to=\" + responseTo + \"&message=\" + message,true);
\t\t    xmlhttp.send();
  \t\t}
\t\t\$(\"#message_to_\"+user.id).val(\"\");
\t}

    function updateMessages()
\t{
\t  \tvar xmlhttp=new XMLHttpRequest();
\t\tvar sender = \"\" ;
\t\tvar output = \"\";
\t\txmlhttp.onreadystatechange=function() {
\t\t\tif (xmlhttp.readyState==4 && xmlhttp.status==200) {
\t\t\t\tvar response = xmlhttp.responseText.split(\"\\n\")
\t\t\t\tvar rl = response.length;
\t\t\t\tvar item = \"\";
\t\t\t\tvar outputs = {};
\t\t\t\tfor (var i = 0; i < rl; i++) {
\t\t\t\t\titem = response[i].split(\"\\\\\");
\t\t\t\t\tif (item[1] != undefined) {
\t\t\t\t\t\tif(item[2] == \"\"){
\t\t\t\t\t\t\toutputs[\"\\\"\"+item[0]+\"\\\"\"] += \"<div class=\\\"msgc\\\" style=\\\"margin-bottom: 30px;\\\">\"+item[1]+\"</div>\"; 
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\toutputs[\"\\\"\"+item[2]+\"\\\"\"] += \"<div class=\\\"msgc\\\" style=\\\"margin-bottom: 30px;\\\"><b>\"+item[1]+\"</b></div>\"; 
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\tif(item[2] == \"\"){
\t\t\t\t\t\t\$(\"#\"+item[0]).html(outputs[\"\\\"\"+item[0]+\"\\\"\"]);
\t\t\t\t\t}else{
\t\t\t\t\t\t\$(\"#\"+item[2]).html(outputs[\"\\\"\"+item[2]+\"\\\"\"]);
\t\t\t\t\t}
\t\t\t\t}
\t\t\t}
\t\t}
\t    xmlhttp.open(\"GET\",\"/get_message?username=admin\" ,true);
\t    xmlhttp.send();
\t}

\tsetInterval(function(){ updateMessages(); }, 500);

    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "messages/messages.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 45,  156 => 44,  138 => 34,  133 => 31,  123 => 28,  119 => 26,  115 => 25,  111 => 24,  105 => 23,  98 => 19,  94 => 18,  89 => 16,  81 => 10,  76 => 9,  74 => 8,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}

{% block body %}

    <div class=\"card bg-transparent border-0 align-item-center  text-center\" style = \"margin-top: 120px;\">
        <div class=\"bs-example\">
\t\t\t<div class=\"accordion\" id=\"accordionExample\">
\t\t\t\t{% if messages|length != 0 %}
        \t\t    {% for i in 0..messages|length-1 %}
\t\t\t\t\t\t<div class=\"card border-0\">
\t\t\t\t\t\t\t<div class=\"row d-flex justify-content-center\">
\t\t\t\t\t\t\t    <div class=\"card card-overlay bg-transparent border-0 rounded-0 shadow\" style=\" margin-bottom:20px; width:60%; padding:10px;\">
\t\t\t\t\t\t\t    \t<div class=\"row align-items-center\">
\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t      \t\t\t\t<div class=\"card-header border-0 bg-transparent\" id=\"#heading{{i}}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h3>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a type=\"button border-0\" data-toggle=\"collapse\" data-target=\"#collapse{{i}}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{{ messages[i]['username'] }}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</h3>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"collapse{{i}}\" class=\"collapse\" aria-labelledby=\"heading{{i}}\" data-parent=\"#accordionExample\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"{{messages[i]['username']}}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% for j in 0..messages[i]['messages']|length-1 %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"msgc\" style=\"margin-bottom: 30px;\"> 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"msg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{{messages[i]['messages'][j]}}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"msgc\" style=\"margin-bottom: 30px;\"> 
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"bottom\"><input type=\"text\" name=\"msginput\" class=\"msginput\" id=\"message_to_{{messages[i]['username']}}\" onkeydown=\"if (event.keyCode == 13) sendResponseTo({{messages[i]['username']}});\" value=\"\" placeholder=\"Enter your message here ... (Press enter to send message)\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t    </div>
\t\t\t\t    \t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t{% endfor %}
        \t\t{% endif %}
\t\t\t</div>
\t\t</div>
    </div>

    <script>
    \$(document).ready(function(){
\t\t\$(\"#messages\").accordion();
    });



\tfunction sendResponseTo(user)
\t{
\t\tvar responseTo = user.id;
\t\tvar message = \$(\"#message_to_\"+user.id ).val();

\t\tif (message != \"\") {

\t\t\tvar xmlhttp=new XMLHttpRequest();

\t\t\txmlhttp.onreadystatechange=function() {
\t\t\t\tif (xmlhttp.readyState==4 && xmlhttp.status==200) {
\t\t\t\t\tmessage = escapehtml(message)
\t\t\t\t}
\t\t\t}
\t\t    xmlhttp.open(\"GET\",\"/response?response_to=\" + responseTo + \"&message=\" + message,true);
\t\t    xmlhttp.send();
  \t\t}
\t\t\$(\"#message_to_\"+user.id).val(\"\");
\t}

    function updateMessages()
\t{
\t  \tvar xmlhttp=new XMLHttpRequest();
\t\tvar sender = \"\" ;
\t\tvar output = \"\";
\t\txmlhttp.onreadystatechange=function() {
\t\t\tif (xmlhttp.readyState==4 && xmlhttp.status==200) {
\t\t\t\tvar response = xmlhttp.responseText.split(\"\\n\")
\t\t\t\tvar rl = response.length;
\t\t\t\tvar item = \"\";
\t\t\t\tvar outputs = {};
\t\t\t\tfor (var i = 0; i < rl; i++) {
\t\t\t\t\titem = response[i].split(\"\\\\\");
\t\t\t\t\tif (item[1] != undefined) {
\t\t\t\t\t\tif(item[2] == \"\"){
\t\t\t\t\t\t\toutputs[\"\\\"\"+item[0]+\"\\\"\"] += \"<div class=\\\"msgc\\\" style=\\\"margin-bottom: 30px;\\\">\"+item[1]+\"</div>\"; 
\t\t\t\t\t\t}else{
\t\t\t\t\t\t\toutputs[\"\\\"\"+item[2]+\"\\\"\"] += \"<div class=\\\"msgc\\\" style=\\\"margin-bottom: 30px;\\\"><b>\"+item[1]+\"</b></div>\"; 
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t\tif(item[2] == \"\"){
\t\t\t\t\t\t\$(\"#\"+item[0]).html(outputs[\"\\\"\"+item[0]+\"\\\"\"]);
\t\t\t\t\t}else{
\t\t\t\t\t\t\$(\"#\"+item[2]).html(outputs[\"\\\"\"+item[2]+\"\\\"\"]);
\t\t\t\t\t}
\t\t\t\t}
\t\t\t}
\t\t}
\t    xmlhttp.open(\"GET\",\"/get_message?username=admin\" ,true);
\t    xmlhttp.send();
\t}

\tsetInterval(function(){ updateMessages(); }, 500);

    </script>
{% endblock %}", "messages/messages.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\messages\\messages.html.twig");
    }
}
