<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* offer/ent_offers.html.twig */
class __TwigTemplate_aa7eaddebe3c0daf3b1d1dd7e345d2a8cedcd68d9c216d3aaf0cec6da97709a0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/ent_offers.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/ent_offers.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "offer/ent_offers.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "\t
\t\t<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t\t    ";
        // line 8
        if ((0 !== twig_compare(twig_length_filter($this->env, (isset($context["ent_offers"]) || array_key_exists("ent_offers", $context) ? $context["ent_offers"] : (function () { throw new RuntimeError('Variable "ent_offers" does not exist.', 8, $this->source); })())), 0))) {
            // line 9
            echo "\t\t\t\t    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["ent_offers"]) || array_key_exists("ent_offers", $context) ? $context["ent_offers"] : (function () { throw new RuntimeError('Variable "ent_offers" does not exist.', 9, $this->source); })())) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 10
                echo "
\t\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t\t        \t\t\t<div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t    \t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t      \t\t<a href=\"";
                // line 18
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_offers"]) || array_key_exists("ent_offers", $context) ? $context["ent_offers"] : (function () { throw new RuntimeError('Variable "ent_offers" does not exist.', 18, $this->source); })()), $context["i"], [], "array", false, false, false, 18), "id_offer", [], "array", false, false, false, 18)]), "html", null, true);
                echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t<h4>";
                // line 19
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_offers"]) || array_key_exists("ent_offers", $context) ? $context["ent_offers"] : (function () { throw new RuntimeError('Variable "ent_offers" does not exist.', 19, $this->source); })()), $context["i"], [], "array", false, false, false, 19), "title", [], "array", false, false, false, 19), "html", null, true);
                echo "</h4>
\t\t\t\t\t\t\t\t\t      \t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t<img src=\"/Images/";
                // line 21
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_offers"]) || array_key_exists("ent_offers", $context) ? $context["ent_offers"] : (function () { throw new RuntimeError('Variable "ent_offers" does not exist.', 21, $this->source); })()), $context["i"], [], "array", false, false, false, 21), "photo", [], "array", false, false, false, 21), "html", null, true);
                echo "\" height=\"100\" alt=\"\" class=\"rounded\" style=\"margin-left:20px;\" />
\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t    </div>

\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t     \t\t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t      \t\t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t<p>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<h5 style=\"color:#9D151C; margin-right:5px;\">Type</h5>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div style=\"color:black;\">";
                // line 35
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_offers"]) || array_key_exists("ent_offers", $context) ? $context["ent_offers"] : (function () { throw new RuntimeError('Variable "ent_offers" does not exist.', 35, $this->source); })()), $context["i"], [], "array", false, false, false, 35), "n_type", [], "array", false, false, false, 35), "html", null, true);
                echo "</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<h5 style=\"color:#9D151C;  margin-right:5px;\">Summary</h5>
\t\t\t\t\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<div style=\"color:black;\">";
                // line 43
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_offers"]) || array_key_exists("ent_offers", $context) ? $context["ent_offers"] : (function () { throw new RuntimeError('Variable "ent_offers" does not exist.', 43, $this->source); })()), $context["i"], [], "array", false, false, false, 43), "description1", [], "array", false, false, false, 43), "html", null, true);
                echo "</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">\t
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t<h5 style=\"color:#9D151C;  margin-right:5px;\">State</h5>
\t\t\t\t\t\t\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t";
                // line 51
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_offers"]) || array_key_exists("ent_offers", $context) ? $context["ent_offers"] : (function () { throw new RuntimeError('Variable "ent_offers" does not exist.', 51, $this->source); })()), $context["i"], [], "array", false, false, false, 51), "state", [], "array", false, false, false, 51), 1))) {
                    // line 52
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-success active\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_offers"]) || array_key_exists("ent_offers", $context) ? $context["ent_offers"] : (function () { throw new RuntimeError('Variable "ent_offers" does not exist.', 52, $this->source); })()), $context["i"], [], "array", false, false, false, 52), "n_state", [], "array", false, false, false, 52), "html", null, true);
                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t";
                } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 53
(isset($context["ent_offers"]) || array_key_exists("ent_offers", $context) ? $context["ent_offers"] : (function () { throw new RuntimeError('Variable "ent_offers" does not exist.', 53, $this->source); })()), $context["i"], [], "array", false, false, false, 53), "state", [], "array", false, false, false, 53), 2))) {
                    // line 54
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-warning active text-white\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_offers"]) || array_key_exists("ent_offers", $context) ? $context["ent_offers"] : (function () { throw new RuntimeError('Variable "ent_offers" does not exist.', 54, $this->source); })()), $context["i"], [], "array", false, false, false, 54), "n_state", [], "array", false, false, false, 54), "html", null, true);
                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t";
                } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 55
(isset($context["ent_offers"]) || array_key_exists("ent_offers", $context) ? $context["ent_offers"] : (function () { throw new RuntimeError('Variable "ent_offers" does not exist.', 55, $this->source); })()), $context["i"], [], "array", false, false, false, 55), "state", [], "array", false, false, false, 55), 3))) {
                    // line 56
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-danger active\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ent_offers"]) || array_key_exists("ent_offers", $context) ? $context["ent_offers"] : (function () { throw new RuntimeError('Variable "ent_offers" does not exist.', 56, $this->source); })()), $context["i"], [], "array", false, false, false, 56), "n_state", [], "array", false, false, false, 56), "html", null, true);
                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t";
                }
                // line 58
                echo "\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t</p>
\t\t\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t\t\t\t   \t</div>\t
\t\t\t\t\t\t\t\t\t</div>\t

\t\t\t\t\t\t\t      \t\t
\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t</div>
\t\t\t\t\t   </div>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo " 
\t\t\t";
        }
        // line 72
        echo "\t\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "offer/ent_offers.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 72,  178 => 70,  160 => 58,  154 => 56,  152 => 55,  147 => 54,  145 => 53,  140 => 52,  138 => 51,  127 => 43,  116 => 35,  99 => 21,  94 => 19,  90 => 18,  80 => 10,  75 => 9,  73 => 8,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}
\t
\t\t<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t\t    {% if ent_offers|length != 0 %}
\t\t\t\t    {% for i in 0..ent_offers|length-1 %}

\t\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t\t        \t\t\t<div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t    \t\t\t\t\t\t\t<div class=\"col-4\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t      \t\t<a href=\"{{ url('offer_show', {'id' : ent_offers[i]['id_offer']}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t<h4>{{ ent_offers[i]['title']}}</h4>
\t\t\t\t\t\t\t\t\t      \t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t<img src=\"/Images/{{ ent_offers[i]['photo']}}\" height=\"100\" alt=\"\" class=\"rounded\" style=\"margin-left:20px;\" />
\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t    </div>

\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t     \t\t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t      \t\t<div class=\"card-body\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t<p>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<h5 style=\"color:#9D151C; margin-right:5px;\">Type</h5>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div style=\"color:black;\">{{ ent_offers[i]['n_type']}}</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<h5 style=\"color:#9D151C;  margin-right:5px;\">Summary</h5>
\t\t\t\t\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t<div style=\"color:black;\">{{ ent_offers[i]['description1']}}</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t<div class=\"col\" style=\"margin-right:-100px;\">\t
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t<h5 style=\"color:#9D151C;  margin-right:5px;\">State</h5>
\t\t\t\t\t\t\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t{% if ent_offers[i]['state'] == 1 %}
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-success active\">{{ ent_offers[i]['n_state']}}</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t{% elseif ent_offers[i]['state'] == 2 %}
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-warning active text-white\">{{ ent_offers[i]['n_state']}}</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t{% elseif ent_offers[i]['state'] == 3 %}
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-danger active\">{{ ent_offers[i]['n_state']}}</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t</p>
\t\t\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t\t\t\t   \t</div>\t
\t\t\t\t\t\t\t\t\t</div>\t

\t\t\t\t\t\t\t      \t\t
\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t</div>
\t\t\t\t\t   </div>
\t\t\t\t\t{% endfor %} 
\t\t\t{% endif %}
\t\t</div>
{% endblock %}", "offer/ent_offers.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\offer\\ent_offers.html.twig");
    }
}
