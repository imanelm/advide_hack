<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* hacker/my_notifications.html.twig */
class __TwigTemplate_921f447ec5647c72005244acc36b56c4f1237a1f7a0ee29c60a57dc7c00f9af9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/my_notifications.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/my_notifications.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "hacker/my_notifications.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "\t
\t\t<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t\t    ";
        // line 8
        if ((0 !== twig_compare(twig_length_filter($this->env, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 8, $this->source); })())), 0))) {
            // line 9
            echo "\t\t\t\t    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 9, $this->source); })())) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 10
                echo "\t\t\t\t    \t";
                if (((((((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 4)) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 5))) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 6))) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 11))) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 12))) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 13)))) {
                    // line 11
                    echo "\t\t\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t\t\t        \t\t\t<div class=\"card card-overlay bg-transparent border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t\t    \t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t      \t\t";
                    // line 18
                    if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 18, $this->source); })()), $context["i"], [], "array", false, false, false, 18), "typeNotif", [], "any", false, false, false, 18), 4))) {
                        // line 19
                        echo "\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprise_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 19, $this->source); })()), $context["i"], [], "array", false, false, false, 19), "idEntreprise", [], "any", false, false, false, 19), "idEntreprise", [], "any", false, false, false, 19)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 20
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 20, $this->source); })()), $context["i"], [], "array", false, false, false, 20), "idEntreprise", [], "any", false, false, false, 20), "nameEnt", [], "any", false, false, false, 20), "html", null, true);
                        echo " 
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t\t has accepted your request for report submission for  
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 23
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 23, $this->source); })()), $context["i"], [], "array", false, false, false, 23), "idOffer", [], "any", false, false, false, 23), "idOffer", [], "any", false, false, false, 23)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 24
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 24, $this->source); })()), $context["i"], [], "array", false, false, false, 24), "idOffer", [], "any", false, false, false, 24), "title", [], "any", false, false, false, 24), "html", null, true);
                        echo ".
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 32
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("sub_report", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 32, $this->source); })()), $context["i"], [], "array", false, false, false, 32), "idOffer", [], "any", false, false, false, 32), "idOffer", [], "any", false, false, false, 32)]), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Submit report</div>
\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    \t\t\t";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 40
(isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 40, $this->source); })()), $context["i"], [], "array", false, false, false, 40), "typeNotif", [], "any", false, false, false, 40), 5))) {
                        // line 41
                        echo "\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprise_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 41, $this->source); })()), $context["i"], [], "array", false, false, false, 41), "idEntreprise", [], "any", false, false, false, 41), "idEntreprise", [], "any", false, false, false, 41)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 42
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 42, $this->source); })()), $context["i"], [], "array", false, false, false, 42), "idEntreprise", [], "any", false, false, false, 42), "nameEnt", [], "any", false, false, false, 42), "html", null, true);
                        echo " 
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t has rejected your request for report submission for  
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 45
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 45, $this->source); })()), $context["i"], [], "array", false, false, false, 45), "idOffer", [], "any", false, false, false, 45), "idOffer", [], "any", false, false, false, 45)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 46
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 46, $this->source); })()), $context["i"], [], "array", false, false, false, 46), "idOffer", [], "any", false, false, false, 46), "title", [], "any", false, false, false, 46), "html", null, true);
                        echo ".
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    \t\t";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 51
(isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 51, $this->source); })()), $context["i"], [], "array", false, false, false, 51), "typeNotif", [], "any", false, false, false, 51), 6))) {
                        // line 52
                        echo "\t\t\t\t\t\t\t      \t\t\t\t\t<div>Congratulations ! You've been admitted into Advise'Hack ! </div>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    ";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 56
(isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 56, $this->source); })()), $context["i"], [], "array", false, false, false, 56), "typeNotif", [], "any", false, false, false, 56), 11))) {
                        // line 57
                        echo "\t\t\t\t\t\t\t\t\t      \t\t\t<div>We are sorry to let you know that you have not been admitted into Advise'Hack. </div>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    ";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 61
(isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 61, $this->source); })()), $context["i"], [], "array", false, false, false, 61), "typeNotif", [], "any", false, false, false, 61), 12))) {
                        // line 62
                        echo "\t\t\t\t\t\t\t\t\t    \t\t\tGreat job ! 
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 63
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprise_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 63, $this->source); })()), $context["i"], [], "array", false, false, false, 63), "idOffer", [], "any", false, false, false, 63), "idEntreprise", [], "any", false, false, false, 63), "idEntreprise", [], "any", false, false, false, 63)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 64
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 64, $this->source); })()), $context["i"], [], "array", false, false, false, 64), "idOffer", [], "any", false, false, false, 64), "idEntreprise", [], "any", false, false, false, 64), "nameEnt", [], "any", false, false, false, 64), "html", null, true);
                        echo " 
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t has accepted your report for  
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 67
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 67, $this->source); })()), $context["i"], [], "array", false, false, false, 67), "idOffer", [], "any", false, false, false, 67), "idOffer", [], "any", false, false, false, 67)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 68
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 68, $this->source); })()), $context["i"], [], "array", false, false, false, 68), "idOffer", [], "any", false, false, false, 68), "title", [], "any", false, false, false, 68), "html", null, true);
                        echo ".
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    ";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 73
(isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 73, $this->source); })()), $context["i"], [], "array", false, false, false, 73), "typeNotif", [], "any", false, false, false, 73), 13))) {
                        // line 74
                        echo "\t\t\t\t\t\t\t\t\t    \t\t\tWe regret to inform you that 
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 75
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprise_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 75, $this->source); })()), $context["i"], [], "array", false, false, false, 75), "idOffer", [], "any", false, false, false, 75), "idEntreprise", [], "any", false, false, false, 75), "idEntreprise", [], "any", false, false, false, 75)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 76
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 76, $this->source); })()), $context["i"], [], "array", false, false, false, 76), "idOffer", [], "any", false, false, false, 76), "idEntreprise", [], "any", false, false, false, 76), "nameEnt", [], "any", false, false, false, 76), "html", null, true);
                        echo " 
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t has rejected your report for  
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 79
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 79, $this->source); })()), $context["i"], [], "array", false, false, false, 79), "idOffer", [], "any", false, false, false, 79), "idOffer", [], "any", false, false, false, 79)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 80
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 80, $this->source); })()), $context["i"], [], "array", false, false, false, 80), "idOffer", [], "any", false, false, false, 80), "title", [], "any", false, false, false, 80), "html", null, true);
                        echo ".
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t   \t\t\t\t";
                    }
                    // line 86
                    echo "\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t      \t\t";
                    // line 89
                    if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 89, $this->source); })()), $context["i"], [], "array", false, false, false, 89), "notifRead", [], "any", false, false, false, 89), 0))) {
                        // line 90
                        echo "\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("mark_as_read_ha", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 90, $this->source); })()), $context["i"], [], "array", false, false, false, 90), "idNotif", [], "any", false, false, false, 90)]), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Mark as read</div>
\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t\t";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 95
(isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 95, $this->source); })()), $context["i"], [], "array", false, false, false, 95), "notifRead", [], "any", false, false, false, 95), 1))) {
                        // line 96
                        echo "\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("mark_as_read_ha", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 96, $this->source); })()), $context["i"], [], "array", false, false, false, 96), "idNotif", [], "any", false, false, false, 96)]), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Mark as unread</div>
\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t\t";
                    }
                    // line 102
                    echo "\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>\t
\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                    // line 108
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("del_notif_ha", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["ha_notifications"]) || array_key_exists("ha_notifications", $context) ? $context["ha_notifications"] : (function () { throw new RuntimeError('Variable "ha_notifications" does not exist.', 108, $this->source); })()), $context["i"], [], "array", false, false, false, 108), "idNotif", [], "any", false, false, false, 108)]), "html", null, true);
                    echo "\">
\t\t\t\t\t\t\t\t\t\t              \t<svg width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-x-circle\" style=\"float:right;margin-right:20px;\" fill=\"#9D151C\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>

\t\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t   </div>
\t\t\t\t\t   ";
                }
                // line 124
                echo "\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " 
\t\t\t";
        }
        // line 126
        echo "\t\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "hacker/my_notifications.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  291 => 126,  282 => 124,  263 => 108,  255 => 102,  245 => 96,  243 => 95,  234 => 90,  232 => 89,  227 => 86,  218 => 80,  214 => 79,  208 => 76,  204 => 75,  201 => 74,  199 => 73,  191 => 68,  187 => 67,  181 => 64,  177 => 63,  174 => 62,  172 => 61,  166 => 57,  164 => 56,  158 => 52,  156 => 51,  148 => 46,  144 => 45,  138 => 42,  133 => 41,  131 => 40,  120 => 32,  109 => 24,  105 => 23,  99 => 20,  94 => 19,  92 => 18,  83 => 11,  80 => 10,  75 => 9,  73 => 8,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}
\t
\t\t<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t\t    {% if ha_notifications|length != 0 %}
\t\t\t\t    {% for i in 0..ha_notifications|length-1 %}
\t\t\t\t    \t{% if ha_notifications[i].typeNotif == 4 or ha_notifications[i].typeNotif == 5 or ha_notifications[i].typeNotif == 6 or ha_notifications[i].typeNotif == 11 or ha_notifications[i].typeNotif == 12 or ha_notifications[i].typeNotif == 13 %}
\t\t\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t\t\t        \t\t\t<div class=\"card card-overlay bg-transparent border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t\t    \t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t      \t\t{% if ha_notifications[i].typeNotif == 4 %}
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('entreprise_show', {'id' : ha_notifications[i].idEntreprise.idEntreprise}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ha_notifications[i].idEntreprise.nameEnt }} 
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t\t has accepted your request for report submission for  
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('offer_show', {'id' : ha_notifications[i].idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ha_notifications[i].idOffer.title }}.
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{url('sub_report', {'id' : ha_notifications[i].idOffer.idOffer}) }}\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Submit report</div>
\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    \t\t\t{% elseif ha_notifications[i].typeNotif == 5 %}
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('entreprise_show', {'id' : ha_notifications[i].idEntreprise.idEntreprise}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ha_notifications[i].idEntreprise.nameEnt }} 
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t has rejected your request for report submission for  
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('offer_show', {'id' : ha_notifications[i].idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ha_notifications[i].idOffer.title }}.
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    \t\t{% elseif ha_notifications[i].typeNotif == 6 %}
\t\t\t\t\t\t\t      \t\t\t\t\t<div>Congratulations ! You've been admitted into Advise'Hack ! </div>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    {% elseif ha_notifications[i].typeNotif == 11 %}
\t\t\t\t\t\t\t\t\t      \t\t\t<div>We are sorry to let you know that you have not been admitted into Advise'Hack. </div>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    {% elseif ha_notifications[i].typeNotif == 12 %}
\t\t\t\t\t\t\t\t\t    \t\t\tGreat job ! 
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('entreprise_show', {'id' : ha_notifications[i].idOffer.idEntreprise.idEntreprise}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ha_notifications[i].idOffer.idEntreprise.nameEnt }} 
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t has accepted your report for  
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('offer_show', {'id' : ha_notifications[i].idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ha_notifications[i].idOffer.title }}.
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    {% elseif ha_notifications[i].typeNotif == 13 %}
\t\t\t\t\t\t\t\t\t    \t\t\tWe regret to inform you that 
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('entreprise_show', {'id' : ha_notifications[i].idOffer.idEntreprise.idEntreprise}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ha_notifications[i].idOffer.idEntreprise.nameEnt }} 
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t has rejected your report for  
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('offer_show', {'id' : ha_notifications[i].idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ ha_notifications[i].idOffer.title }}.
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t   \t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t\t      \t\t{% if ha_notifications[i].notifRead == 0 %}
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{url('mark_as_read_ha', {'id' : ha_notifications[i].idNotif}) }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Mark as read</div>
\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t\t{% elseif ha_notifications[i].notifRead == 1 %}
\t\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{url('mark_as_read_ha', {'id' : ha_notifications[i].idNotif}) }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Mark as unread</div>
\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>\t
\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{url('del_notif_ha', {'id' : ha_notifications[i].idNotif}) }}\">
\t\t\t\t\t\t\t\t\t\t              \t<svg width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-x-circle\" style=\"float:right;margin-right:20px;\" fill=\"#9D151C\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>

\t\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t   </div>
\t\t\t\t\t   {% endif %}
\t\t\t\t\t{% endfor %} 
\t\t\t{% endif %}
\t\t</div>
{% endblock %}", "hacker/my_notifications.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\hacker\\my_notifications.html.twig");
    }
}
