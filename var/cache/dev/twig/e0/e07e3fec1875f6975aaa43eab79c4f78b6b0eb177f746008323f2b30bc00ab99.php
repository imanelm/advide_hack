<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* entreprise/index.html.twig */
class __TwigTemplate_509259c6c0fcd8ac0a732868a9cfca512d71c31e6a549144dd9349e48321b60f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "entreprise/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "entreprise/index.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "entreprise/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
\t<h2 class=\"text-center\" style=\"color:#9D151C; margin-top:110px; margin-bottom:38px;\">They trusted Us</h2>

\t<div class=\"card bg-transparent text-white border-0 align-item-center text-center\" style = \"margin-bottom:30px\">

\t    ";
        // line 10
        if ((0 !== twig_compare(twig_length_filter($this->env, (isset($context["entreprises"]) || array_key_exists("entreprises", $context) ? $context["entreprises"] : (function () { throw new RuntimeError('Variable "entreprises" does not exist.', 10, $this->source); })())), 0))) {
            // line 11
            echo "\t    \t";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 12
                echo "\t\t\t    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["entreprises"]) || array_key_exists("entreprises", $context) ? $context["entreprises"] : (function () { throw new RuntimeError('Variable "entreprises" does not exist.', 12, $this->source); })())) - 1)));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 13
                    echo "\t\t\t    \t<div class=\"row d-flex justify-content-center\">
\t\t\t\t\t    <div class=\"card card-overlay bg-transparent border-0 shadow\" style=\" margin-bottom:20px; width:60%; padding:10px;\">
\t\t\t\t\t    \t<div class=\"row align-items-center\">
    \t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t      \t\t<img src=\"/Images/hackersphotos/";
                    // line 17
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["entreprises"]) || array_key_exists("entreprises", $context) ? $context["entreprises"] : (function () { throw new RuntimeError('Variable "entreprises" does not exist.', 17, $this->source); })()), $context["i"], [], "array", false, false, false, 17), "photo", [], "array", false, false, false, 17), "html", null, true);
                    echo "\" height=\"100\" alt=\"\" class=\"rounded\" style=\"margin-left:20px; margin-right:20px;\" />
\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t    \t\t<h4 class=\"card-body\">
\t\t\t\t\t      \t\t\t\t\t<a href=\"";
                    // line 22
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprise_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["entreprises"]) || array_key_exists("entreprises", $context) ? $context["entreprises"] : (function () { throw new RuntimeError('Variable "entreprises" does not exist.', 22, $this->source); })()), $context["i"], [], "array", false, false, false, 22), "id_entreprise", [], "array", false, false, false, 22)]), "html", null, true);
                    echo "\" style=\"color:#000000;\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["entreprises"]) || array_key_exists("entreprises", $context) ? $context["entreprises"] : (function () { throw new RuntimeError('Variable "entreprises" does not exist.', 22, $this->source); })()), $context["i"], [], "array", false, false, false, 22), "name_ent", [], "array", false, false, false, 22), "html", null, true);
                    echo "</a>
\t\t\t\t      \t\t\t\t\t</h4>
\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t    </div>
\t\t\t\t\t      \t</div>
\t\t\t\t\t    </div>
\t\t    \t\t</div>
\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 29
                echo "   
\t\t\t";
            } else {
                // line 31
                echo "\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["entreprises"]) || array_key_exists("entreprises", $context) ? $context["entreprises"] : (function () { throw new RuntimeError('Variable "entreprises" does not exist.', 31, $this->source); })())) - 1)));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 32
                    echo "\t\t\t\t\t";
                    if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["entreprises"]) || array_key_exists("entreprises", $context) ? $context["entreprises"] : (function () { throw new RuntimeError('Variable "entreprises" does not exist.', 32, $this->source); })()), $context["i"], [], "array", false, false, false, 32), "showAuth", [], "array", false, false, false, 32), "1"))) {
                        // line 33
                        echo "\t\t\t\t    \t<div class=\"row d-flex justify-content-center\">
\t\t\t\t\t\t    <div class=\"card card-overlay bg-transparent border-0 shadow\" style=\" margin-bottom:20px; width:60%; padding:10px;\">
\t\t\t\t\t\t    \t<div class=\"row align-items-center\">
\t    \t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t      \t\t<img src=\"/Images/hackersphotos/";
                        // line 37
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["entreprises"]) || array_key_exists("entreprises", $context) ? $context["entreprises"] : (function () { throw new RuntimeError('Variable "entreprises" does not exist.', 37, $this->source); })()), $context["i"], [], "array", false, false, false, 37), "photo", [], "array", false, false, false, 37), "html", null, true);
                        echo "\" height=\"100\" alt=\"\" class=\"rounded\" style=\"margin-left:20px; margin-right:20px;\" />
\t\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t    \t\t<h4 class=\"card-body\">
\t\t\t\t\t\t      \t\t\t\t\t<a href=\"";
                        // line 42
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprise_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["entreprises"]) || array_key_exists("entreprises", $context) ? $context["entreprises"] : (function () { throw new RuntimeError('Variable "entreprises" does not exist.', 42, $this->source); })()), $context["i"], [], "array", false, false, false, 42), "id_entreprise", [], "array", false, false, false, 42)]), "html", null, true);
                        echo "\" style=\"color:#000000;\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["entreprises"]) || array_key_exists("entreprises", $context) ? $context["entreprises"] : (function () { throw new RuntimeError('Variable "entreprises" does not exist.', 42, $this->source); })()), $context["i"], [], "array", false, false, false, 42), "name_ent", [], "array", false, false, false, 42), "html", null, true);
                        echo "</a>
\t\t\t\t\t      \t\t\t\t\t</h4>
\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t    </div>
\t\t\t    \t\t</div>
\t\t\t\t    ";
                    }
                    // line 50
                    echo "\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 51
                echo "\t\t\t";
            }
            // line 52
            echo "\t\t";
        }
        // line 53
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "entreprise/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 53,  163 => 52,  160 => 51,  154 => 50,  141 => 42,  133 => 37,  127 => 33,  124 => 32,  119 => 31,  115 => 29,  99 => 22,  91 => 17,  85 => 13,  80 => 12,  77 => 11,  75 => 10,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}

\t<h2 class=\"text-center\" style=\"color:#9D151C; margin-top:110px; margin-bottom:38px;\">They trusted Us</h2>

\t<div class=\"card bg-transparent text-white border-0 align-item-center text-center\" style = \"margin-bottom:30px\">

\t    {% if entreprises|length != 0 %}
\t    \t{% if is_granted('ROLE_ADMIN') %}
\t\t\t    {% for i in 0..entreprises|length-1 %}
\t\t\t    \t<div class=\"row d-flex justify-content-center\">
\t\t\t\t\t    <div class=\"card card-overlay bg-transparent border-0 shadow\" style=\" margin-bottom:20px; width:60%; padding:10px;\">
\t\t\t\t\t    \t<div class=\"row align-items-center\">
    \t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t      \t\t<img src=\"/Images/hackersphotos/{{ entreprises[i]['photo'] }}\" height=\"100\" alt=\"\" class=\"rounded\" style=\"margin-left:20px; margin-right:20px;\" />
\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t    \t\t<h4 class=\"card-body\">
\t\t\t\t\t      \t\t\t\t\t<a href=\"{{ url('entreprise_show', {'id' : entreprises[i]['id_entreprise']}) }}\" style=\"color:#000000;\">{{ entreprises[i]['name_ent'] }}</a>
\t\t\t\t      \t\t\t\t\t</h4>
\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t    </div>
\t\t\t\t\t      \t</div>
\t\t\t\t\t    </div>
\t\t    \t\t</div>
\t\t\t\t{% endfor %}   
\t\t\t{% else %}
\t\t\t\t{% for i in 0..entreprises|length-1 %}
\t\t\t\t\t{% if entreprises[i]['showAuth'] == \"1\" %}
\t\t\t\t    \t<div class=\"row d-flex justify-content-center\">
\t\t\t\t\t\t    <div class=\"card card-overlay bg-transparent border-0 shadow\" style=\" margin-bottom:20px; width:60%; padding:10px;\">
\t\t\t\t\t\t    \t<div class=\"row align-items-center\">
\t    \t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t      \t\t<img src=\"/Images/hackersphotos/{{ entreprises[i]['photo'] }}\" height=\"100\" alt=\"\" class=\"rounded\" style=\"margin-left:20px; margin-right:20px;\" />
\t\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t    \t\t<h4 class=\"card-body\">
\t\t\t\t\t\t      \t\t\t\t\t<a href=\"{{ url('entreprise_show', {'id' : entreprises[i]['id_entreprise']}) }}\" style=\"color:#000000;\">{{ entreprises[i]['name_ent'] }}</a>
\t\t\t\t\t      \t\t\t\t\t</h4>
\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t    </div>
\t\t\t    \t\t</div>
\t\t\t\t    {% endif %}
\t\t\t\t{% endfor %}
\t\t\t{% endif %}
\t\t{% endif %}

{% endblock %}
", "entreprise/index.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\entreprise\\index.html.twig");
    }
}
