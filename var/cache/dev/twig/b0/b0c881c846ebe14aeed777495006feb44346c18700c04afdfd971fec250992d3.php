<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* hacker/notifications_admin.html.twig */
class __TwigTemplate_a757570c1a4851386735e4009f308c559709d527a30f99eefb544841e854600c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/notifications_admin.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/notifications_admin.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "hacker/notifications_admin.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "\t
<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t";
        // line 8
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN") && (0 !== twig_compare(twig_length_filter($this->env, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 8, $this->source); })())), 0)))) {
            // line 9
            echo "\t\t    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 9, $this->source); })())) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 10
                echo "\t\t    \t";
                if ((((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 7)) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 8))) || (((((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 4)) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 5))) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 12))) || (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "typeNotif", [], "any", false, false, false, 10), 13))) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 10, $this->source); })()), $context["i"], [], "array", false, false, false, 10), "idHacker", [], "any", false, false, false, 10), "idHacker", [], "any", false, false, false, 10), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 10, $this->source); })()), "user", [], "any", false, false, false, 10), "idHacker", [], "any", false, false, false, 10)))))) {
                    // line 11
                    echo "\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t        \t\t\t<div class=\"card card-overlay bg-transparent border-0 shadow\" style=\" margin-bottom:20px; width:60%\">
        \t\t\t\t\t<div class=\"row align-items-center\">
\t\t\t\t      \t\t\t";
                    // line 14
                    if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 14, $this->source); })()), $context["i"], [], "array", false, false, false, 14), "typeNotif", [], "any", false, false, false, 14), 7))) {
                        // line 15
                        echo "\t    \t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t      \t\t";
                        // line 18
                        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["notifications_admin"] ?? null), $context["i"], [], "array", false, true, false, 18), "idHacker", [], "any", true, true, false, 18)) {
                            // line 19
                            echo "\t\t\t\t\t\t\t\t\t      \t\t\t<div>
\t\t\t\t\t\t\t\t\t      \t\t\t\tWe have a new 
\t\t\t\t\t\t\t\t\t      \t\t\t\t<a href=\"";
                            // line 21
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("hacker_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 21, $this->source); })()), $context["i"], [], "array", false, false, false, 21), "idHacker", [], "any", false, false, false, 21), "idHacker", [], "any", false, false, false, 21)]), "html", null, true);
                            echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\thacker
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t      \t\t\t! 
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t      \t\t\t";
                        } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                         // line 26
($context["notifications_admin"] ?? null), $context["i"], [], "array", false, true, false, 26), "idEntreprise", [], "any", true, true, false, 26)) {
                            // line 27
                            echo "\t\t\t\t\t\t\t\t\t      \t\t\t<div>
\t\t\t\t\t\t\t\t\t      \t\t\t\tWe have a new enterprise ! 
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t      \t\t\t";
                        }
                        // line 31
                        echo "\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t   \t\t\t";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 34
(isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 34, $this->source); })()), $context["i"], [], "array", false, false, false, 34), "typeNotif", [], "any", false, false, false, 34), 8))) {
                        // line 35
                        echo "\t    \t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t      \t\t<a href=\"";
                        // line 38
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprise_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 38, $this->source); })()), $context["i"], [], "array", false, false, false, 38), "idEntreprise", [], "any", false, false, false, 38), "idEntreprise", [], "any", false, false, false, 38)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 39
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 39, $this->source); })()), $context["i"], [], "array", false, false, false, 39), "idEntreprise", [], "any", false, false, false, 39), "nameEnt", [], "any", false, false, false, 39), "html", null, true);
                        echo " 
\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t      \t\t\twants to publish a new
\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 42
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 42, $this->source); })()), $context["i"], [], "array", false, false, false, 42), "idOffer", [], "any", false, false, false, 42), "idOffer", [], "any", false, false, false, 42)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t      \t\t\t\toffer 
\t\t\t\t\t\t\t\t      \t\t\t</a>.
\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t   \t\t";
                    } elseif (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 48
(isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 48, $this->source); })()), $context["i"], [], "array", false, false, false, 48), "typeNotif", [], "any", false, false, false, 48), 4)) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 48, $this->source); })()), $context["i"], [], "array", false, false, false, 48), "idHacker", [], "any", false, false, false, 48), "idHacker", [], "any", false, false, false, 48), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 48, $this->source); })()), "user", [], "any", false, false, false, 48), "idHacker", [], "any", false, false, false, 48))))) {
                        // line 49
                        echo "\t\t\t\t\t\t   \t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t   \t\t\t<a href=\"";
                        // line 52
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprise_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 52, $this->source); })()), $context["i"], [], "array", false, false, false, 52), "idEntreprise", [], "any", false, false, false, 52), "idEntreprise", [], "any", false, false, false, 52)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 53
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 53, $this->source); })()), $context["i"], [], "array", false, false, false, 53), "idEntreprise", [], "any", false, false, false, 53), "nameEnt", [], "any", false, false, false, 53), "html", null, true);
                        echo " 
\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t      \t\t\t has accepted your request for report submission for  
\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 56
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 56, $this->source); })()), $context["i"], [], "array", false, false, false, 56), "idOffer", [], "any", false, false, false, 56), "idOffer", [], "any", false, false, false, 56)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 57
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 57, $this->source); })()), $context["i"], [], "array", false, false, false, 57), "idOffer", [], "any", false, false, false, 57), "title", [], "any", false, false, false, 57), "html", null, true);
                        echo ".
\t\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t\t    \t</div>
\t\t\t\t\t\t   \t\t\t</div>
\t\t\t\t\t   \t\t\t";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 61
(isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 61, $this->source); })()), $context["i"], [], "array", false, false, false, 61), "typeNotif", [], "any", false, false, false, 61), 12))) {
                        // line 62
                        echo "\t\t\t\t\t   \t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t    \t\t\tGreat job ! 
\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 66
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprise_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 66, $this->source); })()), $context["i"], [], "array", false, false, false, 66), "idOffer", [], "any", false, false, false, 66), "idEntreprise", [], "any", false, false, false, 66), "idEntreprise", [], "any", false, false, false, 66)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 67
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 67, $this->source); })()), $context["i"], [], "array", false, false, false, 67), "idOffer", [], "any", false, false, false, 67), "idEntreprise", [], "any", false, false, false, 67), "nameEnt", [], "any", false, false, false, 67), "html", null, true);
                        echo " 
\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t      \t\t\t has accepted your report for  
\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 70
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 70, $this->source); })()), $context["i"], [], "array", false, false, false, 70), "idOffer", [], "any", false, false, false, 70), "idOffer", [], "any", false, false, false, 70)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 71
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 71, $this->source); })()), $context["i"], [], "array", false, false, false, 71), "idOffer", [], "any", false, false, false, 71), "title", [], "any", false, false, false, 71), "html", null, true);
                        echo ".
\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    ";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 76
(isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 76, $this->source); })()), $context["i"], [], "array", false, false, false, 76), "typeNotif", [], "any", false, false, false, 76), 13))) {
                        // line 77
                        echo "\t\t\t\t\t\t\t    \t<div class=\"col-6\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t    \t\t\tWe regret to inform you that 
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 81
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprise_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 81, $this->source); })()), $context["i"], [], "array", false, false, false, 81), "idOffer", [], "any", false, false, false, 81), "idEntreprise", [], "any", false, false, false, 81), "idEntreprise", [], "any", false, false, false, 81)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 82
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 82, $this->source); })()), $context["i"], [], "array", false, false, false, 82), "idOffer", [], "any", false, false, false, 82), "idEntreprise", [], "any", false, false, false, 82), "nameEnt", [], "any", false, false, false, 82), "html", null, true);
                        echo " 
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t has rejected your report for  
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        // line 85
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 85, $this->source); })()), $context["i"], [], "array", false, false, false, 85), "idOffer", [], "any", false, false, false, 85), "idOffer", [], "any", false, false, false, 85)]), "html", null, true);
                        echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t";
                        // line 86
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 86, $this->source); })()), $context["i"], [], "array", false, false, false, 86), "idOffer", [], "any", false, false, false, 86), "title", [], "any", false, false, false, 86), "html", null, true);
                        echo ".
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t   \t\t\t\t\t
\t\t\t\t\t\t   \t\t";
                    }
                    // line 93
                    echo "\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t      \t\t";
                    // line 96
                    if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 96, $this->source); })()), $context["i"], [], "array", false, false, false, 96), "notifRead", [], "any", false, false, false, 96), 0))) {
                        // line 97
                        echo "\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("mark_as_read_admin", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 97, $this->source); })()), $context["i"], [], "array", false, false, false, 97), "idNotif", [], "any", false, false, false, 97)]), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Mark as read</div>
\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t      \t\t";
                    } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 102
(isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 102, $this->source); })()), $context["i"], [], "array", false, false, false, 102), "notifRead", [], "any", false, false, false, 102), 1))) {
                        // line 103
                        echo "\t\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("mark_as_read_admin", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 103, $this->source); })()), $context["i"], [], "array", false, false, false, 103), "idNotif", [], "any", false, false, false, 103)]), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Mark as unread</div>
\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t      \t\t";
                    }
                    // line 109
                    echo "\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    </div>\t
\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t      \t<div class=\"card-body\">
\t\t\t\t\t\t\t      \t\t\t<a href=\"";
                    // line 115
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("del_notif_admin", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["notifications_admin"]) || array_key_exists("notifications_admin", $context) ? $context["notifications_admin"] : (function () { throw new RuntimeError('Variable "notifications_admin" does not exist.', 115, $this->source); })()), $context["i"], [], "array", false, false, false, 115), "idNotif", [], "any", false, false, false, 115)]), "html", null, true);
                    echo "\">
\t\t\t\t\t\t\t\t              \t<svg width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-x-circle\" style=\"float:right;margin-right:20px;\" fill=\"#9D151C\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    </div>
\t\t\t\t\t    </div>
\t\t\t\t\t\t    ";
                }
                // line 129
                echo "\t\t\t\t\t    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 130
            echo "\t\t\t\t    ";
        }
        // line 131
        echo "</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "hacker/notifications_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  300 => 131,  297 => 130,  291 => 129,  274 => 115,  266 => 109,  256 => 103,  254 => 102,  245 => 97,  243 => 96,  238 => 93,  228 => 86,  224 => 85,  218 => 82,  214 => 81,  208 => 77,  206 => 76,  198 => 71,  194 => 70,  188 => 67,  184 => 66,  178 => 62,  176 => 61,  169 => 57,  165 => 56,  159 => 53,  155 => 52,  150 => 49,  148 => 48,  139 => 42,  133 => 39,  129 => 38,  124 => 35,  122 => 34,  117 => 31,  111 => 27,  109 => 26,  101 => 21,  97 => 19,  95 => 18,  90 => 15,  88 => 14,  83 => 11,  80 => 10,  75 => 9,  73 => 8,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}
\t
<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t{% if is_granted('ROLE_ADMIN') and notifications_admin|length != 0 %}
\t\t    {% for i in 0..notifications_admin|length-1 %}
\t\t    \t{% if notifications_admin[i].typeNotif == 7 or notifications_admin[i].typeNotif == 8 or ((notifications_admin[i].typeNotif == 4 or notifications_admin[i].typeNotif == 5 or notifications_admin[i].typeNotif == 12 or notifications_admin[i].typeNotif == 13) and notifications_admin[i].idHacker.idHacker == app.user.idHacker)%}
\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t        \t\t\t<div class=\"card card-overlay bg-transparent border-0 shadow\" style=\" margin-bottom:20px; width:60%\">
        \t\t\t\t\t<div class=\"row align-items-center\">
\t\t\t\t      \t\t\t{% if notifications_admin[i].typeNotif == 7 %}
\t    \t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t      \t\t{% if notifications_admin[i].idHacker is defined %}
\t\t\t\t\t\t\t\t\t      \t\t\t<div>
\t\t\t\t\t\t\t\t\t      \t\t\t\tWe have a new 
\t\t\t\t\t\t\t\t\t      \t\t\t\t<a href=\"{{ url('hacker_show', {'id' :  notifications_admin[i].idHacker.idHacker}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t\thacker
\t\t\t\t\t\t\t\t\t\t      \t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t      \t\t\t! 
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t      \t\t\t{% elseif notifications_admin[i].idEntreprise is defined %}
\t\t\t\t\t\t\t\t\t      \t\t\t<div>
\t\t\t\t\t\t\t\t\t      \t\t\t\tWe have a new enterprise ! 
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t      \t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t   \t\t\t{% elseif notifications_admin[i].typeNotif == 8 %}
\t    \t\t\t\t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t      \t\t<a href=\"{{ url('entreprise_show', {'id' : notifications_admin[i].idEntreprise.idEntreprise}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t      \t\t\t\t{{ notifications_admin[i].idEntreprise.nameEnt }} 
\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t      \t\t\twants to publish a new
\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('offer_show', {'id' : notifications_admin[i].idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t      \t\t\t\toffer 
\t\t\t\t\t\t\t\t      \t\t\t</a>.
\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t   \t\t</div>
\t\t\t\t\t\t   \t\t{% elseif notifications_admin[i].typeNotif == 4 and notifications_admin[i].idHacker.idHacker == app.user.idHacker %}
\t\t\t\t\t\t   \t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t   \t\t\t<a href=\"{{ url('entreprise_show', {'id' : notifications_admin[i].idEntreprise.idEntreprise}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t      \t\t\t\t{{ notifications_admin[i].idEntreprise.nameEnt }} 
\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t      \t\t\t has accepted your request for report submission for  
\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('offer_show', {'id' : notifications_admin[i].idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t      \t\t\t\t{{ notifications_admin[i].idOffer.title }}.
\t\t\t\t\t\t      \t\t\t\t</div>
\t\t\t\t\t\t\t\t    \t</div>
\t\t\t\t\t\t   \t\t\t</div>
\t\t\t\t\t   \t\t\t{% elseif notifications_admin[i].typeNotif == 12 %}
\t\t\t\t\t   \t\t\t\t<div class=\"col-6\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t    \t\t\tGreat job ! 
\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('entreprise_show', {'id' : notifications_admin[i].idOffer.idEntreprise.idEntreprise}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t      \t\t\t\t{{ notifications_admin[i].idOffer.idEntreprise.nameEnt }} 
\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t      \t\t\t has accepted your report for  
\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('offer_show', {'id' : notifications_admin[i].idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t      \t\t\t\t{{ notifications_admin[i].idOffer.title }}.
\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    {% elseif notifications_admin[i].typeNotif == 13 %}
\t\t\t\t\t\t\t    \t<div class=\"col-6\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t\t    \t\t\tWe regret to inform you that 
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('entreprise_show', {'id' : notifications_admin[i].idOffer.idEntreprise.idEntreprise}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ notifications_admin[i].idOffer.idEntreprise.nameEnt }} 
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t      \t\t\t has rejected your report for  
\t\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{ url('offer_show', {'id' : notifications_admin[i].idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t      \t\t\t\t{{ notifications_admin[i].idOffer.title }}.
\t\t\t\t\t\t\t\t\t      \t\t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t   \t\t\t\t\t
\t\t\t\t\t\t   \t\t{% endif %}
\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t      \t<div class=\"card-body\" style=\"color:black\">
\t\t\t\t\t\t\t\t      \t\t{% if notifications_admin[i].notifRead == 0 %}
\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{url('mark_as_read_admin', {'id' : notifications_admin[i].idNotif}) }}\">
\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Mark as read</div>
\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t      \t\t{% elseif notifications_admin[i].notifRead == 1 %}
\t\t\t\t\t\t\t\t      \t\t\t<a href=\"{{url('mark_as_read_admin', {'id' : notifications_admin[i].idNotif}) }}\">
\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Mark as unread</div>
\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t      \t\t{% endif %}
\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    </div>\t
\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t      \t<div class=\"card-body\">
\t\t\t\t\t\t\t      \t\t\t<a href=\"{{url('del_notif_admin', {'id' : notifications_admin[i].idNotif}) }}\">
\t\t\t\t\t\t\t\t              \t<svg width=\"2em\" height=\"2em\" viewBox=\"0 0 16 16\" class=\"bi bi-x-circle\" style=\"float:right;margin-right:20px;\" fill=\"#9D151C\" xmlns=\"http://www.w3.org/2000/svg\">
\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t  <path fill-rule=\"evenodd\" d=\"M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z\"/>
\t\t\t\t\t\t\t\t\t\t\t\t</svg>
\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t      \t</div>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    </div>
\t\t\t\t\t    </div>
\t\t\t\t\t\t    {% endif %}
\t\t\t\t\t    {% endfor %}
\t\t\t\t    {% endif %}
</div>
{% endblock %}", "hacker/notifications_admin.html.twig", "/Users/HoudaBerrada/Sites/advise_hack/templates/hacker/notifications_admin.html.twig");
    }
}
