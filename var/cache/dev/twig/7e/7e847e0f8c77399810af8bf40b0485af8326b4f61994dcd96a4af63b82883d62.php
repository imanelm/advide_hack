<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* hacker/profile.html.twig */
class __TwigTemplate_47601d1a633473dd12a1b1238552b70649546fcf6b5589b5042da02eb768464e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/profile.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/profile.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "hacker/profile.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
 
<div class=\"container\" style=\"margin-top: 120px;\">
   ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 8, $this->source); })()), "flashes", [0 => "message"], "method", false, false, false, 8));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 9
            echo "\t\t\t<div class=\" form-group\">
\t\t\t\t<div class=\"alert alert-success\" role=\"alert\"> ";
            // line 10
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
\t\t\t</div>

\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "    <div class=\"row my-2\">
        <div class=\"col-lg-8 order-lg-2\">
            
            <div class=\"tab-content py-4\">
                <div class=\"tab-pane active\" id=\"profile\">
                    <h5 class=\"mb-3\">";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 19, $this->source); })()), "user", [], "any", false, false, false, 19), "username", [], "any", false, false, false, 19), "html", null, true);
        echo "'s Profile</h5>
                    <div class=\"row\">                  
                        <div class=\"col-md-12\">
                          
                            <table class=\"table table-sm table-hover table-striped\">
                                <tbody>
                                     ";
        // line 25
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 25), "nameEnt", [], "any", true, true, false, 25)) {
            // line 26
            echo "                                    <tr>
                                        <td>
                                            <strong>Entreprise : </strong> ";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 28, $this->source); })()), "user", [], "any", false, false, false, 28), "nameEnt", [], "any", false, false, false, 28), "html", null, true);
            echo "
                                        </td>
                                    </tr>
                                     ";
        }
        // line 32
        echo "\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 33
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 33), "nameEnt", [], "any", true, true, false, 33)) {
            // line 34
            echo "                                    <tr>
                                        <td>
                                            <strong>Username : </strong> ";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 36, $this->source); })()), "user", [], "any", false, false, false, 36), "username", [], "any", false, false, false, 36), "html", null, true);
            echo "
                                        </td>
                                    </tr>
\t\t\t\t\t\t\t\t\t";
        }
        // line 40
        echo "
                                    <tr>
                                        <td>
                                            <strong>First Name : </strong> ";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 43, $this->source); })()), "user", [], "any", false, false, false, 43), "name", [], "any", false, false, false, 43), "html", null, true);
        echo "
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Family Name : </strong> ";
        // line 48
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 48, $this->source); })()), "user", [], "any", false, false, false, 48), "fName", [], "any", false, false, false, 48), "html", null, true);
        echo "
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Email: </strong> ";
        // line 53
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 53, $this->source); })()), "user", [], "any", false, false, false, 53), "email", [], "any", false, false, false, 53), "html", null, true);
        echo "
                                        </td>
                                    </tr>
                                     ";
        // line 56
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 56), "num", [], "any", true, true, false, 56)) {
            // line 57
            echo "                                    <tr>
                                        <td>
                                            
                                            <strong>Phone Number : </strong> ";
            // line 60
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 60, $this->source); })()), "user", [], "any", false, false, false, 60), "num", [], "any", false, false, false, 60), "html", null, true);
            echo "
                                             
                                        </td>
                                    </tr>
                                    ";
        }
        // line 65
        echo "                                    <tr>
                                        <td>
                                            <strong>Country : </strong> ";
        // line 67
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 67, $this->source); })()), "user", [], "any", false, false, false, 67), "country", [], "any", false, false, false, 67), "html", null, true);
        echo "
                                        </td>
                                    </tr>
                                    ";
        // line 70
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 70), "mark", [], "any", true, true, false, 70)) {
            // line 71
            echo "                                        ";
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 71), "adress", [], "any", true, true, false, 71)) {
                // line 72
                echo "                                            <tr>
                                                <td>
                                                    <strong> Adress : </strong> ";
                // line 74
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 74, $this->source); })()), "user", [], "any", false, false, false, 74), "adress", [], "any", false, false, false, 74), "html", null, true);
                echo "
                                                </td>
                                            </tr>
                                        ";
            }
            // line 78
            echo "                                        ";
            if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 78, $this->source); })()), "user", [], "any", false, false, false, 78), "twitter", [], "any", false, false, false, 78))) {
                // line 79
                echo "                                            <tr>
                                                <td>
                                                    <strong> Twitter : </strong> ";
                // line 81
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 81, $this->source); })()), "user", [], "any", false, false, false, 81), "twitter", [], "any", false, false, false, 81), "html", null, true);
                echo "
                                                </td>
                                            </tr>
                                        ";
            }
            // line 85
            echo "                                        ";
            if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 85, $this->source); })()), "user", [], "any", false, false, false, 85), "linkedin", [], "any", false, false, false, 85))) {
                // line 86
                echo "                                            <tr>
                                                <td>
                                                    <strong> LinkedIn : </strong> ";
                // line 88
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 88, $this->source); })()), "user", [], "any", false, false, false, 88), "linkedin", [], "any", false, false, false, 88), "html", null, true);
                echo "
                                                </td>
                                            </tr>
                                        ";
            }
            // line 92
            echo "                                        ";
            if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 92, $this->source); })()), "user", [], "any", false, false, false, 92), "github", [], "any", false, false, false, 92))) {
                // line 93
                echo "                                            <tr>
                                                <td>
                                                    <strong> Git Hub : </strong> ";
                // line 95
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 95, $this->source); })()), "user", [], "any", false, false, false, 95), "github", [], "any", false, false, false, 95), "html", null, true);
                echo "
                                                </td>
                                            </tr>
                                        ";
            }
            // line 99
            echo "                                        ";
            if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 99, $this->source); })()), "user", [], "any", false, false, false, 99), "site", [], "any", false, false, false, 99))) {
                // line 100
                echo "                                            <tr>
                                                <td>
                                                    <strong> Web Site : </strong> ";
                // line 102
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 102, $this->source); })()), "user", [], "any", false, false, false, 102), "site", [], "any", false, false, false, 102), "html", null, true);
                echo "
                                                </td>
                                            </tr>
                                        ";
            }
            // line 106
            echo "                                        ";
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 106, $this->source); })()), "user", [], "any", false, false, false, 106), "fichierSup", [], "any", false, false, false, 106)) {
                // line 107
                echo "                                            <tr>
                                                <td>
                                                    <strong> Additional File : </strong> <a class=\"btn btn-sm text-center text-white\" style=\"background-color: #9D151C;\" href=\"/Images/hackersphotos/";
                // line 109
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 109, $this->source); })()), "user", [], "any", false, false, false, 109), "fichierSup", [], "any", false, false, false, 109), "html", null, true);
                echo "\" download>View File </a> 
                                                </td>
                                            </tr>
                                        ";
            }
            // line 113
            echo "                                    ";
        }
        // line 114
        echo "                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--/row-->
                </div>
                
                
            </div>
        </div>
        <div class=\"col-lg-4 order-lg-1 text-center\">
            <img src=\"/Images/hackersphotos/";
        // line 125
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 125, $this->source); })()), "user", [], "any", false, false, false, 125), "photo", [], "any", false, false, false, 125), "html", null, true);
        echo "\" alt=\"avatar\" style = \"width : 150px; margin-top:30px;\">
            <span style=\"display:block; margin-top:30px\">
\t\t\t    ";
        // line 127
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 127), "nameEnt", [], "any", true, true, false, 127)) {
            // line 128
            echo "                <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("EditProfilEntreprise");
            echo "\" class =\"btn blue shadow\" style = \" font-weight: bold; color :#f8f9fa; background-color: #9D151C; margin:auto;\"> Edit Profil </a>
\t\t\t\t";
        } else {
            // line 130
            echo "                <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("EditProfil");
            echo "\" class =\"btn blue shadow\" style = \" font-weight: bold; color :#f8f9fa; background-color: #9D151C; margin:auto;\"> Edit Profil </a>
\t\t\t\t";
        }
        // line 132
        echo "                <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("EditPass");
        echo "\" class =\"btn blue shadow\" style = \" font-weight: bold; color :#f8f9fa; background-color: #9D151C;  margin:auto;\"> Change Password </a>
\t\t\t</span>
            ";
        // line 134
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 134), "num", [], "any", true, true, false, 134)) {
            // line 135
            echo "                    <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("ent_offres", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 135, $this->source); })()), "user", [], "any", false, false, false, 135), "idEntreprise", [], "any", false, false, false, 135)]), "html", null, true);
            echo "\" class =\"btn blue shadow\" style =\"margin-top:6px; font-weight: bold; color :#f8f9fa; background-color: #9D151C;\"> Check my offers </a>
                    <a href=\"";
            // line 136
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("ent_reports", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 136, $this->source); })()), "user", [], "any", false, false, false, 136), "idEntreprise", [], "any", false, false, false, 136)]), "html", null, true);
            echo "\" class =\"btn blue shadow\" style =\"margin-top:6px; font-weight: bold; color :#f8f9fa; background-color: #9D151C; width:170px;\"> Check my reports </a>
            ";
        }
        // line 138
        echo "            ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 138), "mark", [], "any", true, true, false, 138)) {
            // line 139
            echo "                    <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("hack_reports", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 139, $this->source); })()), "user", [], "any", false, false, false, 139), "idHacker", [], "any", false, false, false, 139)]), "html", null, true);
            echo "\" class =\"btn blue shadow\" style =\"margin-top:6px; font-weight: bold; color :#f8f9fa; background-color: #9D151C; width:170px;\"> Check my reports </a>
            ";
        }
        // line 141
        echo "            ";
        if (((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 141, $this->source); })()), "user", [], "any", false, false, false, 141), "state", [], "any", false, false, false, 141), "idstate", [], "any", false, false, false, 141), 2)) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 141, $this->source); })()), "user", [], "any", false, false, false, 141), "test", [], "any", false, false, false, 141), null)))) {
            // line 142
            echo "                <span style=\"display:block; margin-top:6px;\">
                    <a href=\"";
            // line 143
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("test");
            echo "\" class =\"btn blue shadow\" style = \" font-weight: bold; color :#f8f9fa; background-color: #9D151C; margin:auto;\"> Tests </a>
                </span>
            ";
        }
        // line 145
        echo " 
            ";
        // line 146
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 147
            echo "            <span style=\"display:block;\">
                <a href=\"";
            // line 148
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("new_post");
            echo "\" class =\"btn blue shadow\" style =\"margin-top:6px; font-weight: bold; color :#f8f9fa; background-color: #9D151C; width:170px;\"> New blog post </a>
            </span>
            ";
        }
        // line 151
        echo "
        </div>
    
            
            
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "hacker/profile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  361 => 151,  355 => 148,  352 => 147,  350 => 146,  347 => 145,  341 => 143,  338 => 142,  335 => 141,  329 => 139,  326 => 138,  321 => 136,  316 => 135,  314 => 134,  308 => 132,  302 => 130,  296 => 128,  294 => 127,  289 => 125,  276 => 114,  273 => 113,  266 => 109,  262 => 107,  259 => 106,  252 => 102,  248 => 100,  245 => 99,  238 => 95,  234 => 93,  231 => 92,  224 => 88,  220 => 86,  217 => 85,  210 => 81,  206 => 79,  203 => 78,  196 => 74,  192 => 72,  189 => 71,  187 => 70,  181 => 67,  177 => 65,  169 => 60,  164 => 57,  162 => 56,  156 => 53,  148 => 48,  140 => 43,  135 => 40,  128 => 36,  124 => 34,  122 => 33,  119 => 32,  112 => 28,  108 => 26,  106 => 25,  97 => 19,  90 => 14,  80 => 10,  77 => 9,  73 => 8,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}

 
<div class=\"container\" style=\"margin-top: 120px;\">
   {% for message in app.flashes('message')%}
\t\t\t<div class=\" form-group\">
\t\t\t\t<div class=\"alert alert-success\" role=\"alert\"> {{message}}</div>
\t\t\t</div>

\t{%endfor%}
    <div class=\"row my-2\">
        <div class=\"col-lg-8 order-lg-2\">
            
            <div class=\"tab-content py-4\">
                <div class=\"tab-pane active\" id=\"profile\">
                    <h5 class=\"mb-3\">{{app.user.username}}'s Profile</h5>
                    <div class=\"row\">                  
                        <div class=\"col-md-12\">
                          
                            <table class=\"table table-sm table-hover table-striped\">
                                <tbody>
                                     {% if app.user.nameEnt is defined %}
                                    <tr>
                                        <td>
                                            <strong>Entreprise : </strong> {{app.user.nameEnt}}
                                        </td>
                                    </tr>
                                     {% endif %}
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t{% if app.user.nameEnt is not defined %}
                                    <tr>
                                        <td>
                                            <strong>Username : </strong> {{app.user.username}}
                                        </td>
                                    </tr>
\t\t\t\t\t\t\t\t\t{% endif %}

                                    <tr>
                                        <td>
                                            <strong>First Name : </strong> {{app.user.name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Family Name : </strong> {{app.user.fName}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Email: </strong> {{app.user.email}}
                                        </td>
                                    </tr>
                                     {% if app.user.num is defined %}
                                    <tr>
                                        <td>
                                            
                                            <strong>Phone Number : </strong> {{app.user.num}}
                                             
                                        </td>
                                    </tr>
                                    {% endif %}
                                    <tr>
                                        <td>
                                            <strong>Country : </strong> {{app.user.country}}
                                        </td>
                                    </tr>
                                    {% if app.user.mark is defined %}
                                        {% if app.user.adress is defined %}
                                            <tr>
                                                <td>
                                                    <strong> Adress : </strong> {{app.user.adress}}
                                                </td>
                                            </tr>
                                        {% endif %}
                                        {% if app.user.twitter is not null %}
                                            <tr>
                                                <td>
                                                    <strong> Twitter : </strong> {{app.user.twitter}}
                                                </td>
                                            </tr>
                                        {% endif %}
                                        {% if app.user.linkedin is not null %}
                                            <tr>
                                                <td>
                                                    <strong> LinkedIn : </strong> {{app.user.linkedin}}
                                                </td>
                                            </tr>
                                        {% endif %}
                                        {% if app.user.github is not null %}
                                            <tr>
                                                <td>
                                                    <strong> Git Hub : </strong> {{app.user.github}}
                                                </td>
                                            </tr>
                                        {% endif %}
                                        {% if app.user.site is not null %}
                                            <tr>
                                                <td>
                                                    <strong> Web Site : </strong> {{app.user.site}}
                                                </td>
                                            </tr>
                                        {% endif %}
                                        {% if app.user.fichierSup%}
                                            <tr>
                                                <td>
                                                    <strong> Additional File : </strong> <a class=\"btn btn-sm text-center text-white\" style=\"background-color: #9D151C;\" href=\"/Images/hackersphotos/{{app.user.fichierSup}}\" download>View File </a> 
                                                </td>
                                            </tr>
                                        {% endif %}
                                    {% endif %}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--/row-->
                </div>
                
                
            </div>
        </div>
        <div class=\"col-lg-4 order-lg-1 text-center\">
            <img src=\"/Images/hackersphotos/{{app.user.photo }}\" alt=\"avatar\" style = \"width : 150px; margin-top:30px;\">
            <span style=\"display:block; margin-top:30px\">
\t\t\t    {% if app.user.nameEnt is defined %}
                <a href=\"{{url('EditProfilEntreprise')}}\" class =\"btn blue shadow\" style = \" font-weight: bold; color :#f8f9fa; background-color: #9D151C; margin:auto;\"> Edit Profil </a>
\t\t\t\t{% else %}
                <a href=\"{{url('EditProfil')}}\" class =\"btn blue shadow\" style = \" font-weight: bold; color :#f8f9fa; background-color: #9D151C; margin:auto;\"> Edit Profil </a>
\t\t\t\t{% endif %}
                <a href=\"{{url('EditPass')}}\" class =\"btn blue shadow\" style = \" font-weight: bold; color :#f8f9fa; background-color: #9D151C;  margin:auto;\"> Change Password </a>
\t\t\t</span>
            {% if app.user.num is defined %}
                    <a href=\"{{url('ent_offres', {'id' : app.user.idEntreprise})}}\" class =\"btn blue shadow\" style =\"margin-top:6px; font-weight: bold; color :#f8f9fa; background-color: #9D151C;\"> Check my offers </a>
                    <a href=\"{{url('ent_reports', {'id' : app.user.idEntreprise})}}\" class =\"btn blue shadow\" style =\"margin-top:6px; font-weight: bold; color :#f8f9fa; background-color: #9D151C; width:170px;\"> Check my reports </a>
            {% endif %}
            {% if app.user.mark is defined %}
                    <a href=\"{{url('hack_reports', {'id' : app.user.idHacker})}}\" class =\"btn blue shadow\" style =\"margin-top:6px; font-weight: bold; color :#f8f9fa; background-color: #9D151C; width:170px;\"> Check my reports </a>
            {% endif %}
            {% if app.user.state.idstate == 2 and app.user.test==NULL%}
                <span style=\"display:block; margin-top:6px;\">
                    <a href=\"{{url('test')}}\" class =\"btn blue shadow\" style = \" font-weight: bold; color :#f8f9fa; background-color: #9D151C; margin:auto;\"> Tests </a>
                </span>
            {% endif %} 
            {% if is_granted('ROLE_ADMIN') %}
            <span style=\"display:block;\">
                <a href=\"{{url('new_post')}}\" class =\"btn blue shadow\" style =\"margin-top:6px; font-weight: bold; color :#f8f9fa; background-color: #9D151C; width:170px;\"> New blog post </a>
            </span>
            {% endif %}

        </div>
    
            
            
    </div>
</div>
{% endblock %}
", "hacker/profile.html.twig", "/Users/HoudaBerrada/Sites/advise_hack/templates/hacker/profile.html.twig");
    }
}
