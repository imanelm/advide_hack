<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* entreprise/show.html.twig */
class __TwigTemplate_841f970dd7e50ca6e8241c69b9769dc4d66a27123769f34623b3ac1ac950296c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "entreprise/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "entreprise/show.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "entreprise/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo " 
        \t
<div class=\"card border-0 bg-transparent\" style=\"margin-top:80px\">
\t<div class=\"tab-content py-4 text-center\">
    \t<div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(256, 256, 256, 0.5); width:100%;\">
    \t\t<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t            <div class=\"card-body\">
\t            \t<div class=\"row justify-content-center d-flex align-items-center\">
\t\t\t\t    \t<div class=\"col-3\">
\t\t\t\t\t     \t<h3 class=\"text-title text-white text-center\">";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["entreprise"]) || array_key_exists("entreprise", $context) ? $context["entreprise"] : (function () { throw new RuntimeError('Variable "entreprise" does not exist.', 14, $this->source); })()), "nameEnt", [], "any", false, false, false, 14), "html", null, true);
        echo "'s Profile</h3>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t            </div>
\t        </div>
      </div>
    </div>
    <div class=\"tab-content py-4\">
    \t<div class=\"card-overlay border-0 bg-white shadow mx-auto\" style=\"width:60%;\">
\t        <div class=\"text-card text-center\" style=\"margin-bottom:30px\">
\t        \t<img src=\"/Images/hackersphotos/";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["entreprise"]) || array_key_exists("entreprise", $context) ? $context["entreprise"] : (function () { throw new RuntimeError('Variable "entreprise" does not exist.', 24, $this->source); })()), "photo", [], "any", false, false, false, 24), "html", null, true);
        echo "\" alt=\"\" style = \"width : 150px; margin-top:30px;\">
\t        </div>
\t        <div class=\"row\">                  
\t            <div class=\" mx-auto\">
\t              
\t                <table class=\"table table-borderless\">
\t                    <tbody>
\t\t\t\t\t\t\t<tr>
\t                            <td>
\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\">Name : </strong> <div style=\"float:right\">";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["entreprise"]) || array_key_exists("entreprise", $context) ? $context["entreprise"] : (function () { throw new RuntimeError('Variable "entreprise" does not exist.', 33, $this->source); })()), "nameEnt", [], "any", false, false, false, 33), "html", null, true);
        echo "</div>
\t                            </td>
\t                        </tr>
\t                        <tr>
\t                            <td>
\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\">Manager : </strong> <div style=\"float:right\">";
        // line 38
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["entreprise"]) || array_key_exists("entreprise", $context) ? $context["entreprise"] : (function () { throw new RuntimeError('Variable "entreprise" does not exist.', 38, $this->source); })()), "name", [], "any", false, false, false, 38), "html", null, true);
        echo "</div>
\t                            </td>
\t                        </tr>

\t                        <tr>
\t                            <td>
\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\"g>Country : </strong> <div style=\"float:right\">";
        // line 44
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["entreprise"]) || array_key_exists("entreprise", $context) ? $context["entreprise"] : (function () { throw new RuntimeError('Variable "entreprise" does not exist.', 44, $this->source); })()), "country", [], "any", false, false, false, 44), "html", null, true);
        echo "</div>
\t                            </td>
\t                        </tr>
\t\t\t\t\t\t\t<tr>
\t                            <td>
\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Phone Number : </strong> <div style=\"float:right\">";
        // line 49
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["entreprise"]) || array_key_exists("entreprise", $context) ? $context["entreprise"] : (function () { throw new RuntimeError('Variable "entreprise" does not exist.', 49, $this->source); })()), "num", [], "any", false, false, false, 49), "html", null, true);
        echo "</div>
\t                            </td>
\t                        </tr>
\t\t\t\t\t\t\t\t";
        // line 52
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["entreprise"]) || array_key_exists("entreprise", $context) ? $context["entreprise"] : (function () { throw new RuntimeError('Variable "entreprise" does not exist.', 52, $this->source); })()), "adress", [], "any", false, false, false, 52))) {
            // line 53
            echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Adress : </strong> <div style=\"float:right\">";
            // line 55
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["entreprise"]) || array_key_exists("entreprise", $context) ? $context["entreprise"] : (function () { throw new RuntimeError('Variable "entreprise" does not exist.', 55, $this->source); })()), "adress", [], "any", false, false, false, 55), "html", null, true);
            echo "</div>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    ";
        }
        // line 59
        echo "\t\t\t                    ";
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["entreprise"]) || array_key_exists("entreprise", $context) ? $context["entreprise"] : (function () { throw new RuntimeError('Variable "entreprise" does not exist.', 59, $this->source); })()), "site", [], "any", false, false, false, 59))) {
            // line 60
            echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Website : </strong> 
\t\t\t\t\t\t\t\t\t\t\t<a style=\"float:right; color:#000000;\" href=\"";
            // line 63
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["entreprise"]) || array_key_exists("entreprise", $context) ? $context["entreprise"] : (function () { throw new RuntimeError('Variable "entreprise" does not exist.', 63, $this->source); })()), "site", [], "any", false, false, false, 63), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["entreprise"]) || array_key_exists("entreprise", $context) ? $context["entreprise"] : (function () { throw new RuntimeError('Variable "entreprise" does not exist.', 63, $this->source); })()), "site", [], "any", false, false, false, 63), "html", null, true);
            echo "</a>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    ";
        }
        // line 67
        echo "\t                    </tbody>
\t                </table>

\t                ";
        // line 70
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 71
            echo "\t                \t<div class=\"text-center\">
\t\t                    <a href=\"";
            // line 72
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("entreprise_delete", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["entreprise"]) || array_key_exists("entreprise", $context) ? $context["entreprise"] : (function () { throw new RuntimeError('Variable "entreprise" does not exist.', 72, $this->source); })()), "idEntreprise", [], "any", false, false, false, 72)]), "html", null, true);
            echo "\">
\t\t                        <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); height:40px; margin-bottom:10px\">
\t\t                            <h5 style=\"color:#9D151C;\">Delete</h5>
\t\t                        </button>
\t\t                    </a>
\t\t                </div>
\t\t            ";
        }
        // line 79
        echo "\t                
\t            </div>
\t        </div>
       \t</div>
    </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "entreprise/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 79,  175 => 72,  172 => 71,  170 => 70,  165 => 67,  156 => 63,  151 => 60,  148 => 59,  141 => 55,  137 => 53,  135 => 52,  129 => 49,  121 => 44,  112 => 38,  104 => 33,  92 => 24,  79 => 14,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}
 
        \t
<div class=\"card border-0 bg-transparent\" style=\"margin-top:80px\">
\t<div class=\"tab-content py-4 text-center\">
    \t<div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(256, 256, 256, 0.5); width:100%;\">
    \t\t<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t            <div class=\"card-body\">
\t            \t<div class=\"row justify-content-center d-flex align-items-center\">
\t\t\t\t    \t<div class=\"col-3\">
\t\t\t\t\t     \t<h3 class=\"text-title text-white text-center\">{{entreprise.nameEnt }}'s Profile</h3>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t            </div>
\t        </div>
      </div>
    </div>
    <div class=\"tab-content py-4\">
    \t<div class=\"card-overlay border-0 bg-white shadow mx-auto\" style=\"width:60%;\">
\t        <div class=\"text-card text-center\" style=\"margin-bottom:30px\">
\t        \t<img src=\"/Images/hackersphotos/{{entreprise.photo }}\" alt=\"\" style = \"width : 150px; margin-top:30px;\">
\t        </div>
\t        <div class=\"row\">                  
\t            <div class=\" mx-auto\">
\t              
\t                <table class=\"table table-borderless\">
\t                    <tbody>
\t\t\t\t\t\t\t<tr>
\t                            <td>
\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\">Name : </strong> <div style=\"float:right\">{{entreprise.nameEnt }}</div>
\t                            </td>
\t                        </tr>
\t                        <tr>
\t                            <td>
\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\">Manager : </strong> <div style=\"float:right\">{{ entreprise.name }}</div>
\t                            </td>
\t                        </tr>

\t                        <tr>
\t                            <td>
\t                                <strong style=\"float:left; padding-right:10px; color:#9D151C;\"g>Country : </strong> <div style=\"float:right\">{{entreprise.country}}</div>
\t                            </td>
\t                        </tr>
\t\t\t\t\t\t\t<tr>
\t                            <td>
\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Phone Number : </strong> <div style=\"float:right\">{{entreprise.num}}</div>
\t                            </td>
\t                        </tr>
\t\t\t\t\t\t\t\t{% if entreprise.adress is not null %}
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Adress : </strong> <div style=\"float:right\">{{entreprise.adress}}</div>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    {% endif %}
\t\t\t                    {% if entreprise.site is not null %}
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t                            <td>
\t\t\t\t\t\t\t\t\t\t\t<strong style=\"float:left; padding-right:10px; color:#9D151C;\"> Website : </strong> 
\t\t\t\t\t\t\t\t\t\t\t<a style=\"float:right; color:#000000;\" href=\"{{entreprise.site}}\">{{entreprise.site}}</a>
\t\t\t                            </td>
\t\t\t                        </tr>
\t\t\t                    {% endif %}
\t                    </tbody>
\t                </table>

\t                {% if is_granted('ROLE_ADMIN') %}
\t                \t<div class=\"text-center\">
\t\t                    <a href=\"{{url('entreprise_delete', {'id' : entreprise.idEntreprise }) }}\">
\t\t                        <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); height:40px; margin-bottom:10px\">
\t\t                            <h5 style=\"color:#9D151C;\">Delete</h5>
\t\t                        </button>
\t\t                    </a>
\t\t                </div>
\t\t            {% endif %}
\t                
\t            </div>
\t        </div>
       \t</div>
    </div>
</div>

{% endblock %}


", "entreprise/show.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\entreprise\\show.html.twig");
    }
}
