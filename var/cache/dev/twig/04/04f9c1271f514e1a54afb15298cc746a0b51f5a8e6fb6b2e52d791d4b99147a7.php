<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* offer/show.html.twig */
class __TwigTemplate_68359a51b9b1517c559233b0f7ac89055334fd7911770b4c06a35d5d028be34c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "offer/show.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "offer/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo " 
<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t<div class=\"card border-0 bg-transparent\" style=\"margin-top:85px\">
\t    <div class=\"tab-content py-4 text-center\">
\t    \t<div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(217, 217, 217, 0.5); width:100%;\">
\t            <div class=\"card-body\">
\t            \t<div class=\"row justify-content-center d-flex align-items-center\">
\t\t\t\t    \t<div class=\"col-3\">
\t\t\t\t\t     \t<h4 class=\"text-title text-white text-center\">";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 13, $this->source); })()), "offer", [], "array", false, false, false, 13), "title", [], "any", false, false, false, 13), "html", null, true);
        echo "</h4>
\t\t\t\t\t    </div>
\t\t\t\t\t    <div class=\"col-3\">
\t\t\t\t\t      \t<div class=\"text-title text-white text-center\">";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 16, $this->source); })()), "offer", [], "array", false, false, false, 16), "description1", [], "any", false, false, false, 16), "html", null, true);
        echo "</div>
\t\t\t\t\t    </div>
\t\t\t\t\t    ";
        // line 18
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 18), "mark", [], "any", true, true, false, 18)) {
            // line 19
            echo "\t\t\t\t\t    \t";
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 19, $this->source); })()), "user", [], "any", false, false, false, 19), "state", [], "any", false, false, false, 19), "idState", [], "any", false, false, false, 19), 1))) {
                // line 20
                echo "\t\t\t\t\t\t    \t";
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["result"] ?? null), "report_permit", [], "array", false, true, false, 20), "state", [], "any", true, true, false, 20) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 20, $this->source); })()), "report_permit", [], "array", false, false, false, 20), "state", [], "any", false, false, false, 20), 1)))) {
                    // line 21
                    echo "\t\t\t\t\t\t\t    \t<div class=\"col-3\">
\t\t\t\t\t\t\t\t    \t<a href=\"";
                    // line 22
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("sub_report", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 22, $this->source); })()), "offer", [], "array", false, false, false, 22), "idOffer", [], "any", false, false, false, 22)]), "html", null, true);
                    echo "\">
\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Submit report</div>
\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    ";
                } else {
                    // line 29
                    echo "\t\t\t\t\t\t\t    \t<div class=\"col-3\">
\t\t\t\t\t\t\t\t    \t<a href=\"";
                    // line 30
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("report_ask", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 30, $this->source); })()), "offer", [], "array", false, false, false, 30), "idOffer", [], "any", false, false, false, 30)]), "html", null, true);
                    echo "\">
\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Request report submission</div>
\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    ";
                }
                // line 37
                echo "\t\t\t\t\t\t    ";
            }
            // line 38
            echo "\t\t\t\t\t    ";
        }
        // line 39
        echo "\t\t\t\t\t</div>
\t            </div>
          </div>
      \t";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 42, $this->source); })()), "flashes", [0 => "message"], "method", false, false, false, 42));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 43
            echo "\t\t\t<div class=\" form-group text-center\">
\t\t\t\t<div class=\"alert alert-success mx-auto\" style=\"margin-top:5px; margin-bottom:-10px; width:40%;\" role=\"alert\"> ";
            // line 44
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
\t\t\t</div>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "        </div>
\t    \t<div class=\"card-overlay border-0 bg-white shadow mx-auto\" style=\"width:70%; margin-bottom:20px;\">
\t\t        
\t\t        <div class=\"d-flex flex-column bd-highlight mb-2\" style=\"margin-left:20px; margin-top:20px\">                  
\t                <div class=\"row\">
\t                \t<div class=\"col-10\">
\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Offer Type : </h5> 
\t                        <div class=\"p-2\">";
        // line 54
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 54, $this->source); })()), "info_ent", [], "array", false, false, false, 54), 0, [], "array", false, false, false, 54), "n_type", [], "any", false, false, false, 54), "html", null, true);
        echo "</div>
                        </div>
                        <div class=\"col\">
                        \t<h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Date : </h5> 
\t                        <div class=\"p-2\">";
        // line 58
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 58, $this->source); })()), "offer", [], "array", false, false, false, 58), "dateAdd", [], "any", false, false, false, 58), "m/d/Y"), "html", null, true);
        echo "</div>
                    \t</div>
                \t</div>
\t                <div class=\"row\">
\t                \t<div class=\"col-10\">
\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Offer Description : </h5> 
\t                        <div class=\"p-2\">";
        // line 64
        echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 64, $this->source); })()), "offer", [], "array", false, false, false, 64), "description2", [], "any", false, false, false, 64);
        echo "</div>
                        </div>
                        <div class=\"col\">
                        \t<h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Reports : </h5> 
\t                        <div class=\"p-2\">";
        // line 68
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 68, $this->source); })()), "offer", [], "array", false, false, false, 68), "nbReports", [], "any", false, false, false, 68), "html", null, true);
        echo "</div>
\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Thanks : </h5> 
\t                        <div class=\"p-2\">";
        // line 70
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 70, $this->source); })()), "offer", [], "array", false, false, false, 70), "nbThanks", [], "any", false, false, false, 70), "html", null, true);
        echo "</div>
                    \t</div>
                \t</div>

\t\t                ";
        // line 74
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 75
            echo "\t\t\t\t\t        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_delete", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 75, $this->source); })()), "offer", [], "array", false, false, false, 75), "idOffer", [], "any", false, false, false, 75)]), "html", null, true);
            echo "\">
\t\t\t\t\t            <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); margin-top:10px; margin-bottom:10px\">
\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold\">Delete offer</div>
\t\t\t\t\t            </button>
\t\t\t\t\t        </a>
\t\t\t\t\t        ";
            // line 80
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 80, $this->source); })()), "info_ent", [], "array", false, false, false, 80), 0, [], "array", false, false, false, 80), "state", [], "any", false, false, false, 80), 2))) {
                // line 81
                echo "\t\t\t\t\t        <div class=\"row\">
\t\t\t\t\t        \t<div class=\"col-2\">
\t\t\t\t\t        \t\t<a href=\"";
                // line 83
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_in", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 83, $this->source); })()), "offer", [], "array", false, false, false, 83), "idOffer", [], "any", false, false, false, 83)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t            <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); margin-top:10px; margin-bottom:10px\">
\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold\">Allow offer</div>
\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t        </a>
\t\t\t\t\t        \t</div>
\t\t\t\t\t        \t<div class=\"col-2\">
\t\t\t\t\t        \t\t<a href=\"";
                // line 90
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_out", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 90, $this->source); })()), "offer", [], "array", false, false, false, 90), "idOffer", [], "any", false, false, false, 90)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t            <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); float:left; margin-top:10px; margin-bottom:10px; margin-left:-30px;\">
\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold\">Reject offer</div>
\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t        </a>
\t\t\t\t\t        \t</div>
\t\t\t\t\t        </div>
\t\t\t\t\t        ";
            }
            // line 98
            echo "\t\t\t\t        ";
        } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 98), "idEntreprise", [], "any", true, true, false, 98)) {
            // line 99
            echo "\t\t\t\t        \t";
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 99, $this->source); })()), "info_ent", [], "array", false, false, false, 99), 0, [], "array", false, false, false, 99), "id_entreprise", [], "any", false, false, false, 99), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 99, $this->source); })()), "user", [], "any", false, false, false, 99), "idEntreprise", [], "any", false, false, false, 99)))) {
                // line 100
                echo "\t\t\t\t        \t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_delete", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 100, $this->source); })()), "offer", [], "array", false, false, false, 100), "idOffer", [], "any", false, false, false, 100)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t            <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); margin-top:10px; margin-bottom:10px\">
\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold\">Delete offer</div>
\t\t\t\t\t\t            </button>
\t\t\t\t\t        \t</a>
\t\t\t\t        \t";
            }
            // line 106
            echo "\t\t\t\t      \t";
        }
        // line 107
        echo "\t\t        </div>
\t       \t</div>
\t</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "offer/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  251 => 107,  248 => 106,  238 => 100,  235 => 99,  232 => 98,  221 => 90,  211 => 83,  207 => 81,  205 => 80,  196 => 75,  194 => 74,  187 => 70,  182 => 68,  175 => 64,  166 => 58,  159 => 54,  150 => 47,  141 => 44,  138 => 43,  134 => 42,  129 => 39,  126 => 38,  123 => 37,  113 => 30,  110 => 29,  100 => 22,  97 => 21,  94 => 20,  91 => 19,  89 => 18,  84 => 16,  78 => 13,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}
 
<div style=\"background-image: url('/Images/TitlePattern.jpg');\">
\t<div class=\"card border-0 bg-transparent\" style=\"margin-top:85px\">
\t    <div class=\"tab-content py-4 text-center\">
\t    \t<div class=\"card border-0 rounded-0 shadow\" style=\"background-color:rgba(217, 217, 217, 0.5); width:100%;\">
\t            <div class=\"card-body\">
\t            \t<div class=\"row justify-content-center d-flex align-items-center\">
\t\t\t\t    \t<div class=\"col-3\">
\t\t\t\t\t     \t<h4 class=\"text-title text-white text-center\">{{result['offer'].title}}</h4>
\t\t\t\t\t    </div>
\t\t\t\t\t    <div class=\"col-3\">
\t\t\t\t\t      \t<div class=\"text-title text-white text-center\">{{result['offer'].description1}}</div>
\t\t\t\t\t    </div>
\t\t\t\t\t    {% if app.user.mark is defined %}
\t\t\t\t\t    \t{% if app.user.state.idState == 1 %}
\t\t\t\t\t\t    \t{% if result['report_permit'].state is defined and result['report_permit'].state == 1 %}
\t\t\t\t\t\t\t    \t<div class=\"col-3\">
\t\t\t\t\t\t\t\t    \t<a href=\"{{url('sub_report', {'id' : result['offer'].idOffer}) }}\">
\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Submit report</div>
\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    {% else %}
\t\t\t\t\t\t\t    \t<div class=\"col-3\">
\t\t\t\t\t\t\t\t    \t<a href=\"{{url('report_ask', {'id' : result['offer'].idOffer}) }}\">
\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">Request report submission</div>
\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t    {% endif %}
\t\t\t\t\t\t    {% endif %}
\t\t\t\t\t    {% endif %}
\t\t\t\t\t</div>
\t            </div>
          </div>
      \t{% for message in app.flashes('message')%}
\t\t\t<div class=\" form-group text-center\">
\t\t\t\t<div class=\"alert alert-success mx-auto\" style=\"margin-top:5px; margin-bottom:-10px; width:40%;\" role=\"alert\"> {{message}}</div>
\t\t\t</div>
\t\t{%endfor%}
        </div>
\t    \t<div class=\"card-overlay border-0 bg-white shadow mx-auto\" style=\"width:70%; margin-bottom:20px;\">
\t\t        
\t\t        <div class=\"d-flex flex-column bd-highlight mb-2\" style=\"margin-left:20px; margin-top:20px\">                  
\t                <div class=\"row\">
\t                \t<div class=\"col-10\">
\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Offer Type : </h5> 
\t                        <div class=\"p-2\">{{result['info_ent'][0].n_type}}</div>
                        </div>
                        <div class=\"col\">
                        \t<h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Date : </h5> 
\t                        <div class=\"p-2\">{{result['offer'].dateAdd|date(\"m/d/Y\")}}</div>
                    \t</div>
                \t</div>
\t                <div class=\"row\">
\t                \t<div class=\"col-10\">
\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Offer Description : </h5> 
\t                        <div class=\"p-2\">{{result['offer'].description2|raw}}</div>
                        </div>
                        <div class=\"col\">
                        \t<h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Reports : </h5> 
\t                        <div class=\"p-2\">{{result['offer'].nbReports}}</div>
\t                        <h5 class=\"p-2\" style=\"color:#9D151C; margin-bottom:-10px;\">Thanks : </h5> 
\t                        <div class=\"p-2\">{{result['offer'].nbThanks}}</div>
                    \t</div>
                \t</div>

\t\t                {% if is_granted('ROLE_ADMIN') %}
\t\t\t\t\t        <a href=\"{{url('offer_delete', {'id' : result['offer'].idOffer}) }}\">
\t\t\t\t\t            <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); margin-top:10px; margin-bottom:10px\">
\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold\">Delete offer</div>
\t\t\t\t\t            </button>
\t\t\t\t\t        </a>
\t\t\t\t\t        {% if result['info_ent'][0].state == 2 %}
\t\t\t\t\t        <div class=\"row\">
\t\t\t\t\t        \t<div class=\"col-2\">
\t\t\t\t\t        \t\t<a href=\"{{url('offer_in', {'id' : result['offer'].idOffer}) }}\">
\t\t\t\t\t\t\t            <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); margin-top:10px; margin-bottom:10px\">
\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold\">Allow offer</div>
\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t        </a>
\t\t\t\t\t        \t</div>
\t\t\t\t\t        \t<div class=\"col-2\">
\t\t\t\t\t        \t\t<a href=\"{{url('offer_out', {'id' : result['offer'].idOffer}) }}\">
\t\t\t\t\t\t\t            <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); float:left; margin-top:10px; margin-bottom:10px; margin-left:-30px;\">
\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold\">Reject offer</div>
\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t        </a>
\t\t\t\t\t        \t</div>
\t\t\t\t\t        </div>
\t\t\t\t\t        {% endif %}
\t\t\t\t        {% elseif app.user.idEntreprise is defined %}
\t\t\t\t        \t{% if result['info_ent'][0].id_entreprise == app.user.idEntreprise %}
\t\t\t\t        \t\t<a href=\"{{url('offer_delete', {'id' : result['offer'].idOffer}) }}\">
\t\t\t\t\t\t            <button type=\"button\" class=\"btn shadow-sm\" style=\"background-color:rgba(217, 217, 217); margin-top:10px; margin-bottom:10px\">
\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold\">Delete offer</div>
\t\t\t\t\t\t            </button>
\t\t\t\t\t        \t</a>
\t\t\t\t        \t{% endif %}
\t\t\t\t      \t{% endif %}
\t\t        </div>
\t       \t</div>
\t</div>
</div>
{% endblock %}

", "offer/show.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\offer\\show.html.twig");
    }
}
