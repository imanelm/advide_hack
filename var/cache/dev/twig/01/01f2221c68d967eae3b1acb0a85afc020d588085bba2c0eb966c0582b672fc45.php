<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* hacker/hack_reports.html.twig */
class __TwigTemplate_1d2e78f518320d328c7bb625f3aa77ea2f1159a4573be61171734787830ddcbb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/hack_reports.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "hacker/hack_reports.html.twig"));

        $this->parent = $this->loadTemplate("layout.html.twig", "hacker/hack_reports.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "\t
\t\t<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t\t    ";
        // line 8
        if ((0 !== twig_compare(twig_length_filter($this->env, (isset($context["hack_reports"]) || array_key_exists("hack_reports", $context) ? $context["hack_reports"] : (function () { throw new RuntimeError('Variable "hack_reports" does not exist.', 8, $this->source); })())), 0))) {
            // line 9
            echo "\t\t\t\t    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["hack_reports"]) || array_key_exists("hack_reports", $context) ? $context["hack_reports"] : (function () { throw new RuntimeError('Variable "hack_reports" does not exist.', 9, $this->source); })())) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 10
                echo "
\t\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t\t        \t\t\t<div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t    \t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t      \t\t<div class=\"row justify-content-center d-flex align-items-center\">
\t\t\t\t\t\t\t\t\t\t      \t\t<div class=\"col-3\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t<a href=\"";
                // line 20
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("offer_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["hack_reports"]) || array_key_exists("hack_reports", $context) ? $context["hack_reports"] : (function () { throw new RuntimeError('Variable "hack_reports" does not exist.', 20, $this->source); })()), $context["i"], [], "array", false, false, false, 20), "idOffer", [], "any", false, false, false, 20), "idOffer", [], "any", false, false, false, 20)]), "html", null, true);
                echo "\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<h4>";
                // line 21
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["hack_reports"]) || array_key_exists("hack_reports", $context) ? $context["hack_reports"] : (function () { throw new RuntimeError('Variable "hack_reports" does not exist.', 21, $this->source); })()), $context["i"], [], "array", false, false, false, 21), "idOffer", [], "any", false, false, false, 21), "title", [], "any", false, false, false, 21), "html", null, true);
                echo "</h4>
\t\t\t\t\t\t\t\t\t\t\t      \t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t<div class=\"col-3\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t<img src=\"/Images/";
                // line 25
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["hack_reports"]) || array_key_exists("hack_reports", $context) ? $context["hack_reports"] : (function () { throw new RuntimeError('Variable "hack_reports" does not exist.', 25, $this->source); })()), $context["i"], [], "array", false, false, false, 25), "idOffer", [], "any", false, false, false, 25), "idEntreprise", [], "any", false, false, false, 25), "photo", [], "any", false, false, false, 25), "html", null, true);
                echo "\" height=\"100\" alt=\"\" class=\"rounded\" style=\"margin-left:20px;\" />
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col-3\">
\t\t\t\t\t\t\t\t\t\t\t\t    \t<a href=\"";
                // line 28
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("report_show", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["hack_reports"]) || array_key_exists("hack_reports", $context) ? $context["hack_reports"] : (function () { throw new RuntimeError('Variable "hack_reports" does not exist.', 28, $this->source); })()), $context["i"], [], "array", false, false, false, 28), "idReport", [], "any", false, false, false, 28)]), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">View Report</div>
\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t";
                // line 35
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["hack_reports"]) || array_key_exists("hack_reports", $context) ? $context["hack_reports"] : (function () { throw new RuntimeError('Variable "hack_reports" does not exist.', 35, $this->source); })()), $context["i"], [], "array", false, false, false, 35), "state", [], "any", false, false, false, 35), "idState", [], "any", false, false, false, 35), 1))) {
                    // line 36
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-success active\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["hack_reports"]) || array_key_exists("hack_reports", $context) ? $context["hack_reports"] : (function () { throw new RuntimeError('Variable "hack_reports" does not exist.', 36, $this->source); })()), $context["i"], [], "array", false, false, false, 36), "state", [], "any", false, false, false, 36), "nState", [], "any", false, false, false, 36), "html", null, true);
                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t";
                } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 37
(isset($context["hack_reports"]) || array_key_exists("hack_reports", $context) ? $context["hack_reports"] : (function () { throw new RuntimeError('Variable "hack_reports" does not exist.', 37, $this->source); })()), $context["i"], [], "array", false, false, false, 37), "state", [], "any", false, false, false, 37), "idState", [], "any", false, false, false, 37), 2))) {
                    // line 38
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-warning active text-white\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["hack_reports"]) || array_key_exists("hack_reports", $context) ? $context["hack_reports"] : (function () { throw new RuntimeError('Variable "hack_reports" does not exist.', 38, $this->source); })()), $context["i"], [], "array", false, false, false, 38), "state", [], "any", false, false, false, 38), "nState", [], "any", false, false, false, 38), "html", null, true);
                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t";
                } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                 // line 39
(isset($context["hack_reports"]) || array_key_exists("hack_reports", $context) ? $context["hack_reports"] : (function () { throw new RuntimeError('Variable "hack_reports" does not exist.', 39, $this->source); })()), $context["i"], [], "array", false, false, false, 39), "state", [], "any", false, false, false, 39), "idState", [], "any", false, false, false, 39), 3))) {
                    // line 40
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-danger active\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["hack_reports"]) || array_key_exists("hack_reports", $context) ? $context["hack_reports"] : (function () { throw new RuntimeError('Variable "hack_reports" does not exist.', 40, $this->source); })()), $context["i"], [], "array", false, false, false, 40), "state", [], "any", false, false, false, 40), "nState", [], "any", false, false, false, 40), "html", null, true);
                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t";
                }
                // line 42
                echo "\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t      \t</div>

\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t      \t\t
\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t</div>
\t\t\t\t\t   </div>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo " 
\t\t\t";
        }
        // line 55
        echo "\t\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "hacker/hack_reports.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 55,  158 => 53,  141 => 42,  135 => 40,  133 => 39,  128 => 38,  126 => 37,  121 => 36,  119 => 35,  109 => 28,  103 => 25,  96 => 21,  92 => 20,  80 => 10,  75 => 9,  73 => 8,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"layout.html.twig\" %}


{% block body %}
\t
\t\t<div class=\"card bg-transparent text-white border-0 align-item-center\" style = \"margin-top: 120px; margin-bottom:30px\">

\t\t    {% if hack_reports|length != 0 %}
\t\t\t\t    {% for i in 0..hack_reports|length-1 %}

\t\t\t\t\t    <div class=\"row d-flex justify-content-center\">
\t\t        \t\t\t<div class=\"card card-overlay bg-transparent text-white border-0 shadow\" style=\" margin-bottom:20px; width:60%\">

\t        \t\t\t\t\t<div class=\"row align-items-center\">
\t    \t\t\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t\t\t    \t<div class=\"card bg-white border-0 rounded-0 text-center\">
\t\t\t\t\t\t\t\t\t      \t<div class=\"card-body\">
\t\t\t\t\t\t\t\t\t      \t\t<div class=\"row justify-content-center d-flex align-items-center\">
\t\t\t\t\t\t\t\t\t\t      \t\t<div class=\"col-3\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t<a href=\"{{ url('offer_show', {'id' : hack_reports[i].idOffer.idOffer}) }}\" style=\"color:#9D151C;\">
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<h4>{{ hack_reports[i].idOffer.title}}</h4>
\t\t\t\t\t\t\t\t\t\t\t      \t\t</a>
\t\t\t\t\t\t\t\t\t\t      \t\t</div>
\t\t\t\t\t\t\t\t\t\t      \t\t<div class=\"col-3\">
\t\t\t\t\t\t\t\t\t\t      \t\t\t<img src=\"/Images/{{ hack_reports[i].idOffer.idEntreprise.photo}}\" height=\"100\" alt=\"\" class=\"rounded\" style=\"margin-left:20px;\" />
\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"col-3\">
\t\t\t\t\t\t\t\t\t\t\t\t    \t<a href=\"{{url('report_show', {'id' : hack_reports[i].idReport}) }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t      \t<button type=\"button\" class=\"btn btn-sm bg-white shadow\">
\t\t\t\t\t\t\t\t\t\t\t\t              \t<div style=\"color:#9D151C; font-weight:bold;\">View Report</div>
\t\t\t\t\t\t\t\t\t\t\t\t            </button>
\t\t\t\t\t\t\t\t\t\t\t            </a>
\t\t\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t\t\t\t    <div class=\"col\">
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t{% if hack_reports[i].state.idState == 1 %}
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-success active\">{{ hack_reports[i].state.nState }}</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t{% elseif hack_reports[i].state.idState == 2 %}
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-warning active text-white\">{{ hack_reports[i].state.nState }}</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t{% elseif hack_reports[i].state.idState == 3 %}
\t\t\t\t\t\t\t\t\t\t\t\t      \t\t\t<div class=\"btn btn-sm btn-danger active\">{{ hack_reports[i].state.nState }}</div>
\t\t\t\t\t\t\t\t\t\t\t      \t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t      \t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t      \t</div>

\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t      \t\t
\t\t\t\t\t\t\t\t</div>    \t

\t\t\t\t\t\t\t</div>
\t\t\t\t\t   </div>
\t\t\t\t\t{% endfor %} 
\t\t\t{% endif %}
\t\t</div>
{% endblock %}", "hacker/hack_reports.html.twig", "C:\\wamp64\\www\\advide_hack\\templates\\hacker\\hack_reports.html.twig");
    }
}
