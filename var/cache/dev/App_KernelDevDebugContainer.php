<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\Container3Zaoc9N\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/Container3Zaoc9N/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/Container3Zaoc9N.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\Container3Zaoc9N\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \Container3Zaoc9N\App_KernelDevDebugContainer([
    'container.build_hash' => '3Zaoc9N',
    'container.build_id' => '7337ba59',
    'container.build_time' => 1598718051,
], __DIR__.\DIRECTORY_SEPARATOR.'Container3Zaoc9N');
