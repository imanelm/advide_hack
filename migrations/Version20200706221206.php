<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200706221206 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE entreprise CHANGE num num INT NOT NULL, CHANGE password password VARCHAR(12) NOT NULL, CHANGE roles roles JSON NOT NULL');
        $this->addSql('ALTER TABLE hacker CHANGE state state INT DEFAULT NULL, CHANGE password password VARCHAR(12) NOT NULL, CHANGE roles roles JSON NOT NULL, CHANGE photo photo VARCHAR(255) DEFAULT \'logo.png\'');
        $this->addSql('ALTER TABLE hacker RENAME INDEX state TO IDX_3AA781B7A393D2FB');
        $this->addSql('ALTER TABLE state CHANGE id_state id_state INT AUTO_INCREMENT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE entreprise CHANGE num num BIGINT NOT NULL, CHANGE password password VARCHAR(500) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, CHANGE roles roles JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE hacker CHANGE state state INT DEFAULT 2 NOT NULL, CHANGE password password VARCHAR(500) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, CHANGE photo photo VARCHAR(255) CHARACTER SET utf8 DEFAULT \'logo.png\' NOT NULL COLLATE `utf8_general_ci`, CHANGE roles roles JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE hacker RENAME INDEX idx_3aa781b7a393d2fb TO state');
        $this->addSql('ALTER TABLE state CHANGE id_state id_state INT NOT NULL');
    }
}
