<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200703101200 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE entreprise CHANGE password password VARCHAR(12) NOT NULL, CHANGE roles roles JSON NOT NULL');
        $this->addSql('ALTER TABLE hacker CHANGE password password VARCHAR(12) NOT NULL, CHANGE roles roles JSON NOT NULL, CHANGE photo photo VARCHAR(255) DEFAULT \'logo.png\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user');
        $this->addSql('ALTER TABLE entreprise CHANGE password password VARCHAR(500) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, CHANGE roles roles JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE hacker CHANGE password password VARCHAR(500) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, CHANGE photo photo VARCHAR(255) CHARACTER SET utf8 DEFAULT \'logo.png\' NOT NULL COLLATE `utf8_general_ci`, CHANGE roles roles JSON DEFAULT NULL');
    }
}
