-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  sam. 29 août 2020 à 13:09
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `advise_hack`
--

-- --------------------------------------------------------

--
-- Structure de la table `blog_post`
--

DROP TABLE IF EXISTS `blog_post`;
CREATE TABLE IF NOT EXISTS `blog_post` (
  `id_bp` int(11) NOT NULL AUTO_INCREMENT,
  `id_hacker` int(11) DEFAULT NULL,
  `id_entreprise` int(11) DEFAULT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id_bp`),
  KEY `id_hacker` (`id_hacker`),
  KEY `id_entreprise` (`id_entreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `blog_post`
--

INSERT INTO `blog_post` (`id_bp`, `id_hacker`, `id_entreprise`, `title`, `content`, `photo`, `date`) VALUES
(1, 6, NULL, 'First Post', 'Advise\'Hack is the greatest', 'logo.png', '2020-07-23'),
(3, 6, NULL, 'Third Post', 'Advise\'Hack is the greatest', 'logo.png', '2020-07-21'),
(4, 6, NULL, 'Fourth Post', '<p>Advise\'Hack is the greatest :)</p>', 'logo.png', NULL),
(5, 6, NULL, 'Fifth Post', 'Advise\'Hack is the greatest', 'logo.png', NULL),
(6, 5, NULL, 'New post', '<p>Test test</p>', 'ba860ce00e44607cbef7cba9c9a3957a.jpeg', '2020-08-08'),
(7, 5, NULL, 'New test test 2', '<p>Here we go again. How are you all ? How much did you like AH ? A lot, right ? RIGHT ? Ok bye :)&nbsp;</p>\r\n<p>Second test : this line has been added ! That would mean that the modification works, huh ?&nbsp;</p>', 'b09c27bd68d87ae523b70997379ded57.jpeg', '2020-08-09');

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id_comment` int(11) NOT NULL AUTO_INCREMENT,
  `id_bp` int(11) NOT NULL,
  `id_hacker` int(11) DEFAULT NULL,
  `id_entreprise` int(11) DEFAULT NULL,
  `content` text NOT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id_comment`),
  KEY `id_bp` (`id_bp`),
  KEY `id_hacker` (`id_hacker`),
  KEY `id_entreprise` (`id_entreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id_comment`, `id_bp`, `id_hacker`, `id_entreprise`, `content`, `date`) VALUES
(8, 4, 5, NULL, '<p>Hi :)&nbsp;</p>', '2020-08-09'),
(9, 4, 5, NULL, '<p>Hello&nbsp;</p>', '2020-08-09'),
(11, 4, 5, NULL, 'hi', '2020-08-10');

-- --------------------------------------------------------

--
-- Structure de la table `cvss_base`
--

DROP TABLE IF EXISTS `cvss_base`;
CREATE TABLE IF NOT EXISTS `cvss_base` (
  `id_vuln` int(11) NOT NULL AUTO_INCREMENT,
  `n_vuln` varchar(60) NOT NULL,
  `level_vuln` int(11) DEFAULT NULL,
  `score_vuln` float DEFAULT NULL,
  PRIMARY KEY (`id_vuln`),
  KEY `level_vuln` (`level_vuln`),
  KEY `level_vuln_2` (`level_vuln`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cvss_base`
--

INSERT INTO `cvss_base` (`id_vuln`, `n_vuln`, `level_vuln`, `score_vuln`) VALUES
(1, 'Access Vector', NULL, NULL),
(2, 'Attack Complexity', NULL, NULL),
(3, 'Authentification', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `cvss_env`
--

DROP TABLE IF EXISTS `cvss_env`;
CREATE TABLE IF NOT EXISTS `cvss_env` (
  `id_vuln` int(11) NOT NULL AUTO_INCREMENT,
  `n_vuln` varchar(60) NOT NULL,
  `level_vuln` int(11) DEFAULT NULL,
  `score_vuln` float DEFAULT NULL,
  PRIMARY KEY (`id_vuln`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cvss_env`
--

INSERT INTO `cvss_env` (`id_vuln`, `n_vuln`, `level_vuln`, `score_vuln`) VALUES
(1, 'Collateral Damage Potential', NULL, NULL),
(2, 'Target Distribution', NULL, NULL),
(3, 'Impact Subscore Modifier', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `cvss_impact`
--

DROP TABLE IF EXISTS `cvss_impact`;
CREATE TABLE IF NOT EXISTS `cvss_impact` (
  `id_vuln` int(11) NOT NULL AUTO_INCREMENT,
  `n_vuln` varchar(60) NOT NULL,
  `level_vuln` int(11) DEFAULT NULL,
  `score_vuln` float DEFAULT NULL,
  PRIMARY KEY (`id_vuln`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cvss_impact`
--

INSERT INTO `cvss_impact` (`id_vuln`, `n_vuln`, `level_vuln`, `score_vuln`) VALUES
(1, 'Confidentiality', NULL, NULL),
(2, 'Integrity', NULL, NULL),
(3, 'Availability', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `cvss_temporal`
--

DROP TABLE IF EXISTS `cvss_temporal`;
CREATE TABLE IF NOT EXISTS `cvss_temporal` (
  `id_vuln` int(11) NOT NULL AUTO_INCREMENT,
  `n_vuln` varchar(60) NOT NULL,
  `level_vuln` int(11) DEFAULT NULL,
  `score_vuln` float DEFAULT NULL,
  PRIMARY KEY (`id_vuln`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cvss_temporal`
--

INSERT INTO `cvss_temporal` (`id_vuln`, `n_vuln`, `level_vuln`, `score_vuln`) VALUES
(1, 'Exploitability', NULL, NULL),
(2, 'Remediation Level', NULL, NULL),
(3, 'Report Confidence', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

DROP TABLE IF EXISTS `entreprise`;
CREATE TABLE IF NOT EXISTS `entreprise` (
  `id_entreprise` int(11) NOT NULL AUTO_INCREMENT,
  `name_ent` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(20) NOT NULL,
  `f_name` varchar(20) NOT NULL,
  `num` bigint(20) NOT NULL,
  `country` varchar(20) NOT NULL,
  `password` varchar(500) NOT NULL,
  `roles` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `photo` varchar(255) NOT NULL DEFAULT 'logo.png',
  `showAuth` tinyint(1) NOT NULL DEFAULT 0,
  `adress` varchar(100) DEFAULT NULL,
  `site` varchar(50) DEFAULT NULL,
  `new_notif` int(11) NOT NULL,
  PRIMARY KEY (`id_entreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `entreprise`
--

INSERT INTO `entreprise` (`id_entreprise`, `name_ent`, `email`, `name`, `f_name`, `num`, `country`, `password`, `roles`, `photo`, `showAuth`, `adress`, `site`, `new_notif`) VALUES
(18, 'd', 'entreprise3@entreprise3.com', 'd', 'd', 92, 'd', '$argon2id$v=19$m=65536,t=4,p=1$B/cgMyfRh9cWaUkSQgYixw$zqwNeXdKg3ugR1xjUvA5arUYMLZ/Gzswi56wNTihRr8', '[]', 'ff17db29bf0ad612179ef3d04ea45089.png', 1, NULL, NULL, 0),
(19, 'Ent4', 'ent4@mail.com', 'B', 'H', 9384759405, 'FR', '$argon2id$v=19$m=65536,t=4,p=1$lQYseclwLY3m8lI5glfrLw$ok3piZbIUkVFmS6cMxAnGCEwXapS/rZrbXd3SIAW36I', '[]', 'a5f06d6c1de8d81d664361a160357edc.png', 1, NULL, NULL, 1),
(22, 'oug', 'oug@gmail.com', 'ougali', 'fatiha', 758084608, 'Maroc', '$argon2id$v=19$m=65536,t=4,p=1$emlOU1AuVTVaT3BJYlptYw$MZyFBv0pbbTrf4EOakYpUb/jbwJfW8gr+KIQLvMBdjs', '[]', 'de28640da73f07e434bceaacaa19350a.jpeg', 1, NULL, NULL, 0),
(23, 'newEnt', 'newEnt@hot.com', 'newEnt', 'newEnt', 934756394, 'newEnt', '$argon2id$v=19$m=65536,t=4,p=1$PoZs/4vriwP3XYr5dX1QOQ$FWVW1hYk++EHpSEW4OYgrBHC3UlIwAu5NNJoTkp9Mtw', '[]', 'logo.png', 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `hacker`
--

DROP TABLE IF EXISTS `hacker`;
CREATE TABLE IF NOT EXISTS `hacker` (
  `id_hacker` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(30) NOT NULL,
  `f_name` varchar(30) NOT NULL,
  `country` varchar(30) NOT NULL,
  `mark` int(11) DEFAULT NULL,
  `password` varchar(500) NOT NULL,
  `roles` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT 2,
  `photo` varchar(255) NOT NULL DEFAULT 'logo.png',
  `adress` varchar(100) DEFAULT NULL,
  `twitter` varchar(30) DEFAULT NULL,
  `linkedin` varchar(30) DEFAULT NULL,
  `github` varchar(30) DEFAULT NULL,
  `site` varchar(50) DEFAULT NULL,
  `new_notif` int(11) NOT NULL DEFAULT 0,
  `fichier_sup` varchar(255) DEFAULT NULL,
  `test` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_hacker`),
  KEY `state` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `hacker`
--

INSERT INTO `hacker` (`id_hacker`, `username`, `email`, `name`, `f_name`, `country`, `mark`, `password`, `roles`, `state`, `photo`, `adress`, `twitter`, `linkedin`, `github`, `site`, `new_notif`, `fichier_sup`, `test`) VALUES
(4, '1', 'hr@gmail.com', 'hicham', 'rahj', 'France', 6, '$argon2id$v=19$m=65536,t=4,p=1$FDSTl2iYluPe7123//eUNg$GyA7XOSGIP/F+3rAmAM960bg/iPqTEpumDtxvTSWxmM', '[]', 3, 'logo.png', NULL, NULL, NULL, NULL, '', 3, NULL, NULL),
(5, 'h', 'h@gmail.com', 'h', 'h', 'h', NULL, '$2y$13$.nln99xRQ0FHI6UNN3oPJOCthKNLdoyd14N5tJyQV4IaSMHD.qlKi', '[\"ROLE_ADMIN\"]', 1, '40eae8ee8b86400106efd22e3b677f2e.png', '5 Le Clos de la Cathédrale', 'HoudaB02', 'Houda Berrada', NULL, NULL, 2, 'feb54c444a550231584e8f1e9da0d8b3.jpeg', NULL),
(6, 'f', 'fa@gmail.com', 'f', 'f', 'f', NULL, '$argon2id$v=19$m=65536,t=4,p=1$0I/ARzzkmIHGk9A1Tu01FA$hY6/u10FFYgErAVC+cCW99MN8BiYkLOdfiTcYWfQkYE', '[]', 1, '40eae8ee8b86400106efd22e3b677f2e.png', NULL, NULL, NULL, NULL, '', 3, NULL, NULL),
(7, 'Houdi', 'houdaberrada02@hotmail.com', 'Berrada', 'Houda', 'France', NULL, '$argon2id$v=19$m=65536,t=4,p=1$tMOC/imtyVFzkB80ctxOOg$r82naiXEAPw4xf/+9OBSn7q3w04iCZ0ZcwaBC6wE+mM', '[]', 1, '8e92043349fb7b23abf0cff3d4645a8a.png', NULL, NULL, NULL, NULL, '', 3, NULL, NULL),
(8, 'd', 'f@d.com', 'd', 'd', 'd', NULL, '$argon2id$v=19$m=65536,t=4,p=1$CGbdIaoqxlzF1bkHRBDF0g$q/6YO9e+8O49O3yXvZqZxQw1VI8lmO8JPaqAPc/9+sU', '[]', 2, 'd5589f9557be3f1556a42fc2e1bd81bc.png', NULL, NULL, NULL, NULL, '', 3, NULL, NULL),
(10, 'Houhou', 'houhou@gmail.com', 'Houhou', 'Houhou', 'Houhou', NULL, '$argon2id$v=19$m=65536,t=4,p=1$/FIys7jcBh+ZgbrXMAN4Ew$NRN1yhbpzqmzegAG2A8tOhyuAjFnkZNJ1pzo4DmEaoY', '[]', 2, '1ec68bb4ca56694540a17cc382a1887c.png', NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(11, 'emyy', 'imane.elmoul@gmail.com', 'elmoul', 'imae', 'Maroc', NULL, '$argon2id$v=19$m=65536,t=4,p=1$eXF6QnhveHFQbTVjWTR3ZQ$I4xlyTUyR2rtd/xblpzBnRRTatoneymjgTwPRdtumgQ', '[]', 2, 'logo.png', NULL, NULL, NULL, NULL, NULL, 1, NULL, '7b494e55e8f94e687624a709ee5c5ec8.pdf');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id_message` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(200) NOT NULL,
  `message` text NOT NULL,
  `response_to` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id_notif` int(11) NOT NULL AUTO_INCREMENT,
  `id_entreprise` int(11) DEFAULT NULL,
  `id_hacker` int(11) DEFAULT NULL,
  `id_offer` int(11) DEFAULT NULL,
  `id_report` int(11) DEFAULT NULL,
  `notif_read` int(11) NOT NULL DEFAULT 0,
  `type_notif` int(11) NOT NULL,
  `id_permit` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_notif`),
  KEY `id_entreprise` (`id_entreprise`),
  KEY `id_report` (`id_report`),
  KEY `id_hacker` (`id_hacker`),
  KEY `id_offer` (`id_offer`) USING BTREE,
  KEY `id_permit` (`id_permit`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `notifications`
--

INSERT INTO `notifications` (`id_notif`, `id_entreprise`, `id_hacker`, `id_offer`, `id_report`, `notif_read`, `type_notif`, `id_permit`) VALUES
(60, 19, 6, 13, NULL, 1, 3, 19),
(63, NULL, 6, NULL, NULL, 1, 6, NULL),
(76, NULL, 10, NULL, NULL, 0, 6, NULL),
(77, 19, NULL, 18, NULL, 0, 8, NULL),
(78, 19, NULL, 19, NULL, 0, 8, NULL),
(79, 19, 5, 19, NULL, 1, 3, 20),
(81, 19, NULL, NULL, 40, 1, 1, NULL),
(82, 19, NULL, NULL, 41, 1, 1, NULL),
(83, 19, NULL, NULL, 41, 1, 2, NULL),
(84, 23, 5, 18, NULL, 0, 3, 21),
(85, 19, 5, 17, NULL, 1, 3, 22),
(86, 19, 5, 17, NULL, 1, 4, NULL),
(87, 19, NULL, NULL, 43, 0, 1, NULL),
(88, NULL, 6, 13, NULL, 0, 12, NULL),
(89, NULL, 11, NULL, NULL, 0, 7, NULL);

--
-- Déclencheurs `notifications`
--
DROP TRIGGER IF EXISTS `notif_del_ent`;
DELIMITER $$
CREATE TRIGGER `notif_del_ent` AFTER DELETE ON `notifications` FOR EACH ROW UPDATE entreprise 
SET new_notif = (
    CASE 
    	WHEN (OLD.type_notif < 4 OR OLD.type_notif = 9 OR OLD.type_notif = 10) AND entreprise.new_notif > 0
    		THEN new_notif - 1
    	ELSE new_notif
    END
    )
WHERE OLD.id_entreprise = entreprise.id_entreprise
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `notif_del_ha`;
DELIMITER $$
CREATE TRIGGER `notif_del_ha` AFTER DELETE ON `notifications` FOR EACH ROW UPDATE hacker 
SET new_notif = (
    CASE 
    	WHEN OLD.type_notif > 3 AND hacker.new_notif > 0
    		THEN new_notif - 1
    	ELSE new_notif
    END
    )
WHERE OLD.id_hacker = hacker.id_hacker OR OLD.type_notif = 7 OR OLD.type_notif = 8
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `notif_new_ent`;
DELIMITER $$
CREATE TRIGGER `notif_new_ent` AFTER INSERT ON `notifications` FOR EACH ROW UPDATE entreprise 
SET new_notif = (new_notif + 1
    )
WHERE NEW.id_entreprise = entreprise.id_entreprise AND (NEW.type_notif < 4 OR NEW.type_notif = 9 OR NEW.type_notif = 10)
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `notif_new_ha`;
DELIMITER $$
CREATE TRIGGER `notif_new_ha` AFTER INSERT ON `notifications` FOR EACH ROW UPDATE hacker 
SET new_notif = (new_notif + 1
    )
WHERE (NEW.id_hacker = hacker.id_hacker AND (NEW.type_notif = 4 OR NEW.type_notif = 5 OR NEW.type_notif = 6 OR NEW.type_notif = 11 OR NEW.type_notif = 12 OR NEW.type_notif = 13)) OR NEW.type_notif = 7 OR NEW.type_notif = 8
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `notif_old_ent`;
DELIMITER $$
CREATE TRIGGER `notif_old_ent` AFTER UPDATE ON `notifications` FOR EACH ROW UPDATE entreprise 
SET new_notif = (
    CASE 
    	WHEN NEW.notif_read = 0 AND OLD.notif_read = 1 AND (NEW.type_notif < 4 OR NEW.type_notif = 9 OR NEW.type_notif = 10)
    		THEN new_notif + 1
    	WHEN NEW.notif_read = 1 AND OLD.notif_read = 0 AND (NEW.type_notif < 4 OR NEW.type_notif = 9 OR NEW.type_notif = 10) AND entreprise.new_notif > 0
    		THEN new_notif - 1
    	ELSE new_notif
    END
    )
WHERE NEW.id_entreprise = entreprise.id_entreprise
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `notif_old_ha`;
DELIMITER $$
CREATE TRIGGER `notif_old_ha` AFTER UPDATE ON `notifications` FOR EACH ROW UPDATE hacker 
SET new_notif = (
    CASE 
    	WHEN NEW.notif_read = 0 AND OLD.notif_read = 1 
    		THEN new_notif + 1
    	WHEN NEW.notif_read = 1 AND OLD.notif_read = 0 AND hacker.new_notif > 0
    		THEN new_notif - 1
    	ELSE new_notif
    END
    )
WHERE (NEW.id_hacker = hacker.id_hacker AND (NEW.type_notif = 4 OR NEW.type_notif = 5 OR NEW.type_notif = 6 OR NEW.type_notif = 11 OR NEW.type_notif = 12 OR NEW.type_notif = 13)) OR NEW.type_notif = 7 OR NEW.type_notif = 8
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `offer`
--

DROP TABLE IF EXISTS `offer`;
CREATE TABLE IF NOT EXISTS `offer` (
  `id_offer` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL,
  `description1` text DEFAULT NULL,
  `description2` text NOT NULL,
  `id_entreprise` int(11) DEFAULT NULL,
  `id_type` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT 2,
  `date_add` date DEFAULT NULL,
  `nb_reports` int(11) DEFAULT 0,
  `nb_thanks` int(11) DEFAULT 0,
  PRIMARY KEY (`id_offer`),
  KEY `id_entreprise` (`id_entreprise`),
  KEY `id_type` (`id_type`),
  KEY `state` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `offer`
--

INSERT INTO `offer` (`id_offer`, `title`, `description1`, `description2`, `id_entreprise`, `id_type`, `state`, `date_add`, `nb_reports`, `nb_thanks`) VALUES
(13, 'Offer14', 'Hello :)', '<p>This doesn\'t make any sense but it\'s fine ;)&nbsp;</p>', 19, 2, 1, NULL, 1, 1),
(17, 'Notif test', 'Notif test !', 'Notif test !!', 19, 1, 3, '2020-07-21', 0, 0),
(18, 'test', 'test', 'test', 23, 1, 2, '2020-07-22', 0, 0),
(19, '1', '1', '<p>1</p>', 19, 1, 2, '2020-07-22', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `offer_report`
--

DROP TABLE IF EXISTS `offer_report`;
CREATE TABLE IF NOT EXISTS `offer_report` (
  `id_report` int(11) NOT NULL AUTO_INCREMENT,
  `id_offer` int(11) DEFAULT NULL,
  `id_hacker` int(11) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT 2,
  `report` text DEFAULT NULL,
  `cvss_base` float DEFAULT NULL,
  `cvss_impact` float DEFAULT NULL,
  `cvss_temp` float DEFAULT NULL,
  `cvss_env` float DEFAULT NULL,
  `scope` varchar(250) DEFAULT NULL,
  `end_point` varchar(250) DEFAULT NULL,
  `severity` varchar(20) DEFAULT NULL,
  `payload_fi` int(11) DEFAULT NULL,
  `payload_fifi` int(11) DEFAULT NULL,
  `tech_env` varchar(100) DEFAULT NULL,
  `app_fingerprint` varchar(100) DEFAULT NULL,
  `ip_used` float DEFAULT NULL,
  `date_sub` date DEFAULT NULL,
  PRIMARY KEY (`id_report`),
  KEY `id_offer` (`id_offer`),
  KEY `id_hacker` (`id_hacker`),
  KEY `state` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `offer_report`
--

INSERT INTO `offer_report` (`id_report`, `id_offer`, `id_hacker`, `state`, `report`, `cvss_base`, `cvss_impact`, `cvss_temp`, `cvss_env`, `scope`, `end_point`, `severity`, `payload_fi`, `payload_fifi`, `tech_env`, `app_fingerprint`, `ip_used`, `date_sub`) VALUES
(39, 13, 5, 2, '<p>Buonanotte :3</p>', 12, 8, 7, 3, '6', '1', '1', 2, 3, '4', '3', 4, NULL),
(40, 18, 5, 1, '<p>:)&nbsp;</p>', 8.1, NULL, 31.8, NULL, '1', '1', 'High', 100, 300, '1', '1', 1, '2020-07-22'),
(41, 19, 6, 2, '<p>:)&nbsp;</p>', 8.1, NULL, 31.8, NULL, '1', '1', 'High', 100, 300, '1', '1', 1, '2020-07-22'),
(42, 17, 5, 2, '<p>Report Description :)&nbsp;</p>', 10.1, NULL, 82.4, NULL, '1', '1', 'Critical', 500, 1000, '12', '3', 2, '2020-07-28'),
(43, 13, 6, 1, '<p>D</p>', 0, NULL, 0, NULL, '1', '1', 'Low', 20, 60, '1', '1', 1, '2020-08-03');

--
-- Déclencheurs `offer_report`
--
DROP TRIGGER IF EXISTS `notif_new_report`;
DELIMITER $$
CREATE TRIGGER `notif_new_report` AFTER INSERT ON `offer_report` FOR EACH ROW BEGIN
    INSERT INTO notifications(id_report, id_entreprise, type_notif) 
    VALUES(
      NEW.id_report, 
      (SELECT id_entreprise FROM offer WHERE NEW.id_offer = offer.id_offer),
        1
    );
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `offer_vulnerability`
--

DROP TABLE IF EXISTS `offer_vulnerability`;
CREATE TABLE IF NOT EXISTS `offer_vulnerability` (
  `offer_vulnerability` int(11) NOT NULL AUTO_INCREMENT,
  `id_vulnerability` int(11) DEFAULT NULL,
  `id_offer` int(11) DEFAULT NULL,
  PRIMARY KEY (`offer_vulnerability`),
  KEY `id_vulnerability` (`id_vulnerability`),
  KEY `id_offer` (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `reply`
--

DROP TABLE IF EXISTS `reply`;
CREATE TABLE IF NOT EXISTS `reply` (
  `id_reply` int(11) NOT NULL AUTO_INCREMENT,
  `id_comment` int(11) NOT NULL,
  `id_hacker` int(11) DEFAULT NULL,
  `id_entreprise` int(11) DEFAULT NULL,
  `content` text NOT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id_reply`),
  KEY `id_bp` (`id_comment`),
  KEY `id_hacker` (`id_hacker`),
  KEY `id_entreprise` (`id_entreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `reply`
--

INSERT INTO `reply` (`id_reply`, `id_comment`, `id_hacker`, `id_entreprise`, `content`, `date`) VALUES
(1, 9, 8, NULL, 'Hi', NULL),
(2, 9, 8, NULL, 'Hi', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `report_permit`
--

DROP TABLE IF EXISTS `report_permit`;
CREATE TABLE IF NOT EXISTS `report_permit` (
  `id_permit` int(11) NOT NULL AUTO_INCREMENT,
  `id_notif` int(11) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_permit`),
  KEY `id_notif` (`id_notif`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `report_permit`
--

INSERT INTO `report_permit` (`id_permit`, `id_notif`, `state`) VALUES
(19, 60, 1),
(20, 79, 1),
(21, 84, 0),
(22, 85, 1);

-- --------------------------------------------------------

--
-- Structure de la table `state`
--

DROP TABLE IF EXISTS `state`;
CREATE TABLE IF NOT EXISTS `state` (
  `id_state` int(11) NOT NULL,
  `n_state` varchar(40) NOT NULL,
  PRIMARY KEY (`id_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `state`
--

INSERT INTO `state` (`id_state`, `n_state`) VALUES
(1, 'Accepted'),
(2, 'Being processed'),
(3, 'Rejected');

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

DROP TABLE IF EXISTS `type`;
CREATE TABLE IF NOT EXISTS `type` (
  `id_type` int(11) NOT NULL,
  `n_type` varchar(30) NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type`
--

INSERT INTO `type` (`id_type`, `n_type`) VALUES
(1, 'Find It'),
(2, 'Find It & Fix It');

-- --------------------------------------------------------

--
-- Structure de la table `vulnerability`
--

DROP TABLE IF EXISTS `vulnerability`;
CREATE TABLE IF NOT EXISTS `vulnerability` (
  `id_vulnerability` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `risk` varchar(45) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id_vulnerability`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `blog_post`
--
ALTER TABLE `blog_post`
  ADD CONSTRAINT `blog_post_ibfk_2` FOREIGN KEY (`id_hacker`) REFERENCES `hacker` (`id_hacker`),
  ADD CONSTRAINT `blog_post_ibfk_3` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id_entreprise`);

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`id_hacker`) REFERENCES `hacker` (`id_hacker`),
  ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id_entreprise`),
  ADD CONSTRAINT `comment_ibfk_3` FOREIGN KEY (`id_bp`) REFERENCES `blog_post` (`id_bp`);

--
-- Contraintes pour la table `hacker`
--
ALTER TABLE `hacker`
  ADD CONSTRAINT `hacker_ibfk_1` FOREIGN KEY (`state`) REFERENCES `state` (`id_state`) ON DELETE CASCADE;

--
-- Contraintes pour la table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id_entreprise`) ON DELETE SET NULL,
  ADD CONSTRAINT `notifications_ibfk_2` FOREIGN KEY (`id_report`) REFERENCES `offer_report` (`id_report`) ON DELETE SET NULL,
  ADD CONSTRAINT `notifications_ibfk_3` FOREIGN KEY (`id_hacker`) REFERENCES `hacker` (`id_hacker`),
  ADD CONSTRAINT `notifications_ibfk_4` FOREIGN KEY (`id_offer`) REFERENCES `offer` (`id_offer`),
  ADD CONSTRAINT `notifications_ibfk_5` FOREIGN KEY (`id_permit`) REFERENCES `report_permit` (`id_permit`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `offer`
--
ALTER TABLE `offer`
  ADD CONSTRAINT `offer_ibfk_1` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id_entreprise`),
  ADD CONSTRAINT `offer_ibfk_2` FOREIGN KEY (`id_type`) REFERENCES `type` (`id_type`) ON DELETE CASCADE,
  ADD CONSTRAINT `offer_ibfk_3` FOREIGN KEY (`state`) REFERENCES `state` (`id_state`);

--
-- Contraintes pour la table `offer_report`
--
ALTER TABLE `offer_report`
  ADD CONSTRAINT `offer_report_ibfk_1` FOREIGN KEY (`id_offer`) REFERENCES `offer` (`id_offer`),
  ADD CONSTRAINT `offer_report_ibfk_2` FOREIGN KEY (`id_hacker`) REFERENCES `hacker` (`id_hacker`) ON DELETE CASCADE,
  ADD CONSTRAINT `offer_report_ibfk_3` FOREIGN KEY (`state`) REFERENCES `state` (`id_state`);

--
-- Contraintes pour la table `offer_vulnerability`
--
ALTER TABLE `offer_vulnerability`
  ADD CONSTRAINT `offer_vulnerability_ibfk_1` FOREIGN KEY (`id_vulnerability`) REFERENCES `vulnerability` (`id_vulnerability`),
  ADD CONSTRAINT `offer_vulnerability_ibfk_2` FOREIGN KEY (`id_offer`) REFERENCES `offer` (`id_offer`) ON DELETE CASCADE;

--
-- Contraintes pour la table `reply`
--
ALTER TABLE `reply`
  ADD CONSTRAINT `reply_ibfk_1` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id_entreprise`),
  ADD CONSTRAINT `reply_ibfk_2` FOREIGN KEY (`id_hacker`) REFERENCES `hacker` (`id_hacker`),
  ADD CONSTRAINT `reply_ibfk_3` FOREIGN KEY (`id_comment`) REFERENCES `comment` (`id_comment`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `report_permit`
--
ALTER TABLE `report_permit`
  ADD CONSTRAINT `report_permit_ibfk_3` FOREIGN KEY (`id_notif`) REFERENCES `notifications` (`id_notif`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
