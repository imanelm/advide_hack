$(document).ready(function(){
    update();

$("#contact").click(function(){
    $(".form-popup").show();
  checkCookie();
  //checkCookie();
  //update();
});

function checkCookie()
{
  if(document.cookie.indexOf("messengerUname")==-1)
  {
      chooseUserName();	
  }else{
      $("#loginbox").hide();
      //$("#messagebox").show();
  }
}


function chooseUserName()
{
  $("#loginbox").show();
  $("#messagebox").hide();
  $("#loginChoose").click(function(){
      event.preventDefault();
      var user = $("#pseudo").val();
      document.cookie = "messengerUname="+user+";SameSite=Strict";
      checkCookie();
  });
}	

function getcookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1);
      if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
  }
  return "";
}

function escapehtml(text) {
return text
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;")
    .replace(/'/g, "&#039;");
}

function sendMessage()
{
  event.preventDefault();
  var message = $("#message").val();
  //alert(message);
  if (message != "") {
      var username = getcookie("messengerUname");

      var xmlhttp=new XMLHttpRequest();

      xmlhttp.onreadystatechange=function() {
          if (xmlhttp.readyState==4 && xmlhttp.status==200) {
              message = escapehtml(message)
              document.getElementById("messagebox").innerHTML += "<div class=\"msgc\" style=\"margin-bottom: 30px;\"> <div class=\"msg msgfrom\">" + message + "</div> <div class=\"msgarr msgarrfrom\"></div> <div class=\"msgsentby msgsentbyfrom\">Sent by " + username + "</div> </div>";
              //msginput.value = "";
          }
      }
      xmlhttp.open("GET","/update_message?pseudo=" + username + "&message=" + message,true);
      xmlhttp.send();
    }
  $("#message").val("");
}

$("#sendMessage").click(function(){
  event.preventDefault();
  sendMessage();
  update();
});

function update()
{
    var xmlhttp=new XMLHttpRequest();
  var username = getcookie("messengerUname");
  var output = "";
  xmlhttp.onreadystatechange=function() {
      if (xmlhttp.readyState==4 && xmlhttp.status==200) {
          var response = xmlhttp.responseText.split("\n")
          var rl = response.length
          var item = "";
          for (var i = 0; i < rl; i++) {
              item = response[i].split("\\")
              if (item[1] != undefined) {
                  //console.log('item 0 : ', item);
                  if (item[0] == username) {
                      output += "<div class=\"msgc\" style=\"margin-bottom: 30px;\"> <div class=\"msg msgfrom\">" + item[1] + "</div> <div class=\"msgarr msgarrfrom\"></div> <div class=\"msgsentby msgsentbyfrom\">Sent by " + item[0] + "</div> </div>";
                  } else {
                      output += "<div class=\"msgc\"> <div class=\"msg\">" + item[1] + "</div> <div class=\"msgarr\"></div> <div class=\"msgsentby\">Sent by " + item[0] + "</div> </div>";
                  }
              }
          }

          document.getElementById("message-area").innerHTML = output;
          document.getElementById("message-area").scrollTop = document.getElementById("message-area").scrollHeight;

      }
  }
    xmlhttp.open("GET","/get_message?username=" + username,true);
    xmlhttp.send();
}

$(".cancel").click(function(){
$(".form-popup").hide();
});

setInterval(function(){ update(); }, 500);

});